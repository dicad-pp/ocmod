# OcMOD - Observations closer to MOdel Data
## An NFDI4Earth Pilot

Working with climate model output nearly always includes a validation of this data compared to reference data from observations, to ensure, that the model data chosen is suitable for the individual research question or application. Model data and commonly used observational data have to be obtained from different sources: model data from e.g. the DKRZ and observational data from e.g. servers from public authorities. All data have to be prepared to be in the same formats and standards before it can be used for further analysis. A broad range of users (e.g. climate modelers, impact modelers, climate scientists, providers for climate services and education) are dealing with the same effort and troubles with the same sets of data. The aim of the pilot is to bring observational data close to the model output, to easily access data from public authorities and increase the number of users of various disciplines, and to provide this data in standardized formats for easy usage. As stakeholders, we choose two big national climate change and climate impact projects, both need observations and climate model data for evaluation and climate change analysis. The pilot will integrate the DWD reanalysis dataset COSMO-REA6 (Kaspar et al. 2020) to the infrastructure of ESGF (Earth System Grid Federation - https://esgf.llnl.gov/) as one example. After the end of the pilot, the developed workflow will work out as a base to integrate further datasets from DWD or other public authorities. 

<hr>

<sup>Kaspar, F., et al., 2020: Regional atmospheric reanalysis activities at Deutscher Wetterdienst: review of evaluation results and application examples with a focus on renewable energy, Adv. Sci. Res., 17, 115–128, https://doi.org/10.5194/asr-17-115-2020, 2020.</sup>

## Publication of the standardised COSMO-REA6 data

- The standardised data is published via the Earth System Grid Federation (ESGF - [DKRZ ESGF Node - Project COSMO-REA](https://esgf-data.dkrz.de/projects/cosmo-rea/))
- Errata information is available here: [ERRATA](https://esgf-data.dkrz.de/projects/cosmo-rea/errata)
- The data will also be made available via the DKRZ long term archive World Data Centre for Climate (WDCC). By the end of January 2024, you will find here information how to generally cite the standardised COSMO-REA6 data: [WDCC Project Link](https://www.wdc-climate.de/ui/project?acronym=COSMO-REA)
- A part of the standardised COSMO-REA6 data is also available via the DKRZ swift cloud. In the following Jupyter Notebook you find a basic example on how to access and remap this data: [JupyterNB Link](https://gitlab.dkrz.de/dicad-pp/ocmod/-/blob/main/COSMO-REA6/scripts/COSMO-REA_plot-example.ipynb). Provided under the following link is [Jupyter Notebook](/COSMO-REA6/scripts/DataCube_prepare_for_cloud.ipynb) showing how this data was made avaiable via the cloud.

The data can be lazily accessed:
```python
# Import intake - intake zarr plugin required https://intake.readthedocs.io/en/latest/plugin-directory.html
import intake

# Load the catalogue - either load it via our swift cloud or clone the following repo,
#  that includes the catalogue and all necessary metadata:
#   https://gitlab.dkrz.de/data-infrastructure-services/cosmo-rea-kerchunks
cat=intake.open_catalog(
    "https://swift.dkrz.de/v1/dkrz_4236b71e-04df-456b-8a32-5d66641510f2/catalogs/cosmo-rea/main.yaml"
)

#ds=cat(protocol="file")["1hr_atmos"].to_dask() # On the DKRZ HPC levante
ds=cat(protocol="http")["1hr_atmos"].to_dask() # from elsewhere

# available are:
# 1hr_atmos
# 1hrPt_atmos
# 6hr_atmos
# day_atmos
# mon_atmos
# 1hr_land
# 1hrPt_land
# day_land
# mon_land
# 1hrPt_landIce
# day_landIce
# mon_landIce

# Plot or analysis
ds.hfls.isel(time=0).plot()
```

## Publication of further Project Results

- Guidelines to make available datasets from public authorities for the resarch community: `TBA`
- OcMOD - Project report of the NFDI4Earth Pilot: `TBA`
- The standardization scripts and data standard of the COSMO-REA6 standardization are explained in the following:

## Contents of this Repository

### CMOR tables / MIP tables (subfolder [`cmor_tables`](/COSMO-REA6/cmor_tables/))

The CMOR tables hold the Controlled Vocabulary (CV) for the project standard, incl. the variable definitions. The standard is versioned and derived from CORDEX and CMIP6 standards. The tables that hold the variable definitions are included in `json` and `csv` formats. So far, two versions/tags of the standard have been created:
- [Version/Tag 1.0](https://gitlab.dkrz.de/dicad-pp/ocmod/-/tree/1.0/COSMO-REA6/cmor_tables?ref_type=tags)
- [Version/Tag 1.1](https://gitlab.dkrz.de/dicad-pp/ocmod/-/tree/1.1/COSMO-REA6/cmor_tables?ref_type=tags) - GitLab Changelog [CV](https://gitlab.dkrz.de/dicad-pp/ocmod/-/commit/f36e1fefb3426ea1b0101498937277651a9a7317) and [CMOR tables](https://gitlab.dkrz.de/dicad-pp/ocmod/-/commit/eba14b17271972ccb54bf9ee20e87aee40a568ee) - [Changelog](https://c6dreq.dkrz.de/varmapping/ocmod/cosmo/drequpdates.php)
- Version/Tag 1.2 (**R6G2**, TBA) - GitLab Changelog TBA - Changelog TBA

### Mapping Database (subfolder [`DB`](/COSMO-REA6/DB/))

The variable mapping between the COSMO-REA6 output and the data standard has been created with the [Data Request and Variable Mapping Web GUI](https://c6dreq.dkrz.de): [Direct Link (Login required)](https://c6dreq.dkrz.de/varmapping/ocmod/cosmo/index.php).
The resulting database and several maintenance scripts are also part of this repository.
Deviations in the variable mapping information between COSMO-REA6 and the second installment of COSMO-REA6, R6G2, are captured here: [Direct Link (Login required)](https://c6dreq.dkrz.de/varmapping/ocmod/cosmor6g2/index.php)

### Standardisation Scripts (subfolder [`scripts`](/COSMO-REA6/scripts/))

The scripts and configuration used to conduct the standardisation of the COSMO-REA6 output. The scripts are in part automatically created using the [Variable Mapping WebGUI](https://c6dreq.dkrz.de). How to make use of this WebGUI is shown below.

## How to make use of the Standardisation Scripts

- The post processing is split into:
  - **aggregation**: temporal averaging, vertical interpolation, diagnostic variables serving as input for more than one CMOR variable
  - **diagnostic**: derivation of the CMOR variable out of one or multiple model variables
  - **cmor-rewrite**: Rewrite in the specified project format, incl. all required metadata
  - The korn-shell run-script *.runpp combines script fragments for each of the processing parts listed above and orchestrates the workflow. It should be submitted for single simulation years only:
    `sbatch COSMO-REA6.runpp 1995 1995` (SLURM batch scheduler) or `ksh COSMO-REA6.runpp 1995 1995` (interactive)
  - ** Directory paths in `COSMO-REA6.runpp` require adjustment **
  - `COSMO-REA6.runpp` loads a configuration from subfolder [`scripts/conf/`](/COSMO-REA6/scripts/conf). Which configuration is loaded and the configuration itself may require adjustments as well.
  - **NCO fixes**: 3D atmos hybrid height and 3D soil variables require their bounds to be fixed (workaround for a cdo problem). For that purpose two shell scripts applying the NCO operator `ncap2` have been set up. `ncap2` loads the entire file to be edited in memory, which may cause problems for the atmos 40 level hybrid height data depending on the system.
  - **NCO fixes 2**: Instead of running the entire CMORisation workflow again for minor metadata fixes/changes, a shell script applying the NCO operator `ncatted` is provided which can be configured as needed.
  - The following commits illustrate how to add further variables to the scripting and configuration:
    - [Commit](https://gitlab.dkrz.de/dicad-pp/ocmod/-/commit/cb7072e16829a061aa2ff52bb1ea0f60445b5eea) covering aggregation, zaxis description, run script and variable processing configuration (`scripts/conf/`)
    - [Commit](https://gitlab.dkrz.de/dicad-pp/ocmod/-/commit/e45ad86071de8324a2ed5fe408cfe78f67f410f6) covering mapping table, diagnostic and cmor-rewrite script (this changes go hand in hand with a change of the mapping database via the WebGUI as described below)
  - Do not hesitate to open an issue if there are problems / questions regarding the configuration and execution of the standardisation scripts


## [Variable Mapping WebGUI](https://c6dreq.dkrz.de)

Here the variable mapping information is entered and stored. The entered information can be automatically compiled into script fragments, helping to perform the diagnostics and CMORisation of the model output (example [diagnostic](https://gitlab.dkrz.de/dicad-pp/ocmod/-/blob/main/COSMO-REA6/scripts/ocmod_diagnostic_COSMO_COSMO_echam6_auto.h), [CMORisation](https://gitlab.dkrz.de/dicad-pp/ocmod/-/blob/main/scripts/ocmod_cmor-rewrite_COSMO_COSMO_auto.h)). Little modifications have been conducted (see below). The aggregation and run-scripts, however, have been manually created. Examples: [Aggregation](https://gitlab.dkrz.de/dicad-pp/ocmod/-/blob/main/scripts/ocmod_aggregation_COSMO_COSMO_auto.h), [Run-script (COSMO-REA6.runpp)](https://gitlab.dkrz.de/dicad-pp/ocmod/-/blob/main/scripts/COSMO-REA6.runpp)

### Register

Please register at the given [website](https://c6dreq.dkrz.de) if you want to edit the variable mapping information for COSMO-REA6 / R6G2.

### Accessing the variable mapping database

- Open the `Variable Mapping` tab on the [main page of the website](https://c6dreq.dkrz.de)
- Select the project `OcMOD` and click on `Select`
- Select the `submodel` you want to add mapping information for (COSMO-REA6 or R6G2) and click on `Edit`

![Access](docs/source/OcMODMapping1.png){width=50%}

### Filters 

![Filters](docs/source/OcMODMapping2.png){width=80%}

- `Filters` help to find the variables of the PalMod2 variable list you want to map to your model output
- Most filters allow the usage of the wildcard character `%`, which is SQL's expression of the asterisk (`*`) character you are familiar with from Linux shells.
- The filter `Mapping Information` accesses filenames, COSMO variable names (without underscores) and diagnostic recipes. For example one could search for `%sellevel%` if one is interested in variables requiring the `cdo expr,sellevel` diagnostic, or for `%1hrpt%albs%` if one is interested in the COSMO variable `ALBS` with frequency `1hrPt`. 

### Entering variable mapping information

- When entering variable mapping information for R6G2, mapping information entered for COSMO-REA6 will be available under the `Mapping Information of other submodels` header. 
- Quickly editing a variable is possible by clicking anywhere on the row of interest, which expands a form.
- Clicking instead on the rows `Edit` button opens the form in a new tab, with more detailed information about the CMOR variable, editing history, etc.
- Then additional information is displayed. For example, when entering variable mapping information for R6G2, mapping information entered for the basic COSMO-REA6 will be available under the `Mapping Information of other submodels` header.
- Under `Similar Variables`, one can select to submit the entered variable mapping information also for chosen similar variables, and later only change a few specifics for said variables (eg. the frequency in the filename, or the selection of a different level in the diagnostic recipe).
- Unfortunately later versions of browsers do not allow the scripted closing of tabs. Thus when clicking on `Submit`, the tab has to thereafter be closed manually.

![Editing1](docs/source/OcMODMapping3.png){width=80%}

- The model variable information related to the model output filenames / variable names to be entered, is thus the state after the `aggregation`!
- The following information has to be provided:
  - `filename patterns` along with `variable names (netCDF) or codes (GRIB)` for all source variables of the CMOR variable (`DATE` serves as placeholder for the file timestamp, see examples) - multiple input files / variables can be specified if required (for the diagnostic recipe)
  - _optionally_ a diagnostic recipe that can be interpreted by the [`cdo expr` operator](https://code.mpimet.mpg.de/projects/cdo/embedded/index.html#x1-3240002.7.1)
  - the `units` in the state _after_ the diagnostic (if applicable)
  - if applicable: the `positive` flux direction in the state after the diagnostic
  - _optionally_ provide a `comment` which is written to the file by `cdo cmor` - usually done if the model variable differs in some way from the requested CMOR variable or there have are problems or special notes related to that specific variable
  - _optionally_ add an `Editor's note` for example with information about the required aggregation (the information entered here will not show up in the CMORised file, it could be eg. `ToDo: verify variable units`)
  - `Submit` (and in newer browsers: Close the (reloaded) tab if no ERROR is displayed)

![Editing2](docs/source/OcMODMapping4.png){width=80%}


In the following a few examples of entered diagnostic recipes:
- temporary variables (denoted by the leading underscore) can be part of the recipes, different recipes have to be separated by a semi-colon:
```
_TS=ln((298.15-thetao)/(thetao+273.15));o2sat=0.0446596*exp(2.00907+3.22014*_TS+4.05010*_TS^2+4.94457*_TS^3-0.256847*_TS^4+3.88767*_TS^5-so*(0.00624523+_TS*0.00737614+0.0103410*_TS^2+0.00817083*_TS^3+0.000000488682*so))
```
- the output variable name does not have to be specified, as long as there is only one resulting variable
- variables from multiple input files can be part of the diagnostic recipe, you do not need to worry about the merging of the input files
- `cdo expr` supports a variety of operator functions, for example level selection (by value or index) and vertical integration/summing 
```
1000*(sellevidx(var84,1)+lay2factor*sellevel(var84,10))
```
```
0.01201*vertsum(sellevidxrange(var32,1,4)+sellevidxrange(var34,1,4)+sellevidxrange(var36,1,4)+sellevidxrange(var38,1,4)+sellevidxrange(var39,1,4)+sellevidxrange(var42,1,4)+sellevidxrange(var44,1,4)+sellevidxrange(var46,1,4)+sellevidxrange(var48,1,4)+sellevidxrange(var49,1,4))/vertsum(sellevidxrange(var12,1,4))
```

### Compile and download the scripts
- Open the `Post Processing` tab
- Configure the script generation via the mandatory steps:
  - (1) Select `OcMOD`, then click on the next step (2)

![PP1](docs/source/OcMODMapping5.png){width=30%}

  - (2) Select `COSMO-REA6` and/or `R6G2`. Multiple selection with held down CTRL key is possible. Click on `Generate Tables`.
  - Expand the result logs by clicking on `File: ...` and check if any Errors occured. If so, address the errors (by editing the variable mapping information) and `Generate Tables` again. Once succesful, click on the final step (4).

![PP2](docs/source/OcMODMapping6.png){width=60%}

  - (4) Click on `Create Script Templates` and again expand the logs with `File: ...`. If successful the logs include shell commands to download and extract the generated scripts and mapping tables:
    - In your cloned git repository, move to the `scripts/` folder then:
    - `mkdir new`
    - `cd new`
    - Paste the curl and unzip commands
    - Rename the tables and script fragments: `rename COSMO COSMO_COSMO *.h tables/*` (not all shells support this kind of renaming and instead require `rename -e 's/COSMO/COSMO_COSMO/g' *.h tables/*`
    - Compare the mapping table as well as the diagnostic and cmor-rewrite scripts and transfer the desired changes to the git versioned files (using eg. `meld` or `vimdiff`):
      - Make sure the `cdo cmor` calls in the `cmor-rewrite` script fragment include `om=r` for one hourly frequencies (1hr, 1hrPt) for monthly chunked output!
      - Unfortunately the scripts cannot be exchanged 1:1 since a few manual edits are/were necessary.

### Further information

More detailed **HowTos** related to the WebGUI are available here:
- https://c6dreq.dkrz.de/info/howto.php
- https://docs.google.com/presentation/d/1BaSHpF6I1DIbNnl2sKyiYmzAg8PqsGJFK2ILlty-pYI/edit?usp=sharing


 

#!/bin/bash

basedir=/work/bk1261/OcMOD/COSMO-REA6/archive_ecmwf/COSMO-REA

echo "#######################################################"
echo "# Land/soil variables - fixing depth and depth_bnds   #"
echo "#######################################################"
vars=("mrfsol_" "mrsol_" "tsl_")
bnds_0="0.,0.01,0.03,0.09,0.27,0.81,2.43,7.29"
bnds_1="0.01,0.03,0.09,0.27,0.81,2.43,7.29,21.87"
vals="0.005,0.02,0.06,0.18,0.54,1.62,4.86,14.58"
echo $bnds_0
echo $bnds_1

echo " Applying..."
if [[ -z "$bnds_1" ]] || [[ -z "$bnds_0" ]]; then echo "ERROR: No bounds specified"; exit 0; fi
for var in ${vars[@]}; do
  varx=$(echo $var | cut -d"_" -f1 )
  ff=( $(find $basedir -name "${var}*.nc" ) )
  for ifile in ${ff[@]}; do
    echo "---------------------------------------------------"
    echo "$ifile"
    ncap2 -A -h -s "depth(:)={$vals};depth_bnds(:,0)={$bnds_0};depth_bnds(:,1)={$bnds_1};" $ifile $ifile && echo "Successful." || { echo "ERROR: Setting bounds failed!" && exit 0; }
    echo "Set the following depth & depth_bnds:"
    ncdump -h $ifile | grep "double depth"
    ncdump -v depth $ifile | grep -A 1 "depth ="
    ncdump -v depth_bnds $ifile | grep -A 8 "depth\_bnds ="
  done
done

#zaxistype = depth_below_land
#size      = 8
#name      = depth
#longname  = "depth_below_land"
#units     = "m"
#levels    = 0.005 0.02 0.06 0.18 0.54 1.62 4.86 14.58
#lbounds   = 0 0.01 0.03 0.09 0.27 0.81 2.43 7.29
#ubounds   = 0.01 0.03 0.09 0.27 0.81 2.43 7.29 21.87
#axis = "Z"

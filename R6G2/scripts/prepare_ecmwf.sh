#!/bin/bash

##****  PREREQUISITS  ****##
##
##  Files need to reside in $DL_BASE
##  Files need to be extracted from tar archive
##  Files need to have timeRangeIndicator=0
##  Files need to have the sponge cut off
##
##****----------------****##

#set -x
###=== Process R6G2 files from ATOS ===###

#=== Parent directory for the downloaded files, folder structure of URLs will be craeted below this directory
DL_BASE=/media/x23056/ODTRANSFER_R6G2/OcMOD_data

#=== OUTDATA Base (where to extract the downloaded files to)
OUTDATA_BASE=/media/x23056/OcMOD/outdata

#=== If prerequisits are not met pre-process data
EXTR=TRUE    # TRUE  extracts archived data into $DL_BASE
PREPRO=TRUE  # TRUE  preforms pre-processing
COPY=TRUE    # TRUE  copies extracted from $DL_BASE to $OUTDATA_BASE
AGGR=TRUE    # TRUE  aggregate copied data into monthly files

#=== Grid(s)
# GRID="2D 3D 3Dp 3Dz"
# GRID=3D
GRID=2D

#=== Frequencies to download
FREQ=hourly  # with these data there only is hourly

#=== Filter for a single variable
VAR=
#VAR=PS
#VAR=PP
#VAR=QV
#VAR=TKE
#VAR=V
#VAR=U
#VAR=T
#VAR=W

#=== Filter for a single YYYY / YYYYMM / YYYYMMDD
#=== it is not expected that $TIME is empty, i.e., covers the complete period
#=== coverage of one year is the maximum
TIME=201502


#########################################################################################


#############
##  START  ##
#############

#=== sanity checks
{ [[ ${#TIME} != 4 ]] && [[ ${#TIME} != 6 ]] && [[ ${#TIME} != 8 ]] ; } && { echo "ERROR: length of TIME variable is unexpected, exiting!" ; exit 1 ; }
[[ ${FREQ} != "hourly" ]] && { echo "ERROR: type of FREQ is not set to mandatory choice hourly, exiting!" ; exit 1 ; }
[[ ${GRID} != "2D" ]] && { echo "WARNING: only GRID type 2D has been tested thoroughly" ; }


if [[ "$EXTR" == "TRUE" ]]; then
    echo "  +++ EXTRACTING ARCHIVED DATA"
    for g in $GRID; do
	if [[ "$g" == "2D" ]]; then
            FILELIST=( $(find $DL_BASE/.. -name "*$g.grb.tar" | grep -E $TIME ) )
	else
            FILELIST=( $(find $DL_BASE/.. -name "*.$g.tar" | grep -E $TIME ) )
	fi

        #== I need to extract archive and delete it from $DL_BASE
        for i in ${FILELIST[*]}; do
	    if [[ -z "$VAR" ]]; then
		echo "    +++  extracting $VAR of file $i into $DL_BASE"
		tar xf $i -C $DL_BASE
	    else
		echo "    +++  extracting complete content of file $i into $DL_BASE"
		tar xf $i -C $DL_BASE --wildcards *$VAR*
	    fi
        done
        # fi

        if [[ "$g" == "2D" ]] && ([[ $VAR == "TOT_PREC" ]] || [[ -z "$VAR" ]]); then
            FLIST=( $(find $DL_BASE -name 'TOT_PREC.2D.*.grb' | grep -E "$TIME") )
            for i in ${FLIST[*]}; do
                mv $i ${i/TOT_PREC/TOTPRECIP}
            done
        fi
    done
fi


if [[ "$PREPRO" == "TRUE" ]]; then
    echo "  +++ PREPROCESSING EXTRACTED DATA"
    for i in $DL_BASE/*$TIME*.grb; do
        varext=(${i//./ })
        Ni=`grib_get -p Ni $i`
        tri=`grib_get -p timeRangeIndicator $i`
        Ni=${Ni:0:3}
        tri=${tri:0:1}
        echo "  +++  checking tri and extent of file $i"
        if [[ $Ni == 880 && $tri != 0 ]]; then
            grib_set -stimeRangeIndicator=0 $i ${i/.grb/.tmp.grb}
            cdo selindexbox,17,864,17,840 ${i/.grb/.tmp.grb} $i
            rm -f ${i/.grb/.tmp.grb}
        elif [[ $Ni == 880 && $tri == 0 ]]; then
            cdo selindexbox,17,864,17,840 $i ${i/.grb/.tmp.grb}
            mv ${i/.grb/.tmp.grb} $i
        elif [[ $Ni != 880 && $Ni != 848 ]]; then
            echo "ERROR: Ni should be either 880 or 848, otherwise there is something seriously wrong. ABORT"
            echo "   ***   Ni = "$Ni
            exit 1
        fi
    done
fi


if [[ "$COPY" == "TRUE" ]]; then
    echo "  +++ COPYING EXTRACTED DATA TO FINAL DESTINATION"
    for g in $GRID; do
	if [[ -z "$VAR" ]]; then
	    # all files for time period of all variables
	    FILELIST=( $(find $DL_BASE -type f | grep -E "$g\.$TIME" ) )
	else
	    # all files for time period of one variable
	    [[ "$VAR" == "TOT_PREC" ]] && VAR=TOTPRECIP
	    FILELIST=( $(find $DL_BASE -type f | grep "$VAR" | grep -E "$g\.$TIME" ) )
	fi

	for i in $(seq 0 $((${#FILELIST[@]}-1))); do

	    iname=${FILELIST[$i]}
            fname=$( basename $iname | sed "s/_//g" | sed "s/.orig././g")
            vname=${fname%%.*}

            l_f=
            IntervalVars1hr=(AEVAPS ALBRAD ALHFLBS ALHFLPL ALHFLS ALWUS ASHFLS ASOBS ASOBT ASWDIFDS ASWDIFUS ASWDIRS ATHBS ATHBT ATHDS AUMFLS AVMFLS DURSUN RAINCON RAINGSP RUNOFFG RUNOFFS SNOWCON SNOWGSP TOTPRECIP VABSMX10M VGUSTCON10M VGUSTDYN10M VMAX10M TMAX2M TMIN2M)
            IntervalVars1hrPt=(CAPE3KM CAPECON CAPEML CAPEMU CINML CINMU CLCH CLCM CLCL CLCT HPBL HSNOW HTOPDC HZEROCL PMSL PS QV2M QVS RELHUM2M RHOSNOW SOBSRAD SODTRAD SWDIFDSRAD SWDIFUSRAD SWDIRSRAD T2M THBSRAD THBTRAD THDSRAD TQC TQI TQV U10M V10M TSNOW WSO)
            [[ " ${IntervalVars1hr[@]} " == *" $vname "* ]] && l_f=1hr
            [[ " ${IntervalVars1hrPt[@]} " == *" $vname "* ]] && l_f=1hrPt
            [[ -z "$l_f" ]] && { echo "ERROR: The variable name '$vname' could not be assigned to an aggregation group!" ; exit 1; }

            #=== Create target path
            targetdir=$OUTDATA_BASE/$l_f/$vname.$g
            mkdir -p $targetdir || { echo "ERROR: Could not create directory '$targetdir'!"; exit 1 ; }
            if [[ ${#TIME} -gt 6 ]]; then
		if [[ $fname == *"$TIME"* ]]; then
                    datestr=${TIME:0:6}
                    ofile=$( echo $fname | sed "s/$TIME/$datestr"/g )
		fi
            else
		ofile=$fname
            fi
            \cp -v $iname $targetdir/$ofile || { echo "ERROR: Could not copy file '$l_outpath' to '$targetdir'!"; exit 1 ; }
	done
    done
fi


if [[ $AGGR == "TRUE" ]]; then
    echo "  +++ AGGREGATING COPIED DATA"
    for g in $GRID; do
        if  [[ ${#TIME} -eq 6 ]]; then  # monthly chunks
	    if [[ -z "$VAR" ]]; then
		# all files within one month for all variables
		FILELIST=( $(find $OUTDATA_BASE -name "*$g.$TIME*" ) )
            else
		# all files within one month of one variable
		[[ "$VAR" == "TOT_PREC" ]] && VAR=TOTPRECIP
		FILELIST=( $(find $OUTDATA_BASE -name "$VAR.$g.$TIME*" ) )
	    fi
            # get list of variables
            vname1=dummy
            for i in $(seq 0 $((${#FILELIST[@]}-1))); do
		fname=$( basename ${FILELIST[$i]} )
		dname=$( dirname ${FILELIST[$i]} )
		vname=${fname%%.*}
		if [[ "$vname1" != "$vname" ]]; then
                    vname1=$vname
                    oname=$dname/${fname%%.*}.2D.$TIME.grb
                    # echo "  ===*** neue Ausgabedatei:  $oname"
                    echo "  +++ cdo mergetiming $oname"
                    cdo mergetime `find $OUTDATA_BASE -name "$vname1.$g.$TIME*"` $oname
		fi
		rm -fv ${FILELIST[$i]}
            done
      	else
	    echo "  ***   ERROR   ***: $TIME has an unexpected length!"
	    exit 1
	fi
    done
fi

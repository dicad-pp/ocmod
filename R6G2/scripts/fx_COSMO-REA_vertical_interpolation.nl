!*************************************************************************************
! Fieldextra Receipt to interpolate QV, T on CORDEX pressure levels;
! Input fields are HSURF as well as PP, QV, T on model levels
!-------------------------------------------------------------------------------------
! Using cookbook recipe "vertical_interpolation"as well as namelist
! density.REA6.v12.8.nl by Michael Borsche and Thomas Spangehl
!*************************************************************************************

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Remarks from README
!The places in the filesystem where data are produced is controlled by
!(1) standard error & output of shell where fieldextra is started
!(2) value of &RunSpecification:diagnostic_path
!    (current working directory of fieldextra when not set)
!(3) values of &Process:out_file
!(4) (if set) value of &RunSpecification:ready_flag_dir
!(5) (if set) value of &RunSpecification:stop_wait_flag
!(6) (if set) value of &RunSpecification:stop_flag
!
!!!!!!!!!!!!!!!!!!
! Output
!!!!!!!!!!!!!!!!!!
! The following steps help to potentially reduce the memory footprint:
! - use time loop construct on output to group data by validation date;
! - set "in_read_order"
! - Experiment with n_ompthread_collect, n_ompthread_generate, and out_cost_expensive to find the
!   optimal solution for your problem
!   (set additional_profiling to true and use the detailed profiling available in fieldextra.diagnostic)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! Remarks from testing
!(1) COSMO_REA6_CONST_withOUTsponge.grb/nc does only hold HSURF for 2007-2012. For the remaining
!    experiment, a different HSURF was used.
!(2) The GRIB1-variant has to be used. In a netCDF file, HSURF is not recognized
!    ("missing meta data ... product category")
!(3) It does not work to compute "W" in the same way as "T" or "QV":
!%WARNING(0734) prepare_data: trying to compute field W, but no parent defined in active
!dictionary cosmo

!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!Header
!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! Run time specifications, compulsory
!  stop_timestamp and additional diagnostic only necessary when testing the namelist
!  clone_missing_input=.true. ! file generated from template with missing values
!  wait_time=600 ! after 600s waiting time
!  mx_wait_time=603 ! exit 1 after waiting for more than one file
! n_ompthread=24
!  n_ompthread_collect=10
!  n_ompthread_generate=16
!   stop_timestamp = "INIT"
&RunSpecification
  soft_memory_limit=10
  enable_exit_status=.true.
  enable_repeat_mode = 'none'
  verbosity='high'
  additional_diagnostic=.true.
  additional_profiling=.true.
  strict_usage=.true.
/

! Program resources & configuration, compulsory
!  dictionary, locations, etc.
&GlobalResource
  dictionary            = "/work/bk1261/OcMOD/COSMO-REA6/fieldextra/resources/dictionary_cosmo.txt"
  grib_definition_path  = "/work/bk1261/OcMOD/COSMO-REA6/fieldextra/resources/eccodes_definitions_cosmo", "/work/bk1261/OcMOD/COSMO-REA6/fieldextra/resources/eccodes_definitions_vendor" /

!  default_out_type_stdlongitude = .true.
!   default_dictionary    = "cosmo",
&GlobalSettings
  default_model_name    = "cosmo",
  location_to_gridpoint = "sn"
/

! Attributes of NetCDF imported file, 0 or 1 occurence
!&NetCDFImport

! Attributes of NetCDF output, 0 or 1 occurence
!&NetCDFExport

! Attributes of INSPECT ouput, 0 or 1 occurence
!&InspectSpecification


! Model specifications, at least 1 occurence
&ModelSpecification
  model_name = "cosmo"
  earth_axis_large = 6371229.
  earth_axis_small = 6371229.
  vertical_coordinate_coef=
1.0200000000e+02, 4.0000000000e+01, 1.0000000000e+05, 2.8814990234e+02, 4.2000000000e+01, 1.1430000000e+04, 2.2700000000e+04, 2.0800000000e+04,
1.9100000000e+04, 1.7550000000e+04, 1.6150000000e+04, 1.4900000000e+04, 1.3800000000e+04, 1.2785000000e+04, 1.1875000000e+04, 1.1020000000e+04,
1.0205000000e+04, 9.4400000000e+03, 8.7100000000e+03, 8.0150000000e+03, 7.3550000000e+03, 6.7250000000e+03, 6.1300000000e+03, 5.5650000000e+03,
5.0350000000e+03, 4.5300000000e+03, 4.0600000000e+03, 3.6150000000e+03, 3.2000000000e+03, 2.8150000000e+03, 2.4550000000e+03, 2.1250000000e+03,
1.8200000000e+03, 1.5450000000e+03, 1.2950000000e+03, 1.0700000000e+03, 8.7000000000e+02, 6.9500000000e+02, 5.4200000000e+02, 4.1200000000e+02,
3.0300000000e+02, 2.1400000000e+02, 1.4300000000e+02, 8.9000000000e+01, 4.9000000000e+01, 2.0000000000e+01, 0.0000000000e+00, 0.0000000000e+00,
0.0000000000e+00, 0.0000000000e+00, 7.5000000000e+01, 1.0000000000e+04
/



!###################
! INCORE storage
!======================================================================
! * HSURF and vertical_coordinate_coef are needed
!   to calculate HFL for vertical interplolation
!----------------------------------------------------------------------

&Process
  in_file="HSURFINFILE"
  out_type="INCORE"
/

!# Read HSURF
&Process in_field ="HSURF" /



!#################
! A. Extract constant fields from INCORE storage
!-----------------------------------------------

&Process
  in_type  = "INCORE"
  out_file = "OUTFILE"
  out_type = "NETCDF"
/

!# get HSURF and calculate HFL
&Process in_field ="HSURF" tag="HSURF"/
&Process  in_field ="HFL" /
&Process  in_field ="HHL" /
&Process  in_field ="P0FL" tag="P0FL"/



!################
! B. Extract other fields from files
!-----------------------------------------------

&Process
  in_file="PPFILE"
  out_file= "OUTFILE"
  out_type = "NETCDF"
/
&Process in_field ="PP", level_class='k_full' /

&Process
  in_file="TFILE"
  out_file= "OUTFILE"
  out_type = "NETCDF"
/
&Process in_field ="T", level_class='k_full' /

&Process
  in_file="QFILE"
  out_file= "OUTFILE"
  out_type = "NETCDF"
/
&Process in_field ="QV", level_class='k_full' /

&Process  out_field="P" /
&Process
  out_field = "T"
  tag = "T_on_p"
  voper = "intpl_k2p,lnp"
  voper_lev_units = "hPa"
  voper_lev = 1000,925,850,750,700,600,500,400,300,250,200,150,100,70,50
  poper="replace_undef,9999"
/
&Process
  out_field = "QV"
  tag = "QV_on_p"
  voper = "intpl_k2p,lnp"
  voper_lev_units = "hPa"
  voper_lev = 1000,925,850,750,700,600,500,400,300,250,200,150,100,70,50
  poper="replace_undef,9999"
/
!&Process out_field = "P0FL" /

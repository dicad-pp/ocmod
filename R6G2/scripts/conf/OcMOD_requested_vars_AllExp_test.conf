# ##################################################
# ##################################################
# CMIP6 PostProcessing Workflow Configuration File
# ##################################################
# This configuration file specifies which variables will be
#  processed for which time intervals.
# It contains + DREQSETTINGS (computed from the CMIP6-DataRequest) and
#             + USERSETTINGS (specified by the user)
# for each experiment.
#
# There are 3 types of settings: Standard, TimeSlice, GridAndTimeSlice.
#
# -Use '#' for comments (also inline).
# ##################################################


# Data Request Version: 01.00.01
# Supported MIPs (1): OcMOD
# Supported Experiments (1): REA6


##################################################
EXP=AllExp
##################################################

DREQSETTINGS
fx             : fx          = slice: first_step
tsoilmask      : tsoilmask   = slice: first_step
#FillerFiles    : FillerFiles = slice: 1996050401-1996050600,2005101319-2005101319,2005111819-2005111819,2005112300-2005112300 # Time periods that Lost/Missing input files are filled with Missing Value files (supported only QV, T, U, V)

USERSETTINGS
# ---> Specify your settings for Experiment AllExp here
# General     #
TOTAL          : TOTAL       = False     # If set to "False", only produces variables set to "True" below
					 # If set to "True", produces all variables, but the ones set to "False" below
FillerFiles    : FillerFiles = False     # Lost/Missing input files are filled with Missing Value files (supported only QV, T, U, V)
# Aggregation #
aggregation    : fx        = False        # General time invariant variables
aggregation    : HSURF     = False        # Generate annual HSURF input files from correct HSURF source file
mon            : orog      = False        # Monthly orog required due to varying HSURF for 2007-2012 and rest of the experiment
aggregation    : tsoilmask = False        # tsl mask fix
aggregation    : 1hrPt3DUVFX     = False # FIELDEXTRA Destaggering (U, V) - time avg, grid description
aggregation    : 1hrPt3DUVPLEVFX = False # FIELDEXTRA Interpolation & Destaggering (U, V) - time avg, grid description
aggregation    : 1hrPt3DPLEVFX   = False # FIELDEXTRA Interpolation (QV, T, ...) - time avg, grid description
aggregation    : 1hrPt3D40       = False # QV, T: 3D variables on 40 model levels (ecmwf variant) - time avg, grid description
aggregation    : 1hrPt3D         = False # TKE (optionally T, QV): 3D variables on 6 model levels (opendata variant) - grid description
aggregation    : 1hrPt3Dheight   = False  # 3D variables written out on height levels
aggregation    : day3D           = False  # 3D variables (opendata archive)
aggregation    : mon3D           = False  # 3D variables (opendata archive)
aggregation    : 1hr             = True  # 2D variables (opendata archive)
aggregation    : 1hrPt           = True  # 2D variables (opendata archive)
aggregation    : 1hrTOday        = True  # 2D variables (opendata archive)
aggregation    : 1hrPtTOday      = True  # 2D variables (opendata archive)
aggregation    : 6hr             = True  # 2D variables (opendata archive)
aggregation    : day             = True  # 2D variables (opendata archive)
aggregation    : mon             = True  # 2D variables (opendata archive)
aggregation    : phase1          = False # 1st phase - data on DWD opendata server
aggregation    : phase2          = False # 2nd phase - 3D data & data in ECMWF archive
aggregation    : phase3          = False # 3rd phase - FIELDEXTRA variables, errata
aggregation    : phase4          = True  # 4th phase - R6G2
# Diag&CMOR   #
#fx             : fx      = True
#uas            : uas     = True # requiring re-rotation to common coordinate system
#vas            : vas     = True # requiring re-rotation to common coordinate system
tas            : tas     = True
psl            : psl     = True
ps             : ps      = True
hurs           : hurs    = True
tasmax         : tasmax  = True
tasmin         : tasmin  = True
pr             : pr      = True
prc            : prc     = True
prls           : prls    = True
prsn           : prsn    = True
prsnc          : prsnc   = True
prsnls         : prsnls  = True
prw            : prw     = True
rsdsdir        : rsdsdir = True
rsus           : rsus    = True
rlus           : rlus    = True
rsds           : rsds    = True
rlds           : rlds    = True
rlut           : rlut    = True # data in ECMWF archive
rsdt           : rsdt    = True # data in ECMWF archive
rsut           : rsut    = True # data in ECMWF archive
clt            : clt     = True
zmla           : zmla    = True
huss           : huss    = True
#ts             : ts      = True
sund           : sund    = True
#mrro           : mrro    = True    ??? was ist das ???
mrros          : mrros   = True
wsgsmax        : wsgsmax = True
#snw            : snw     = True
snd            : snd     = True
#snc            : snc     = True    ??? was ist das ???
#hfls           : 1hrPt   = True
#hfls           : mon     = True # data in ECMWF archive
#hfls           : day     = True # data in ECMWF archive
#hfls           : 1hr     = True # data in ECMWF archive
#hfss           : 1hrPt   = True
#hfss           : mon     = True # data in ECMWF archive
#hfss           : day     = True # data in ECMWF archive
#hfss           : 1hr     = True # data in ECMWF archive
sfcWindmax     : sfcWindmax = True # data in ECMWF archive
tdps           : tdps    = True # data in ECMWF archive
#clwvi          : clwvi   = True # data in ECMWF archive       ??? warum nicht in paramter list? was ist das ???
clivi          : clivi   = True # data in ECMWF archive
#evspsbl        : evspsbl = True # data in ECMWF archive
clh            : clh     = True # data in ECMWF archive
clm            : clm     = True # data in ECMWF archive
cll            : cll     = True # data in ECMWF archive
#tauu           : tauu    = True # data in ECMWF archive
#tauv           : tauv    = True # data in ECMWF archive
#twp            : twp     = True # data in ECMWF archive # TWATER vertically integrated water
wsgscmax       : wsgscmax= True # data in ECMWF archive # required registration of new standard name
wsgstmax       : wsgstmax= True # data in ECMWF archive # required registration of new standard name
#zf             : zf      = True  # data in ECMWF archive # HZEROCL freezing lvl altitude
#albs           : albs    = False # nProv # # surface albed ALB_RAD # unrealistic values Russia/Georgia # data in ECMWF archive
snden          : snden   = False # nProv - instead added comment how to calculate it from snw and snd # surface snow density RHO_SNOW
#snl            : snl     = False # nProv # SNOWLMT
#### 3D variables
# 3D soil variables and their vertical integrals
#tsl            : tsl     = True # 3D soil variable # data in ECMWF archive
#mrfsol         : mrfsol  = True # 3D soil variable # data in ECMWF archive
#mrsol          : mrsol   = True # 3D soil variable # data in ECMWF archive
#mrfso          : mrfso   = False # nProv # data in ECMWF archive # vertical sum of mrfsol dominated by lower levels and deviating distinctly from comparable CORDEX data -> up to the user which levels are considered (mrfsol)
#mrfsos         : mrfsos  = False # nProv # mrfso near surface # data in ECMWF archive
#mrso           : mrso    = False # nProv # data in ECMWF archive # vertical sum of mrsol dominated by lower levels and deviating distinctly from comparable CORDEX data -> up to the user which levels are considered (mrsol)
#mrsos          : mrsos   = False # nProv # mrso near surface # data in ECMWF archive
# air density on 8 height levels
#ad200m         : ad200m    = True
#ad175m         : ad175m    = True
#ad150m         : ad150m    = True
#ad125m         : ad125m    = True
#ad100m         : ad100m    = True
#ad80m          : ad80m     = True
#ad60m          : ad60m     = True
#ad40m          : ad40m     = True
# wind speed on 9 height levels
#ws200m         : ws200m    = True
#ws175m         : ws175m    = True
#ws150m         : ws150m    = True
#ws125m         : ws125m    = True
#ws100m         : ws100m    = True
#ws80m          : ws80m     = True
#ws60m          : ws60m     = True
#ws40m          : ws40m     = True
#sfcWind        : sfcWind   = True
# wind direction on 9 height levels
#wd200m         : wd200m    = True
#wd175m         : wd175m    = True
#wd150m         : wd150m    = True
#wd125m         : wd125m    = True
#wd100m         : wd100m    = True
#wd80m          : wd80m     = True
#wd60m          : wd60m     = True
#wd40m          : wd40m     = True
#wd10m          : wd10m     = True
# u/v on 6 and 40 hybrid height levels
#ua             : ua        = True  # 3D on 40 model levels # data in ECMWF archive
#ua6            : ua6       = True  # 3D on 6 model levels # data in ECMWF archive
#va             : va        = True  # 3D on 40 model levels # data in ECMWF archive
#va6            : va6       = True  # 3D on 6 model levels # data in ECMWF archive
# w, T, QV, TKE on 6 and 40 hybrid height levels
#wa             : wa        = False # nProv # 3D on 40 model levels # data in ECMWF archive
#wa6            : wa6       = False # nProv # 3D on 6 model levels # data in ECMWF archive
#ta             : ta        = True  # 3D on 40 model levels # data in ECMWF archive
#ta6            : ta6       = True  # 3D on 6 model levels # data in ECMWF archive
#hus            : hus       = True  # 3D on 40 model levels # data in ECMWF archive
#hus6           : hus6      = True  # 3D on 6 model levels # data in ECMWF archive
# TKE - data from opendata archive will be produced (6 model levels), data from ECMWF archive not requested for retrieval
#tke            : tke       = True  # 3D on 6 model levels # data in opendata archive # required registration of new CF standard_name
#tke            : tke      = False  # nProv # 3D on 40 model levels # data in ECMWF archive # required registration of new CF standard_name
#tke6           : tke6      = False  # nProv # 3D on 6 model levels # data in ECMWF archive # required registration of new CF standard_name
# pfull on 6 and 40 hybrid height levels
#pfull          : pfull     = True # data in ECMWF archive
#pfull6         : pfull6    = True # data in ECMWF archive
# u/v on 8 height levels
#uXm            : uXm       = False # nProv - instead added comment how to calculate it from wind_speed and wind_from_direction
#vXm            : vXm       = False # nProv - instead added comment how to calculate it from wind_speed and wind_from_direction
# u,v,w,hus,ta on 18 pressure levels
#ua1000         : ua1000    = True # data in ECMWF archive
#ua925          : ua925     = True # data in ECMWF archive
#ua850          : ua850     = True # data in ECMWF archive
#ua750          : ua750     = True # data in ECMWF archive
#ua700          : ua700     = True # data in ECMWF archive
#ua600          : ua600     = True # data in ECMWF archive
#ua500          : ua500     = True # data in ECMWF archive
#ua400          : ua400     = True # data in ECMWF archive
#ua300          : ua300     = True # data in ECMWF archive
#ua250          : ua250     = True # data in ECMWF archive
#ua200          : ua200     = True # data in ECMWF archive
#ua150          : ua150     = True # data in ECMWF archive
#ua100          : ua100     = True # data in ECMWF archive
#ua70           : ua70      = True # data in ECMWF archive
#ua50           : ua50      = True # data in ECMWF archive
#ua30           : ua30      = False # nProv # model TOA lower than this pressure level # data in ECMWF archive
#ua20           : ua20      = False # nProv # model TOA lower than this pressure level # data in ECMWF archive
#ua10           : ua10      = False # nProv # model TOA lower than this pressure level # data in ECMWF archive
#va1000         : va1000    = True # data in ECMWF archive
#va925          : va925     = True # data in ECMWF archive
#va850          : va850     = True # data in ECMWF archive
#va750          : va750     = True # data in ECMWF archive
#va700          : va700     = True # data in ECMWF archive
#va600          : va600     = True # data in ECMWF archive
#va500          : va500     = True # data in ECMWF archive
#va400          : va400     = True # data in ECMWF archive
#va300          : va300     = True # data in ECMWF archive
#va250          : va250     = True # data in ECMWF archive
#va200          : va200     = True # data in ECMWF archive
#va150          : va150     = True # data in ECMWF archive
#va100          : va100     = True # data in ECMWF archive
#va70           : va70      = True # data in ECMWF archive
#va50           : va50      = True # data in ECMWF archive
#va30           : va30      = False # nProv # model TOA lower than this pressure level # data in ECMWF archive
#va20           : va20      = False # nProv # model TOA lower than this pressure level # data in ECMWF archive
#va10           : va10      = False # nProv # model TOA lower than this pressure level # data in ECMWF archive
#wa1000         : wa1000    = False # nProv # FIELDEXTRA issues # data in ECMWF archive
#wa925          : wa925     = False # nProv # FIELDEXTRA issues # data in ECMWF archive
#wa850          : wa850     = False # nProv # FIELDEXTRA issues # data in ECMWF archive
#wa750          : wa750     = False # nProv # FIELDEXTRA issues # data in ECMWF archive
#wa700          : wa700     = False # nProv # FIELDEXTRA issues # data in ECMWF archive
#wa600          : wa600     = False # nProv # FIELDEXTRA issues # data in ECMWF archive
#wa500          : wa500     = False # nProv # FIELDEXTRA issues # data in ECMWF archive
#wa400          : wa400     = False # nProv # FIELDEXTRA issues # data in ECMWF archive
#wa300          : wa300     = False # nProv # FIELDEXTRA issues # data in ECMWF archive
#wa250          : wa250     = False # nProv # FIELDEXTRA issues # data in ECMWF archive
#wa200          : wa200     = False # nProv # FIELDEXTRA issues # data in ECMWF archive
#wa150          : wa150     = False # nProv # FIELDEXTRA issues # data in ECMWF archive
#wa100          : wa100     = False # nProv # FIELDEXTRA issues # data in ECMWF archive
#wa70           : wa70      = False # nProv # FIELDEXTRA issues # data in ECMWF archive
#wa50           : wa50      = False # nProv # FIELDEXTRA issues # data in ECMWF archive
#wa30           : wa30      = False # nProv # FIELDEXTRA issues # data in ECMWF archive
#wa20           : wa20      = False # nProv # FIELDEXTRA issues # data in ECMWF archive
#wa10           : wa10      = False # nProv # FIELDEXTRA issues # data in ECMWF archive
#ta1000         : ta1000    = True # data in ECMWF archive
#ta925          : ta925     = True # data in ECMWF archive
#ta850          : ta850     = True # data in ECMWF archive
#ta750          : ta750     = True # data in ECMWF archive
#ta700          : ta700     = True # data in ECMWF archive
#ta600          : ta600     = True # data in ECMWF archive
#ta500          : ta500     = True # data in ECMWF archive
#ta400          : ta400     = True # data in ECMWF archive
#ta300          : ta300     = True # data in ECMWF archive
#ta250          : ta250     = True # data in ECMWF archive
#ta200          : ta200     = True # data in ECMWF archive
#ta150          : ta150     = True # data in ECMWF archive
#ta100          : ta100     = True # data in ECMWF archive
#ta70           : ta70      = True # data in ECMWF archive
#ta50           : ta50      = True # data in ECMWF archive
#ta30           : ta30      = False # nProv # model TOA lower than this pressure level # data in ECMWF archive
#ta20           : ta20      = False # nProv # model TOA lower than this pressure level # data in ECMWF archive
#ta10           : ta10      = False # nProv # model TOA lower than this pressure level # data in ECMWF archive
#hus1000         : hus1000  = True # data in ECMWF archive
#hus925          : hus925   = True # data in ECMWF archive
#hus850          : hus850   = True # data in ECMWF archive
#hus750          : hus750   = True # data in ECMWF archive
#hus700          : hus700   = True # data in ECMWF archive
#hus600          : hus600   = True # data in ECMWF archive
#hus500          : hus500   = True # data in ECMWF archive
#hus400          : hus400   = True # data in ECMWF archive
#hus300          : hus300   = True # data in ECMWF archive
#hus250          : hus250   = True # data in ECMWF archive
#hus200          : hus200   = True # data in ECMWF archive
#hus150          : hus150   = True # data in ECMWF archive
#hus100          : hus100   = True # data in ECMWF archive
#hus70           : hus70    = True # data in ECMWF archive
#hus50           : hus50    = True # data in ECMWF archive
#hus30           : hus30    = False # nProv # model TOA lower than this pressure level # data in ECMWF archive
#hus20           : hus20    = False # nProv # model TOA lower than this pressure level # data in ECMWF archive
#hus10           : hus10    = False # nProv # model TOA lower than this pressure level # data in ECMWF archive
#<---- Specify your settings for Experiment AllExp here
# nProv: will not be provided

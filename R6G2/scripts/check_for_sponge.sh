#!/bin/bash
#SBATCH -J check_for_sponge         # Specify job name
#SBATCH -p compute
#SBATCH --mem=250G
#SBATCH -N 1                   # Specify number of nodes
#SBATCH -t 08:00:00            # Set a limit on the total run time
#SBATCH -A bm0021              # Charge resources on this project account
#SBATCH --qos=esgf
#SBATCH -o check_for_sponge     # File name for standard output
#SBATCH -e check_for_sponge     # File name for standard error output

#Nohup:
# nohup bash Run_Job.sh > nohup_check_for_sponge.out 2>&1 &
# salloc -p compute -A bm0021 -n 1 -t 60 -- /bin/bash -c 'ssh -X $SLURM_JOB_NODELIST'

HOSTNAME=$( hostname )

echo Running on $HOSTNAME
echo with PID $$


maxjobs=32

n=0 #Do not change

#Unpredictable extension
#filename="${script##*/}"

#Usual extension 
filename=$(basename -- "$script") #/path/to/script.py -> script.py

#Extract Filename and Extension
extension="${filename##*.}"
filename="${filename%.*}"

# Define parent directory
dir=/work/kd1292/k204212/OcMOD/outdata_3D/1hrPt

#Loop in parallel over parameters
for var in U.3D V.3D W.3D T.3D PP.3D QV.3D
do
  echo "$(date) Starting ${var} ..."
  ifiles=( $(ls $dir/$var | sort ) )
  echo "${#ifiles[@]} found."
  for ifile in ${ifiles[@]}; do
  (
     cdo verifygrid $dir/$var/$ifile | grep "Grid consists of" | grep -q -v "(848x824)" && {
       echo $dir/$var/$ifile
     }
     echo $ifile >> checked_for_sponge
  )&
  # limit jobs
  if (( $(($((++n)) % $maxjobs)) == 0 )) ; then
     wait # wait until all have finished (not optimal, but most times good enough)
     #echo $n wait
  fi
  done
  wait
  echo "... $(date) finished ${var}."
  echo "#############################################################################################"
done

exit 0

#! /usr/bin/env python
'''\
Check CMORized COSMO-REA datasets for gaps in time axis,
for time steps the entire field has a constant value.

Quick and dirty replacement for QA-DKRZ time checks.
Does not:
- check time bounds
- check timestamps of the filename
- discover all-NaN slices (xarray will prompt a warning though in that case)

Written by Martin Schupfner, 2023
'''

import xarray as xr
import cf_xarray as cfxr
import cftime
import glob, os, sys
from datetime import datetime, timedelta
import numpy as np
import argparse as ap

# Example path + filename
#/work/bk1261/COSMO-REA/reanalysis/EUR-6km/DWD/ECMWF-ERAINT/REA6/r1i1p1f1/COSMO/v1/mon/atmos/sund/v20230314/sund_EUR-6km_ECMWF-ERAINT_REA6_r1i1p1f1_COSMO_v1_mon_


def printtimedelta(d):
    'Return timedelta (s) as either min, hours, days, whatever fits best.'
    if d > 86000:
        return f"{d/86400.} days"
    if d > 3500: 
        return f"{d/3600.} hours"
    if d > 50:
        return f"{d/60.} minutes"
    else: return f"{d} seconds"

def getmax(obj):
    'Return timeseries of field maxima'
    return xr.apply_ufunc(np.nanmax, obj, input_core_dims=[["rlat", "rlon"]], kwargs={"axis": (-2,-1)})

def getmin(obj):
    'Return timeseries of field minima'
    return xr.apply_ufunc(np.nanmin, obj, input_core_dims=[["rlat", "rlon"]], kwargs={"axis": (-2,-1)})



# Definition of maximum deviations from the given frequency
deltdic={}
deltdic["monmax"]=timedelta(days=31.5).total_seconds()
deltdic["monmin"]=timedelta(days=27.5).total_seconds()
deltdic["daymax"]=timedelta(days=1.1).total_seconds()
deltdic["daymin"]=timedelta(days=0.9).total_seconds()
deltdic["1hrmin"]=timedelta(hours=0.9).total_seconds()
deltdic["1hrmax"]=timedelta(hours=1.1).total_seconds()
deltdic["1hrPtmin"]=timedelta(hours=0.9).total_seconds()
deltdic["1hrPtmax"]=timedelta(hours=1.1).total_seconds()
deltdic["6hrmin"]=timedelta(hours=5.9).total_seconds()
deltdic["6hrmax"]=timedelta(hours=6.1).total_seconds()



# Variables
# Opendata 2D
vars = ["ad100m", "ad125m", "ad150m", "ad175m", "ad200m", "ad40m", "ad60m", "ad80m", "clt", "hurs", "huss", "pr", "prc", "prls", "prsn", "prsnc", "prsnls", "prw", "ps", "psl", "rlds", "rlus", "rsds", "rsdsdir", "rsus", "sfcWind", "sund", "tas", "tasmax", "tasmin", "ts", "uas", "vas", "wd100m", "wd10m", "wd125m", "wd150m", "wd175m", "wd200m", "wd40m", "wd60m", "wd80m", "ws100m", "ws125m", "ws150m", "ws175m", "ws200m", "ws40m", "ws60m", "ws80m", "wsgsmax", "zmla", "mrro", "mrros", "snw", "snc", "snd"]
# ECMWF 2D
vars = ["albs", "hfls", "hfss", "rlut", "rsdt", "rsut", "sfcWindmax", "tauu", "tauv", "wsgscmax", "wsgstmax", "clh",  "clivi", "cll", "clm", "clwvi", "rlut", "rsdt", "rsut", "tauu", "tauv", "tdps", "twp", "zf", "mrfso", "mrfsol", "mrso", "mrsol", "tsl", "evspsbl"]
# 3D
vars = ["tke", "hus", "ta"]
vars = ["albs"]

# Frequency
#freq="1hrPt"
#freq="1hr"
#freq="day"
#freq="6hr"
freq="mon"

#parentdir="/work/bk1261/COSMO-REA"
#parentdir="/work/bk1261/OcMOD/COSMO-REA6/archive_ecmwf/COSMO-REA"
parentdir="/work/bk1261/OcMOD/COSMO-REA6/archive_3D/COSMO-REA"



# Deal with command line attributes
cl = ap.ArgumentParser(description="Check for gaps in time axis and time steps with the field being constant.", prefix_chars='-+')
cl.add_argument('-v', '--variables', type=str, nargs='+', required=False, dest="vars", help='space separated list of variables to process, eg. pr prc snd')
cl.add_argument('-f', '--frequency', type=str, nargs=1, required=False, dest="freq", help='frequency, eg. 1hrPt')
cl.add_argument('-d', '--directory', type=str, nargs=1, required=False, dest="dir", help='parent directory, eg. /work/bk1261/COSMO-REA')
cl.add_argument('-n', '--noconstcheck', required=False, dest="no_const_check", action='store_true', help='do not perform check for constant fields')
cl.add_argument('-w', '--nowarnings', required=False, dest="warn", action='store_true', help='turn off warnings, eg. from xarray')
args = cl.parse_args()
if args.vars:
    vars=list(args.vars)
if args.freq:
    freq=str(args.freq[0])
    if not freq in ["1hrPt", "1hr", "mon", "day"]:
        sys.stderr.write(f"Frequency '{freq}' not supported.\n")
        sys.exit(1)
if args.dir:
    parentdir=str(args.dir[0])        
    if not os.path.isdir(parentdir):
        sys.stderr.write(f"Specified directory '{parentdir}' does not exist.\n")
        sys.exit(1)
if args.warn:
    import warnings
    warnings.simplefilter("ignore")



# Perform checks
for var in vars:
    realm="atmos"
    if var in ["mrros", "mrro", "mrfso", "mrfsol", "mrso", "mrsol", "tsl", "evspsbl"]: realm="land"
    elif var in ["snc", "snd", "snw"]: realm="landIce"
    print("--"*50)
    print(var)

    pardir=f"{parentdir}/reanalysis/EUR-6km/DWD/ECMWF-ERAINT/REA6/r1i1p1f1/COSMO/v1/{freq}/{realm}/{var}/v20230314/"
    try:
        os.chdir(pardir)
    except:
        print(f"    Folder for {pardir} not found!")
        continue
    files=sorted(list(glob.glob("*_v1_*_*-*.nc")))
    errors=[]


    print(f"  {len(files)} files found for variable {var}({freq}).")

    prevt=""
    currt=""
    
    for f in files:
        d=xr.open_dataset(f)
        #print("-----------------"*3)
        #print(d.time.values[0])
        if d.time.size == 0:
            errors.append(f"'{f}': time axis of length 0")
        elif d.time.size == 1:
            currt = d.time[0]
            if prevt:
                delt = currt - prevt
                #print(timedelta(delt[0]).seconds)
                delts = delt.values / np.timedelta64(1, 's')
                if delts > deltdic[freq+"max"] or delts < deltdic[freq+"min"]:
                    errors.append(f"    {f}: Irregularity in time axis - gap between files - previous {prevt.values} - current {currt.values} - delta-t {printtimedelta(delts)}")
            prevt = currt
        else:
            # gaps between files
            currt = d.time[0]
            if prevt:
                delt = currt - prevt
                delts = delt.values / np.timedelta64(1, 's')
                if delts > deltdic[freq+"max"] or delts < deltdic[freq+"min"]:
                    errors.append(f"    {f}: Irregularity in time axis - gap between files - previous {prevt.values} - current {currt.values} - delta-t {printtimedelta(delts)}")
            prevt = d.time[-1]

            # gaps within files
            deltfs = (d.time.values[1:] - d.time.values[:-1]) / np.timedelta64(1, 's')
            #print(deltfs)
            ta = np.ones(len(deltfs)+1, np.float64)
            ta[:-1]=deltfs[:]
            ta[-1]=deltdic[freq+"min"]
            #print(ta)
            tb = xr.DataArray(data=ta,
                              dims=["time"],
                              coords=dict(time=d.time))
            #print(tb)
            tc = xr.where(tb<deltdic[freq+"min"], 1, 0)
            te = xr.where(tb>deltdic[freq+"max"], 1, 0)
            tf = tc + te
            tg = tb.time.where(tf>0, drop=True)
            th = tb.where(tf>0, drop=True)
            #print(tc)
            #print(te)
            #print(tf)
            #print(tg)
            #print(th)
            for tstep in range(0, th.size):
                errors.append(f"    {f}: Irregularity in time axis  - {tg.values[tstep]} delta-t {printtimedelta(th.values[tstep])} from next timestep!")

        if args.no_const_check:
            continue
        # check for constant fields
        # >3D data or 1D data (time, ?, ?, lat, lon or time, lat or time, lon)
        if len(d[d.attrs["variable_id"]].dims)>4 or len(d[var].dims)<3:
            errors.append(f"    {f}: Skipping Constant field check - {len(d[d.attrs['variable_id']].dims)} dimensions not supported.")
        # 3D data (time, height/depth, lat, lon)
        elif len(d[d.attrs["variable_id"]].dims)==4:
            #Test: d[var][5,5,:,:]=3.
            ccmax=getmax(d[d.attrs["variable_id"]])
            ccmin=getmin(d[d.attrs["variable_id"]])
            cc=xr.where(ccmin==ccmax, 1, 0)
            for depthi in range(0, d[d[d.attrs["variable_id"]].dims[1]].size):
                if "depth" in d.dims:
                    cctimes = d.time.where(cc.isel(depth=depthi, drop=True)>0, drop=True)
                    ccvals = ccmax.isel(depth=depthi, drop=True).where(cc.isel(depth=depthi, drop=True)>0, drop=True)
                elif "lev" in d.dims:
                    cctimes = d.time.where(cc.isel(lev=depthi, drop=True)>0, drop=True)
                    ccvals = ccmax.isel(lev=depthi, drop=True).where(cc.isel(lev=depthi, drop=True)>0, drop=True)
                else:
                    errors.append(f"    {f}: Skipping Constant field check for variable with vertical dimension. Only 'lev' and 'depth' supported (given one of {', '.join([dimi for dimi in d.dims])}).")
                    continue
                #print(cctimes.values)
                for tstep in range(0, cctimes.size):
                    errors.append(f"    {f}: Constant field for time {cctimes.values[tstep]} ({ccvals.values[tstep]}) at level {depthi}.")
        # 2D data (time, lat, lon)
        else:
            ccmax=getmax(d[d.attrs["variable_id"]])
            ccmin=getmin(d[d.attrs["variable_id"]])
            cc=xr.where(ccmin==ccmax, 1, 0)
            cctimes = d.time.where(cc>0, drop=True)
            ccvals = ccmax.where(cc>0, drop=True)
            #print(cctimes.values)
            for tstep in range(0, cctimes.size):
                errors.append(f"    {f}: Constant field for time {cctimes.values[tstep]} ({ccvals.values[tstep]}).")

    for e in errors: print(e)
 

# TODO: find all-nan time slices
#def _nan_argminmax_object(func, fill_value, value, axis=None, **kwargs):
#    """In house nanargmin, nanargmax for object arrays. Always return integer
#    type
#    """
#    valid_count = count(value, axis=axis)
#    value = fillna(value, fill_value)
#    data = getattr(np, func)(value, axis=axis, **kwargs)
#
#    # TODO This will evaluate dask arrays and might be costly.
#    if (valid_count == 0).any():
#        raise ValueError("All-NaN slice encountered")
#
#    return data


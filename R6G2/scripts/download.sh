#!/bin/bash
# Crawl and download variables from opendata.dwd.de
# assuming variables are stored as for COSMO-REA6

# URL Base
URL_BASE=https://opendata.dwd.de/climate_environment/REA/COSMO_REA6

# Parent directory for the downloaded files, folder structure of URLs will be craeted below this directory
DL_BASE=/work/bk1261/OcMOD/COSMO-REA6/download
#DL_BASE=/work/kd1292/k204212/OcMOD/download

# OUTDATA Base (where to extract the downloaded files to)
OUTDATA_BASE=/work/bk1261/OcMOD/COSMO-REA6/outdata
#OUTDATA_BASE=/work/kd1292/k204212/OcMOD/outdata

# Grid(s)
GRID="2D 3D"
GRID=2D
GRID=3D

# Frequencies to download
FREQ="monthly daily hourly"
#FREQ=daily
#FREQ=monthly
#FREQ=hourly

# Filter for a single variable
VAR=
#VAR=V
#VAR=U
#VAR=T
#VAR=TKE
#VAR=Q

# When downloading special subfolders of URL_BASE that are not one of FREQ
#  - of format "/SUBFOLDERNAME"
EXTRA="/converted"
EXTRA=

# Filter for a single YYYY / YYYYMM / YYYYMMDD
TIME=
#TIME=201305

#########################################################################################

# Copy / Extract DWD opendata archives / files to destination directory (OUTDATA_BASE/<frequency>/<variable>.<grid>/)
function extract {

  { [[ -z "$1" ]] || [[ -z "$2" ]] || [[ -z "$3" ]] || [[ -z "$4" ]] ; } && { echo "ERROR: Parameter(s) not set in function extract!" ; exit 1 ; }

  l_outpath=$1
  l_var=$(echo $2 | sed "s,/,,g" )
  l_vargrid=${l_var}.$3
  l_freq=$4

  l_dldir=$( dirname $l_outpath )
  l_file=$( basename $l_outpath )

  grib_set=/sw/spack-levante/mambaforge-4.11.0-0-Linux-x86_64-sobz6z/bin/grib_set

  [[ ! -d "$l_dldir" ]] && { echo "ERROR: directory '$l_dldir' does not exist!" ; exit 1 ; }
  [[ ! -e "$l_outpath" ]] && { echo "ERROR: file '$l_outpath' does not exist!" ; exit 1 ; }

  l_f=
  [[ "$l_freq" == "hourly" ]] && {
  IntervalVars=( AUMFL_S AVMFL_S TMIN_2M VGUST_DYN VGUST_CON VABSMX_1M VABSMX_10M HTOP_CON VMAX_1M TMAX_2M VMAX_10M ATHB_T ASWDIFU_S ASOD_T ASOB_T ASHFL_S ALHFL_S ALHFL_PL ALHFL_BS ALB_RAD ATHD_S ATHB_S ASWDIR_S ASWDIFD_S ASOB_S ALWU_S TDIV_HUM AEVAP_S SNOW_GSP SNOW_CON RUNOFF_G RUNOFF_S DURSUN RAIN_GSP RAIN_CON TOT_PRECIP )
  l_f=1hrPt
  [[ " ${IntervalVars[@]} " == *" $l_var "* ]] && l_f=1hr
  }
  [[ "$l_freq" == "daily" ]] && l_f=day
  [[ "$l_freq" == "monthly" ]] && l_f=mon
  [[ -z "$l_f" ]] && { echo "ERROR: The frequency '$l_freq' could not be translated!" ; exit 1; }

  # Create target path
  tmp=tmp/tmp_$(uuid)
  mkdir -p $tmp
  l_targetdir=$OUTDATA_BASE/$l_f/$(echo $l_vargrid | sed "s/_//g" )
  mkdir -p $l_targetdir || { echo "ERROR: Could not create directory '$l_targetdir'!"; exit 1 ; }
  if [[ "$l_file" == *".grb" ]]; then
    \cp -v $l_outpath $tmp/$( echo $l_file | sed "s/_//g" ) || { echo "ERROR: Could not copy file '$l_outpath' to '$l_targetdir'!"; exit 1 ; }
  elif [[ "$l_file" == *".nc4" ]]; then
    \cp -v $l_outpath $tmp/$( echo $l_file | sed "s/_//g" ) || { echo "ERROR: Could not copy file '$l_outpath' to '$l_targetdir'!"; exit 1 ; }
  elif [[ "$l_file" == *".nc" ]]; then
    \cp -v $l_outpath $tmp/$( echo $l_file | sed "s/_//g" ) || { echo "ERROR: Could not copy file '$l_outpath' to '$l_targetdir'!"; exit 1 ; }
  elif [[ "$l_file" == *".grb.bz2" ]]; then
    bzip2 -fckd $l_outpath > $tmp/$( echo $l_file | rev | cut -d "." -f2- | rev | sed "s/_//g" ) || { echo "ERROR: Could not extract file '$l_outpath' to '$l_targetdir/$( echo $l_file | rev | cut -d "." -f2- | rev )'!"; exit 1 ; }
  elif [[ "$l_file" == *".tgz" ]]; then
    tar --transform='flags=r;s|_||g' --overwrite -xvf $l_outpath -C $tmp || { echo "ERROR: Could not extract file '$l_outpath' to '$l_targetdir'!" ; exit 1 ; }
  elif [[ "$l_file" == *".tar.bz2" ]]; then
    tar --transform='flags=r;s|_||g' --overwrite -xvf $l_outpath -C $tmp || { echo "ERROR: Could not extract file '$l_outpath' to '$l_targetdir'!" ; exit 1 ; }
  else
    echo "ERROR: No rule to extract '$l_outpath'!"
    exit 1
  fi

  for l_grbfile in $( find $tmp -type f -name "*.grb" ); do
    l_bn=$(basename $l_grbfile)
    $grib_set -stimeRangeIndicator=0 $l_grbfile $l_targetdir/$l_bn || { echo "ERROR: grib_set failed for $l_grbfile" ; exit 1 ; }
    echo "grib_set -stimeRangeIndicator=0 $l_grbfile $l_targetdir/$l_bn"
  done
  #ls $tmp/*.nc &>/dev/null && mv -v $tmp/*.nc $l_targetdir
  #ls $tmp/*.nc4 &>/dev/null && mv -v $tmp/*.nc4 $l_targetdir
  find $tmp \( -type f -or -type l \) -and \( -name "*.nc" -or -name "*.nc4" \) -exec cp -v {} $l_targetdir \; || echo "ERROR"
  find $tmp \( -type f -or -type l \) -and \( -not -name "*.nc" -and -not -name "*.grb" -and -not -name "*.nc4" \) -exec sh -c 'echo "ERROR: no rule to treat {}"' \;
}


# Search DWD opendata server for desired files and download, eventually call extraction function
for f in $FREQ
do
  for g in $GRID
  do
    # Retrieve all variables for the current settings
    URL=${URL_BASE}${EXTRA}/$f/$g
    if [[ -z "$VAR" ]]
    then
      VARLIST=( $(curl -sL "$URL" | grep "^<a href=" | cut -d "\"" -f 2 ) )
    else
      VARLIST=( $(curl -sL "$URL" | grep "^<a href=" | grep "$VAR" | cut -d "\"" -f 2 ) )
    fi
    [[ -z "$VARLIST" ]] && { echo "ERROR: No variables found under '$URL' with variable filter '$VAR'!" ; continue ; }

    # Retrieve all files for the current variable
    for v in ${VARLIST[@]}
    do
      FILELIST=()
      FILESIZES=()
      if [[ "$g" == "2D" ]]
      then
        vars=( $v )
      elif [[ "$g" == "3D" ]]
      then
        YEARLIST=( $(curl -sL "$URL/$v" | grep "^<a href=" | cut -d "\"" -f 2 ) )
        [[ -z "$YEARLIST" ]] && { echo "ERROR: No subdirectories found under '$URL/$v' with for Grid '3D'!" ; continue ; }
        vars=()
        for yyyy in ${YEARLIST[@]}
        do
          [[ "$yyyy" =~ "^[0-9]+$" ]] && { echo "ERROR: Subdirectories found under '$URL/$v' with for Grid '3D' are not properly named!" ; continue ; }
          vars+=( ${v}${yyyy} )
        done
      else
        echo "ERROR: Illegal grid option '$g'. Has to be '2D' or '3D'!"
        exit 1
      fi
      for vat in ${vars[@]}
      do
        if [[ -z "$TIME" ]]
        then
          FILESIZES+=( $(curl -sL "$URL/$vat" | grep "^<a href=" | cut -d "\"" -f 3 | awk 'BEGIN {FS = "[[:space:]]+"} ; {print $4}' ) )
          if [[ "$g" == "2D" ]]
          then
            FILELIST+=( $(curl -sL "$URL/$vat" | grep "^<a href=" | cut -d "\"" -f 2 ) )
          elif [[ "$g" == "3D" ]]
          then
            FILELIST_tmp=( $(curl -sL "$URL/$vat" | grep "^<a href=" | cut -d "\"" -f 2 ) )
            FILELIST+=( $(printf "${vat: -5}%q "  "${FILELIST_tmp[@]}") )
          fi
        else
          #echo $URL/$vat
          FILESIZES+=( $(curl -sL "$URL/$vat" | grep "^<a href=" | grep ".$TIME" | cut -d "\"" -f 3 | awk 'BEGIN {FS = "[[:space:]]+"} ; {print $4}' ) )
          if [[ "$g" == "2D" ]]
          then
            FILELIST+=( $(curl -sL "$URL/$vat" | grep "^<a href=" | grep ".$TIME" | cut -d "\"" -f 2 ) )
          elif [[ "$g" == "3D" ]]
          then
            FILELIST_tmp=( $(curl -sL "$URL/$vat" | grep "^<a href=" | grep ".$TIME" | cut -d "\"" -f 2 ) )
            [[ ! -z "$FILELIST_tmp" ]] && FILELIST+=( $(printf "${vat: -5}%q "  "${FILELIST_tmp[@]}") )
          fi
        fi
      done
      #echo ${FILESIZES[@]}
      #echo ${#FILESIZES[@]}
      #echo ${#FILELIST[@]}
      #echo ${FILELIST[@]}
      [[ -z "$FILELIST" ]] && { echo "ERROR: No variables found under '$URL/$v' for time filter '$TIME'!" ; continue ; }
      [[ ${#FILELIST[@]} -ne ${#FILESIZES[@]} ]] && { echo "ERROR: Number of read filenames and filesizes does not match for '$URL/$v' for time filter '$TIME'!" ; continue ; }
      echo
      echo "Found ${#FILELIST[@]} files ($(IFS=+; echo "$(((${FILESIZES[*]})/1000/1000 ))") MB ) to download under $URL/$v with time filter '$TIME'. Downloading..."

      # Create Download directory
      outpath=$DL_BASE/$f/$g/$v
      mkdir -p $outpath

      # parallelization settings
      n=0
      maxjobs=6

      for i in $(seq 0 $((${#FILELIST[@]}-1)))
      do
        (
        [[ -z "${FILELIST[$i]}" ]] && { echo "ERROR: Empty filename. Something went wrong with retrieving the filelist from '$URL/$v'!" ; continue ;}
        [[ -z "${FILESIZES[$i]}" ]] && { echo "ERROR: Empty filesize. Something went wrong with retrieving the filelist from '$URL/$v'!" ; continue ;}
        # Download all files, if not present or incomplete
        FILE_URL=$URL/${v}${FILELIST[$i]}
        # check local and remote filesize
        lfl=
        [[ -e $outpath/$( basename ${FILELIST[$i]} ) ]] && lfl=$(wc -c $outpath/$( basename ${FILELIST[$i]} ) | awk '{print $1}')
        rfl=$( curl -sI $FILE_URL | grep "content-length:" | cut -d" " -f2 | tr -d '\n' | tr -d '\r' )
        #echo $FILE_URL
        #echo "'$lfl'"
        #echo "rfl='$rfl'"
        #echo "fs='${FILESIZES[$i]}'"
        #echo "'${FILELIST[$i]}'"
        #[[ "${FILESIZES[$i]}" =~ "^[0-9]+$" ]] && echo "regex"
        #[[ "$rfl" != "${FILESIZES[$i]}" ]] && echo "remote"
        #exit 0
        { [[ "${FILESIZES[$i]}" =~ "^[0-9]+$" ]] || [[ $rfl -ne ${FILESIZES[$i]} ]] ; } && { echo "ERROR: Something went wrong retrieving the remote filesize of file '${FILE_URL}'!" ; continue ; }
        if [[ "$lfl" != "${FILESIZES[$i]}" ]]
        then
          [[ ! -z "$lfl" ]] && {
          echo "Removing partially downloaded file: '$outpath/$( basename ${FILELIST[$i]}) '..."
          rm -f $outpath/$( basename ${FILELIST[$i]} )
          }
        else
          echo "File '${FILE_URL}' already exists locally, skipping download ..."
          extract $outpath/$( basename ${FILELIST[$i]} ) ${v} ${g} $f
          continue
        fi
        wget -q -P $outpath $FILE_URL
        [[ -e $outpath/$( basename ${FILELIST[$i]} ) ]] && lfl=$(wc -c $outpath/$( basename ${FILELIST[$i]} ) | awk '{print $1}')
        if [[ ! -z "$lfl" ]] && [[ "$lfl" != "${FILESIZES[$i]}" ]]
        then
          echo "ERROR: File '${FILE_URL}' could not be downloaded!"
          rm -f $outpath/$( basename ${FILELIST[$i]} )
        elif [[ -z "$lfl" ]]
        then
          echo "ERROR: File '${FILE_URL}' could not be downloaded!"
        else
          echo "File '${FILE_URL}' successfully downloaded."
          extract $outpath/$( basename ${FILELIST[$i]} ) ${v} ${g} $f
        fi
        )&
        if (( $(($((++n)) % $maxjobs)) == 0 )) ; then
          wait # wait until all have finished (not optimal, but most times good enough)
        fi
      done # FILELIST
      wait
    done # VARLIST
  done # GRID
done # FREQ

echo "################"
echo "Double files:"
find $DL_BASE -type f -name "*.1" -name "*.2" -name "*.3"

#curl -sI https://opendata.dwd.de/climate_environment/REA/COSMO_REA6/daily/2D/T_SNOW/T_SNOW.2D.199603.DayMean.grb.bz2 | grep "content-length:" | cut -d" " -f2
#if [[ $1 =~ ^[0-9]+$ ]]


#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hr
cn='evspsbl mrro prsn rsds rsus rsut'
for var in $cn; do
  { (if_requested $member $esmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  ifile=${sdir}/out_diag/1hr_${var}_$period.nc
  echo $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hr 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hr
cn='albs'
find_file "$sdir" "*_*_*_ALBRAD_2D_1hr_${period}.nc" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hr 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hr
cn='hfls'
find_file "$sdir" "*_*_*_ALHFLS_2D_1hr_${period}.nc" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hr 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hr
cn='rlus'
find_file "$sdir" "*_*_*_ALWUS_2D_1hr_${period}.nc" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hr 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hr
cn='hfss'
find_file "$sdir" "*_*_*_ASHFLS_2D_1hr_${period}.nc" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hr 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hr
cn='rsdt'
find_file "$sdir" "*_*_*_ASODT_2D_1hr_${period}.nc" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hr 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hr
cn='rsdsdir'
find_file "$sdir" "*_*_*_ASWDIRS_2D_1hr_${period}.nc" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hr 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hr
cn='rlut'
find_file "$sdir" "*_*_*_ATHBT_2D_1hr_${period}.nc" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hr 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hr
cn='rlds'
find_file "$sdir" "*_*_*_ATHDS_2D_1hr_${period}.nc" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hr 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hr
cn='tauu'
find_file "$sdir" "*_*_*_AUMFLS_2D_1hr_${period}.nc" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hr 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hr
cn='tauv'
find_file "$sdir" "*_*_*_AVMFLS_2D_1hr_${period}.nc" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hr 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hr
cn='sund'
find_file "$sdir" "*_*_*_DURSUN_2D_1hr_${period}.nc" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hr 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hr
cn='prc'
find_file "$sdir" "*_*_*_RAINCON_2D_1hr_${period}.nc" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hr 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hr
cn='prls'
find_file "$sdir" "*_*_*_RAINGSP_2D_1hr_${period}.nc" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hr 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hr
cn='mrros'
find_file "$sdir" "*_*_*_RUNOFFS_2D_1hr_${period}.nc" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hr 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hr
cn='prsnc'
find_file "$sdir" "*_*_*_SNOWCON_2D_1hr_${period}.nc" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hr 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hr
cn='prsnls'
find_file "$sdir" "*_*_*_SNOWGSP_2D_1hr_${period}.nc" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hr 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hr
cn='pr'
find_file "$sdir" "*_*_*_TOTPRECIP_2D_1hr_${period}.nc" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hr 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hr
cn='sfcWindmax'
find_file "$sdir" "*_*_*_VABSMX10M_2D_1hr_${period}.nc" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hr 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hr
cn='wsgscmax'
find_file "$sdir" "*_*_*_VGUSTCON_2D_1hr_${period}.nc" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hr 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hr
cn='wsgstmax'
find_file "$sdir" "*_*_*_VGUSTDYN_2D_1hr_${period}.nc" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hr 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hr
cn='wsgsmax'
find_file "$sdir" "*_*_*_VMAX10M_2D_1hr_${period}.nc" ifile >> $err.find_file.1hr 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hr $var $chunk || continue
  mkdir -p $dr/$submodel/1hr_${var}
  echo $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hr,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hr 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='clwvi hus10 hus100 hus1000 hus150 hus20 hus200 hus250 hus30 hus300 hus400 hus50 hus500 hus6 hus600 hus70 hus700 hus750 hus850 hus925 mrfso mrso pfull6 rlus rsds rsus rsut snc ta10 ta100 ta1000 ta150 ta20 ta200 ta250 ta30 ta300 ta400 ta50 ta500 ta6 ta600 ta70 ta700 ta750 ta850 ta925 tke6 ua10 ua100 ua1000 ua150 ua20 ua200 ua250 ua30 ua300 ua400 ua50 ua500 ua6 ua600 ua70 ua700 ua750 ua850 ua925 va10 va100 va1000 va150 va20 va200 va250 va30 va300 va400 va50 va500 va6 va600 va70 va700 va750 va850 va925 wa10 wa100 wa1000 wa150 wa20 wa200 wa250 wa30 wa300 wa400 wa50 wa500 wa6 wa600 wa70 wa700 wa750 wa850 wa925'
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  ifile=${sdir}/out_diag/1hrPt_${var}_$period.nc
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='clh'
find_file "$sdir" "*_*_*_CLCH_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='cll'
find_file "$sdir" "*_*_*_CLCL_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='clm'
find_file "$sdir" "*_*_*_CLCM_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='clt'
find_file "$sdir" "*_*_*_CLCT_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='ad40m'
find_file "$sdir" "*_*_*_DENS040m_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='ad60m'
find_file "$sdir" "*_*_*_DENS060m_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='ad80m'
find_file "$sdir" "*_*_*_DENS080m_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='ad100m'
find_file "$sdir" "*_*_*_DENS100m_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='ad125m'
find_file "$sdir" "*_*_*_DENS125m_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='ad150m'
find_file "$sdir" "*_*_*_DENS150m_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='ad175m'
find_file "$sdir" "*_*_*_DENS175m_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='ad200m'
find_file "$sdir" "*_*_*_DENS200m_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='zmla'
find_file "$sdir" "*_*_*_HPBL_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='snd'
find_file "$sdir" "*_*_*_HSNOW_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='zf'
find_file "$sdir" "*_*_*_HZEROCL_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='hfls'
find_file "$sdir" "*_*_*_LHFLS_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='psl'
find_file "$sdir" "*_*_*_PMSL_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='ps'
find_file "$sdir" "*_*_*_PS_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='pfull'
find_file "$sdir" "*_*_*_P_3D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='huss'
find_file "$sdir" "*_*_*_QV2M_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='hus'
find_file "$sdir" "*_*_*_QV_3D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='hurs'
find_file "$sdir" "*_*_*_RELHUM2M_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='snden'
find_file "$sdir" "*_*_*_RHOSNOW_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='hfss'
find_file "$sdir" "*_*_*_SHFLS_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='rsdt'
find_file "$sdir" "*_*_*_SODTRAD_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='rsdsdir'
find_file "$sdir" "*_*_*_SWDIRSRAD_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='tas'
find_file "$sdir" "*_*_*_T2M_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='tdps'
find_file "$sdir" "*_*_*_TD2M_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='ts'
find_file "$sdir" "*_*_*_TG_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='rlut'
find_file "$sdir" "*_*_*_THBTRAD_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='rlds'
find_file "$sdir" "*_*_*_THDSRAD_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='tke'
find_file "$sdir" "*_*_*_TKE_3D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='clivi'
find_file "$sdir" "*_*_*_TQI_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='prw'
find_file "$sdir" "*_*_*_TQV_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='tsl'
find_file "$sdir" "*_*_*_TSOIL_3D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='twp'
find_file "$sdir" "*_*_*_TWATER_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='ta'
find_file "$sdir" "*_*_*_T_3D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='tauu'
find_file "$sdir" "*_*_*_UMFLS_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='ua'
find_file "$sdir" "*_*_*_U_3D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='tauv'
find_file "$sdir" "*_*_*_VMFLS_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='va'
find_file "$sdir" "*_*_*_V_3D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='wd10m'
find_file "$sdir" "*_*_*_WD010m_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='wd40m'
find_file "$sdir" "*_*_*_WD040m_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='wd60m'
find_file "$sdir" "*_*_*_WD060m_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='wd80m'
find_file "$sdir" "*_*_*_WD080m_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='wd100m'
find_file "$sdir" "*_*_*_WD100m_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='wd125m'
find_file "$sdir" "*_*_*_WD125m_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='wd150m'
find_file "$sdir" "*_*_*_WD150m_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='wd175m'
find_file "$sdir" "*_*_*_WD175m_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='wd200m'
find_file "$sdir" "*_*_*_WD200m_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='sfcWind'
find_file "$sdir" "*_*_*_WS010m_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='ws40m'
find_file "$sdir" "*_*_*_WS040m_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='ws60m'
find_file "$sdir" "*_*_*_WS060m_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='ws80m'
find_file "$sdir" "*_*_*_WS080m_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='ws100m'
find_file "$sdir" "*_*_*_WS100m_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='ws125m'
find_file "$sdir" "*_*_*_WS125m_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='ws150m'
find_file "$sdir" "*_*_*_WS150m_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='ws175m'
find_file "$sdir" "*_*_*_WS175m_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='ws200m'
find_file "$sdir" "*_*_*_WS200m_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='snw'
find_file "$sdir" "*_*_*_WSNOW_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='mrfsol'
find_file "$sdir" "*_*_*_WSOILICE_3D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='mrsol'
find_file "$sdir" "*_*_*_WSOIL_3D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='wa'
find_file "$sdir" "*_*_*_W_3D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 1hrPt
cn='uas vas'
find_file "$sdir" "*_UV10M_2D_1hrPt_${period}.nc" ifile >> $err.find_file.1hrPt 2>&1
for var in $cn; do
  { (if_requested $member $esmod 1hrPt $var $chunk || continue
  mkdir -p $dr/$submodel/1hrPt_${var}
  echo $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,1hrPt,om=r,i=$it,mt=$mt,dr=$dr/$submodel/1hrPt_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.1hrPt 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 6hr
cn='tasmax'
find_file "$sdir" "*_*_*_TMAX2M_2D_6hr_${period}.nc" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $esmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.6hr 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) 6hr
cn='tasmin'
find_file "$sdir" "*_*_*_TMIN2M_2D_6hr_${period}.nc" ifile >> $err.find_file.6hr 2>&1
for var in $cn; do
  { (if_requested $member $esmod 6hr $var $chunk || continue
  mkdir -p $dr/$submodel/6hr_${var}
  echo $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,6hr,i=$it,mt=$mt,dr=$dr/$submodel/6hr_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.6hr 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='clwvi evspsbl hus10 hus100 hus1000 hus150 hus20 hus200 hus250 hus30 hus300 hus400 hus50 hus500 hus6 hus600 hus70 hus700 hus750 hus850 hus925 mrfso mrro mrso pfull6 prsn rsds rsus rsut snc ta10 ta100 ta1000 ta150 ta20 ta200 ta250 ta30 ta300 ta400 ta50 ta500 ta6 ta600 ta70 ta700 ta750 ta850 ta925 tke6 ua10 ua100 ua1000 ua150 ua20 ua200 ua250 ua30 ua300 ua400 ua50 ua500 ua6 ua600 ua70 ua700 ua750 ua850 ua925 va10 va100 va1000 va150 va20 va200 va250 va30 va300 va400 va50 va500 va6 va600 va70 va700 va750 va850 va925 wa10 wa100 wa1000 wa150 wa20 wa200 wa250 wa30 wa300 wa400 wa50 wa500 wa6 wa600 wa70 wa700 wa750 wa850 wa925'
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  ifile=${sdir}/out_diag/day_${var}_$period.nc
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='albs'
find_file "$sdir" "*_*_*_ALBRAD_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='hfls'
find_file "$sdir" "*_*_*_ALHFLS_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='rlus'
find_file "$sdir" "*_*_*_ALWUS_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='hfss'
find_file "$sdir" "*_*_*_ASHFLS_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='rsdt'
find_file "$sdir" "*_*_*_ASODT_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='rsdsdir'
find_file "$sdir" "*_*_*_ASWDIRS_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='rlut'
find_file "$sdir" "*_*_*_ATHBT_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='rlds'
find_file "$sdir" "*_*_*_ATHDS_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='tauu'
find_file "$sdir" "*_*_*_AUMFLS_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='tauv'
find_file "$sdir" "*_*_*_AVMFLS_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='clh'
find_file "$sdir" "*_*_*_CLCH_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='cll'
find_file "$sdir" "*_*_*_CLCL_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='clm'
find_file "$sdir" "*_*_*_CLCM_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='clt'
find_file "$sdir" "*_*_*_CLCT_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='ad40m'
find_file "$sdir" "*_*_*_DENS040m_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='ad60m'
find_file "$sdir" "*_*_*_DENS060m_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='ad80m'
find_file "$sdir" "*_*_*_DENS080m_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='ad100m'
find_file "$sdir" "*_*_*_DENS100m_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='ad125m'
find_file "$sdir" "*_*_*_DENS125m_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='ad150m'
find_file "$sdir" "*_*_*_DENS150m_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='ad175m'
find_file "$sdir" "*_*_*_DENS175m_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='ad200m'
find_file "$sdir" "*_*_*_DENS200m_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='sund'
find_file "$sdir" "*_*_*_DURSUN_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='zmla'
find_file "$sdir" "*_*_*_HPBL_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='snd'
find_file "$sdir" "*_*_*_HSNOW_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='zf'
find_file "$sdir" "*_*_*_HZEROCL_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='psl'
find_file "$sdir" "*_*_*_PMSL_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='ps'
find_file "$sdir" "*_*_*_PS_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='pfull'
find_file "$sdir" "*_*_*_P_3D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='huss'
find_file "$sdir" "*_*_*_QV2M_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='hus'
find_file "$sdir" "*_*_*_QV_3D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='prc'
find_file "$sdir" "*_*_*_RAINCON_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='prls'
find_file "$sdir" "*_*_*_RAINGSP_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='hurs'
find_file "$sdir" "*_*_*_RELHUM2M_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='snden'
find_file "$sdir" "*_*_*_RHOSNOW_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='mrros'
find_file "$sdir" "*_*_*_RUNOFFS_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='prsnc'
find_file "$sdir" "*_*_*_SNOWCON_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='prsnls'
find_file "$sdir" "*_*_*_SNOWGSP_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='tas'
find_file "$sdir" "*_*_*_T2M_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='tdps'
find_file "$sdir" "*_*_*_TD2M_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='ts'
find_file "$sdir" "*_*_*_TG_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='tke'
find_file "$sdir" "*_*_*_TKE_3D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='tasmax'
find_file "$sdir" "*_*_*_TMAX2M_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='tasmin'
find_file "$sdir" "*_*_*_TMIN2M_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='pr'
find_file "$sdir" "*_*_*_TOTPRECIP_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='clivi'
find_file "$sdir" "*_*_*_TQI_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='prw'
find_file "$sdir" "*_*_*_TQV_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='tsl'
find_file "$sdir" "*_*_*_TSOIL_3D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='twp'
find_file "$sdir" "*_*_*_TWATER_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='ta'
find_file "$sdir" "*_*_*_T_3D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='ua'
find_file "$sdir" "*_*_*_U_3D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='sfcWindmax'
find_file "$sdir" "*_*_*_VABSMX10M_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='wsgscmax'
find_file "$sdir" "*_*_*_VGUSTCON_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='wsgstmax'
find_file "$sdir" "*_*_*_VGUSTDYN_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='wsgsmax'
find_file "$sdir" "*_*_*_VMAX10M_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='va'
find_file "$sdir" "*_*_*_V_3D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='wd10m'
find_file "$sdir" "*_*_*_WD010m_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='wd40m'
find_file "$sdir" "*_*_*_WD040m_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='wd60m'
find_file "$sdir" "*_*_*_WD060m_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='wd80m'
find_file "$sdir" "*_*_*_WD080m_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='wd100m'
find_file "$sdir" "*_*_*_WD100m_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='wd125m'
find_file "$sdir" "*_*_*_WD125m_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='wd150m'
find_file "$sdir" "*_*_*_WD150m_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='wd175m'
find_file "$sdir" "*_*_*_WD175m_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='wd200m'
find_file "$sdir" "*_*_*_WD200m_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='sfcWind'
find_file "$sdir" "*_*_*_WS010m_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='ws40m'
find_file "$sdir" "*_*_*_WS040m_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='ws60m'
find_file "$sdir" "*_*_*_WS060m_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='ws80m'
find_file "$sdir" "*_*_*_WS080m_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='ws100m'
find_file "$sdir" "*_*_*_WS100m_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='ws125m'
find_file "$sdir" "*_*_*_WS125m_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='ws150m'
find_file "$sdir" "*_*_*_WS150m_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='ws175m'
find_file "$sdir" "*_*_*_WS175m_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='ws200m'
find_file "$sdir" "*_*_*_WS200m_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='snw'
find_file "$sdir" "*_*_*_WSNOW_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='mrfsol'
find_file "$sdir" "*_*_*_WSOILICE_3D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='mrsol'
find_file "$sdir" "*_*_*_WSOIL_3D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='wa'
find_file "$sdir" "*_*_*_W_3D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) day
cn='uas vas'
find_file "$sdir" "*_UV10M_2D_day_${period}.nc" ifile >> $err.find_file.day 2>&1
for var in $cn; do
  { (if_requested $member $esmod day $var $chunk || continue
  mkdir -p $dr/$submodel/day_${var}
  echo $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,day,i=$it,mt=$mt,dr=$dr/$submodel/day_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.day 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) fx
cn='sftlf'
for var in $cn; do
  { (if_requested $member $esmod fx $var $chunk || continue
  mkdir -p $dr/$submodel/fx_${var}
  ifile=${sdir}/out_diag/fx_$var.nc
  echo $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.fx 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) fx
cn='areacella'
find_file "$sdir" "*_*_*_AREACELLA_2D_fx.nc" ifile >> $err.find_file.fx 2>&1
for var in $cn; do
  { (if_requested $member $esmod fx $var $chunk || continue
  mkdir -p $dr/$submodel/fx_${var}
  echo $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.fx 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) fx
cn='sftlaf'
find_file "$sdir" "*_*_*_FRLAKE_2D_fx.nc" ifile >> $err.find_file.fx 2>&1
for var in $cn; do
  { (if_requested $member $esmod fx $var $chunk || continue
  mkdir -p $dr/$submodel/fx_${var}
  echo $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.fx 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) fx
cn='orog'
find_file "$sdir" "*_*_*_HSURF_2D_fx.nc" ifile >> $err.find_file.fx 2>&1
for var in $cn; do
  { (if_requested $member $esmod fx $var $chunk || continue
  mkdir -p $dr/$submodel/fx_${var}
  echo $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.fx 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) fx
cn='lai'
find_file "$sdir" "*_*_*_LAI_2D_fx.nc" ifile >> $err.find_file.fx 2>&1
for var in $cn; do
  { (if_requested $member $esmod fx $var $chunk || continue
  mkdir -p $dr/$submodel/fx_${var}
  echo $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.fx 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) fx
cn='rootd'
find_file "$sdir" "*_*_*_ROOTDP_2D_fx.nc" ifile >> $err.find_file.fx 2>&1
for var in $cn; do
  { (if_requested $member $esmod fx $var $chunk || continue
  mkdir -p $dr/$submodel/fx_${var}
  echo $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.fx 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) fx
cn='z0'
find_file "$sdir" "*_*_*_Z0_2D_fx.nc" ifile >> $err.find_file.fx 2>&1
for var in $cn; do
  { (if_requested $member $esmod fx $var $chunk || continue
  mkdir -p $dr/$submodel/fx_${var}
  echo $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.fx 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='clwvi evspsbl hus10 hus100 hus1000 hus150 hus20 hus200 hus250 hus30 hus300 hus400 hus50 hus500 hus6 hus600 hus70 hus700 hus750 hus850 hus925 mrfso mrro mrso pfull6 prsn rsds rsus rsut snc ta10 ta100 ta1000 ta150 ta20 ta200 ta250 ta30 ta300 ta400 ta50 ta500 ta6 ta600 ta70 ta700 ta750 ta850 ta925 tke6 ua10 ua100 ua1000 ua150 ua20 ua200 ua250 ua30 ua300 ua400 ua50 ua500 ua6 ua600 ua70 ua700 ua750 ua850 ua925 va10 va100 va1000 va150 va20 va200 va250 va30 va300 va400 va50 va500 va6 va600 va70 va700 va750 va850 va925 wa10 wa100 wa1000 wa150 wa20 wa200 wa250 wa30 wa300 wa400 wa50 wa500 wa6 wa600 wa70 wa700 wa750 wa850 wa925'
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  ifile=${sdir}/out_diag/mon_${var}_$period.nc
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='albs'
find_file "$sdir" "*_*_*_ALBRAD_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='hfls'
find_file "$sdir" "*_*_*_ALHFLS_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='rlus'
find_file "$sdir" "*_*_*_ALWUS_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='hfss'
find_file "$sdir" "*_*_*_ASHFLS_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='rsdt'
find_file "$sdir" "*_*_*_ASODT_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='rsdsdir'
find_file "$sdir" "*_*_*_ASWDIRS_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='rlut'
find_file "$sdir" "*_*_*_ATHBT_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='rlds'
find_file "$sdir" "*_*_*_ATHDS_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='tauu'
find_file "$sdir" "*_*_*_AUMFLS_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='tauv'
find_file "$sdir" "*_*_*_AVMFLS_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='clh'
find_file "$sdir" "*_*_*_CLCH_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='cll'
find_file "$sdir" "*_*_*_CLCL_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='clm'
find_file "$sdir" "*_*_*_CLCM_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='clt'
find_file "$sdir" "*_*_*_CLCT_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='ad40m'
find_file "$sdir" "*_*_*_DENS040m_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='ad60m'
find_file "$sdir" "*_*_*_DENS060m_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='ad80m'
find_file "$sdir" "*_*_*_DENS080m_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='ad100m'
find_file "$sdir" "*_*_*_DENS100m_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='ad125m'
find_file "$sdir" "*_*_*_DENS125m_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='ad150m'
find_file "$sdir" "*_*_*_DENS150m_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='ad175m'
find_file "$sdir" "*_*_*_DENS175m_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='ad200m'
find_file "$sdir" "*_*_*_DENS200m_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='sund'
find_file "$sdir" "*_*_*_DURSUN_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='zmla'
find_file "$sdir" "*_*_*_HPBL_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='snd'
find_file "$sdir" "*_*_*_HSNOW_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='orog'
find_file "$sdir" "*_*_*_HSURF_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='zf'
find_file "$sdir" "*_*_*_HZEROCL_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='psl'
find_file "$sdir" "*_*_*_PMSL_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='ps'
find_file "$sdir" "*_*_*_PS_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='pfull'
find_file "$sdir" "*_*_*_P_3D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='huss'
find_file "$sdir" "*_*_*_QV2M_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='hus'
find_file "$sdir" "*_*_*_QV_3D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='prc'
find_file "$sdir" "*_*_*_RAINCON_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='prls'
find_file "$sdir" "*_*_*_RAINGSP_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='hurs'
find_file "$sdir" "*_*_*_RELHUM2M_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='snden'
find_file "$sdir" "*_*_*_RHOSNOW_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='mrros'
find_file "$sdir" "*_*_*_RUNOFFS_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='prsnc'
find_file "$sdir" "*_*_*_SNOWCON_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='prsnls'
find_file "$sdir" "*_*_*_SNOWGSP_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='tas'
find_file "$sdir" "*_*_*_T2M_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='tdps'
find_file "$sdir" "*_*_*_TD2M_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='ts'
find_file "$sdir" "*_*_*_TG_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='tke'
find_file "$sdir" "*_*_*_TKE_3D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='tasmax'
find_file "$sdir" "*_*_*_TMAX2M_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='tasmin'
find_file "$sdir" "*_*_*_TMIN2M_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='pr'
find_file "$sdir" "*_*_*_TOTPRECIP_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='clivi'
find_file "$sdir" "*_*_*_TQI_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='prw'
find_file "$sdir" "*_*_*_TQV_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='tsl'
find_file "$sdir" "*_*_*_TSOIL_3D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='twp'
find_file "$sdir" "*_*_*_TWATER_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='ta'
find_file "$sdir" "*_*_*_T_3D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='ua'
find_file "$sdir" "*_*_*_U_3D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='sfcWindmax'
find_file "$sdir" "*_*_*_VABSMX10M_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='wsgscmax'
find_file "$sdir" "*_*_*_VGUSTCON_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='wsgstmax'
find_file "$sdir" "*_*_*_VGUSTDYN_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='wsgsmax'
find_file "$sdir" "*_*_*_VMAX10M_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='va'
find_file "$sdir" "*_*_*_V_3D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='wd10m'
find_file "$sdir" "*_*_*_WD010m_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='wd40m'
find_file "$sdir" "*_*_*_WD040m_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='wd60m'
find_file "$sdir" "*_*_*_WD060m_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='wd80m'
find_file "$sdir" "*_*_*_WD080m_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='wd100m'
find_file "$sdir" "*_*_*_WD100m_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='wd125m'
find_file "$sdir" "*_*_*_WD125m_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='wd150m'
find_file "$sdir" "*_*_*_WD150m_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='wd175m'
find_file "$sdir" "*_*_*_WD175m_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='wd200m'
find_file "$sdir" "*_*_*_WD200m_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='sfcWind'
find_file "$sdir" "*_*_*_WS010m_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='ws40m'
find_file "$sdir" "*_*_*_WS040m_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='ws60m'
find_file "$sdir" "*_*_*_WS060m_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='ws80m'
find_file "$sdir" "*_*_*_WS080m_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='ws100m'
find_file "$sdir" "*_*_*_WS100m_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='ws125m'
find_file "$sdir" "*_*_*_WS125m_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='ws150m'
find_file "$sdir" "*_*_*_WS150m_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='ws175m'
find_file "$sdir" "*_*_*_WS175m_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='ws200m'
find_file "$sdir" "*_*_*_WS200m_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='snw'
find_file "$sdir" "*_*_*_WSNOW_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='mrfsol'
find_file "$sdir" "*_*_*_WSOILICE_3D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='mrsol'
find_file "$sdir" "*_*_*_WSOIL_3D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='wa'
find_file "$sdir" "*_*_*_W_3D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done

#-- CMOR-rewrite for cosmo (ESM: COSMO) mon
cn='uas vas'
find_file "$sdir" "*_UV10M_2D_mon_${period}.nc" ifile >> $err.find_file.mon 2>&1
for var in $cn; do
  { (if_requested $member $esmod mon $var $chunk || continue
  mkdir -p $dr/$submodel/mon_${var}
  echo $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,mon,i=$it,mt=$mt,dr=$dr/$submodel/mon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.mon 2>&1
done


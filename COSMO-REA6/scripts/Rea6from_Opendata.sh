#!/bin/sh/
# Download COSMO-REA6 reanalysis data from open data https://opendata.dwd.de/climate_environment/REA/ and get to miklip-server
# Original script from Jennifer Ostermoeller/ Deutscher Wetterdienst, adapted for CES by Deborah Niermann
# Initial Version: 2018/09/27                                                                                        
#                                            

# For running this script, CDO (Climate Data Operators) is needed, tested with CDO 1.8.2 and NCO 4.6.2
# module load cdo/1.9.0
#
# Remove download_history.log when using for the first time
##############################################################
#################### USER SETTINGS ##########################
#############################################################

WORKDIR=/kp/kp05/dnierman/original/COSMO-REA6

cmor_table=1hr          # select MIP table, where Outputvariable is specified (Amon=atmosphere,Emon, Omon=Ocean,Lmon=Land,SImon=SeaIce, day,1hr)
VAR_LIST="QV_2M" 	#" QV_2M TMAX_2M TMIN_2M T_2M TOT_PRECIP PS PMSL RELHUM_2M U_10M V_10M ASWDIFD_S ASWDIR_S ATHD_S WS_010 VMAX_10M TQV CLCT" if new variables are added, expand mapping_table.txt
#cmor_var=(uas vas)        # () makes an array, call elements with ${cmor_var[0]}
TEMP_RES="monthly"	# named as in opendata (hourly, daily, monthly)		
DIMENSION="2D"          # 3D variables are Q,T,TKE,U,V
CDO_MODE="-s -f nc4 -z zip"		#silent output and compression

START_YEAR=1995		#data is stored as monthly time series for each variable
START_MONTH=01		#add leading 0!

END_YEAR=2018
END_MONTH=12
ON_OFF="ON" # if OFF only yearly merge is done, be careful of filenames then

# Parallel computing
NTASK=2 # allows for yearly parallel computing (NTASK= number years)
let imulti=0

###########################################################
######### CES SETTINGS and CMOR OUTPUTDIR AREA ###########
############################## ############################

product=reanalysis
institute=DWD
model=COSMO
experiment=REA6
if [[ ${TEMP_RES} = "hourly" ]]; then
    time_frequency=1hr
elif [[ ${TEMP_RES} = "daily" ]]; then
    time_frequency=day
else
    time_frequency=mon
fi
realm=atmos
ensemble=r1i1p1
#project="reanalysis"


USER_OUTPUTDIR=/kp/kp05/dnierman/STRUCTURE4LINK

#################################################################
################### logfiles #################################
###############################################################

touch ${WORKDIR}/missing_files.log			#logs all missing files
touch ${WORKDIR}/download_history.log			#logs all files successfully downloaded
echo $(date) >> ${WORKDIR}/missing_files.log
echo $(date) >> ${WORKDIR}/download_history.log

################################################################
######## create date array from start to end date ##############
###############################################################

declare -a date_array=" "

start_date="${START_YEAR}"-"${START_MONTH}"-"01"
end_date="${END_YEAR}"-"${END_MONTH}"-"01"

m="$start_date"

while [ "$m" != "${end_date}" ]
do
	newform=$(date -d "$m" +%Y%m)
	date_array+=("$newform")
	m=$(date -I -d "$m + 1 month")
done

last_date=$(date -d "$m" +%Y%m)
date_array+=("${last_date}")

####################### CMOR names ############################
########### save all CMOR names of VAR_LIST into one array

declare -a cmor_names_array=" "

###############################################################

export IGNORE_ATT_COORDINATES=1

##############################################################
########### download of constants from CDC ###################
########### retrieve lon/lat information (as it is not part of the nc files)

if [[ ! -d ${WORKDIR}/constants ]]
then

	mkdir -p ${WORKDIR}/constants
	cd ${WORKDIR}/constants

	echo "Download of constants."

	WGET_CONST='wget --secure-protocol=TLSv1_2 -q --recursive -nH --cut-dirs=4  --accept bz2 https://opendata.dwd.de/climate_environment/REA/COSMO_REA6/constant/COSMO_REA6_CONST_withOUTsponge.nc.bz2'
	`$WGET_CONST`
	bunzip2 COSMO_REA6_CONST_withOUTsponge.nc.bz2

	cdo ${CDO_MODE} copy COSMO_REA6_CONST_withOUTsponge.nc COSMO_REA6_CONST_withOUTsponge_new.nc 		#convert to nc4 for uniform file format
	ncks -v RLAT,RLON,rlat,rlon COSMO_REA6_CONST_withOUTsponge_new.nc latlon_tmp.nc				#extract coordinates
	ncap2 -s 'rlat=float(rlat); rlon=float(rlon)' latlon_tmp.nc latlon.nc					#convert to float for uniform data format (cannot be combined otherwise)
	ncrename -h -v RLAT,lat latlon.nc
	ncrename -h -v RLON,lon latlon.nc
	ncatted -O -a standard_name,lat,c,c,"latitude" latlon.nc
	ncatted -O -a standard_name,lon,c,c,"longitude" latlon.nc	

	echo "COSMO_REA6_CONST_withOUTsponge.nc" >> ${WORKDIR}/download_history.log

	rm latlon_tmp.nc COSMO_REA6_CONST_withOUTsponge_new.nc
fi
###################################################################
########### download of climate data from CDC#######################
####################################################################

for VAR in $VAR_LIST 
do 
if [[ ${ON_OFF} = "ON" ]]; then 

    if [ ! -d ${WORKDIR}/${VAR}/${TEMP_RES} ]; then
	mkdir -p ${WORKDIR}/${VAR}/${TEMP_RES}
    fi

    cd ${WORKDIR}/${VAR}/${TEMP_RES}
	
    if [[ ${TEMP_RES} = 'daily'  ||  ${TEMP_RES} = 'monthly' ]]; then
	    
	if [[ ${TEMP_RES} = 'daily' ]]; then
	    tempres='.Day'
	else	
      	    tempres='.Mon'
	fi

	case ${VAR} in

	    "TMAX_2M" )
		data_aggregation=${tempres}"Max."
		;;
	    "VMAX_10M" )
		data_aggregation=${tempres}"Max."
		;;
	    "TMIN_2M")
		data_aggregation=${tempres}"Min."
		;;
	    "TOT_PRECIP")
		data_aggregation=${tempres}"Sum."
		;;
	    *)	
		data_aggregation=${tempres}"Mean."
		;;
	esac
    else
	data_aggregation='.'	      
    fi
    echo "Download climate data..."${data_aggragtion}

    for date in ${date_array[@]}
    do	
	if [[ ${TEMP_RES} = 'hourly' ]]; then
	    if [[
 ${VAR} = "WS_010" ]]; then 
		file_name=${VAR}"m."${DIMENSION}"."${date}${data_aggregation}"nc.bz2"
	    elif [[ ${VAR} = "WS_100" ]]; then
		file_name=${VAR}"m."${DIMENSION}"."${date}${data_aggregation}"nc4"
	    else
		file_name=${VAR}"."${DIMENSION}"."${date}${data_aggregation}"grb.bz2"
	    fi
	elif [[ ${TEMP_RES} = 'daily' ]]; then
	    if [[ ${VAR} = "WS_010" ]]; then 
		file_name=${VAR}"m."${DIMENSION}"."${date}${data_aggregation}"nc"
	    elif [[ ${VAR} = "WS_100" ]]; then
		file_name=${VAR}"m."${DIMENSION}"."${date}${data_aggregation}"nc4"
	    else
		file_name=${VAR}"."${DIMENSION}"."${date}${data_aggregation}"grb"  
	    fi	
	else 
	    year="${date:0:4}"
	    if [[ ${VAR} = "WS_010" ]]; then 
		file_name=${VAR}"m.${DIMENSION}."${year}${data_aggregation}"nc"
	    elif [[ ${VAR} = "WS_100" ]]; then
		file_name=${VAR}"m."${DIMENSION}"."${date}${data_aggregation}"nc4"
	    else
		file_name=${VAR}".${DIMENSION}."${year}${data_aggregation}"grb"  
	    fi
	fi
	echo $file_name
    
    # check if file already exists in download history
	if grep -q "${file_name}" ${WORKDIR}/download_history.log; then 

	    echo "File already exists."
	    
	else
	    if [[ ${TEMP_RES} = 'hourly' ]]; then
		if [[ ${VAR} = 'WS_010' ]]; then 
		    WGET_STR='wget --secure-protocol=TLSv1_2 -q -nH --cut-dirs=6  --accept bz2 https://opendata.dwd.de/climate_environment/REA/COSMO_REA6/converted/'$TEMP_RES'/'${DIMENSION}'/'$VAR'/'${VAR}'m.2D.'${date}'.nc.bz2'
		    `$WGET_STR`
		elif [[ ${VAR} = "WS_100" ]]; then
		    WGET_STR='wget --secure-protocol=TLSv1_2 -q -nH --cut-dirs=6 https://opendata.dwd.de/climate_environment/REA/COSMO_REA6/converted/'$TEMP_RES'/'${DIMENSION}'/'$VAR'/'${VAR}'m.2D.'${date}'.nc4'
		    `$WGET_STR`
		else
		    WGET_STR='wget --secure-protocol=TLSv1_2 -q --recursive -nH --cut-dirs=6  --accept bz2 https://opendata.dwd.de/climate_environment/REA/COSMO_REA6/'$TEMP_RES'/'${DIMENSION}'/'$VAR'/'${file_name}
		    `$WGET_STR` 
		fi
	    else
		if [[ ${VAR} = 'WS_010' ]]; then 
		    WGET_STR='wget --secure-protocol=TLSv1_2 -q -nH --cut-dirs=6 https://opendata.dwd.de/climate_environment/REA/COSMO_REA6/converted/'$TEMP_RES'/'${DIMENSION}'/'$VAR'/'${file_name}
		    `$WGET_STR` 
		elif [[ ${VAR} = "WS_100" ]]; then
		    WGET_STR='wget --secure-protocol=TLSv1_2 -q -nH --cut-dirs=6 https://opendata.dwd.de/climate_environment/REA/COSMO_REA6/converted/'$TEMP_RES'/'${DIMENSION}'/'$VAR'/'${VAR}'m.2D.'${date}'.nc4'
		    `$WGET_STR`
		else
		    WGET_STR='wget --secure-protocol=TLSv1_2 -q --recursive -nH --cut-dirs=6 https://opendata.dwd.de/climate_environment/REA/COSMO_REA6/'$TEMP_RES'/'${DIMENSION}'/'$VAR'/'${file_name}
		    `$WGET_STR` 
		fi
	    fi
	    echo "runterladen abgeschlossen"
       
      #if download failed, try again
	    if [[ ! -f "${file_name}" ]]; then
		echo "File "${file_name}" does not exist. Try again..." >> ${WORKDIR}/missing_files.log
		ii=0
		while [[ ! -f "${file_name}" ]]
		do
		    echo ${WGET_STR}
		    `$WGET_STR` 
		    echo ${ii}
		    ii=$[ii + 1]
		    pwd
    		done
		echo "Done." >> ${WORKDIR}/missing_files.log
	    fi
	fi
	echo ${file_name} >> ${WORKDIR}/download_history.log
    done
##############################################################
####################### Bunzip Files #########################
##############################################################
    for date in ${date_array[@]}
    do
	if [[ ${TEMP_RES} = 'hourly' ]]; then
	    if [[ ${VAR} = "WS_010" ]]; then 
		file_name=${VAR}"m."${DIMENSION}"."${date}${data_aggregation}"nc.bz2"
	    elif [[ ${VAR} = "WS_100" ]]; then
		file_name=${VAR}"m."${DIMENSION}"."${date}${data_aggregation}"nc4"
	    else
		file_name=${VAR}"."${DIMENSION}"."${date}${data_aggregation}"grb.bz2"
	    fi
	elif [[ ${TEMP_RES} = 'daily' ]]; then
	    if [[ ${VAR} = "WS_010" || ${VAR} = "WS_100" ]]; then 
		file_name=${VAR}"m."${DIMENSION}"."${date}${data_aggregation}"nc"
	    elif [[ ${VAR} = "WS_100" ]]; then
		file_name=${VAR}"m."${DIMENSION}"."${date}${data_aggregation}"nc4"
	    else
		file_name=${VAR}"."${DIMENSION}"."${date}${data_aggregation}"grb"  
	    fi	
	else 
	    year="${date:0:4}"
	    if [[ ${VAR} = "WS_010" || ${VAR} = "WS_100" ]]; then 
		file_name=${VAR}"m.${DIMENSION}."${year}${data_aggregation}"nc"
	    elif [[ ${VAR} = "WS_100" ]]; then
		file_name=${VAR}"m."${DIMENSION}"."${date}${data_aggregation}"nc4"
	    else
		file_name=${VAR}".${DIMENSION}."${year}${data_aggregation}"grb"  
	    fi
	fi	
	echo "Extract file..."
	if [[ ${TEMP_RES} = 'hourly' ]]; then
  	    nice -n 10 bunzip2 ${file_name} &
	    fname=$(basename ${file_name} .grb.bz2)
	else
	    fname=$(basename ${file_name} .grb)
	fi
	
	f_size_grb=$(ls -lh $(basename ${file_name} .bz2) | awk '{print $5}')
	echo "f_size grib: "${f_size_grb}
	
	let imulti=imulti+1
        if [ $imulti -ge $NTASK ]; then
	    wait
	    let imulti=0
	fi
	
    done # end of monthly loop
    wait
    imulti=0
fi
 ##################################################################################
 #########merge time to yearly file output and rename date DRS element#############
 ##################################################################################
#if [[ ${TEMP_RES} = 'hourly' || ${TEMP_RES} = "daily" ]]; then
    echo "Merge yearly Files"
    cd ${WORKDIR}/${VAR}/${TEMP_RES}
    for i in $(seq $START_YEAR $END_YEAR)
    do
	if [[ ${TEMP_RES} = 'daily'  ||  ${TEMP_RES} = 'hourly' ]]; then
	    if [[ ${VAR} = "WS_010" ]]; then
		files=$(ls ${VAR}m.${DIMENSION}.${i}*${data_aggregation}nc)
	    elif [[ ${VAR} = "WS_100" ]]; then
		files=$(ls ${VAR}m.${DIMENSION}.${i}*${data_aggregation}nc4)
	    else
		files=$(ls ${VAR}.${DIMENSION}.${i}*${data_aggregation}grb)
	    fi
	    echo ${files} &	
       	    #nice -n 10 cdo ${CDO_MODE} copy -setmissval,9999 -mergetime ${WORKDIR}/${VAR}/${TEMP_RES}/${files} ${WORKDIR}/${VAR}/${VAR}.${DIMENSION}.${i}${data_aggregation}nc4 & # cdo adaption should be only in scripts ../cmor/
	    if [[ ${VAR} = "WS_010" || ${VAR} = "WS_100" ]]; then
		nice -n 10 cdo mergetime ${WORKDIR}/${VAR}/${TEMP_RES}/${files} ${WORKDIR}/${VAR}/${VAR}.${DIMENSION}.${i}${data_aggregation}nc &
	    else
		nice -n 10 cdo mergetime ${WORKDIR}/${VAR}/${TEMP_RES}/${files} ${WORKDIR}/${VAR}/${VAR}.${DIMENSION}.${i}${data_aggregation}grb &
	    fi
	    let imulti=imulti+1
	    if [ $imulti -ge $NTASK ]; then # ge=greater equal
		wait
		let imulti=0
	    fi
	else # tempres==monthly
	    if [[ ${VAR} = "WS_010" || ${VAR} = "WS_100" ]]; then
		mv ${WORKDIR}/${VAR}/${TEMP_RES}/${VAR}m.${DIMENSION}.${i}${data_aggregation}nc ${WORKDIR}/${VAR}/${VAR}.${DIMENSION}.${i}${data_aggregation}nc
	    else
		mv ${WORKDIR}/${VAR}/${TEMP_RES}/${VAR}.${DIMENSION}.${i}${data_aggregation}grb ${WORKDIR}/${VAR}/${VAR}.${DIMENSION}.${i}${data_aggregation}grb
	    fi
	    
	fi
    done # loop over years
    wait
    ##count number of files of one year, if eq 12 remove folder containing monthly files
    ##nfiles=$(ls ${WORKDIR}/${VAR}/${TEMP_RES}/ | wc -l)		
    ##if [[ ${nfiles} == 12 ]]; then
    rm -r ${WORKDIR}/${VAR}/${TEMP_RES}/
    ##fi
    
done # end of variable loop




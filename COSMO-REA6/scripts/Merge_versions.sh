#!/bin/bash

drs_root=/work/bk1261/COSMO-REA
new_version=v20230918

cd $drs_root
vars=( $(find $drs_root -maxdepth 11 -mindepth 11 -type d) )


##
## Check?:
## It should find folders à la
##    /work/bk1261/COSMO-REA/reanalysis/EUR-6km/DWD/ECMWF-ERAINT/REA6/r1i1p1f1/COSMO/v1/6hr/atmos/tasmax
##  or
##   ./work/bk1261/COSMO-REA/reanalysis/EUR-6km/DWD/ECMWF-ERAINT/REA6/r1i1p1f1/COSMO/v1/6hr/atmos/tasmax
##
echo ${vars[@]}
echo ${#vars[@]}
exit 0



for var in ${vars[@]}; do
  echo ".. $var"
  cd $drs_root
  cd $var
  mkdir -p $new_version
  mv -v v????????/*.nc $new_version
done



# Delete empty directories afterwards:
find $drs_root -type d -empty -delete

#!/bin/bash
# set -x
########################################                                                                            
#                                                                                                                   
# Script written by                                                                                                 
# Christopher Kadow                                                                                                 
# Institute of Meteorology                                                                                          
# Freie Universität Berlin                                                                                          
# christopher.kadow@met.fu-berlin.de                                                                                
# MiKlip - INTEGRATION                                                                                              
#                                            
########################################
# EDITABLE SCRIPT FOR STANDARDIZING DATA IN PARALLEL MODE
#     A simple script to edit, its NOT working without configuration!
#     BUT it helps to set easily the options you need for your data transformations.
#     Think about your data, what do you have, what will it be!?
# EXAMPLE
#     This example takes YEARLY netCDF files (already split up).
#     Of course you can add one/two lines for splitting up your data. 
#     BUT this is not working in the pipe, so DON'T add just an option.
# PARALLEL    
#     It takes care of parallel processing, pretty fast on the MiKlip Server.
#     The YEARLY data is processed parallel, if you want to process other t-frequencies, edit.
#     For example: take one INPUTFILE with all YEARS, 
#     then use -selyear,xxxx as one of the first CDO_OPTIONS (THAT MEANS LAST OPTIONS!)
# TIP
#     1. If you have a GRIB file, change at the very first moment to netcdf,
#     then everything you do is saved into the netcdf-history
#     For example: cdo -f nc4 -z zip copy gribfile.grb netcdffile.nc
#                  netCDF4 ZIP Format -> takes longer in processing, BUT smaller data!
# NAMING CONVENTIONS
#     The naming conventions depending on obs4mips and ana4mips. Informations you find in
#     /miklip/integration/data4miklip/documentation and obs4mips.llnl.gov:8080/wiki and
#     http://www2-pcmdi.llnl.gov/cmor/documentation/cmor-2-0-users-guide-html
#     The given data and structure in data4miklip can help to name the files and directories. 
########################################


###################################export PATH=$PATH:/sw/rhel6-x64/eccodes/eccodes-2.6.0-gcc64/bin # need eccodes for setting timerangeindicator

###############
#CONFIGAREA
###############
    STARTYEAR=1995
    ENDYEAR=2018
    NTASK=20                                          # How many parallel tasks should be handled

#for ii in {1..1} ; do # loop over ensembles
    
    ##################
    # CMOR OUTPUTDIR AREA
    ################
    variable=huss   # huss clt, hurs, pr, tas, ps, psl, uas, vas, wsmax, rsdsiff, rsdsdir, sfcWind, 100mWind
    product=reanalysis
    institute=DWD
    model=COSMO
    experiment=COSMO-REA6
    time_frequency=mon #1hr, day, mon
    realm=atmos
    ensemble=r1i1p1
    cmor_table=amon   #1hr, day, amon
	
    ##################
    # CMOR VARIABLE ATTRIBUTE AREA
    #################
    #if [[ ${VAR} = "tas" || ${VAR} = "tmax" || ${VAR} = "tmin" ]]; then
    if [[ ${variable} = "pr" ]]; then
	variable_rea=TOT_PRECIP
	long_name="Precipitation"
	standard_name="precipitation_flux"
	units="kg m-2 s-1"
	if [[ ${time_frequency} = "mon" ]]; then
	    units="mm"
	    long_name="Precipitation"
	    standard_name="precipitation_sum"
	fi
	RAWVAR=var61
    elif [[ ${variable} = "huss" ]]; then
         variable_rea=QV_2M
         long_name="Near-Surface Specific Humidity"
         standard_name="specific_humidity"
         units="1"
         RAWVAR=var51                                               # Put in the variable of the file2change
    elif [[ ${variable} = "tas" ]]; then
	variable_rea=T_2M
	long_name="Near-Surface Air Temperature"
	standard_name="air_temperature"
	units="K"
	RAWVAR=var11 
    elif [[ ${variable} = "clt" ]]; then
	variable_rea=CLCT
	long_name="Total Cloud Fraction"
	standard_name="cloud_area_fraction"
	units="%"
	RAWVAR=var71                                                # Put in the variable of the file2change
    elif [[ ${variable} = "ps" ]]; then
	variable_rea=PS
	long_name="Surface Air Pressure"
	standard_name="surface_air_pressure"
	units="Pa"
	RAWVAR=var1                                               # Put in the variable of the file2change
    elif [[ ${variable} = "psl" ]]; then
	variable_rea=PMSL
	long_name="Sea Level Pressure"
	standard_name="air_pressure_at_mean_sea_level"
	units="Pa"
	RAWVAR=var2                                               # Put in the variable of the file2change
    elif [[ ${variable} = "hurs" ]]; then
	variable_rea=RELHUM_2M
	long_name="Near-Surface Relative Humidity"
	standard_name="relative_humidity"
	units="%"
	RAWVAR=var52                                               # Put in the variable of the file2change
    elif [[ ${variable} = "uas" ]]; then
	variable_rea=U_10M
	long_name="Eastward Near-Surface Wind"
	standard_name="eastward_wind"
	units="m s-1"
	RAWVAR=var33
    elif [[ ${variable} = "vas" ]]; then
	variable_rea=V_10M
	long_name="Northward Near-Surface Wind"
	standard_name="northward_wind"
	units="m s-1"
	RAWVAR=var34
    elif [[ ${variable} = "wsmax" ]]; then
	variable_rea=VMAX_10M
	long_name="Maximum Near-Surface Wind Speed"
	standard_name="maximum_wind_speed"
	units="m s-1"
	RAWVAR=var187
    elif [[ ${variable} = "sfcWind" ]]; then
	variable_rea=WS_010
	long_name="Near-Surface Wind Speed "
	standard_name="wind_speed"
	units="m s-1"
	RAWVAR=wind_speed
    elif [[ ${variable} = "100mWind" ]]; then
	variable_rea=WS_100
	long_name="Wind Speed at 100 m "
	standard_name="wind_speed"
	units="m s-1"
	RAWVAR=wind_speed
    elif [[ ${variable} = "rsdsdiff" ]]; then
	variable_rea=ASWDIFD_S
	long_name="Surface Diffuse Downwelling Shortwave Radiation"
	standard_name="surface_diffuse_downwelling_shortwave_flux_in_air"
	units="w m-2"
	RAWVAR=var23
    elif [[ ${variable} = "rsdsdir" ]]; then
	variable_rea=ASWDIR_S
	long_name="Surface Direct Downwelling Shortwave Radiation"
	standard_name="surface_direct_downwelling_shortwave_flux_in_air"
	units="W m-2"
	RAWVAR=var22
    elif [[ ${variable} = "prw" ]]; then
	variable_rea=TQV
	long_name="Water Vapor Path"
	standard_name="atmosphere_water_vapor_ontent"
	units="kg m-2"
	RAWVAR="var54"
    fi


   ###############
    # THIS GOES INTO THE CMOR_OUTPUTDIR AND THE OUTPUTFILE NAME!
    ############################################################
    
    if [[ ${time_frequency} = "day" ]]; then
	timename="Day"
    elif [[ ${time_frequency} = "mon" ]]; then
	timename="Mon"
    fi

    INPUTDIR=/kp/kp05/dnierman/original/COSMO-REA6/${variable_rea}
    if [[ ${time_frequency} = "day" ]]; then
	if [[ ${variable} = "sfcWind" || ${variable} = "100mWind" ]]; then
	    INPUTFILE2=${variable_rea}.2D.xxxx.nc
	else
	    
	    case ${variable_rea} in
		
		"TMAX_2M" )
		    data_aggregation="Max."
		    ;;
		"VMAX_10M" )
		    data_aggregation="Max."
		    ;;
		"TMIN_2M")
		    data_aggregation="Min."
		    ;;
		"TOT_PRECIP")
		    data_aggregation="Sum."
		    ;;
		*)	
		    data_aggregation="Mean."
		    ;;
	    esac
	    INPUTFILE=${variable_rea}.2D.xxxx.${timename}${data_aggregation}grb        # THE xxxx will be replaced by the year
	    INPUTFILE2=${variable_rea}.2D.xxxx2.grb        # for using ncwa options (removing height dimension)input+output file is needed
	fi

    elif [[ ${time_frequency} = "mon" ]]; then
		    
	    case ${variable_rea} in
		
		"TMAX_2M" )
		    data_aggregation="Max."
		    ;;
		"VMAX_10M" )
		    data_aggregation="Max."
		    ;;
		"TMIN_2M")
		    data_aggregation="Min."
		    ;;
		"TOT_PRECIP")
		    data_aggregation="Sum."
		    ;;
		*)	
		    data_aggregation="Mean."
		    ;;
	    esac
	    INPUTFILE=${variable_rea}.2D.xxxx.${timename}${data_aggregation}grb        # THE xxxx will be replaced by the year
	    INPUTFILE2=${variable_rea}.2D.xxxx2.grb        # for using ncwa options (removing height dimension)input+output file is needed
	    if [[ ${variable} = "sfcWind" || ${variable} = "100mWind" ]]; then
		INPUTFILE2=${variable_rea}.2D.xxxx.${timename}${data_aggregation}nc
	    fi
       
    elif [[ ${time_frequency} = "1hr" ]]; then

	if [[ ${variable} = "sfcWind" || ${variable} = "100mWind" ]]; then
	    INPUTFILE2=${variable_rea}.2D.xxxx.nc
	elif [[ ${variable} = "uas" || ${variable} = "vas" ]]; then
	    INPUTFILE=${variable_rea}.rotated.xxxx.grb
	    INPUTFILE2=${variable_rea}.rotated.xxxx2.grb
	else
	    INPUTFILE=${variable_rea}.2D.xxxx.grb        # THE xxxx will be replaced by the year
	    INPUTFILE2=${variable_rea}.2D.xxxx2.grb       # for setting timerangeindicator with eccodes input and outputfile is needed 
	fi
	
    else
	echo "timefrequency is not daily or hourly or monthly "
    fi

    INPUTFILE3=${variable_rea}.2D.xxxx3.nc   # for using ncwa options (removing height dimension)input+output file is needed     
    USER_OUTPUTDIR=/kp/kp05/dnierman/STRUCTURE4LINK
    CMOR_OUTPUTDIR=${product}/${institute}/${model}/${experiment}/${time_frequency}/${realm}/${variable}/${ensemble}
    OUTPUTDIR=$USER_OUTPUTDIR/$CMOR_OUTPUTDIR
    OUTPUTFILE=${variable}_${cmor_table}_${product}_${experiment}_${ensemble}_xxxx010100-xxxx123123.nc        
    # THE xxxx will automatically be replaced by the year
	
    MISSVAL=1.e+20                                           #
    MISSVAL2=1.0e+20

	
    CMORVAR=${variable}                                          # Put in the CMOR variable name, don't forget standard and longname
	
    CMORLON=rlon
    RAWLON=rlon
    
    CMORLAT=rlat
    RAWLAT=rlat
    
    CMORTIME=time
    RAWTIME=time
    
    #CMORLEV=lev #mlev/plev                                                # prepared for level dimension, see also NCATTED_OPTIONS
    #RAWLEV=lev


    #############
    # CDO PART
    ##########
    # The cdo command is handling the changes in the file. 
    # LAST2FIRST -> Last option before INPUT is the first that starts in CDO! Order matters in piping! 
    # Put in more or less options if you want, but not everything is possible through piping, like splitting!
    declare -a CDO_OPTIONS                                                       # Just to define an array   
    if [[ ${variable} = "100mWind" || ${variable} = "sfcWind" ]]; then
	CDO_OPTIONS+=("-f nc4 -z zip copy")
    else
	CDO_OPTIONS+=("-f nc4 -z zip copy -setgridtype,curvilinear")
    fi
    CDO_OPTIONS+=("-setmisstoc,$MISSVAL")
    if [[ ${variable} = "pr" ]]; then
	if [[ ${time_frequency} = "day" ]]; then
	    CDO_OPTIONS+=("-divc,86400") # unit of precipitation must be changed
	elif [[ ${time_frequency} = "1hr" ]]; then
	    CDO_OPTIONS+=("-divc,3600")
	else
	    echo "timefrequency is not daily or hourly"
	fi
    fi

#    if [[ ${variable} = "uas" || ${variable} = "vas" ]]; then
#	CDO_OPTIONS+=("-setrtomiss,200,20000")
#	CDO_OPTIONS+=("-setrtomiss,-800,-200")
#    else
    CDO_OPTIONS+=("-setctomiss,9999")   # in rea6 missvalues arent defined, this is a problem when changing the values(example temperature in K +273.15) missvalue 9999 in rea6 is 3272.15 then,
                                        #  so first all timesteps with value=9999 are defined as missvalue (nan). Afterwards missvalue can be set to cmor standard
#    fi
#   CDO_OPTIONS+=("-setreftime,$STARTYEAR-01-01,00:00:00")
    #CDO_OPTIONS+=("-invertlat")
    #CDO_OPTIONS+=("-setreftime,1995-01-01,00:00:00,day -shifttime,-1day -shifttime,11hours")

    if [[ ${time_frequency} = "day" ]]; then
    CDO_OPTIONS+=("-setreftime,1995-01-01,00:00:00,day -shifttime,11hours")
    else
    CDO_OPTIONS+=("-setreftime,1995-01-01,00:00:00,day")  # no timeshift needed for hourly data because of timrenageindicator
    fi
    #CDO_OPTIONS+=("FILL_ME, eg with -selyear,xxxx")
    

    # NCO PART
    ##########
    # The ncrename command renames variables or dimensions, put in more if you want! Order is not important!      
    declare -a NCRENAME_OPTIONS                                                  # Just to define an array
    if [[ "$RAWVAR" != "$CMORVAR" ]]; then
	NCRENAME_OPTIONS+=("-v $RAWVAR,$CMORVAR")                                    # Example for changing variable        
    fi
    if [[ "$RAWLON" != "$CMORLON" ]]; then
	NCRENAME_OPTIONS+=("-d $RAWLON,$CMORLON")
    fi
    if [[ "$RAWLON" != "$CMORLON" ]]; then
	NCRENAME_OPTIONS+=("-v $RAWLON,$CMORLON")
    fi
    if [[ "$RAWLAT" != "$CMORLAT" ]]; then
	NCRENAME_OPTIONS+=("-d $RAWLAT,$CMORLAT")
    fi
    if [[ "$RAWLAT" != "$CMORLAT" ]]; then
	NCRENAME_OPTIONS+=("-v $RAWLAT,$CMORLAT")
    fi
    if [[ "$RAWTIME" != "$CMORTIME" ]]; then
	NCRENAME_OPTIONS+=("-d $RAWTIME,$CMORTIME")     
    fi
   # if [[ "$RAWLEV" != "$CMORLEV" ]]; then
   #	NCRENAME_OPTIONS+=("-d $RAWLEV,$CMORLEV")                                   # Example for changing dimension
   # fi
    #NCRENAME_OPTIONS+=("FILL_ME")

# NCKS OPTION, for adding lon and lat variables from constant file
    declare -a NCKS_OPTIONS
    # for new versio the coordinates in non rotated grid are set by setgridtype,curvilinear, but it does not work for "preprocessed variables"
    if [[ ${variable} = "100mWind" || ${variable} = "sfcWind" ]]; then
	NCKS_OPTIONS+=("-h -A -v lon,lat /kp/kp05/dnierman/original/COSMO-REA6/constants/latlon.nc")  # nc kitchen sink: extracted variables from nc file
    fi

# NCWA OPTION, for removing the 4. dimension height
    declare -a NCWA_OPTIONS
    NCWA_OPTIONS+=("-a height")  # remove 4. dimension height, which has only one level for 2D variables
    
  
# The ncatted command adds attributes, put in more if you want! Order is not important!
    declare -a NCATTED_OPTIONS                                                  # Just to define an array
    
    NCATTED_OPTIONS+=("--attribute=long_name,$CMORVAR,o,c,$long_name")          # Set longname from CMOR table
    NCATTED_OPTIONS+=("--attribute=standard_name,$CMORVAR,o,c,$standard_name")  # Set standardname from CMOR table
    NCATTED_OPTIONS+=("--attribute=units,$CMORVAR,o,c,$units")                  # Set units from CMOR table
    NCATTED_OPTIONS+=("--attribute=coordinates,$CMORVAR,o,c,lon lat")         # Set coordinates from CMOR table
    NCATTED_OPTIONS+=("--attribute=missing_value,$CMORVAR,o,f,$MISSVAL2")         # Set missing value # must be f (float) isntead of c (character) otherwise warning when opening with r
    
    NCATTED_OPTIONS+=("--attribute=institute_id,global,o,c,$institute")         # Example for setting a global att
    NCATTED_OPTIONS+=("--attribute=experiment_id,global,o,c,$experiment")       # Example for setting a global att
    NCATTED_OPTIONS+=("--attribute=model_id,global,o,c,$model")                 # Example for setting a global att
    NCATTED_OPTIONS+=("--attribute=product,global,o,c,$product")                # Example for setting a global att
    NCATTED_OPTIONS+=("--attribute=frequency,global,o,c,$time_frequency")       # Example for setting a global att
    NCATTED_OPTIONS+=("--attribute=modeling_realm,global,o,c,$realm")           # Example for setting a global att
    if [[ ${variable} = "uas" || ${variable} = "vas" ]]; then
	NCATTED_OPTIONS+=("--attribute=comment,global,o,c,the windcomponents uas and vas are transformed to the originally backrotated COSMO grid by cdo rotuvb. Now they are valid for the geographical, irregular lonlat grid, but NOT the original rotated, regular lonlat grid.")           # Example for setting a global att
	NCATTED_OPTIONS+=("--attribute=grid_mapping,$CMORVAR,d,c,")  # delete grid_mapping attribute for wind components, bacause they are not related to the rotated grid anymore, after cdo rotuvb
    fi

    NCATTED_OPTIONS+=("--attribute=long_name,$CMORLON,o,c,longitude in rotated pole grid")       # Set longname from CMOR table
    #NCATTED_OPTIONS+=("--attribute=standard_name,$CMORLON,o,c,longitude")  # Set standardname from CMOR table
    NCATTED_OPTIONS+=("--attribute=units,$CMORLON,o,c,degrees_east")

    NCATTED_OPTIONS+=("--attribute=long_name,$CMORLAT,o,c,latitude in rotated pole grid")      # Set longname from CMOR table
    #NCATTED_OPTIONS+=("--attribute=standard_name,$CMORLAT,o,c,latitude")  # Set standardname from CMOR table
    NCATTED_OPTIONS+=("--attribute=units,$CMORLAT,o,c,degrees_north")

    NCATTED_OPTIONS+=("--attribute=long_name,$CMORTIME,o,c,time")      # Set longname from CMOR table
    NCATTED_OPTIONS+=("--attribute=standard_name,$CMORTIME,o,c,time")  # Set standardname from CMOR table

    #NCATTED_OPTIONS+=("--attribute=long_name,$CMORLEV,o,c,pressure")          # Set longname from CMOR table
    #NCATTED_OPTIONS+=("--attribute=standard_name,$CMORLEV,o,c,air_pressure")  # Set standardname from CMOR table
    #NCATTED_OPTIONS+=("--attribute=units,$CMORLEV,o,c,Pa")

    #NCATTED_OPTIONS+=("FILL_ME")


    ################
    # END CONFIGAREA
    ################


    #############
    # LOAD MODULES & MKDIR & SET LOG
    ########################module purge
    #######################module load cdo
    #######################module load nco                                                                # cdo of course is needed
    
    mkdir -p $OUTPUTDIR
    ##################################
    # CODE FOR PARALLEL (NO NEED2TOUCH)
    COMPUTE_SEQ="seq $STARTYEAR $ENDYEAR"
    COMPUTE_XARGS="xargs -n 1 -P $NTASK -Ixxxx"


    ##################################################
    # rotate windcomponents u_10m and v_10m. Therefore both components need to be in the same file
    if [[ ${variable} = "uas" || ${variable} = "vas" ]]; then
	$COMPUTE_SEQ | $COMPUTE_XARGS filename = /kp/kp05/dnierman/original/COSMO-REA6/windcomponents/windcomponents_rotated.xxxx.grb 
	echo "this is the windcomponent file " ${filename}
	if [ ! -f ${filename}  ]; then      
	    $COMPUTE_SEQ | $COMPUTE_XARGS echo cdo merge /kp/kp05/dnierman/original/COSMO-REA6/U_10M/U_10M.2D.xxxx.grb /kp/kp05/dnierman/original/COSMO-REA6/V_10M/V_10M.2D.xxxx.grb /kp/kp05/dnierman/original/COSMO-REA6/windcomponents/windcomponents.xxxx.grb
	    $COMPUTE_SEQ | $COMPUTE_XARGS cdo merge /kp/kp05/dnierman/original/COSMO-REA6/U_10M/U_10M.2D.xxxx.grb /kp/kp05/dnierman/original/COSMO-REA6/V_10M/V_10M.2D.xxxx.grb /kp/kp05/dnierman/original/COSMO-REA6/windcomponents/windcomponents.xxxx.grb
	
	$COMPUTE_SEQ | $COMPUTE_XARGS echo cdo -rotuvb,var33,var34 /kp/kp05/dnierman/original/COSMO-REA6/windcomponents/windcomponents.xxxx.grb /kp/kp05/dnierman/original/COSMO-REA6/windcomponents/windcomponents_rotated.xxxx.grb
	$COMPUTE_SEQ | $COMPUTE_XARGS cdo -rotuvb,var33,var34 /kp/kp05/dnierman/original/COSMO-REA6/windcomponents/windcomponents.xxxx.grb /kp/kp05/dnierman/original/COSMO-REA6/windcomponents/windcomponents_rotated.xxxx.grb
	$COMPUTE_SEQ | $COMPUTE_XARGS echo cdo selname,var33 /kp/kp05/dnierman/original/COSMO-REA6/windcomponents/windcomponents_rotated.xxxx.grb /kp/kp05/dnierman/original/COSMO-REA6/U_10M/U_10M.rotated.xxxx.grb
	$COMPUTE_SEQ | $COMPUTE_XARGS echo cdo selname,var34 /kp/kp05/dnierman/original/COSMO-REA6/windcomponents/windcomponents_rotated.xxxx.grb /kp/kp05/dnierman/original/COSMO-REA6/V_10M/V_10M.rotated.xxxx.grb
	$COMPUTE_SEQ | $COMPUTE_XARGS cdo selname,var33 /kp/kp05/dnierman/original/COSMO-REA6/windcomponents/windcomponents_rotated.xxxx.grb /kp/kp05/dnierman/original/COSMO-REA6/U_10M/U_10M.rotated.xxxx.grb
	$COMPUTE_SEQ | $COMPUTE_XARGS cdo selname,var34 /kp/kp05/dnierman/original/COSMO-REA6/windcomponents/windcomponents_rotated.xxxx.grb /kp/kp05/dnierman/original/COSMO-REA6/V_10M/V_10M.rotated.xxxx.grb
	fi
    fi



    ##if [[ ${variable} = "uas" || ${variable} = "vas" ]]; then
     ##   $COMPUTE_SEQ | $COMPUTE_XARGS  ncatted -hO -a history,global,d,, $INPUTDIR/$INPUTFILE   # delete long history
    ##fi
    if [[ ${variable} != "sfcWind" && ${variable} != "100mWind" ]]; then
	$COMPUTE_SEQ | $COMPUTE_XARGS echo grib_set -stimeRangeIndicator=0 $INPUTDIR/$INPUTFILE $INPUTDIR/$INPUTFILE2
	$COMPUTE_SEQ | $COMPUTE_XARGS grib_set -stimeRangeIndicator=0 $INPUTDIR/$INPUTFILE $INPUTDIR/$INPUTFILE2
        #$COMPUTE_SEQ | $COMPUTE_XARGS mv $INPUTDIR/$INPUTFILE2 $INPUTDIR/$INPUTFILE
    fi
    if [[ ${#CDO_OPTIONS[@]} != 0 ]]; then
	$COMPUTE_SEQ | $COMPUTE_XARGS echo cdo -s ${CDO_OPTIONS[@]} $INPUTDIR/$INPUTFILE2 $INPUTDIR/$INPUTFILE3 # JUST2ECHO
	$COMPUTE_SEQ | $COMPUTE_XARGS cdo -s ${CDO_OPTIONS[@]} $INPUTDIR/$INPUTFILE2 $INPUTDIR/$INPUTFILE3
   fi
##### there is no 4. dimension for tot_precip    # terminated by signal 9. operation needs to many RAM
    if [[ ${#NCWA_OPTIONS[@]} != 0 ]]; then   
	if  [[ ${variable} = "tos" || ${variable} = "hors"  || ${variable} = "wsmax" || ${time_frequency} = "mon" ]]; then
	    $COMPUTE_SEQ | $COMPUTE_XARGS echo ncwa ${NCWA_OPTIONS[@]} $INPUTDIR/$INPUTFILE3 $OUTPUTDIR/$OUTPUTFILE  # JUST2ECHO
	    $COMPUTE_SEQ | $COMPUTE_XARGS ncwa ${NCWA_OPTIONS[@]} $INPUTDIR/$INPUTFILE3 $OUTPUTDIR/$OUTPUTFILE
	    #$COMPUTE_SEQ | $COMPUTE_XARGS rm  $INPUTDIR/$INPUTFILE2
	    #$COMPUTE_SEQ | $COMPUTE_XARGS rm  $INPUTDIR/$INPUTFILE3
	elif [[ ${variable} = "tas" || ${variable} = "huss" || ${variable} = "hurs" || ${variable} = "pr" || ${variable} = "ps" || ${variable} = "psl" || ${variable} = "rsdsdir" || ${variable} = "rsdsdiff"|| ${variable} = "uas" || ${variable} = "vas"|| ${variable} = "wsmax" || ${variable} = "clt" || ${variable} = "prw" || ${variable} = "100mWind" || ${variable} = "sfcWind" ]]; then
	    $COMPUTE_SEQ | $COMPUTE_XARGS echo mv $INPUTDIR/$INPUTFILE3 $OUTPUTDIR/$OUTPUTFILE	    
	    $COMPUTE_SEQ | $COMPUTE_XARGS mv $INPUTDIR/$INPUTFILE3 $OUTPUTDIR/$OUTPUTFILE
	    #$COMPUTE_SEQ | $COMPUTE_XARGS rm  $INPUTDIR/$INPUTFILE2
	else
	    echo "This variable is not integrated yet"
	fi

    fi
    if [[ ${#NCKS_OPTIONS[@]} != 0 ]]; then                                                          # adding lon lat fields
	$COMPUTE_SEQ | $COMPUTE_XARGS echo ncks ${NCKS_OPTIONS[@]} $OUTPUTDIR/$OUTPUTFILE            # JUST2ECHO
	$COMPUTE_SEQ | $COMPUTE_XARGS ncks ${NCKS_OPTIONS[@]} $OUTPUTDIR/$OUTPUTFILE
   fi
   if [[ ${#NCRENAME_OPTIONS[@]} != 0 ]]; then
	$COMPUTE_SEQ | $COMPUTE_XARGS echo ncrename ${NCRENAME_OPTIONS[@]} $OUTPUTDIR/$OUTPUTFILE            # JUST2ECHO
	$COMPUTE_SEQ | $COMPUTE_XARGS ncrename ${NCRENAME_OPTIONS[@]} $OUTPUTDIR/$OUTPUTFILE
   fi
    if [[ ${#NCATTED_OPTIONS[@]} != 0 ]]; then
	$COMPUTE_SEQ | $COMPUTE_XARGS echo ncatted "${NCATTED_OPTIONS[@]}" $OUTPUTDIR/$OUTPUTFILE            # JUST2ECHO
	$COMPUTE_SEQ | $COMPUTE_XARGS ncatted "${NCATTED_OPTIONS[@]}" $OUTPUTDIR/$OUTPUTFILE 
    fi
    

    # the "" are necessary, because of spaces in long_name
    # CLEAR THE ARRAYS 
    unset CDO_OPTIONS NCRENAME_OPTIONS NCATTED_OPTIONS

#done


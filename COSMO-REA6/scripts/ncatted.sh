#!/bin/bash
set -ue

module load nco


# Attributes
dataspecsversion="01.01.00"
CREATION_DATE="2023-10-01T10:00:00Z"
HISTORY="2023-10-01T10:00:00Z ; CMOR rewrote data to be consistent with CF-1.7 and CF standards, the Controlled Vocabulary and Data Reference Syntax are based on CORDEX and CMIP6."
REFERENCES="Reanalysis conducted in cooperation with Hans-Ertel-Centre.\nBollmeyer, C., Keller, J. D., Ohlwein, C., Wahl, S., Crewell, S., Friederichs, P., Hense, A., Keune, J., Kneifel, S., Pscheidt, I., Redl, S., and Steinke, S.: Towards a high-resolution regional reanalysis for the European CORDEX domain, Q. J. R. Meteorol. Soc., 141, 1–15, 2015, https://doi.org/10.1002/qj.2486.\nKaspar, F., Niermann, D., Borsche, M., Fiedler, S., Keller, J., Potthast, R., Rösch, T., Spangehl, T., and Tinz, B.: Regional atmospheric reanalysis activities at Deutscher Wetterdienst: review of evaluation results and application examples with a focus on renewable energy, Adv. Sci. Res., 17, 115–128, https://doi.org/10.5194/asr-17-115-2020, 2020."
LICENSE="COSMO-REA model data produced by DWD is licensed under a Creative Commons Attribution 4.0 International License (CC BY 4.0; https://creativecommons.org/licenses/). Consult \'https://www.wdc-climate.de/ui/project?acronym=COSMO-REA6\' for terms of use governing COSMO-REA output, including citation requirements and proper acknowledgment. The data producers and data providers make no warranty, either express or implied, including, but not limited to, warranties of merchantability and fitness for a particular purpose. All liabilities arising from the supply of the information (including any liability arising in negligence) are excluded to the fullest extent permitted by law."
declare -A TABLE_INFO
# 01.00.00
TABLE_INFO[1hr]="Creation Date:(27 Jan 2023) MD5:509fc5c33e1ece5b4d5def947e355264"
TABLE_INFO[1hrPt]="Creation Date:(27 Jan 2023) MD5:509fc5c33e1ece5b4d5def947e355264"
TABLE_INFO[6hr]="Creation Date:(27 Jan 2023) MD5:509fc5c33e1ece5b4d5def947e355264"
TABLE_INFO[day]="Creation Date:(27 Jan 2023) MD5:509fc5c33e1ece5b4d5def947e355264"
TABLE_INFO[mon]="Creation Date:(27 Jan 2023) MD5:509fc5c33e1ece5b4d5def947e355264"
TABLE_INFO[fx]="Creation Date:(27 Jan 2023) MD5:509fc5c33e1ece5b4d5def947e355264"
# 01.01.00
TABLE_INFO[1hr]="Creation Date:(01 Sep 2023) MD5:da644e15786f7b8daa83bbad28a0d9be"
TABLE_INFO[1hrPt]="Creation Date:(01 Sep 2023) MD5:da644e15786f7b8daa83bbad28a0d9be"
TABLE_INFO[6hr]="Creation Date:(01 Sep 2023) MD5:da644e15786f7b8daa83bbad28a0d9be"
TABLE_INFO[day]="Creation Date:(01 Sep 2023) MD5:da644e15786f7b8daa83bbad28a0d9be"
TABLE_INFO[mon]="Creation Date:(01 Sep 2023) MD5:da644e15786f7b8daa83bbad28a0d9be"
TABLE_INFO[fx]="Creation Date:(01 Sep 2023) MD5:da644e15786f7b8daa83bbad28a0d9be"

echo "------------------------"
echo $CREATION_DATE
echo $HISTORY
echo $REFERENCES
echo $LICENSE
echo ${TABLE_INFO[@]}
echo "-------------------------"

ifolder=/work/bk1261/COSMO-REA

ifiles=($(find $ifolder -type f))
echo ${#ifiles[@]} files found.
i=0
for ifile in ${ifiles[@]}; do
    i=$((i+1))
    echo $i $ifile
    if [[ "$ifile" == *"/fx/"* ]]; then
        table=fx
    else
        table=$(echo $ifile | rev | cut -d '/' -f 1 | rev | cut -d '_' -f 8)
    fi
    memberid=$(echo $ifile | rev | cut -d '/' -f 1 | rev | cut -d '_' -f 5)
    uuid="hdl:21.14103/$(uuidgen)" # PID with CORDEX prefix
    var=$(basename $ifile | cut -d "_" -f 1)

    # Run ncatted
    ### All files:
    ncatted -O -h -a further_info_url,global,d,, -a data_specs_version,global,m,c,"$dataspecsversion" -a table_info,global,m,c,"${TABLE_INFO[$table]}" -a tracking_id,global,m,c,"$uuid" -a references,global,m,c,"$REFERENCES" -a license,global,m,c,"$LICENSE" -a history,global,m,c,"$HISTORY" -a creation_date,global,m,c,"$CREATION_DATE" $ifile || echo "ERROR $ifile"
    if [[ "$var" == "ta" ]] || [[ "$var" == "ua" ]] || [[ "$var" == "va" ]] || [[ "$var" == "wa" ]] || [[ "$var" == "tke" ]] || [[ "$var" == "pfull" ]] || [[ "$var" == "hus" ]]; then
        ### 3D hybrid-height files (add attributes to auxiliary coordinate variable orog)
        echo "Executing additional ncatted command regarding auxiliary variable orog ('surface altitude') comments and metadata."
        ncatted -O -h -a standard_name,orog,c,c,"surface_altitude" -a comment,orog,c,c,"Owing to the experimental nature of this regional reanalysis, two slightly different time-invariant fields were used as surface altitude. One for the period 2007-2012, another for the rest of the experiment (i.e. 1995-2006 and 2013-2019). The surface altitude serves as auxiliary coordinate variable in describing the height based hybrid Gal-Chen coordinate. The surface called 'surface' means the lower boundary of the atmosphere. Altitude is the (geometric) height above the geoid, which is the reference geopotential surface. The geoid is similar to mean sea level." -a cell_methods,orog,c,c,"area: mean" -a cell_measures,orog,c,c,"area: areacella" -a grid_mapping,orog,c,c,"rotated_latitude_longitude" -a coordinates,orog,c,c,"latitude longitude" $ifile || echo "ERROR $ifile"
    fi
done

exit 0

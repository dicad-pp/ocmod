#!/bin/bash
### Batch Queuing System is SLURM
#SBATCH --partition=compute
#SBATCH --nodes=1
#SBATCH --time=10:00:00
#SBATCH --mail-type=FAIL
#SBATCH --account=bm0021
#SBATCH --output=submit-rsync.log
#SBATCH --error=submit-rsync-err.log
#SBATCH --qos=esgf
cd /work/bk1261/OcMOD/COSMO-REA6/archive_3D_ecmwf/40lvl/
find COSMO-REA -mindepth 13 -maxdepth 13 | xargs -n1 -P16 -I% rsync -rPa % /work/kd1292/k204212/OcMOD/DOKU/%

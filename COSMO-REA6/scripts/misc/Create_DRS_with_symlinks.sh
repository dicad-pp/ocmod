#!/bin/bash

dirs=( $(find /work/bk1261/OcMOD/COSMO-REA6/archive_3D_ecmwf/40lvl/COSMO-REA/ -mindepth 12 -type d ) )

for d in ${dirs[@]}; do
  locald=/work/kd1292/k204212/OcMOD/DOKU/COSMO-REA/$( echo $d | awk -F "/COSMO-REA/" '{print $2}' )
  echo "$d -> $locald"
  mkdir -p $locald
  #ln -s $d/*.nc $locald 
done

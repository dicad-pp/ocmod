mkdir -p 6lvl
mkdir -p 40lvl
mkdir -p plvl
mkdir -p orog
cp -rl cmor_*/COSMO/*_{ua,va,wa,ta,hus}??*/COSMO-REA plvl/
cp -rl cmor_*/COSMO/*_{ua,va,wa,ta,hus,pfull}/COSMO-REA 40lvl/
cp -rl cmor_*/COSMO/*_{ua,va,wa,ta,hus,pfull}6/COSMO-REA 6lvl/
cp -rl cmor_*/COSMO/*_tke/COSMO-REA 6lvl/
cp -rl cmor_*/COSMO/*_orog/COSMO-REA orog/

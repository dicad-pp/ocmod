#!/bin/bash
### Batch Queuing System is SLURM
#SBATCH --partition=compute
#SBATCH --nodes=1
#SBATCH --mem=250G
#SBATCH --time=01:00:00
#SBATCH --mail-type=none
#####SBATCH --account=bk1261
#SBATCH --account=bm0021
#SBATCH --qos=esgf
#SBATCH --exclusive
#SBATCH --output=run_fx_%j.log

# Set environment
#   note that OMP environment is only required when the code
#   is run in parallel mode; the proposed binding policy has
#   proven to provide good performances
ulimit -s unlimited
export OMP_STACKSIZE=500M
export OMP_PLACES=sockets
export OMP_PROC_BIND="spread,close,close"

# Start program
#   where control_file contains Fortran namelists
#   controling the execution of the code
rm -f fieldextra.rmode
#/work/bk1261/OcMOD/COSMO-REA6/fieldextra/bin/fieldextra_gnu_opt_omp  fx_controlfile_vertint_COSMO-REA6.nl 
#/work/bk1261/OcMOD/COSMO-REA6/fieldextra/bin/fieldextra_gnu_opt_omp fx_COSMO-REA_destagger_ngeog_HHL.nl
/work/bk1261/OcMOD/COSMO-REA6/fieldextra1420/bin/fieldextra_gnu_opt_omp ./fx.nl
#/work/bk1261/OcMOD/COSMO-REA6/fieldextra/bin/fieldextra_gnu_opt_omp hsurf.nl 
#/work/bk1261/OcMOD/COSMO-REA6/fieldextra/bin/fieldextra_gnu_opt_omp fx_COSMO-REA_vertical_interpolation_destagger_ngeog.nl
#/work/bk1261/OcMOD/COSMO-REA6/fieldextra/bin/fieldextra_gnu_opt_omp fx_COSMO-REA_vertical_interpolation.nl

#COMPULSORY:
#Control file:
#  ASCII file containing Fortran 90 namelists defining program behaviour.
#  (Refered to as "namelist" in the remaining of this document)
#Field dictionaries:
#  ASCII file(s) containing definition of field names and characteristics.
#Data files:
#  collection of files containing input fields, coded in NetCDF (CF convention),
#  GRIB edition 1 or 2, or as BLK_TABLE release 1.8 or higher.
#OPTIONAL:
#ecCodes definition files:
#  set of ASCII files used to code/decode GRIB 2 records.


#! /bin/bash


n=0
maxjobs=8

mkdir testgriddes
mkdir testgriddesdiff
for ifile in $(ls U.3D | sort ); do
(
   a=$(basename $ifile)
   cdo griddes U.3D/$ifile > ./testgriddes/${a}.txt
   diff ./U_a.txt ./testgriddes/${a}.txt > ./testgriddesdiff/${a}.txt
)&
  if (( $(($((++n)) % $maxjobs)) == 0 )) ; then
   wait # wait until all have finished (not optimal, but most times good enough)
   #echo $n wait
  fi
done
wait


echo "incorrect griddes:"
find testgriddesdiff -not -empty -type f | sort
echo done


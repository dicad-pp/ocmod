#-- Diagnostic for cosmo (ESM: COSMO) variable evspsbl / table 1hr
{ (if_requested $member $esmod 1hr evspsbl $chunk && {
  find_file -e            "$sdir" "*_*_*_AEVAPS_2D_1hr_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'evspsbl=-AEVAPS;' \
    $ifile ${sdir}/out_diag/1hr_evspsbl_$period.nc || echo ERROR
}; )&; }>$err.evspsbl.1hr 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable mrro / table 1hr
# Editor Note: Requires calculation of accumulated to average values.
{ (if_requested $member $esmod 1hr mrro $chunk && {
  find_file -e            "$sdir" "*_*_*_RUNOFFG_2D_1hr_${period}.nc" ifile1
  find_file -e            "$sdir" "*_*_*_RUNOFFS_2D_1hr_${period}.nc" ifile2
  $cdo -f nc -O \
    expr,'mrro=RUNOFFS+RUNOFFG;' \
    -merge -selname,RUNOFFG $ifile1 -selname,RUNOFFS $ifile2 \
    ${sdir}/out_diag/1hr_mrro_$period.nc || echo ERROR
}; )&; }>$err.mrro.1hr 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable prsn / table 1hr
# Editor Note: Aggregation required to calculate time interval averages out of accumulated values
{ (if_requested $member $esmod 1hr prsn $chunk && {
  find_file -e            "$sdir" "*_*_*_SNOWCON_2D_1hr_${period}.nc" ifile1
  find_file -e            "$sdir" "*_*_*_SNOWGSP_2D_1hr_${period}.nc" ifile2
  $cdo -f nc -O \
    expr,'prsn=SNOWGSP+SNOWCON;' \
    -merge -selname,SNOWCON $ifile1 -selname,SNOWGSP $ifile2 \
    ${sdir}/out_diag/1hr_prsn_$period.nc || echo ERROR
}; )&; }>$err.prsn.1hr 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable rsds / table 1hr
# Editor Note: Calculated as SWDirect+SWDiffuse
{ (if_requested $member $esmod 1hr rsds $chunk && {
  find_file -e            "$sdir" "*_*_*_ASWDIFDS_2D_1hr_${period}.nc" ifile1
  find_file -e            "$sdir" "*_*_*_ASWDIRS_2D_1hr_${period}.nc" ifile2
  $cdo -f nc -O \
    expr,'rsds=ASWDIRS+ASWDIFDS;' \
    -merge -selname,ASWDIFDS $ifile1 -selname,ASWDIRS $ifile2 \
    ${sdir}/out_diag/1hr_rsds_$period.nc || echo ERROR
}; )&; }>$err.rsds.1hr 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable rsus / table 1hr
# Editor Note: Calculated as NetSW-DirectSW-DiffuseSW
{ (if_requested $member $esmod 1hr rsus $chunk && {
  find_file -e            "$sdir" "*_*_*_ASOBS_2D_1hr_${period}.nc" ifile1
  find_file -e            "$sdir" "*_*_*_ASWDIFDS_2D_1hr_${period}.nc" ifile2
  find_file -e            "$sdir" "*_*_*_ASWDIRS_2D_1hr_${period}.nc" ifile3
  $cdo -f nc -O \
    expr,'rsus=ASOBS-ASWDIRS-ASWDIFDS;' \
    -merge -selname,ASOBS $ifile1 -selname,ASWDIFDS $ifile2 -selname,ASWDIRS $ifile3 \
    ${sdir}/out_diag/1hr_rsus_$period.nc || echo ERROR
}; )&; }>$err.rsus.1hr 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable rsut / table 1hr
# Editor Note: Calculated as NetSW_TOA-DownwardSW_TOA
{ (if_requested $member $esmod 1hr rsut $chunk && {
  find_file -e            "$sdir" "*_*_*_ASOBT_2D_1hr_${period}.nc" ifile1
  find_file -e            "$sdir" "*_*_*_ASODT_2D_1hr_${period}.nc" ifile2
  $cdo -f nc -O \
    expr,'rsut=ASOBT-ASODT;' \
    -merge -selname,ASOBT $ifile1 -selname,ASODT $ifile2 \
    ${sdir}/out_diag/1hr_rsut_$period.nc || echo ERROR
}; )&; }>$err.rsut.1hr 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable clwvi / table 1hrPt
{ (if_requested $member $esmod 1hrPt clwvi $chunk && {
  find_file -e            "$sdir" "*_*_*_TQC_2D_1hrPt_${period}.nc" ifile1
  find_file -e            "$sdir" "*_*_*_TQI_2D_1hrPt_${period}.nc" ifile2
  $cdo -f nc -O \
    expr,'clwvi=TQI+TQC;' \
    -merge -selname,TQC $ifile1 -selname,TQI $ifile2 \
    ${sdir}/out_diag/1hrPt_clwvi_$period.nc || echo ERROR
}; )&; }>$err.clwvi.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus10 / table 1hrPt
{ (if_requested $member $esmod 1hrPt hus10 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus10=sellevel(QV,10);' \
    $ifile ${sdir}/out_diag/1hrPt_hus10_$period.nc || echo ERROR
}; )&; }>$err.hus10.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus100 / table 1hrPt
{ (if_requested $member $esmod 1hrPt hus100 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus100=sellevel(QV,100);' \
    $ifile ${sdir}/out_diag/1hrPt_hus100_$period.nc || echo ERROR
}; )&; }>$err.hus100.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus1000 / table 1hrPt
{ (if_requested $member $esmod 1hrPt hus1000 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus1000=sellevel(QV,1000);' \
    $ifile ${sdir}/out_diag/1hrPt_hus1000_$period.nc || echo ERROR
}; )&; }>$err.hus1000.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus150 / table 1hrPt
{ (if_requested $member $esmod 1hrPt hus150 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus150=sellevel(QV,150);' \
    $ifile ${sdir}/out_diag/1hrPt_hus150_$period.nc || echo ERROR
}; )&; }>$err.hus150.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus20 / table 1hrPt
{ (if_requested $member $esmod 1hrPt hus20 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus20=sellevel(QV,20);' \
    $ifile ${sdir}/out_diag/1hrPt_hus20_$period.nc || echo ERROR
}; )&; }>$err.hus20.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus200 / table 1hrPt
{ (if_requested $member $esmod 1hrPt hus200 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus200=sellevel(QV,200);' \
    $ifile ${sdir}/out_diag/1hrPt_hus200_$period.nc || echo ERROR
}; )&; }>$err.hus200.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus250 / table 1hrPt
{ (if_requested $member $esmod 1hrPt hus250 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus250=sellevel(QV,250);' \
    $ifile ${sdir}/out_diag/1hrPt_hus250_$period.nc || echo ERROR
}; )&; }>$err.hus250.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus30 / table 1hrPt
{ (if_requested $member $esmod 1hrPt hus30 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus30=sellevel(QV,30);' \
    $ifile ${sdir}/out_diag/1hrPt_hus30_$period.nc || echo ERROR
}; )&; }>$err.hus30.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus300 / table 1hrPt
{ (if_requested $member $esmod 1hrPt hus300 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus300=sellevel(QV,300);' \
    $ifile ${sdir}/out_diag/1hrPt_hus300_$period.nc || echo ERROR
}; )&; }>$err.hus300.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus400 / table 1hrPt
{ (if_requested $member $esmod 1hrPt hus400 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus400=sellevel(QV,400);' \
    $ifile ${sdir}/out_diag/1hrPt_hus400_$period.nc || echo ERROR
}; )&; }>$err.hus400.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus50 / table 1hrPt
{ (if_requested $member $esmod 1hrPt hus50 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus50=sellevel(QV,50);' \
    $ifile ${sdir}/out_diag/1hrPt_hus50_$period.nc || echo ERROR
}; )&; }>$err.hus50.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus500 / table 1hrPt
{ (if_requested $member $esmod 1hrPt hus500 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus500=sellevel(QV,500);' \
    $ifile ${sdir}/out_diag/1hrPt_hus500_$period.nc || echo ERROR
}; )&; }>$err.hus500.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus6 / table 1hrPt
{ (if_requested $member $esmod 1hrPt hus6 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3D_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    setzaxis,${ZAXISFILE6} -expr,'hus6=sellevidxrange(QV,35,40);orog=orog;' \
    $ifile ${sdir}/out_diag/1hrPt_hus6_$period.nc || echo ERROR
}; )&; }>$err.hus6.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus600 / table 1hrPt
{ (if_requested $member $esmod 1hrPt hus600 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus600=sellevel(QV,600);' \
    $ifile ${sdir}/out_diag/1hrPt_hus600_$period.nc || echo ERROR
}; )&; }>$err.hus600.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus70 / table 1hrPt
{ (if_requested $member $esmod 1hrPt hus70 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus70=sellevel(QV,70);' \
    $ifile ${sdir}/out_diag/1hrPt_hus70_$period.nc || echo ERROR
}; )&; }>$err.hus70.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus700 / table 1hrPt
{ (if_requested $member $esmod 1hrPt hus700 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus700=sellevel(QV,700);' \
    $ifile ${sdir}/out_diag/1hrPt_hus700_$period.nc || echo ERROR
}; )&; }>$err.hus700.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus750 / table 1hrPt
{ (if_requested $member $esmod 1hrPt hus750 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus750=sellevel(QV,750);' \
    $ifile ${sdir}/out_diag/1hrPt_hus750_$period.nc || echo ERROR
}; )&; }>$err.hus750.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus850 / table 1hrPt
{ (if_requested $member $esmod 1hrPt hus850 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus850=sellevel(QV,850);' \
    $ifile ${sdir}/out_diag/1hrPt_hus850_$period.nc || echo ERROR
}; )&; }>$err.hus850.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus925 / table 1hrPt
{ (if_requested $member $esmod 1hrPt hus925 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus925=sellevel(QV,925);' \
    $ifile ${sdir}/out_diag/1hrPt_hus925_$period.nc || echo ERROR
}; )&; }>$err.hus925.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable mrfso / table 1hrPt
{ (if_requested $member $esmod 1hrPt mrfso $chunk && {
  find_file -e            "$sdir" "*_*_*_WSOILICE_3D_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'mrfso=vertsum(WSOILICE);' \
    $ifile ${sdir}/out_diag/1hrPt_mrfso_$period.nc || echo ERROR
}; )&; }>$err.mrfso.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable mrso / table 1hrPt
{ (if_requested $member $esmod 1hrPt mrso $chunk && {
  find_file -e            "$sdir" "*_*_*_WSOIL_3D_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'mrso=vertsum(WSOIL);' \
    $ifile ${sdir}/out_diag/1hrPt_mrso_$period.nc || echo ERROR
}; )&; }>$err.mrso.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable pfull6 / table 1hrPt
{ (if_requested $member $esmod 1hrPt pfull6 $chunk && {
  find_file -e            "$sdir" "*_*_*_P_3D_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    setzaxis,${ZAXISFILE6} -expr,'pfull6=sellevidxrange(P,35,40);orog=orog;' \
    $ifile ${sdir}/out_diag/1hrPt_pfull6_$period.nc || echo ERROR
}; )&; }>$err.pfull6.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable rlus / table 1hrPt
# Editor Note: LWNetSfc minus LWDownwardSfc
{ (if_requested $member $esmod 1hrPt rlus $chunk && {
  find_file -e            "$sdir" "*_*_*_THBSRAD_2D_1hrPt_${period}.nc" ifile1
  find_file -e            "$sdir" "*_*_*_THDSRAD_2D_1hrPt_${period}.nc" ifile2
  $cdo -f nc -O \
    expr,'rlus=THBSRAD-THDSRAD;' \
    -merge -selname,THBSRAD $ifile1 -selname,THDSRAD $ifile2 \
    ${sdir}/out_diag/1hrPt_rlus_$period.nc || echo ERROR
}; )&; }>$err.rlus.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable rsds / table 1hrPt
# Editor Note: Calculated as SWDirect+SWDiffuse
{ (if_requested $member $esmod 1hrPt rsds $chunk && {
  find_file -e            "$sdir" "*_*_*_SWDIFDSRAD_2D_1hrPt_${period}.nc" ifile1
  find_file -e            "$sdir" "*_*_*_SWDIRSRAD_2D_1hrPt_${period}.nc" ifile2
  $cdo -f nc -O \
    expr,'rsds=SWDIRSRAD+SWDIFDSRAD;' \
    -merge -selname,SWDIFDSRAD $ifile1 -selname,SWDIRSRAD $ifile2 \
    ${sdir}/out_diag/1hrPt_rsds_$period.nc || echo ERROR
}; )&; }>$err.rsds.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable rsus / table 1hrPt
# Editor Note: Calculated as NetSW-DirectSW-DiffuseSW
{ (if_requested $member $esmod 1hrPt rsus $chunk && {
  find_file -e            "$sdir" "*_*_*_SOBSRAD_2D_1hrPt_${period}.nc" ifile1
  find_file -e            "$sdir" "*_*_*_SWDIFDSRAD_2D_1hrPt_${period}.nc" ifile2
  find_file -e            "$sdir" "*_*_*_SWDIRSRAD_2D_1hrPt_${period}.nc" ifile3
  $cdo -f nc -O \
    expr,'rsus=SOBSRAD-SWDIRSRAD-SWDIFDSRAD;' \
    -merge -selname,SOBSRAD $ifile1 -selname,SWDIFDSRAD $ifile2 -selname,SWDIRSRAD $ifile3 \
    ${sdir}/out_diag/1hrPt_rsus_$period.nc || echo ERROR
}; )&; }>$err.rsus.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable rsut / table 1hrPt
# Editor Note: Calculated as NetSW_TOA-DownwardSW_TOA
{ (if_requested $member $esmod 1hrPt rsut $chunk && {
  find_file -e            "$sdir" "*_*_*_SOBTRAD_2D_1hrPt_${period}.nc" ifile1
  find_file -e            "$sdir" "*_*_*_SODTRAD_2D_1hrPt_${period}.nc" ifile2
  $cdo -f nc -O \
    expr,'rsut=SOBTRAD-SODTRAD;' \
    -merge -selname,SOBTRAD $ifile1 -selname,SODTRAD $ifile2 \
    ${sdir}/out_diag/1hrPt_rsut_$period.nc || echo ERROR
}; )&; }>$err.rsut.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable snc / table 1hrPt
# Editor Note: WSNOW needs to be in units m, however at least in this COSMO version it is provided in units kg m-2. Thus the additional division by RHOSNOW or substitution by HSNOW.
{ (if_requested $member $esmod 1hrPt snc $chunk && {
  find_file -e            "$sdir" "*_*_*_HSNOW_2D_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'FRSNOW=max(0.01,min(1.,HSNOW/0.015))*(HSNOW>5e-7);' \
    $ifile ${sdir}/out_diag/1hrPt_snc_$period.nc || echo ERROR
}; )&; }>$err.snc.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta10 / table 1hrPt
{ (if_requested $member $esmod 1hrPt ta10 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta10=sellevel(T,10);' \
    $ifile ${sdir}/out_diag/1hrPt_ta10_$period.nc || echo ERROR
}; )&; }>$err.ta10.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta100 / table 1hrPt
{ (if_requested $member $esmod 1hrPt ta100 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta100=sellevel(T,100);' \
    $ifile ${sdir}/out_diag/1hrPt_ta100_$period.nc || echo ERROR
}; )&; }>$err.ta100.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta1000 / table 1hrPt
{ (if_requested $member $esmod 1hrPt ta1000 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta1000=sellevel(T,1000);' \
    $ifile ${sdir}/out_diag/1hrPt_ta1000_$period.nc || echo ERROR
}; )&; }>$err.ta1000.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta150 / table 1hrPt
{ (if_requested $member $esmod 1hrPt ta150 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta150=sellevel(T,150);' \
    $ifile ${sdir}/out_diag/1hrPt_ta150_$period.nc || echo ERROR
}; )&; }>$err.ta150.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta20 / table 1hrPt
{ (if_requested $member $esmod 1hrPt ta20 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta20=sellevel(T,20);' \
    $ifile ${sdir}/out_diag/1hrPt_ta20_$period.nc || echo ERROR
}; )&; }>$err.ta20.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta200 / table 1hrPt
{ (if_requested $member $esmod 1hrPt ta200 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta200=sellevel(T,200);' \
    $ifile ${sdir}/out_diag/1hrPt_ta200_$period.nc || echo ERROR
}; )&; }>$err.ta200.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta250 / table 1hrPt
{ (if_requested $member $esmod 1hrPt ta250 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta250=sellevel(T,250);' \
    $ifile ${sdir}/out_diag/1hrPt_ta250_$period.nc || echo ERROR
}; )&; }>$err.ta250.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta30 / table 1hrPt
{ (if_requested $member $esmod 1hrPt ta30 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta30=sellevel(T,30);' \
    $ifile ${sdir}/out_diag/1hrPt_ta30_$period.nc || echo ERROR
}; )&; }>$err.ta30.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta300 / table 1hrPt
{ (if_requested $member $esmod 1hrPt ta300 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta300=sellevel(T,300);' \
    $ifile ${sdir}/out_diag/1hrPt_ta300_$period.nc || echo ERROR
}; )&; }>$err.ta300.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta400 / table 1hrPt
{ (if_requested $member $esmod 1hrPt ta400 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta400=sellevel(T,400);' \
    $ifile ${sdir}/out_diag/1hrPt_ta400_$period.nc || echo ERROR
}; )&; }>$err.ta400.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta50 / table 1hrPt
{ (if_requested $member $esmod 1hrPt ta50 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta50=sellevel(T,50);' \
    $ifile ${sdir}/out_diag/1hrPt_ta50_$period.nc || echo ERROR
}; )&; }>$err.ta50.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta500 / table 1hrPt
{ (if_requested $member $esmod 1hrPt ta500 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta500=sellevel(T,500);' \
    $ifile ${sdir}/out_diag/1hrPt_ta500_$period.nc || echo ERROR
}; )&; }>$err.ta500.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta6 / table 1hrPt
{ (if_requested $member $esmod 1hrPt ta6 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3D_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    setzaxis,${ZAXISFILE6} -expr,'ta6=sellevidxrange(T,35,40);orog=orog;' \
    $ifile ${sdir}/out_diag/1hrPt_ta6_$period.nc || echo ERROR
}; )&; }>$err.ta6.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta600 / table 1hrPt
{ (if_requested $member $esmod 1hrPt ta600 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta600=sellevel(T,600);' \
    $ifile ${sdir}/out_diag/1hrPt_ta600_$period.nc || echo ERROR
}; )&; }>$err.ta600.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta70 / table 1hrPt
{ (if_requested $member $esmod 1hrPt ta70 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta70=sellevel(T,70);' \
    $ifile ${sdir}/out_diag/1hrPt_ta70_$period.nc || echo ERROR
}; )&; }>$err.ta70.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta700 / table 1hrPt
{ (if_requested $member $esmod 1hrPt ta700 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta700=sellevel(T,700);' \
    $ifile ${sdir}/out_diag/1hrPt_ta700_$period.nc || echo ERROR
}; )&; }>$err.ta700.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta750 / table 1hrPt
{ (if_requested $member $esmod 1hrPt ta750 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta750=sellevel(T,750);' \
    $ifile ${sdir}/out_diag/1hrPt_ta750_$period.nc || echo ERROR
}; )&; }>$err.ta750.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta850 / table 1hrPt
{ (if_requested $member $esmod 1hrPt ta850 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta850=sellevel(T,850);' \
    $ifile ${sdir}/out_diag/1hrPt_ta850_$period.nc || echo ERROR
}; )&; }>$err.ta850.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta925 / table 1hrPt
{ (if_requested $member $esmod 1hrPt ta925 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta925=sellevel(T,925);' \
    $ifile ${sdir}/out_diag/1hrPt_ta925_$period.nc || echo ERROR
}; )&; }>$err.ta925.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable tke6 / table 1hrPt
{ (if_requested $member $esmod 1hrPt tke6 $chunk && {
  find_file -e            "$sdir" "*_*_*_TKE_3D_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    setzaxis,${ZAXISFILE6} -expr,'tke6=sellevidxrange(TKE,35,40);orog=orog;' \
    $ifile ${sdir}/out_diag/1hrPt_tke6_$period.nc || echo ERROR
}; )&; }>$err.tke6.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua10 / table 1hrPt
{ (if_requested $member $esmod 1hrPt ua10 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua10=sellevel(U,10);' \
    $ifile ${sdir}/out_diag/1hrPt_ua10_$period.nc || echo ERROR
}; )&; }>$err.ua10.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua100 / table 1hrPt
{ (if_requested $member $esmod 1hrPt ua100 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua100=sellevel(U,100);' \
    $ifile ${sdir}/out_diag/1hrPt_ua100_$period.nc || echo ERROR
}; )&; }>$err.ua100.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua1000 / table 1hrPt
{ (if_requested $member $esmod 1hrPt ua1000 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua1000=sellevel(U,1000);' \
    $ifile ${sdir}/out_diag/1hrPt_ua1000_$period.nc || echo ERROR
}; )&; }>$err.ua1000.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua150 / table 1hrPt
{ (if_requested $member $esmod 1hrPt ua150 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua150=sellevel(U,150);' \
    $ifile ${sdir}/out_diag/1hrPt_ua150_$period.nc || echo ERROR
}; )&; }>$err.ua150.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua20 / table 1hrPt
{ (if_requested $member $esmod 1hrPt ua20 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua20=sellevel(U,20);' \
    $ifile ${sdir}/out_diag/1hrPt_ua20_$period.nc || echo ERROR
}; )&; }>$err.ua20.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua200 / table 1hrPt
{ (if_requested $member $esmod 1hrPt ua200 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua200=sellevel(U,200);' \
    $ifile ${sdir}/out_diag/1hrPt_ua200_$period.nc || echo ERROR
}; )&; }>$err.ua200.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua250 / table 1hrPt
{ (if_requested $member $esmod 1hrPt ua250 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua250=sellevel(U,250);' \
    $ifile ${sdir}/out_diag/1hrPt_ua250_$period.nc || echo ERROR
}; )&; }>$err.ua250.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua30 / table 1hrPt
{ (if_requested $member $esmod 1hrPt ua30 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua30=sellevel(U,30);' \
    $ifile ${sdir}/out_diag/1hrPt_ua30_$period.nc || echo ERROR
}; )&; }>$err.ua30.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua300 / table 1hrPt
{ (if_requested $member $esmod 1hrPt ua300 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua300=sellevel(U,300);' \
    $ifile ${sdir}/out_diag/1hrPt_ua300_$period.nc || echo ERROR
}; )&; }>$err.ua300.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua400 / table 1hrPt
{ (if_requested $member $esmod 1hrPt ua400 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua400=sellevel(U,400);' \
    $ifile ${sdir}/out_diag/1hrPt_ua400_$period.nc || echo ERROR
}; )&; }>$err.ua400.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua50 / table 1hrPt
{ (if_requested $member $esmod 1hrPt ua50 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua50=sellevel(U,50);' \
    $ifile ${sdir}/out_diag/1hrPt_ua50_$period.nc || echo ERROR
}; )&; }>$err.ua50.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua500 / table 1hrPt
{ (if_requested $member $esmod 1hrPt ua500 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua500=sellevel(U,500);' \
    $ifile ${sdir}/out_diag/1hrPt_ua500_$period.nc || echo ERROR
}; )&; }>$err.ua500.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua6 / table 1hrPt
{ (if_requested $member $esmod 1hrPt ua6 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3D_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    setzaxis,${ZAXISFILE6} -expr,'ua6=sellevidxrange(U,35,40);orog=orog;' \
    $ifile ${sdir}/out_diag/1hrPt_ua6_$period.nc || echo ERROR
}; )&; }>$err.ua6.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua600 / table 1hrPt
{ (if_requested $member $esmod 1hrPt ua600 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua600=sellevel(U,600);' \
    $ifile ${sdir}/out_diag/1hrPt_ua600_$period.nc || echo ERROR
}; )&; }>$err.ua600.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua70 / table 1hrPt
{ (if_requested $member $esmod 1hrPt ua70 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua70=sellevel(U,70);' \
    $ifile ${sdir}/out_diag/1hrPt_ua70_$period.nc || echo ERROR
}; )&; }>$err.ua70.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua700 / table 1hrPt
{ (if_requested $member $esmod 1hrPt ua700 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua700=sellevel(U,700);' \
    $ifile ${sdir}/out_diag/1hrPt_ua700_$period.nc || echo ERROR
}; )&; }>$err.ua700.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua750 / table 1hrPt
{ (if_requested $member $esmod 1hrPt ua750 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua750=sellevel(U,750);' \
    $ifile ${sdir}/out_diag/1hrPt_ua750_$period.nc || echo ERROR
}; )&; }>$err.ua750.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua850 / table 1hrPt
{ (if_requested $member $esmod 1hrPt ua850 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua850=sellevel(U,850);' \
    $ifile ${sdir}/out_diag/1hrPt_ua850_$period.nc || echo ERROR
}; )&; }>$err.ua850.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua925 / table 1hrPt
{ (if_requested $member $esmod 1hrPt ua925 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua925=sellevel(U,925);' \
    $ifile ${sdir}/out_diag/1hrPt_ua925_$period.nc || echo ERROR
}; )&; }>$err.ua925.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va10 / table 1hrPt
{ (if_requested $member $esmod 1hrPt va10 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va10=sellevel(V,10);' \
    $ifile ${sdir}/out_diag/1hrPt_va10_$period.nc || echo ERROR
}; )&; }>$err.va10.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va100 / table 1hrPt
{ (if_requested $member $esmod 1hrPt va100 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va100=sellevel(V,100);' \
    $ifile ${sdir}/out_diag/1hrPt_va100_$period.nc || echo ERROR
}; )&; }>$err.va100.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va1000 / table 1hrPt
{ (if_requested $member $esmod 1hrPt va1000 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va1000=sellevel(V,1000);' \
    $ifile ${sdir}/out_diag/1hrPt_va1000_$period.nc || echo ERROR
}; )&; }>$err.va1000.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va150 / table 1hrPt
{ (if_requested $member $esmod 1hrPt va150 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va150=sellevel(V,150);' \
    $ifile ${sdir}/out_diag/1hrPt_va150_$period.nc || echo ERROR
}; )&; }>$err.va150.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va20 / table 1hrPt
{ (if_requested $member $esmod 1hrPt va20 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va20=sellevel(V,20);' \
    $ifile ${sdir}/out_diag/1hrPt_va20_$period.nc || echo ERROR
}; )&; }>$err.va20.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va200 / table 1hrPt
{ (if_requested $member $esmod 1hrPt va200 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va200=sellevel(V,200);' \
    $ifile ${sdir}/out_diag/1hrPt_va200_$period.nc || echo ERROR
}; )&; }>$err.va200.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va250 / table 1hrPt
{ (if_requested $member $esmod 1hrPt va250 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va250=sellevel(V,250);' \
    $ifile ${sdir}/out_diag/1hrPt_va250_$period.nc || echo ERROR
}; )&; }>$err.va250.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va30 / table 1hrPt
{ (if_requested $member $esmod 1hrPt va30 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va30=sellevel(V,30);' \
    $ifile ${sdir}/out_diag/1hrPt_va30_$period.nc || echo ERROR
}; )&; }>$err.va30.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va300 / table 1hrPt
{ (if_requested $member $esmod 1hrPt va300 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va300=sellevel(V,300);' \
    $ifile ${sdir}/out_diag/1hrPt_va300_$period.nc || echo ERROR
}; )&; }>$err.va300.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va400 / table 1hrPt
{ (if_requested $member $esmod 1hrPt va400 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va400=sellevel(V,400);' \
    $ifile ${sdir}/out_diag/1hrPt_va400_$period.nc || echo ERROR
}; )&; }>$err.va400.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va50 / table 1hrPt
{ (if_requested $member $esmod 1hrPt va50 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va50=sellevel(V,50);' \
    $ifile ${sdir}/out_diag/1hrPt_va50_$period.nc || echo ERROR
}; )&; }>$err.va50.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va500 / table 1hrPt
{ (if_requested $member $esmod 1hrPt va500 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va500=sellevel(V,500);' \
    $ifile ${sdir}/out_diag/1hrPt_va500_$period.nc || echo ERROR
}; )&; }>$err.va500.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va6 / table 1hrPt
{ (if_requested $member $esmod 1hrPt va6 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3D_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    setzaxis,${ZAXISFILE6} -expr,'va6=sellevidxrange(V,35,40);orog=orog;' \
    $ifile ${sdir}/out_diag/1hrPt_va6_$period.nc || echo ERROR
}; )&; }>$err.va6.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va600 / table 1hrPt
{ (if_requested $member $esmod 1hrPt va600 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va600=sellevel(V,600);' \
    $ifile ${sdir}/out_diag/1hrPt_va600_$period.nc || echo ERROR
}; )&; }>$err.va600.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va70 / table 1hrPt
{ (if_requested $member $esmod 1hrPt va70 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va70=sellevel(V,70);' \
    $ifile ${sdir}/out_diag/1hrPt_va70_$period.nc || echo ERROR
}; )&; }>$err.va70.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va700 / table 1hrPt
{ (if_requested $member $esmod 1hrPt va700 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va700=sellevel(V,700);' \
    $ifile ${sdir}/out_diag/1hrPt_va700_$period.nc || echo ERROR
}; )&; }>$err.va700.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va750 / table 1hrPt
{ (if_requested $member $esmod 1hrPt va750 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va750=sellevel(V,750);' \
    $ifile ${sdir}/out_diag/1hrPt_va750_$period.nc || echo ERROR
}; )&; }>$err.va750.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va850 / table 1hrPt
{ (if_requested $member $esmod 1hrPt va850 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va850=sellevel(V,850);' \
    $ifile ${sdir}/out_diag/1hrPt_va850_$period.nc || echo ERROR
}; )&; }>$err.va850.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va925 / table 1hrPt
{ (if_requested $member $esmod 1hrPt va925 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va925=sellevel(V,925);' \
    $ifile ${sdir}/out_diag/1hrPt_va925_$period.nc || echo ERROR
}; )&; }>$err.va925.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa10 / table 1hrPt
{ (if_requested $member $esmod 1hrPt wa10 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa10=sellevel(W,10);' \
    $ifile ${sdir}/out_diag/1hrPt_wa10_$period.nc || echo ERROR
}; )&; }>$err.wa10.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa100 / table 1hrPt
{ (if_requested $member $esmod 1hrPt wa100 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa100=sellevel(W,100);' \
    $ifile ${sdir}/out_diag/1hrPt_wa100_$period.nc || echo ERROR
}; )&; }>$err.wa100.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa1000 / table 1hrPt
{ (if_requested $member $esmod 1hrPt wa1000 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa1000=sellevel(W,1000);' \
    $ifile ${sdir}/out_diag/1hrPt_wa1000_$period.nc || echo ERROR
}; )&; }>$err.wa1000.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa150 / table 1hrPt
{ (if_requested $member $esmod 1hrPt wa150 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa150=sellevel(W,150);' \
    $ifile ${sdir}/out_diag/1hrPt_wa150_$period.nc || echo ERROR
}; )&; }>$err.wa150.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa20 / table 1hrPt
{ (if_requested $member $esmod 1hrPt wa20 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa20=sellevel(W,20);' \
    $ifile ${sdir}/out_diag/1hrPt_wa20_$period.nc || echo ERROR
}; )&; }>$err.wa20.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa200 / table 1hrPt
{ (if_requested $member $esmod 1hrPt wa200 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa200=sellevel(W,200);' \
    $ifile ${sdir}/out_diag/1hrPt_wa200_$period.nc || echo ERROR
}; )&; }>$err.wa200.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa250 / table 1hrPt
{ (if_requested $member $esmod 1hrPt wa250 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa250=sellevel(W,250);' \
    $ifile ${sdir}/out_diag/1hrPt_wa250_$period.nc || echo ERROR
}; )&; }>$err.wa250.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa30 / table 1hrPt
{ (if_requested $member $esmod 1hrPt wa30 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa30=sellevel(W,30);' \
    $ifile ${sdir}/out_diag/1hrPt_wa30_$period.nc || echo ERROR
}; )&; }>$err.wa30.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa300 / table 1hrPt
{ (if_requested $member $esmod 1hrPt wa300 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa300=sellevel(W,300);' \
    $ifile ${sdir}/out_diag/1hrPt_wa300_$period.nc || echo ERROR
}; )&; }>$err.wa300.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa400 / table 1hrPt
{ (if_requested $member $esmod 1hrPt wa400 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa400=sellevel(W,400);' \
    $ifile ${sdir}/out_diag/1hrPt_wa400_$period.nc || echo ERROR
}; )&; }>$err.wa400.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa50 / table 1hrPt
{ (if_requested $member $esmod 1hrPt wa50 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa50=sellevel(W,50);' \
    $ifile ${sdir}/out_diag/1hrPt_wa50_$period.nc || echo ERROR
}; )&; }>$err.wa50.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa500 / table 1hrPt
{ (if_requested $member $esmod 1hrPt wa500 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa500=sellevel(W,500);' \
    $ifile ${sdir}/out_diag/1hrPt_wa500_$period.nc || echo ERROR
}; )&; }>$err.wa500.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa6 / table 1hrPt
{ (if_requested $member $esmod 1hrPt wa6 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3D_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    setzaxis,${ZAXISFILE6} -expr,'wa6=sellevidxrange(W,35,40);orog=orog;' \
    $ifile ${sdir}/out_diag/1hrPt_wa6_$period.nc || echo ERROR
}; )&; }>$err.wa6.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa600 / table 1hrPt
{ (if_requested $member $esmod 1hrPt wa600 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa600=sellevel(W,600);' \
    $ifile ${sdir}/out_diag/1hrPt_wa600_$period.nc || echo ERROR
}; )&; }>$err.wa600.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa70 / table 1hrPt
{ (if_requested $member $esmod 1hrPt wa70 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa70=sellevel(W,70);' \
    $ifile ${sdir}/out_diag/1hrPt_wa70_$period.nc || echo ERROR
}; )&; }>$err.wa70.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa700 / table 1hrPt
{ (if_requested $member $esmod 1hrPt wa700 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa700=sellevel(W,700);' \
    $ifile ${sdir}/out_diag/1hrPt_wa700_$period.nc || echo ERROR
}; )&; }>$err.wa700.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa750 / table 1hrPt
{ (if_requested $member $esmod 1hrPt wa750 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa750=sellevel(W,750);' \
    $ifile ${sdir}/out_diag/1hrPt_wa750_$period.nc || echo ERROR
}; )&; }>$err.wa750.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa850 / table 1hrPt
{ (if_requested $member $esmod 1hrPt wa850 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa850=sellevel(W,850);' \
    $ifile ${sdir}/out_diag/1hrPt_wa850_$period.nc || echo ERROR
}; )&; }>$err.wa850.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa925 / table 1hrPt
{ (if_requested $member $esmod 1hrPt wa925 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_1hrPt_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa925=sellevel(W,925);' \
    $ifile ${sdir}/out_diag/1hrPt_wa925_$period.nc || echo ERROR
}; )&; }>$err.wa925.1hrPt 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable clwvi / table day
{ (if_requested $member $esmod day clwvi $chunk && {
  find_file -e            "$sdir" "*_*_*_TQC_2D_day_${period}.nc" ifile1
  find_file -e            "$sdir" "*_*_*_TQI_2D_day_${period}.nc" ifile2
  $cdo -f nc -O \
    expr,'clwvi=TQI+TQC;' \
    -merge -selname,TQC $ifile1 -selname,TQI $ifile2 \
    ${sdir}/out_diag/day_clwvi_$period.nc || echo ERROR
}; )&; }>$err.clwvi.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable evspsbl / table day
{ (if_requested $member $esmod day evspsbl $chunk && {
  find_file -e            "$sdir" "*_*_*_AEVAPS_2D_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'evspsbl=-AEVAPS;' \
    $ifile ${sdir}/out_diag/day_evspsbl_$period.nc || echo ERROR
}; )&; }>$err.evspsbl.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus10 / table day
{ (if_requested $member $esmod day hus10 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus10=sellevel(QV,10);' \
    $ifile ${sdir}/out_diag/day_hus10_$period.nc || echo ERROR
}; )&; }>$err.hus10.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus100 / table day
{ (if_requested $member $esmod day hus100 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus100=sellevel(QV,100);' \
    $ifile ${sdir}/out_diag/day_hus100_$period.nc || echo ERROR
}; )&; }>$err.hus100.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus1000 / table day
{ (if_requested $member $esmod day hus1000 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus1000=sellevel(QV,1000);' \
    $ifile ${sdir}/out_diag/day_hus1000_$period.nc || echo ERROR
}; )&; }>$err.hus1000.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus150 / table day
{ (if_requested $member $esmod day hus150 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus150=sellevel(QV,150);' \
    $ifile ${sdir}/out_diag/day_hus150_$period.nc || echo ERROR
}; )&; }>$err.hus150.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus20 / table day
{ (if_requested $member $esmod day hus20 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus20=sellevel(QV,20);' \
    $ifile ${sdir}/out_diag/day_hus20_$period.nc || echo ERROR
}; )&; }>$err.hus20.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus200 / table day
{ (if_requested $member $esmod day hus200 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus200=sellevel(QV,200);' \
    $ifile ${sdir}/out_diag/day_hus200_$period.nc || echo ERROR
}; )&; }>$err.hus200.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus250 / table day
{ (if_requested $member $esmod day hus250 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus250=sellevel(QV,250);' \
    $ifile ${sdir}/out_diag/day_hus250_$period.nc || echo ERROR
}; )&; }>$err.hus250.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus30 / table day
{ (if_requested $member $esmod day hus30 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus30=sellevel(QV,30);' \
    $ifile ${sdir}/out_diag/day_hus30_$period.nc || echo ERROR
}; )&; }>$err.hus30.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus300 / table day
{ (if_requested $member $esmod day hus300 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus300=sellevel(QV,300);' \
    $ifile ${sdir}/out_diag/day_hus300_$period.nc || echo ERROR
}; )&; }>$err.hus300.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus400 / table day
{ (if_requested $member $esmod day hus400 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus400=sellevel(QV,400);' \
    $ifile ${sdir}/out_diag/day_hus400_$period.nc || echo ERROR
}; )&; }>$err.hus400.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus50 / table day
{ (if_requested $member $esmod day hus50 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus50=sellevel(QV,50);' \
    $ifile ${sdir}/out_diag/day_hus50_$period.nc || echo ERROR
}; )&; }>$err.hus50.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus500 / table day
{ (if_requested $member $esmod day hus500 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus500=sellevel(QV,500);' \
    $ifile ${sdir}/out_diag/day_hus500_$period.nc || echo ERROR
}; )&; }>$err.hus500.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus6 / table day
{ (if_requested $member $esmod day hus6 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3D_day_${period}.nc" ifile
  $cdo -f nc -O \
    setzaxis,${ZAXISFILE6} -expr,'hus6=sellevidxrange(QV,35,40);orog=orog;' \
    $ifile ${sdir}/out_diag/day_hus6_$period.nc || echo ERROR
}; )&; }>$err.hus6.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus600 / table day
{ (if_requested $member $esmod day hus600 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus600=sellevel(QV,600);' \
    $ifile ${sdir}/out_diag/day_hus600_$period.nc || echo ERROR
}; )&; }>$err.hus600.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus70 / table day
{ (if_requested $member $esmod day hus70 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus70=sellevel(QV,70);' \
    $ifile ${sdir}/out_diag/day_hus70_$period.nc || echo ERROR
}; )&; }>$err.hus70.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus700 / table day
{ (if_requested $member $esmod day hus700 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus700=sellevel(QV,700);' \
    $ifile ${sdir}/out_diag/day_hus700_$period.nc || echo ERROR
}; )&; }>$err.hus700.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus750 / table day
{ (if_requested $member $esmod day hus750 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus750=sellevel(QV,750);' \
    $ifile ${sdir}/out_diag/day_hus750_$period.nc || echo ERROR
}; )&; }>$err.hus750.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus850 / table day
{ (if_requested $member $esmod day hus850 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus850=sellevel(QV,850);' \
    $ifile ${sdir}/out_diag/day_hus850_$period.nc || echo ERROR
}; )&; }>$err.hus850.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus925 / table day
{ (if_requested $member $esmod day hus925 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus925=sellevel(QV,925);' \
    $ifile ${sdir}/out_diag/day_hus925_$period.nc || echo ERROR
}; )&; }>$err.hus925.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable mrfso / table day
{ (if_requested $member $esmod day mrfso $chunk && {
  find_file -e            "$sdir" "*_*_*_WSOILICE_3D_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'mrfso=vertsum(WSOILICE);' \
    $ifile ${sdir}/out_diag/day_mrfso_$period.nc || echo ERROR
}; )&; }>$err.mrfso.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable mrro / table day
# Editor Note: Requires calculation of accumulated to average values.
{ (if_requested $member $esmod day mrro $chunk && {
  find_file -e            "$sdir" "*_*_*_RUNOFFG_2D_day_${period}.nc" ifile1
  find_file -e            "$sdir" "*_*_*_RUNOFFS_2D_day_${period}.nc" ifile2
  $cdo -f nc -O \
    expr,'mrro=RUNOFFS+RUNOFFG;' \
    -merge -selname,RUNOFFG $ifile1 -selname,RUNOFFS $ifile2 \
    ${sdir}/out_diag/day_mrro_$period.nc || echo ERROR
}; )&; }>$err.mrro.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable mrso / table day
{ (if_requested $member $esmod day mrso $chunk && {
  find_file -e            "$sdir" "*_*_*_WSOIL_3D_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'mrso=vertsum(WSOIL);' \
    $ifile ${sdir}/out_diag/day_mrso_$period.nc || echo ERROR
}; )&; }>$err.mrso.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable pfull6 / table day
{ (if_requested $member $esmod day pfull6 $chunk && {
  find_file -e            "$sdir" "*_*_*_P_3D_day_${period}.nc" ifile
  $cdo -f nc -O \
    setzaxis,${ZAXISFILE6} -expr,'pfull6=sellevidxrange(P,35,40);orog=orog;' \
    $ifile ${sdir}/out_diag/day_pfull6_$period.nc || echo ERROR
}; )&; }>$err.pfull6.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable prsn / table day
# Editor Note: Aggregation required to calculate time interval averages out of accumulated values
{ (if_requested $member $esmod day prsn $chunk && {
  find_file -e            "$sdir" "*_*_*_SNOWCON_2D_day_${period}.nc" ifile1
  find_file -e            "$sdir" "*_*_*_SNOWGSP_2D_day_${period}.nc" ifile2
  $cdo -f nc -O \
    expr,'prsn=SNOWGSP+SNOWCON;' \
    -merge -selname,SNOWCON $ifile1 -selname,SNOWGSP $ifile2 \
    ${sdir}/out_diag/day_prsn_$period.nc || echo ERROR
}; )&; }>$err.prsn.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable rsds / table day
# Editor Note: Calculated as SWDirect+SWDiffuse
{ (if_requested $member $esmod day rsds $chunk && {
  find_file -e            "$sdir" "*_*_*_ASWDIFDS_2D_day_${period}.nc" ifile1
  find_file -e            "$sdir" "*_*_*_ASWDIRS_2D_day_${period}.nc" ifile2
  $cdo -f nc -O \
    expr,'rsds=ASWDIRS+ASWDIFDS;' \
    -merge -selname,ASWDIFDS $ifile1 -selname,ASWDIRS $ifile2 \
    ${sdir}/out_diag/day_rsds_$period.nc || echo ERROR
}; )&; }>$err.rsds.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable rsus / table day
# Editor Note: Calculated as NetSW-DirectSW-DiffuseSW
{ (if_requested $member $esmod day rsus $chunk && {
  find_file -e            "$sdir" "*_*_*_ASOBS_2D_day_${period}.nc" ifile1
  find_file -e            "$sdir" "*_*_*_ASWDIFDS_2D_day_${period}.nc" ifile2
  find_file -e            "$sdir" "*_*_*_ASWDIRS_2D_day_${period}.nc" ifile3
  $cdo -f nc -O \
    expr,'rsus=ASOBS-ASWDIRS-ASWDIFDS;' \
    -merge -selname,ASOBS $ifile1 -selname,ASWDIFDS $ifile2 -selname,ASWDIRS $ifile3 \
    ${sdir}/out_diag/day_rsus_$period.nc || echo ERROR
}; )&; }>$err.rsus.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable rsut / table day
# Editor Note: Calculated as NetSW_TOA-DownwardSW_TOA
{ (if_requested $member $esmod day rsut $chunk && {
  find_file -e            "$sdir" "*_*_*_ASOBT_2D_day_${period}.nc" ifile1
  find_file -e            "$sdir" "*_*_*_ASODT_2D_day_${period}.nc" ifile2
  $cdo -f nc -O \
    expr,'rsut=ASOBT-ASODT;' \
    -merge -selname,ASOBT $ifile1 -selname,ASODT $ifile2 \
    ${sdir}/out_diag/day_rsut_$period.nc || echo ERROR
}; )&; }>$err.rsut.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable snc / table day
# Editor Note: WSNOW needs to be in units m, however at least in this COSMO version it is provided in units kg m-2. Thus the additional division by RHOSNOW or substitution by HSNOW.
{ (if_requested $member $esmod day snc $chunk && {
  find_file -e            "$sdir" "*_*_*_HSNOW_2D_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'FRSNOW=max(0.01,min(1.,HSNOW/0.015))*(HSNOW>5e-7);' \
    $ifile ${sdir}/out_diag/day_snc_$period.nc || echo ERROR
}; )&; }>$err.snc.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta10 / table day
{ (if_requested $member $esmod day ta10 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta10=sellevel(T,10);' \
    $ifile ${sdir}/out_diag/day_ta10_$period.nc || echo ERROR
}; )&; }>$err.ta10.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta100 / table day
{ (if_requested $member $esmod day ta100 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta100=sellevel(T,100);' \
    $ifile ${sdir}/out_diag/day_ta100_$period.nc || echo ERROR
}; )&; }>$err.ta100.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta1000 / table day
{ (if_requested $member $esmod day ta1000 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta1000=sellevel(T,1000);' \
    $ifile ${sdir}/out_diag/day_ta1000_$period.nc || echo ERROR
}; )&; }>$err.ta1000.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta150 / table day
{ (if_requested $member $esmod day ta150 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta150=sellevel(T,150);' \
    $ifile ${sdir}/out_diag/day_ta150_$period.nc || echo ERROR
}; )&; }>$err.ta150.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta20 / table day
{ (if_requested $member $esmod day ta20 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta20=sellevel(T,20);' \
    $ifile ${sdir}/out_diag/day_ta20_$period.nc || echo ERROR
}; )&; }>$err.ta20.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta200 / table day
{ (if_requested $member $esmod day ta200 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta200=sellevel(T,200);' \
    $ifile ${sdir}/out_diag/day_ta200_$period.nc || echo ERROR
}; )&; }>$err.ta200.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta250 / table day
{ (if_requested $member $esmod day ta250 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta250=sellevel(T,250);' \
    $ifile ${sdir}/out_diag/day_ta250_$period.nc || echo ERROR
}; )&; }>$err.ta250.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta30 / table day
{ (if_requested $member $esmod day ta30 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta30=sellevel(T,30);' \
    $ifile ${sdir}/out_diag/day_ta30_$period.nc || echo ERROR
}; )&; }>$err.ta30.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta300 / table day
{ (if_requested $member $esmod day ta300 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta300=sellevel(T,300);' \
    $ifile ${sdir}/out_diag/day_ta300_$period.nc || echo ERROR
}; )&; }>$err.ta300.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta400 / table day
{ (if_requested $member $esmod day ta400 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta400=sellevel(T,400);' \
    $ifile ${sdir}/out_diag/day_ta400_$period.nc || echo ERROR
}; )&; }>$err.ta400.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta50 / table day
{ (if_requested $member $esmod day ta50 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta50=sellevel(T,50);' \
    $ifile ${sdir}/out_diag/day_ta50_$period.nc || echo ERROR
}; )&; }>$err.ta50.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta500 / table day
{ (if_requested $member $esmod day ta500 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta500=sellevel(T,500);' \
    $ifile ${sdir}/out_diag/day_ta500_$period.nc || echo ERROR
}; )&; }>$err.ta500.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta6 / table day
{ (if_requested $member $esmod day ta6 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3D_day_${period}.nc" ifile
  $cdo -f nc -O \
    setzaxis,${ZAXISFILE6} -expr,'ta6=sellevidxrange(T,35,40);orog=orog;' \
    $ifile ${sdir}/out_diag/day_ta6_$period.nc || echo ERROR
}; )&; }>$err.ta6.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta600 / table day
{ (if_requested $member $esmod day ta600 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta600=sellevel(T,600);' \
    $ifile ${sdir}/out_diag/day_ta600_$period.nc || echo ERROR
}; )&; }>$err.ta600.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta70 / table day
{ (if_requested $member $esmod day ta70 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta70=sellevel(T,70);' \
    $ifile ${sdir}/out_diag/day_ta70_$period.nc || echo ERROR
}; )&; }>$err.ta70.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta700 / table day
{ (if_requested $member $esmod day ta700 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta700=sellevel(T,700);' \
    $ifile ${sdir}/out_diag/day_ta700_$period.nc || echo ERROR
}; )&; }>$err.ta700.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta750 / table day
{ (if_requested $member $esmod day ta750 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta750=sellevel(T,750);' \
    $ifile ${sdir}/out_diag/day_ta750_$period.nc || echo ERROR
}; )&; }>$err.ta750.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta850 / table day
{ (if_requested $member $esmod day ta850 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta850=sellevel(T,850);' \
    $ifile ${sdir}/out_diag/day_ta850_$period.nc || echo ERROR
}; )&; }>$err.ta850.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta925 / table day
{ (if_requested $member $esmod day ta925 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta925=sellevel(T,925);' \
    $ifile ${sdir}/out_diag/day_ta925_$period.nc || echo ERROR
}; )&; }>$err.ta925.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable tke6 / table day
{ (if_requested $member $esmod day tke6 $chunk && {
  find_file -e            "$sdir" "*_*_*_TKE_3D_day_${period}.nc" ifile
  $cdo -f nc -O \
    setzaxis,${ZAXISFILE6} -expr,'tke6=sellevidxrange(TKE,35,40);orog=orog;' \
    $ifile ${sdir}/out_diag/day_tke6_$period.nc || echo ERROR
}; )&; }>$err.tke6.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua10 / table day
{ (if_requested $member $esmod day ua10 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua10=sellevel(U,10);' \
    $ifile ${sdir}/out_diag/day_ua10_$period.nc || echo ERROR
}; )&; }>$err.ua10.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua100 / table day
{ (if_requested $member $esmod day ua100 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua100=sellevel(U,100);' \
    $ifile ${sdir}/out_diag/day_ua100_$period.nc || echo ERROR
}; )&; }>$err.ua100.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua1000 / table day
{ (if_requested $member $esmod day ua1000 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua1000=sellevel(U,1000);' \
    $ifile ${sdir}/out_diag/day_ua1000_$period.nc || echo ERROR
}; )&; }>$err.ua1000.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua150 / table day
{ (if_requested $member $esmod day ua150 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua150=sellevel(U,150);' \
    $ifile ${sdir}/out_diag/day_ua150_$period.nc || echo ERROR
}; )&; }>$err.ua150.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua20 / table day
{ (if_requested $member $esmod day ua20 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua20=sellevel(U,20);' \
    $ifile ${sdir}/out_diag/day_ua20_$period.nc || echo ERROR
}; )&; }>$err.ua20.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua200 / table day
{ (if_requested $member $esmod day ua200 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua200=sellevel(U,200);' \
    $ifile ${sdir}/out_diag/day_ua200_$period.nc || echo ERROR
}; )&; }>$err.ua200.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua250 / table day
{ (if_requested $member $esmod day ua250 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua250=sellevel(U,250);' \
    $ifile ${sdir}/out_diag/day_ua250_$period.nc || echo ERROR
}; )&; }>$err.ua250.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua30 / table day
{ (if_requested $member $esmod day ua30 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua30=sellevel(U,30);' \
    $ifile ${sdir}/out_diag/day_ua30_$period.nc || echo ERROR
}; )&; }>$err.ua30.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua300 / table day
{ (if_requested $member $esmod day ua300 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua300=sellevel(U,300);' \
    $ifile ${sdir}/out_diag/day_ua300_$period.nc || echo ERROR
}; )&; }>$err.ua300.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua400 / table day
{ (if_requested $member $esmod day ua400 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua400=sellevel(U,400);' \
    $ifile ${sdir}/out_diag/day_ua400_$period.nc || echo ERROR
}; )&; }>$err.ua400.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua50 / table day
{ (if_requested $member $esmod day ua50 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua50=sellevel(U,50);' \
    $ifile ${sdir}/out_diag/day_ua50_$period.nc || echo ERROR
}; )&; }>$err.ua50.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua500 / table day
{ (if_requested $member $esmod day ua500 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua500=sellevel(U,500);' \
    $ifile ${sdir}/out_diag/day_ua500_$period.nc || echo ERROR
}; )&; }>$err.ua500.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua6 / table day
{ (if_requested $member $esmod day ua6 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3D_day_${period}.nc" ifile
  $cdo -f nc -O \
    setzaxis,${ZAXISFILE6} -expr,'ua6=sellevidxrange(U,35,40);orog=orog;' \
    $ifile ${sdir}/out_diag/day_ua6_$period.nc || echo ERROR
}; )&; }>$err.ua6.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua600 / table day
{ (if_requested $member $esmod day ua600 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua600=sellevel(U,600);' \
    $ifile ${sdir}/out_diag/day_ua600_$period.nc || echo ERROR
}; )&; }>$err.ua600.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua70 / table day
{ (if_requested $member $esmod day ua70 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua70=sellevel(U,70);' \
    $ifile ${sdir}/out_diag/day_ua70_$period.nc || echo ERROR
}; )&; }>$err.ua70.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua700 / table day
{ (if_requested $member $esmod day ua700 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua700=sellevel(U,700);' \
    $ifile ${sdir}/out_diag/day_ua700_$period.nc || echo ERROR
}; )&; }>$err.ua700.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua750 / table day
{ (if_requested $member $esmod day ua750 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua750=sellevel(U,750);' \
    $ifile ${sdir}/out_diag/day_ua750_$period.nc || echo ERROR
}; )&; }>$err.ua750.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua850 / table day
{ (if_requested $member $esmod day ua850 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua850=sellevel(U,850);' \
    $ifile ${sdir}/out_diag/day_ua850_$period.nc || echo ERROR
}; )&; }>$err.ua850.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua925 / table day
{ (if_requested $member $esmod day ua925 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua925=sellevel(U,925);' \
    $ifile ${sdir}/out_diag/day_ua925_$period.nc || echo ERROR
}; )&; }>$err.ua925.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va10 / table day
{ (if_requested $member $esmod day va10 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va10=sellevel(V,10);' \
    $ifile ${sdir}/out_diag/day_va10_$period.nc || echo ERROR
}; )&; }>$err.va10.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va100 / table day
{ (if_requested $member $esmod day va100 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va100=sellevel(V,100);' \
    $ifile ${sdir}/out_diag/day_va100_$period.nc || echo ERROR
}; )&; }>$err.va100.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va1000 / table day
{ (if_requested $member $esmod day va1000 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va1000=sellevel(V,1000);' \
    $ifile ${sdir}/out_diag/day_va1000_$period.nc || echo ERROR
}; )&; }>$err.va1000.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va150 / table day
{ (if_requested $member $esmod day va150 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va150=sellevel(V,150);' \
    $ifile ${sdir}/out_diag/day_va150_$period.nc || echo ERROR
}; )&; }>$err.va150.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va20 / table day
{ (if_requested $member $esmod day va20 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va20=sellevel(V,20);' \
    $ifile ${sdir}/out_diag/day_va20_$period.nc || echo ERROR
}; )&; }>$err.va20.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va200 / table day
{ (if_requested $member $esmod day va200 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va200=sellevel(V,200);' \
    $ifile ${sdir}/out_diag/day_va200_$period.nc || echo ERROR
}; )&; }>$err.va200.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va250 / table day
{ (if_requested $member $esmod day va250 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va250=sellevel(V,250);' \
    $ifile ${sdir}/out_diag/day_va250_$period.nc || echo ERROR
}; )&; }>$err.va250.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va30 / table day
{ (if_requested $member $esmod day va30 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va30=sellevel(V,30);' \
    $ifile ${sdir}/out_diag/day_va30_$period.nc || echo ERROR
}; )&; }>$err.va30.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va300 / table day
{ (if_requested $member $esmod day va300 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va300=sellevel(V,300);' \
    $ifile ${sdir}/out_diag/day_va300_$period.nc || echo ERROR
}; )&; }>$err.va300.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va400 / table day
{ (if_requested $member $esmod day va400 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va400=sellevel(V,400);' \
    $ifile ${sdir}/out_diag/day_va400_$period.nc || echo ERROR
}; )&; }>$err.va400.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va50 / table day
{ (if_requested $member $esmod day va50 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va50=sellevel(V,50);' \
    $ifile ${sdir}/out_diag/day_va50_$period.nc || echo ERROR
}; )&; }>$err.va50.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va500 / table day
{ (if_requested $member $esmod day va500 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va500=sellevel(V,500);' \
    $ifile ${sdir}/out_diag/day_va500_$period.nc || echo ERROR
}; )&; }>$err.va500.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va6 / table day
{ (if_requested $member $esmod day va6 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3D_day_${period}.nc" ifile
  $cdo -f nc -O \
    setzaxis,${ZAXISFILE6} -expr,'va6=sellevidxrange(V,35,40);orog=orog;' \
    $ifile ${sdir}/out_diag/day_va6_$period.nc || echo ERROR
}; )&; }>$err.va6.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va600 / table day
{ (if_requested $member $esmod day va600 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va600=sellevel(V,600);' \
    $ifile ${sdir}/out_diag/day_va600_$period.nc || echo ERROR
}; )&; }>$err.va600.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va70 / table day
{ (if_requested $member $esmod day va70 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va70=sellevel(V,70);' \
    $ifile ${sdir}/out_diag/day_va70_$period.nc || echo ERROR
}; )&; }>$err.va70.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va700 / table day
{ (if_requested $member $esmod day va700 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va700=sellevel(V,700);' \
    $ifile ${sdir}/out_diag/day_va700_$period.nc || echo ERROR
}; )&; }>$err.va700.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va750 / table day
{ (if_requested $member $esmod day va750 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va750=sellevel(V,750);' \
    $ifile ${sdir}/out_diag/day_va750_$period.nc || echo ERROR
}; )&; }>$err.va750.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va850 / table day
{ (if_requested $member $esmod day va850 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va850=sellevel(V,850);' \
    $ifile ${sdir}/out_diag/day_va850_$period.nc || echo ERROR
}; )&; }>$err.va850.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va925 / table day
{ (if_requested $member $esmod day va925 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va925=sellevel(V,925);' \
    $ifile ${sdir}/out_diag/day_va925_$period.nc || echo ERROR
}; )&; }>$err.va925.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa10 / table day
{ (if_requested $member $esmod day wa10 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa10=sellevel(W,10);' \
    $ifile ${sdir}/out_diag/day_wa10_$period.nc || echo ERROR
}; )&; }>$err.wa10.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa100 / table day
{ (if_requested $member $esmod day wa100 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa100=sellevel(W,100);' \
    $ifile ${sdir}/out_diag/day_wa100_$period.nc || echo ERROR
}; )&; }>$err.wa100.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa1000 / table day
{ (if_requested $member $esmod day wa1000 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa1000=sellevel(W,1000);' \
    $ifile ${sdir}/out_diag/day_wa1000_$period.nc || echo ERROR
}; )&; }>$err.wa1000.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa150 / table day
{ (if_requested $member $esmod day wa150 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa150=sellevel(W,150);' \
    $ifile ${sdir}/out_diag/day_wa150_$period.nc || echo ERROR
}; )&; }>$err.wa150.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa20 / table day
{ (if_requested $member $esmod day wa20 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa20=sellevel(W,20);' \
    $ifile ${sdir}/out_diag/day_wa20_$period.nc || echo ERROR
}; )&; }>$err.wa20.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa200 / table day
{ (if_requested $member $esmod day wa200 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa200=sellevel(W,200);' \
    $ifile ${sdir}/out_diag/day_wa200_$period.nc || echo ERROR
}; )&; }>$err.wa200.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa250 / table day
{ (if_requested $member $esmod day wa250 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa250=sellevel(W,250);' \
    $ifile ${sdir}/out_diag/day_wa250_$period.nc || echo ERROR
}; )&; }>$err.wa250.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa30 / table day
{ (if_requested $member $esmod day wa30 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa30=sellevel(W,30);' \
    $ifile ${sdir}/out_diag/day_wa30_$period.nc || echo ERROR
}; )&; }>$err.wa30.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa300 / table day
{ (if_requested $member $esmod day wa300 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa300=sellevel(W,300);' \
    $ifile ${sdir}/out_diag/day_wa300_$period.nc || echo ERROR
}; )&; }>$err.wa300.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa400 / table day
{ (if_requested $member $esmod day wa400 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa400=sellevel(W,400);' \
    $ifile ${sdir}/out_diag/day_wa400_$period.nc || echo ERROR
}; )&; }>$err.wa400.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa50 / table day
{ (if_requested $member $esmod day wa50 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa50=sellevel(W,50);' \
    $ifile ${sdir}/out_diag/day_wa50_$period.nc || echo ERROR
}; )&; }>$err.wa50.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa500 / table day
{ (if_requested $member $esmod day wa500 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa500=sellevel(W,500);' \
    $ifile ${sdir}/out_diag/day_wa500_$period.nc || echo ERROR
}; )&; }>$err.wa500.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa6 / table day
{ (if_requested $member $esmod day wa6 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3D_day_${period}.nc" ifile
  $cdo -f nc -O \
    setzaxis,${ZAXISFILE6} -expr,'wa6=sellevidxrange(W,35,40);orog=orog;' \
    $ifile ${sdir}/out_diag/day_wa6_$period.nc || echo ERROR
}; )&; }>$err.wa6.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa600 / table day
{ (if_requested $member $esmod day wa600 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa600=sellevel(W,600);' \
    $ifile ${sdir}/out_diag/day_wa600_$period.nc || echo ERROR
}; )&; }>$err.wa600.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa70 / table day
{ (if_requested $member $esmod day wa70 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa70=sellevel(W,70);' \
    $ifile ${sdir}/out_diag/day_wa70_$period.nc || echo ERROR
}; )&; }>$err.wa70.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa700 / table day
{ (if_requested $member $esmod day wa700 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa700=sellevel(W,700);' \
    $ifile ${sdir}/out_diag/day_wa700_$period.nc || echo ERROR
}; )&; }>$err.wa700.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa750 / table day
{ (if_requested $member $esmod day wa750 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa750=sellevel(W,750);' \
    $ifile ${sdir}/out_diag/day_wa750_$period.nc || echo ERROR
}; )&; }>$err.wa750.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa850 / table day
{ (if_requested $member $esmod day wa850 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa850=sellevel(W,850);' \
    $ifile ${sdir}/out_diag/day_wa850_$period.nc || echo ERROR
}; )&; }>$err.wa850.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa925 / table day
{ (if_requested $member $esmod day wa925 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_day_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa925=sellevel(W,925);' \
    $ifile ${sdir}/out_diag/day_wa925_$period.nc || echo ERROR
}; )&; }>$err.wa925.day 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable sftlf / table fx
{ (if_requested $member $esmod fx sftlf $chunk && {
  find_file -e            "$sdir" "*_*_*_FRLAKE_2D_fx.nc" ifile1
  find_file -e            "$sdir" "*_*_*_FRLAND_2D_fx.nc" ifile2
  $cdo -f nc -O \
    expr,'sftlf=FRLAND+FRLAKE;' \
    -merge -selname,FRLAKE $ifile1 -selname,FRLAND $ifile2 \
    ${sdir}/out_diag/fx_sftlf.nc || echo ERROR
}; )&; }>$err.sftlf.fx 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable clwvi / table mon
{ (if_requested $member $esmod mon clwvi $chunk && {
  find_file -e            "$sdir" "*_*_*_TQC_2D_mon_${period}.nc" ifile1
  find_file -e            "$sdir" "*_*_*_TQI_2D_mon_${period}.nc" ifile2
  $cdo -f nc -O \
    expr,'clwvi=TQI+TQC;' \
    -merge -selname,TQC $ifile1 -selname,TQI $ifile2 \
    ${sdir}/out_diag/mon_clwvi_$period.nc || echo ERROR
}; )&; }>$err.clwvi.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable evspsbl / table mon
{ (if_requested $member $esmod mon evspsbl $chunk && {
  find_file -e            "$sdir" "*_*_*_AEVAPS_2D_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'evspsbl=-AEVAPS;' \
    $ifile ${sdir}/out_diag/mon_evspsbl_$period.nc || echo ERROR
}; )&; }>$err.evspsbl.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus10 / table mon
{ (if_requested $member $esmod mon hus10 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus10=sellevel(QV,10);' \
    $ifile ${sdir}/out_diag/mon_hus10_$period.nc || echo ERROR
}; )&; }>$err.hus10.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus100 / table mon
{ (if_requested $member $esmod mon hus100 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus100=sellevel(QV,100);' \
    $ifile ${sdir}/out_diag/mon_hus100_$period.nc || echo ERROR
}; )&; }>$err.hus100.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus1000 / table mon
{ (if_requested $member $esmod mon hus1000 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus1000=sellevel(QV,1000);' \
    $ifile ${sdir}/out_diag/mon_hus1000_$period.nc || echo ERROR
}; )&; }>$err.hus1000.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus150 / table mon
{ (if_requested $member $esmod mon hus150 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus150=sellevel(QV,150);' \
    $ifile ${sdir}/out_diag/mon_hus150_$period.nc || echo ERROR
}; )&; }>$err.hus150.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus20 / table mon
{ (if_requested $member $esmod mon hus20 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus20=sellevel(QV,20);' \
    $ifile ${sdir}/out_diag/mon_hus20_$period.nc || echo ERROR
}; )&; }>$err.hus20.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus200 / table mon
{ (if_requested $member $esmod mon hus200 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus200=sellevel(QV,200);' \
    $ifile ${sdir}/out_diag/mon_hus200_$period.nc || echo ERROR
}; )&; }>$err.hus200.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus250 / table mon
{ (if_requested $member $esmod mon hus250 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus250=sellevel(QV,250);' \
    $ifile ${sdir}/out_diag/mon_hus250_$period.nc || echo ERROR
}; )&; }>$err.hus250.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus30 / table mon
{ (if_requested $member $esmod mon hus30 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus30=sellevel(QV,30);' \
    $ifile ${sdir}/out_diag/mon_hus30_$period.nc || echo ERROR
}; )&; }>$err.hus30.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus300 / table mon
{ (if_requested $member $esmod mon hus300 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus300=sellevel(QV,300);' \
    $ifile ${sdir}/out_diag/mon_hus300_$period.nc || echo ERROR
}; )&; }>$err.hus300.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus400 / table mon
{ (if_requested $member $esmod mon hus400 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus400=sellevel(QV,400);' \
    $ifile ${sdir}/out_diag/mon_hus400_$period.nc || echo ERROR
}; )&; }>$err.hus400.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus50 / table mon
{ (if_requested $member $esmod mon hus50 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus50=sellevel(QV,50);' \
    $ifile ${sdir}/out_diag/mon_hus50_$period.nc || echo ERROR
}; )&; }>$err.hus50.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus500 / table mon
{ (if_requested $member $esmod mon hus500 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus500=sellevel(QV,500);' \
    $ifile ${sdir}/out_diag/mon_hus500_$period.nc || echo ERROR
}; )&; }>$err.hus500.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus6 / table mon
{ (if_requested $member $esmod mon hus6 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3D_mon_${period}.nc" ifile
  $cdo -f nc -O \
    setzaxis,${ZAXISFILE6} -expr,'hus6=sellevidxrange(QV,35,40);orog=orog;' \
    $ifile ${sdir}/out_diag/mon_hus6_$period.nc || echo ERROR
}; )&; }>$err.hus6.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus600 / table mon
{ (if_requested $member $esmod mon hus600 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus600=sellevel(QV,600);' \
    $ifile ${sdir}/out_diag/mon_hus600_$period.nc || echo ERROR
}; )&; }>$err.hus600.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus70 / table mon
{ (if_requested $member $esmod mon hus70 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus70=sellevel(QV,70);' \
    $ifile ${sdir}/out_diag/mon_hus70_$period.nc || echo ERROR
}; )&; }>$err.hus70.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus700 / table mon
{ (if_requested $member $esmod mon hus700 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus700=sellevel(QV,700);' \
    $ifile ${sdir}/out_diag/mon_hus700_$period.nc || echo ERROR
}; )&; }>$err.hus700.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus750 / table mon
{ (if_requested $member $esmod mon hus750 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus750=sellevel(QV,750);' \
    $ifile ${sdir}/out_diag/mon_hus750_$period.nc || echo ERROR
}; )&; }>$err.hus750.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus850 / table mon
{ (if_requested $member $esmod mon hus850 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus850=sellevel(QV,850);' \
    $ifile ${sdir}/out_diag/mon_hus850_$period.nc || echo ERROR
}; )&; }>$err.hus850.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable hus925 / table mon
{ (if_requested $member $esmod mon hus925 $chunk && {
  find_file -e            "$sdir" "*_*_*_QV_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'hus925=sellevel(QV,925);' \
    $ifile ${sdir}/out_diag/mon_hus925_$period.nc || echo ERROR
}; )&; }>$err.hus925.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable mrfso / table mon
{ (if_requested $member $esmod mon mrfso $chunk && {
  find_file -e            "$sdir" "*_*_*_WSOILICE_3D_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'mrfso=vertsum(WSOILICE);' \
    $ifile ${sdir}/out_diag/mon_mrfso_$period.nc || echo ERROR
}; )&; }>$err.mrfso.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable mrro / table mon
# Editor Note: Requires calculation of accumulated to average values.
{ (if_requested $member $esmod mon mrro $chunk && {
  find_file -e            "$sdir" "*_*_*_RUNOFFG_2D_mon_${period}.nc" ifile1
  find_file -e            "$sdir" "*_*_*_RUNOFFS_2D_mon_${period}.nc" ifile2
  $cdo -f nc -O \
    expr,'mrro=RUNOFFS+RUNOFFG;' \
    -merge -selname,RUNOFFG $ifile1 -selname,RUNOFFS $ifile2 \
    ${sdir}/out_diag/mon_mrro_$period.nc || echo ERROR
}; )&; }>$err.mrro.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable mrso / table mon
{ (if_requested $member $esmod mon mrso $chunk && {
  find_file -e            "$sdir" "*_*_*_WSOIL_3D_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'mrso=vertsum(WSOIL);' \
    $ifile ${sdir}/out_diag/mon_mrso_$period.nc || echo ERROR
}; )&; }>$err.mrso.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable pfull6 / table mon
{ (if_requested $member $esmod mon pfull6 $chunk && {
  find_file -e            "$sdir" "*_*_*_P_3D_mon_${period}.nc" ifile
  $cdo -f nc -O \
    setzaxis,${ZAXISFILE6} -expr,'pfull6=sellevidxrange(P,35,40);orog=orog;' \
    $ifile ${sdir}/out_diag/mon_pfull6_$period.nc || echo ERROR
}; )&; }>$err.pfull6.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable prsn / table mon
# Editor Note: Aggregation required to calculate time interval averages out of accumulated values
{ (if_requested $member $esmod mon prsn $chunk && {
  find_file -e            "$sdir" "*_*_*_SNOWCON_2D_mon_${period}.nc" ifile1
  find_file -e            "$sdir" "*_*_*_SNOWGSP_2D_mon_${period}.nc" ifile2
  $cdo -f nc -O \
    expr,'prsn=SNOWGSP+SNOWCON;' \
    -merge -selname,SNOWCON $ifile1 -selname,SNOWGSP $ifile2 \
    ${sdir}/out_diag/mon_prsn_$period.nc || echo ERROR
}; )&; }>$err.prsn.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable rsds / table mon
# Editor Note: Calculated as SWDirect+SWDiffuse
{ (if_requested $member $esmod mon rsds $chunk && {
  find_file -e            "$sdir" "*_*_*_ASWDIFDS_2D_mon_${period}.nc" ifile1
  find_file -e            "$sdir" "*_*_*_ASWDIRS_2D_mon_${period}.nc" ifile2
  $cdo -f nc -O \
    expr,'rsds=ASWDIRS+ASWDIFDS;' \
    -merge -selname,ASWDIFDS $ifile1 -selname,ASWDIRS $ifile2 \
    ${sdir}/out_diag/mon_rsds_$period.nc || echo ERROR
}; )&; }>$err.rsds.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable rsus / table mon
# Editor Note: Calculated as NetSW-DirectSW-DiffuseSW
{ (if_requested $member $esmod mon rsus $chunk && {
  find_file -e            "$sdir" "*_*_*_ASOBS_2D_mon_${period}.nc" ifile1
  find_file -e            "$sdir" "*_*_*_ASWDIFDS_2D_mon_${period}.nc" ifile2
  find_file -e            "$sdir" "*_*_*_ASWDIRS_2D_mon_${period}.nc" ifile3
  $cdo -f nc -O \
    expr,'rsus=ASOBS-ASWDIRS-ASWDIFDS;' \
    -merge -selname,ASOBS $ifile1 -selname,ASWDIFDS $ifile2 -selname,ASWDIRS $ifile3 \
    ${sdir}/out_diag/mon_rsus_$period.nc || echo ERROR
}; )&; }>$err.rsus.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable rsut / table mon
# Editor Note: Calculated as NetSW_TOA-DownwardSW_TOA
{ (if_requested $member $esmod mon rsut $chunk && {
  find_file -e            "$sdir" "*_*_*_ASOBT_2D_mon_${period}.nc" ifile1
  find_file -e            "$sdir" "*_*_*_ASODT_2D_mon_${period}.nc" ifile2
  $cdo -f nc -O \
    expr,'rsut=ASOBT-ASODT;' \
    -merge -selname,ASOBT $ifile1 -selname,ASODT $ifile2 \
    ${sdir}/out_diag/mon_rsut_$period.nc || echo ERROR
}; )&; }>$err.rsut.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable snc / table mon
# Editor Note: WSNOW needs to be in units m, however at least in this COSMO version it is provided in units kg m-2. Thus the additional division by RHOSNOW or substitution by HSNOW.
{ (if_requested $member $esmod mon snc $chunk && {
  find_file -e            "$sdir" "*_*_*_HSNOW_2D_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'FRSNOW=max(0.01,min(1.,HSNOW/0.015))*(HSNOW>5e-7);' \
    $ifile ${sdir}/out_diag/mon_snc_$period.nc || echo ERROR
}; )&; }>$err.snc.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta10 / table mon
{ (if_requested $member $esmod mon ta10 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta10=sellevel(T,10);' \
    $ifile ${sdir}/out_diag/mon_ta10_$period.nc || echo ERROR
}; )&; }>$err.ta10.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta100 / table mon
{ (if_requested $member $esmod mon ta100 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta100=sellevel(T,100);' \
    $ifile ${sdir}/out_diag/mon_ta100_$period.nc || echo ERROR
}; )&; }>$err.ta100.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta1000 / table mon
{ (if_requested $member $esmod mon ta1000 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta1000=sellevel(T,1000);' \
    $ifile ${sdir}/out_diag/mon_ta1000_$period.nc || echo ERROR
}; )&; }>$err.ta1000.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta150 / table mon
{ (if_requested $member $esmod mon ta150 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta150=sellevel(T,150);' \
    $ifile ${sdir}/out_diag/mon_ta150_$period.nc || echo ERROR
}; )&; }>$err.ta150.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta20 / table mon
{ (if_requested $member $esmod mon ta20 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta20=sellevel(T,20);' \
    $ifile ${sdir}/out_diag/mon_ta20_$period.nc || echo ERROR
}; )&; }>$err.ta20.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta200 / table mon
{ (if_requested $member $esmod mon ta200 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta200=sellevel(T,200);' \
    $ifile ${sdir}/out_diag/mon_ta200_$period.nc || echo ERROR
}; )&; }>$err.ta200.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta250 / table mon
{ (if_requested $member $esmod mon ta250 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta250=sellevel(T,250);' \
    $ifile ${sdir}/out_diag/mon_ta250_$period.nc || echo ERROR
}; )&; }>$err.ta250.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta30 / table mon
{ (if_requested $member $esmod mon ta30 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta30=sellevel(T,30);' \
    $ifile ${sdir}/out_diag/mon_ta30_$period.nc || echo ERROR
}; )&; }>$err.ta30.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta300 / table mon
{ (if_requested $member $esmod mon ta300 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta300=sellevel(T,300);' \
    $ifile ${sdir}/out_diag/mon_ta300_$period.nc || echo ERROR
}; )&; }>$err.ta300.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta400 / table mon
{ (if_requested $member $esmod mon ta400 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta400=sellevel(T,400);' \
    $ifile ${sdir}/out_diag/mon_ta400_$period.nc || echo ERROR
}; )&; }>$err.ta400.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta50 / table mon
{ (if_requested $member $esmod mon ta50 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta50=sellevel(T,50);' \
    $ifile ${sdir}/out_diag/mon_ta50_$period.nc || echo ERROR
}; )&; }>$err.ta50.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta500 / table mon
{ (if_requested $member $esmod mon ta500 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta500=sellevel(T,500);' \
    $ifile ${sdir}/out_diag/mon_ta500_$period.nc || echo ERROR
}; )&; }>$err.ta500.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta6 / table mon
{ (if_requested $member $esmod mon ta6 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3D_mon_${period}.nc" ifile
  $cdo -f nc -O \
    setzaxis,${ZAXISFILE6} -expr,'ta6=sellevidxrange(T,35,40);orog=orog;' \
    $ifile ${sdir}/out_diag/mon_ta6_$period.nc || echo ERROR
}; )&; }>$err.ta6.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta600 / table mon
{ (if_requested $member $esmod mon ta600 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta600=sellevel(T,600);' \
    $ifile ${sdir}/out_diag/mon_ta600_$period.nc || echo ERROR
}; )&; }>$err.ta600.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta70 / table mon
{ (if_requested $member $esmod mon ta70 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta70=sellevel(T,70);' \
    $ifile ${sdir}/out_diag/mon_ta70_$period.nc || echo ERROR
}; )&; }>$err.ta70.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta700 / table mon
{ (if_requested $member $esmod mon ta700 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta700=sellevel(T,700);' \
    $ifile ${sdir}/out_diag/mon_ta700_$period.nc || echo ERROR
}; )&; }>$err.ta700.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta750 / table mon
{ (if_requested $member $esmod mon ta750 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta750=sellevel(T,750);' \
    $ifile ${sdir}/out_diag/mon_ta750_$period.nc || echo ERROR
}; )&; }>$err.ta750.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta850 / table mon
{ (if_requested $member $esmod mon ta850 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta850=sellevel(T,850);' \
    $ifile ${sdir}/out_diag/mon_ta850_$period.nc || echo ERROR
}; )&; }>$err.ta850.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ta925 / table mon
{ (if_requested $member $esmod mon ta925 $chunk && {
  find_file -e            "$sdir" "*_*_*_T_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ta925=sellevel(T,925);' \
    $ifile ${sdir}/out_diag/mon_ta925_$period.nc || echo ERROR
}; )&; }>$err.ta925.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable tke6 / table mon
{ (if_requested $member $esmod mon tke6 $chunk && {
  find_file -e            "$sdir" "*_*_*_TKE_3D_mon_${period}.nc" ifile
  $cdo -f nc -O \
    setzaxis,${ZAXISFILE6} -expr,'tke6=sellevidxrange(TKE,35,40);orog=orog;' \
    $ifile ${sdir}/out_diag/mon_tke6_$period.nc || echo ERROR
}; )&; }>$err.tke6.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua10 / table mon
{ (if_requested $member $esmod mon ua10 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua10=sellevel(U,10);' \
    $ifile ${sdir}/out_diag/mon_ua10_$period.nc || echo ERROR
}; )&; }>$err.ua10.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua100 / table mon
{ (if_requested $member $esmod mon ua100 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua100=sellevel(U,100);' \
    $ifile ${sdir}/out_diag/mon_ua100_$period.nc || echo ERROR
}; )&; }>$err.ua100.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua1000 / table mon
{ (if_requested $member $esmod mon ua1000 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua1000=sellevel(U,1000);' \
    $ifile ${sdir}/out_diag/mon_ua1000_$period.nc || echo ERROR
}; )&; }>$err.ua1000.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua150 / table mon
{ (if_requested $member $esmod mon ua150 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua150=sellevel(U,150);' \
    $ifile ${sdir}/out_diag/mon_ua150_$period.nc || echo ERROR
}; )&; }>$err.ua150.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua20 / table mon
{ (if_requested $member $esmod mon ua20 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua20=sellevel(U,20);' \
    $ifile ${sdir}/out_diag/mon_ua20_$period.nc || echo ERROR
}; )&; }>$err.ua20.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua200 / table mon
{ (if_requested $member $esmod mon ua200 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua200=sellevel(U,200);' \
    $ifile ${sdir}/out_diag/mon_ua200_$period.nc || echo ERROR
}; )&; }>$err.ua200.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua250 / table mon
{ (if_requested $member $esmod mon ua250 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua250=sellevel(U,250);' \
    $ifile ${sdir}/out_diag/mon_ua250_$period.nc || echo ERROR
}; )&; }>$err.ua250.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua30 / table mon
{ (if_requested $member $esmod mon ua30 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua30=sellevel(U,30);' \
    $ifile ${sdir}/out_diag/mon_ua30_$period.nc || echo ERROR
}; )&; }>$err.ua30.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua300 / table mon
{ (if_requested $member $esmod mon ua300 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua300=sellevel(U,300);' \
    $ifile ${sdir}/out_diag/mon_ua300_$period.nc || echo ERROR
}; )&; }>$err.ua300.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua400 / table mon
{ (if_requested $member $esmod mon ua400 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua400=sellevel(U,400);' \
    $ifile ${sdir}/out_diag/mon_ua400_$period.nc || echo ERROR
}; )&; }>$err.ua400.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua50 / table mon
{ (if_requested $member $esmod mon ua50 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua50=sellevel(U,50);' \
    $ifile ${sdir}/out_diag/mon_ua50_$period.nc || echo ERROR
}; )&; }>$err.ua50.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua500 / table mon
{ (if_requested $member $esmod mon ua500 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua500=sellevel(U,500);' \
    $ifile ${sdir}/out_diag/mon_ua500_$period.nc || echo ERROR
}; )&; }>$err.ua500.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua6 / table mon
{ (if_requested $member $esmod mon ua6 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3D_mon_${period}.nc" ifile
  $cdo -f nc -O \
    setzaxis,${ZAXISFILE6} -expr,'ua6=sellevidxrange(U,35,40);orog=orog;' \
    $ifile ${sdir}/out_diag/mon_ua6_$period.nc || echo ERROR
}; )&; }>$err.ua6.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua600 / table mon
{ (if_requested $member $esmod mon ua600 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua600=sellevel(U,600);' \
    $ifile ${sdir}/out_diag/mon_ua600_$period.nc || echo ERROR
}; )&; }>$err.ua600.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua70 / table mon
{ (if_requested $member $esmod mon ua70 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua70=sellevel(U,70);' \
    $ifile ${sdir}/out_diag/mon_ua70_$period.nc || echo ERROR
}; )&; }>$err.ua70.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua700 / table mon
{ (if_requested $member $esmod mon ua700 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua700=sellevel(U,700);' \
    $ifile ${sdir}/out_diag/mon_ua700_$period.nc || echo ERROR
}; )&; }>$err.ua700.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua750 / table mon
{ (if_requested $member $esmod mon ua750 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua750=sellevel(U,750);' \
    $ifile ${sdir}/out_diag/mon_ua750_$period.nc || echo ERROR
}; )&; }>$err.ua750.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua850 / table mon
{ (if_requested $member $esmod mon ua850 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua850=sellevel(U,850);' \
    $ifile ${sdir}/out_diag/mon_ua850_$period.nc || echo ERROR
}; )&; }>$err.ua850.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable ua925 / table mon
{ (if_requested $member $esmod mon ua925 $chunk && {
  find_file -e            "$sdir" "*_*_*_U_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'ua925=sellevel(U,925);' \
    $ifile ${sdir}/out_diag/mon_ua925_$period.nc || echo ERROR
}; )&; }>$err.ua925.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va10 / table mon
{ (if_requested $member $esmod mon va10 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va10=sellevel(V,10);' \
    $ifile ${sdir}/out_diag/mon_va10_$period.nc || echo ERROR
}; )&; }>$err.va10.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va100 / table mon
{ (if_requested $member $esmod mon va100 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va100=sellevel(V,100);' \
    $ifile ${sdir}/out_diag/mon_va100_$period.nc || echo ERROR
}; )&; }>$err.va100.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va1000 / table mon
{ (if_requested $member $esmod mon va1000 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va1000=sellevel(V,1000);' \
    $ifile ${sdir}/out_diag/mon_va1000_$period.nc || echo ERROR
}; )&; }>$err.va1000.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va150 / table mon
{ (if_requested $member $esmod mon va150 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va150=sellevel(V,150);' \
    $ifile ${sdir}/out_diag/mon_va150_$period.nc || echo ERROR
}; )&; }>$err.va150.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va20 / table mon
{ (if_requested $member $esmod mon va20 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va20=sellevel(V,20);' \
    $ifile ${sdir}/out_diag/mon_va20_$period.nc || echo ERROR
}; )&; }>$err.va20.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va200 / table mon
{ (if_requested $member $esmod mon va200 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va200=sellevel(V,200);' \
    $ifile ${sdir}/out_diag/mon_va200_$period.nc || echo ERROR
}; )&; }>$err.va200.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va250 / table mon
{ (if_requested $member $esmod mon va250 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va250=sellevel(V,250);' \
    $ifile ${sdir}/out_diag/mon_va250_$period.nc || echo ERROR
}; )&; }>$err.va250.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va30 / table mon
{ (if_requested $member $esmod mon va30 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va30=sellevel(V,30);' \
    $ifile ${sdir}/out_diag/mon_va30_$period.nc || echo ERROR
}; )&; }>$err.va30.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va300 / table mon
{ (if_requested $member $esmod mon va300 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va300=sellevel(V,300);' \
    $ifile ${sdir}/out_diag/mon_va300_$period.nc || echo ERROR
}; )&; }>$err.va300.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va400 / table mon
{ (if_requested $member $esmod mon va400 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va400=sellevel(V,400);' \
    $ifile ${sdir}/out_diag/mon_va400_$period.nc || echo ERROR
}; )&; }>$err.va400.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va50 / table mon
{ (if_requested $member $esmod mon va50 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va50=sellevel(V,50);' \
    $ifile ${sdir}/out_diag/mon_va50_$period.nc || echo ERROR
}; )&; }>$err.va50.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va500 / table mon
{ (if_requested $member $esmod mon va500 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va500=sellevel(V,500);' \
    $ifile ${sdir}/out_diag/mon_va500_$period.nc || echo ERROR
}; )&; }>$err.va500.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va6 / table mon
{ (if_requested $member $esmod mon va6 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3D_mon_${period}.nc" ifile
  $cdo -f nc -O \
    setzaxis,${ZAXISFILE6} -expr,'va6=sellevidxrange(V,35,40);orog=orog;' \
    $ifile ${sdir}/out_diag/mon_va6_$period.nc || echo ERROR
}; )&; }>$err.va6.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va600 / table mon
{ (if_requested $member $esmod mon va600 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va600=sellevel(V,600);' \
    $ifile ${sdir}/out_diag/mon_va600_$period.nc || echo ERROR
}; )&; }>$err.va600.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va70 / table mon
{ (if_requested $member $esmod mon va70 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va70=sellevel(V,70);' \
    $ifile ${sdir}/out_diag/mon_va70_$period.nc || echo ERROR
}; )&; }>$err.va70.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va700 / table mon
{ (if_requested $member $esmod mon va700 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va700=sellevel(V,700);' \
    $ifile ${sdir}/out_diag/mon_va700_$period.nc || echo ERROR
}; )&; }>$err.va700.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va750 / table mon
{ (if_requested $member $esmod mon va750 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va750=sellevel(V,750);' \
    $ifile ${sdir}/out_diag/mon_va750_$period.nc || echo ERROR
}; )&; }>$err.va750.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va850 / table mon
{ (if_requested $member $esmod mon va850 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va850=sellevel(V,850);' \
    $ifile ${sdir}/out_diag/mon_va850_$period.nc || echo ERROR
}; )&; }>$err.va850.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable va925 / table mon
{ (if_requested $member $esmod mon va925 $chunk && {
  find_file -e            "$sdir" "*_*_*_V_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'va925=sellevel(V,925);' \
    $ifile ${sdir}/out_diag/mon_va925_$period.nc || echo ERROR
}; )&; }>$err.va925.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa10 / table mon
{ (if_requested $member $esmod mon wa10 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa10=sellevel(W,10);' \
    $ifile ${sdir}/out_diag/mon_wa10_$period.nc || echo ERROR
}; )&; }>$err.wa10.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa100 / table mon
{ (if_requested $member $esmod mon wa100 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa100=sellevel(W,100);' \
    $ifile ${sdir}/out_diag/mon_wa100_$period.nc || echo ERROR
}; )&; }>$err.wa100.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa1000 / table mon
{ (if_requested $member $esmod mon wa1000 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa1000=sellevel(W,1000);' \
    $ifile ${sdir}/out_diag/mon_wa1000_$period.nc || echo ERROR
}; )&; }>$err.wa1000.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa150 / table mon
{ (if_requested $member $esmod mon wa150 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa150=sellevel(W,150);' \
    $ifile ${sdir}/out_diag/mon_wa150_$period.nc || echo ERROR
}; )&; }>$err.wa150.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa20 / table mon
{ (if_requested $member $esmod mon wa20 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa20=sellevel(W,20);' \
    $ifile ${sdir}/out_diag/mon_wa20_$period.nc || echo ERROR
}; )&; }>$err.wa20.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa200 / table mon
{ (if_requested $member $esmod mon wa200 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa200=sellevel(W,200);' \
    $ifile ${sdir}/out_diag/mon_wa200_$period.nc || echo ERROR
}; )&; }>$err.wa200.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa250 / table mon
{ (if_requested $member $esmod mon wa250 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa250=sellevel(W,250);' \
    $ifile ${sdir}/out_diag/mon_wa250_$period.nc || echo ERROR
}; )&; }>$err.wa250.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa30 / table mon
{ (if_requested $member $esmod mon wa30 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa30=sellevel(W,30);' \
    $ifile ${sdir}/out_diag/mon_wa30_$period.nc || echo ERROR
}; )&; }>$err.wa30.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa300 / table mon
{ (if_requested $member $esmod mon wa300 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa300=sellevel(W,300);' \
    $ifile ${sdir}/out_diag/mon_wa300_$period.nc || echo ERROR
}; )&; }>$err.wa300.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa400 / table mon
{ (if_requested $member $esmod mon wa400 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa400=sellevel(W,400);' \
    $ifile ${sdir}/out_diag/mon_wa400_$period.nc || echo ERROR
}; )&; }>$err.wa400.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa50 / table mon
{ (if_requested $member $esmod mon wa50 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa50=sellevel(W,50);' \
    $ifile ${sdir}/out_diag/mon_wa50_$period.nc || echo ERROR
}; )&; }>$err.wa50.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa500 / table mon
{ (if_requested $member $esmod mon wa500 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa500=sellevel(W,500);' \
    $ifile ${sdir}/out_diag/mon_wa500_$period.nc || echo ERROR
}; )&; }>$err.wa500.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa6 / table mon
{ (if_requested $member $esmod mon wa6 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3D_mon_${period}.nc" ifile
  $cdo -f nc -O \
    setzaxis,${ZAXISFILE6} -expr,'wa6=sellevidxrange(W,35,40);orog=orog;' \
    $ifile ${sdir}/out_diag/mon_wa6_$period.nc || echo ERROR
}; )&; }>$err.wa6.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa600 / table mon
{ (if_requested $member $esmod mon wa600 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa600=sellevel(W,600);' \
    $ifile ${sdir}/out_diag/mon_wa600_$period.nc || echo ERROR
}; )&; }>$err.wa600.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa70 / table mon
{ (if_requested $member $esmod mon wa70 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa70=sellevel(W,70);' \
    $ifile ${sdir}/out_diag/mon_wa70_$period.nc || echo ERROR
}; )&; }>$err.wa70.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa700 / table mon
{ (if_requested $member $esmod mon wa700 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa700=sellevel(W,700);' \
    $ifile ${sdir}/out_diag/mon_wa700_$period.nc || echo ERROR
}; )&; }>$err.wa700.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa750 / table mon
{ (if_requested $member $esmod mon wa750 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa750=sellevel(W,750);' \
    $ifile ${sdir}/out_diag/mon_wa750_$period.nc || echo ERROR
}; )&; }>$err.wa750.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa850 / table mon
{ (if_requested $member $esmod mon wa850 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa850=sellevel(W,850);' \
    $ifile ${sdir}/out_diag/mon_wa850_$period.nc || echo ERROR
}; )&; }>$err.wa850.mon 2>&1

#-- Diagnostic for cosmo (ESM: COSMO) variable wa925 / table mon
{ (if_requested $member $esmod mon wa925 $chunk && {
  find_file -e            "$sdir" "*_*_*_W_3Dplev_mon_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'wa925=sellevel(W,925);' \
    $ifile ${sdir}/out_diag/mon_wa925_$period.nc || echo ERROR
}; )&; }>$err.wa925.mon 2>&1


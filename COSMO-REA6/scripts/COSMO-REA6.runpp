#!/bin/ksh
#
# PalMod .runpp
#
# Wrapper Script Agg+Diag+CMOR for COSMO-REA6 by Martin Schupfner, DKRZ
#
### Batch Queuing System is SLURM
#SBATCH --partition=compute
#SBATCH --nodes=1
#SBATCH --mem=250G
#SBATCH --time=02:00:00
#SBATCH --mail-type=none
######SBATCH --account=bk1261
#SBATCH --account=bm0021
#SBATCH --qos=esgf
#SBATCH --exclusive
#SBATCH --output=runpp_%j.log

DEBUG_LEVEL=${DEBUG_LEVEL:-0}

# Support log style output
export LANG=C
print () { command print "$(date +'%F %T'):" "$@"; }
print_re='^[0-9]+-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}'

warn () { print 'Hey:' "$@" >&2; }
die () { print 'Oops:' "$@" >&2; return 1; }

# Bail out on error
trap 'print Error at line $LINENO >&2' ERR
set -eu
# Print command info
[[ $DEBUG_LEVEL -ge 2 ]] && set -x

#module load nco || { echo "Oops: Could not load NCO!" && exit 1 ; }
alias ncatted=/sw/spack-levante/nco-5.0.6-3xkdth/bin/ncatted
alias fieldextra=/work/bk1261/OcMOD/COSMO-REA6/fieldextra1420/bin/fieldextra_gnu_opt_omp

# Disable core file creation
ulimit -c 0
# Set necessary fieldextra env variables
ulimit -s unlimited
export OMP_STACKSIZE=500M
export OMP_PLACES=sockets
export OMP_PROC_BIND="spread,close,close"

#echo $SLURM_TASKS_PER_NODE tasks per node
#echo $SLURM_NTASKS_PER_NODE ntasks per node

# Process ID
PID=$SLURM_JOB_ID
[[ -z "$PID" ]] && PID=$$

#########################################
# Define specifics about the simulation
#########################################

#Root directory of all scripts, outdata, logs etc
EXP_ID=REA6
EXP_DIR_TRUNK=/work/bk1261/OcMOD/COSMO-REA6
EXP_DIR_TRUNK=/work/kd1292/k204212/OcMOD

# RAW Data - path and sub-simulations
RAW_EXP_DIR_TRUNK=/work/bk1261/OcMOD/COSMO-REA6/outdata
RAW_EXP_DIR_TRUNK=/work/kd1292/k204212/OcMOD/outdata

#Work dir
WORK_DIR=$EXP_DIR_TRUNK/work

#Where to find the scripts (runpp etc.)
SCRIPT_DIR=${EXP_DIR_TRUNK}/scripts
SCRIPT_DIR=/work/bk1261/OcMOD/COSMO-REA6/scripts



############################
#Define necessary variables
############################

#Where to find cdo (incl. CMOR operator), eg.:
#cdo="/work/bm0021/cdo_incl_cmor/cdo-2022-05-11_cmor3.6.0_gcc/bin/cdo -v"
cdo="/work/bm0021/cdo_incl_cmor/cdo-2022-09-20_cmor3.6.0_gcc/bin/cdo -v"
cdo="/work/bm0021/cdo_incl_cmor/cdo-2023-03-02_cmor3.6.0_gcc/bin/cdo -v"
cdo="/work/bm0021/cdo_incl_cmor/cdo-2023-07-01_cmor3.6.0_gcc/bin/cdo -v"

#Grib-Tools grib_set
grib_set=/sw/spack-levante/mambaforge-4.11.0-0-Linux-x86_64-sobz6z/bin/grib_set

# Base directory for DataRequest related headers/scripts
SCRIPT_ROOT=${SCRIPT_DIR}

#Where to find the functions
fpath=${SCRIPT_ROOT}/functions

#Where to find the configuration
cpath=${SCRIPT_ROOT}/conf

#Models
frequencies=( 1hr 1hrPt day mon )
esmod=COSMO

#InfoTable(s)
# Define here the "cdocmorinfo"-File or "eum"-Files
# Multiple files possible with "," as delimiter, eg:
#  it=expinfo.txt,userinfo.txt,modelinfo.txt
it_temp="${SCRIPT_ROOT}/COSMO-REA6.cdocmorinfo"
#ca+=([submodel]="${SCRIPT_ROOT}/cdocmorinfo_submodel")

# Grid file
GRIDFILE="${SCRIPT_ROOT}/griddes_cosmo_180.txt"
GRIDFILE_BNDS="${SCRIPT_ROOT}/griddes_cosmo_180_bounds.txt"
GRIDFILE_PROJ="${SCRIPT_ROOT}/griddes_cosmo_projection.txt"
ZAXISFILE6="${SCRIPT_ROOT}/zaxisdes_6lvl.txt"
ZAXISFILE40="${SCRIPT_ROOT}/zaxisdes_40lvl.txt"
ZAXISFILEPLEV="${SCRIPT_ROOT}/zaxisdes_plev.txt"
ZAXISSOIL="${SCRIPT_ROOT}/zaxisdes_soil.txt"
FXFILE="$RAW_EXP_DIR_TRUNK/fx/COSMO_REA6_CONST_withOUTsponge.nc"

#Experiment etc
experiment=REA6
member=r1i1p1f1
mip=OcMOD
vd=v20230314
#vd=v$(date '+%Y%m%d') #CMOR version directory name, should be a date in format YYYYMMDD

#Where to store errors
logdir=${EXP_DIR_TRUNK}/logs
logdir=/work/bk1261/OcMOD/COSMO-REA6/logs
errdir=${logdir}/errors
loadBal=$logdir/LoadBalance_${PID}.txt
errdir_cmor=${errdir}_cmor
errdir_diag=${errdir}_diag
errdir_agg=${errdir}_agg

#initial and final year of the experiment
iniyear=1995
finyear=2019
inimonth=01
finmonth=08

# Command line options
RUN_AGG=true
RUN_DIAGS=true
RUN_CMOR=true
SUFFIX=
CLIMSUFFIX=
ARCHIVE_SUFFIX=
USE_CHUNKS=false
REMOVE_CHUNKS=false
PROCESS_CLIM=false
while [[ "$1" == -* ]]
do
    OPT=
    ARG=
    case "$1" in
        -d|--diags|--diags-only) RUN_CMOR=false; RUN_AGG=false;;
        -c|--cmor|--cmor-only) RUN_DIAGS=false; RUN_AGG=false;;
        -A|--agg|--agg-only) RUN_DIAGS=false; RUN_CMOR=false;;
        -C|--clim) PROCESS_CLIM=true;;
        -s|--suffix) OPT=-s; ARG=SUFFIX;;
        -S|--archive-suffix) OPT=-S; ARG=ARCHIVE_SUFFIX;;
        -f|--force) REMOVE_CHUNKS=true;;
        -a|--append) USE_CHUNKS=true;;
        --) shift; break;;
        -*) die "invalid option '$1'";;
    esac
    if [[ -n "$OPT" ]]
    then
        shift
        [[ $# -lt 1 ]] && die "missing argument for option '$OPT'"
        eval "$ARG='$1'"
    fi
    shift
done

#Write Output to:
dr_trunk=${EXP_DIR_TRUNK}/archive$ARCHIVE_SUFFIX
dr_trunk=/work/bk1261/OcMOD/COSMO-REA6/archive$ARCHIVE_SUFFIX

#time interval of the experiment you want to standardize
[[ -z "$2" ]] && [[ "$PROCESS_CLIM" == "false" ]] && die 'invalid number of parameters; need start year and end year as YYYY'
cmorstart=${1:-$iniyear}
cmorend=${2:-$finyear}



###############################################################################
# No user input beyond this line necessary (usually)
###############################################################################

# Store parameters
MESSAGE='OcMOD'
$RUN_AGG && MESSAGE+=' - Aggregation - '
$RUN_DIAGS && MESSAGE+=' - Diagnostics - '
$RUN_CMOR && MESSAGE+=" - CMOR rewriting - "
$PROCESS_CLIM && MESSAGE+=' for only climatological variables'
print "$(date +%Y-%m-%dT%H:%M:%S): $MESSAGE started for ${cmorstart}-${cmorend} $SUFFIX"

#exit 0



##############################
# Load the required functions
#   function_Read_request_config: read data request / user configuration and initialize the if_requested check
#   function_if_requested: checks if the currently processed variable is requested for the current timestep
#   function_find_file: searches for inputfiles
##############################
. $fpath/function_Read_request_config
. $fpath/function_if_requested
. $fpath/function_find_file

#Load predefined timeslices (depends on $iniyear/$finyear)
. $cpath/TimeSlices.h

##Additionally define custom TimeSlices
## Example on how to define TimeSlices
## format: YYYYMMDDHH-YYYYMMDDHH
##
#TimeSlices+=([custom1]=(1850010100-1850010100))
#TimeSlices+=([custom2]=(1850010100-1850123124))
##TimeSlice dependend on variable
#TimeSlices+=([custom3]=(${iniyear}010100-${iniyear}123124))
##TimeSlice dependend on arithmetics using variable
#TimeSlices+=([custom4]=($((iniyear+121))010100-$((iniyear+150))123124))

##Define SettingsContainer
## The following Option 3hrtest will activate the listed 3hr variables and deactivate any other variable for the given member (= realisation)
## The UserSettings for the given experiment have to contain the line:
## Option: 3hrtest = True
## or the setting will not be active!
#SettingsContainer+=([3hrtest]=([3hr]=(clt hfls hfss pr prc ps rlds rlus rsds rsus tas tos uas vas)))
#SettingsContainer[3hrtest]+=([${member}]=(${member}:False))

#Initialize DataRequest/User Configuration for the if_requested function
# This will read all SettingsContainers, TimeSlices and the configuration file
rrc_option=-s
[[ $DEBUG_LEVEL -ge 1 ]] && rrc_option=
[[ $DEBUG_LEVEL -ge 2 ]] && rrc_option=-v



#################################
# Run agg, diag and cmor-rewrite
#################################

function run_agg
{
    # Loop over file output periods and load diagnostic script fragment
    if $RUN_AGG; then

    for YYYY in $(seq ${cmorstart} ${cmorend}); do

    # perform time shift for selected variables
    #cdochain="-shifttime,-300s"

    for MM in 01 02 03 04 05 06 07 08 09 10 11 12; do

        if [[ ${YYYY}${MM} -lt ${iniyear}${inimonth} ]] || [[ ${YYYY}${MM} -gt ${finyear}${finmonth} ]]; then
            continue
        fi

        # Define the chunk that is tested by if_requested (YYYYMMDDHH-YYYYMMDDHH)
        period=${YYYY}${MM}
        chunk=${period}0100-${period}3124
        l_chunk=$(echo $chunk | cut -d "-" -f1)
        r_chunk=$(echo $chunk | cut -d "-" -f2)

        l_chunk_year=${l_chunk%??????}
        r_chunk_year=${r_chunk%??????}

        if [[ $MM -eq 12 ]]; then
            nextperiod=$((YYYY+1))01
        else
            nextperiod=${YYYY}$(printf "%02d" $((MM+1)))
        fi

        # Clean up stdout/stderr from previous run of this script
        errtemp_agg="${errdir_agg}${SUFFIX}${CLIMSUFFIX}/${period}"
        mkdir -p ${errtemp_agg}
        err="${errtemp_agg}/err${period}"
        rm -f ${err}*

        # Perform aggregation for each submodel
        for submodel in ${esmod}; do

            # Show progress
            print "$(date +%Y-%m-%dT%H:%M:%S): Aggregation started for ${EXP_ID} $submodel $chunk ($period) ..."
            sh -c 'scontrol update JobId=$SLURM_JOB_ID Comment="$*"' scomment aggr/${EXP_ID}/$submodel/$period

            # Location of input/output
            sdir=${EXP_DIR_TRUNK}/processed/
            rawsdir=${EXP_DIR_TRUNK}/outdata/
            rawsdir=$RAW_EXP_DIR_TRUNK/
            echo "... writing output to $sdir/out_aggr"
            echo "... writing log to $errtemp_agg"

            # Create output dirs
            mkdir -p ${sdir}/tmp_aggr
            mkdir -p ${sdir}/out_aggr

            # Create softlinks to raw model output
            #ln -sf $rawsdir/*_${period}* $sdir/ 2>/dev/null 1>&2 || { echo "ERROR creating links for $period ${EXP_ID} $submodel" && exit 1 ; }

            # Load and run the aggr ScriptFragment
            . ${SCRIPT_DIR}/ocmod_aggregation_${esmod}_${submodel}.h
            wait
            print "$(date +%Y-%m-%dT%H:%M:%S): Aggregation ended for ${EXP_ID} $submodel $chunk ($period) ..."

        done # submodel
    done # MM
    done # YYYY
    fi # if RUN_AGG
}

function run_diag
{
    # Loop over file output periods and load diagnostic script fragment
    if $RUN_DIAGS; then

    for YYYY in $(seq ${cmorstart} ${cmorend}); do

    # perform time shift for selected variables
    #cdochain="-shifttime,-300s"

    for MM in 01 02 03 04 05 06 07 08 09 10 11 12; do

        if [[ ${YYYY}${MM} -lt ${iniyear}${inimonth} ]] || [[ ${YYYY}${MM} -gt ${finyear}${finmonth} ]]; then
            continue
        fi

        # Define the chunk that is tested by if_requested (YYYYMMDDHH-YYYYMMDDHH)
        period=${YYYY}${MM}
        chunk=${period}0100-${period}3124
        l_chunk=$(echo $chunk | cut -d "-" -f1)
        r_chunk=$(echo $chunk | cut -d "-" -f2)

        l_chunk_year=${l_chunk%??????}
        r_chunk_year=${r_chunk%??????}

        # Clean up stdout/stderr from previous run of this script
        errtemp_diag="${errdir_diag}${SUFFIX}${CLIMSUFFIX}/${period}"
        mkdir -p ${errtemp_diag}
        err="${errtemp_diag}/err${period}"
        rm -f ${err}*

        # Perform aggregation for each submodel
        for submodel in ${esmod}; do


            # Show progress
            print "$(date +%Y-%m-%dT%H:%M:%S): Diagnostic started for ${EXP_ID} $submodel $chunk ($period) ..."
            sh -c 'scontrol update JobId=$SLURM_JOB_ID Comment="$*"' scomment diag/${EXP_ID}/$submodel/$period

            # Location of input/output
            sdir=${EXP_DIR_TRUNK}/processed/
            echo "... writing output to $sdir/out_diag"
            echo "... writing log to $errtemp_diag"

            # Diagnostic Output
            mkdir -p ${sdir}/tmp_diag
            mkdir -p ${sdir}/out_diag

            # Load and run the diag ScriptFragment
            . ${SCRIPT_DIR}/ocmod_diagnostic_${esmod}_${submodel}_auto.h
            wait
            print "$(date +%Y-%m-%dT%H:%M:%S): Diagnostic ended for ${EXP_ID} $submodel $chunk ($period) ..."

        done # submodel
    done # MM
    done # YYYY
    fi # if RUN_DIAG
}

function run_cmor
{
    # Loop over file output periods and load diagnostic script fragment
    if $RUN_CMOR; then

    # Use working directory to hold .CHUNK and other tmp files
    current_dir=${WORK_DIR}/cmor${SUFFIX}_${cmorstart}-${cmorend}${CLIMSUFFIX}
    if [[ -e $current_dir ]] && [[ "$PROCESS_CLIM" == "false" ]]
    then
        if $USE_CHUNKS
        then
            warn "appending to existing chunk"
        elif $REMOVE_CHUNKS
        then
            warn "moving chunk info to backup"
            mkdir -p ${WORK_DIR}/backup
            mv -v --backup=numbered $current_dir ${WORK_DIR}/backup
        else
            die "please check if cmor is running or has already been run for this chunk. Use --force to re-run"
        fi
    else
        if $USE_CHUNKS
        then
            die "cannot find chunk info for appending"
        fi
    fi
    mkdir -p $current_dir
    previous_dir=$PWD
    # Link gridinfo files
    #ln -sf $SCRIPT_DIR/gridinfo*nc $current_dir/
    cd $current_dir

    # Define DRS root dir
    dr=$dr_trunk/cmor${SUFFIX}_${cmorstart}-${cmorend}${CLIMSUFFIX}
    mkdir -p $dr


    for YYYY in $(seq ${cmorstart} ${cmorend}); do

    # perform time shift for selected variables
    #cdochain="-shifttime,-300s"

    for MM in 01 02 03 04 05 06 07 08 09 10 11 12; do

        if [[ ${YYYY}${MM} -lt ${iniyear}${inimonth} ]] || [[ ${YYYY}${MM} -gt ${finyear}${finmonth} ]]; then
            continue
        fi

        # Define the chunk that is tested by if_requested (YYYYMMDDHH-YYYYMMDDHH)
        period=${YYYY}${MM}
        chunk=${period}0100-${period}3124
        l_chunk=$(echo $chunk | cut -d "-" -f1)
        r_chunk=$(echo $chunk | cut -d "-" -f2)

        l_chunk_year=${l_chunk%??????}
        r_chunk_year=${r_chunk%??????}

        # Clean up stdout/stderr from previous run of this script
        errtemp_cmor="${errdir_cmor}${SUFFIX}${CLIMSUFFIX}/${period}"
        mkdir -p ${errtemp_cmor}
        err="${errtemp_cmor}/err${period}"
        rm -f ${err}*

        # Perform CMOR rewrite for each submodel
        for submodel in ${esmod}; do

            # Show progress
            print "$(date +%Y-%m-%dT%H:%M:%S): CMORisation started for ${EXP_ID} $submodel $chunk ($period) ..."
            sh -c 'scontrol update JobId=$SLURM_JOB_ID Comment="$*"' scomment cmor/${EXP_ID}/$submodel/$period

            # Location of input/output
            sdir=${EXP_DIR_TRUNK}/processed/
            echo "... writing output to $dr/$submodel"
            echo "... writing log to $errtemp_cmor"

            # Diagnostic Output
            mkdir -p $dr/$submodel

	    #Location of Mapping table:
            mt="${SCRIPT_DIR}/tables/${esmod}_${submodel}_${mip}_mapping.txt"

            #Define cdocmorinfo
            it="${it_temp}"  #,${ca[${submodel}]}

            # Load and run the diag ScriptFragment
            . ${SCRIPT_DIR}/ocmod_cmor-rewrite_${esmod}_${submodel}_auto.h
            wait
            print "$(date +%Y-%m-%dT%H:%M:%S): CMORisation ended for ${EXP_ID} $submodel $chunk ($period) ..."

        done # submodel
    done # MM
    done # YYYY

    # Run NCO ncatted cleanup of unappropriate/unnecessary attributes
    #  this is necessary since CMOR sets attributes necessary only for CMIP6
    #  that are unnecessary/wrong for PalMod
    #post_nco

    fi # if RUN_CMOR
}

function post_nco {

print "$(date +%Y-%m-%dT%H:%M:%S): NCO cleanup started for $dr"
# Load nco
#module load nco || { echo "Oops: Could not load NCO!" && exit 1 ; }

# Post Processing with ncatted
#Find netCDF files in DRS directory
flist=( $( find $dr -name "*.nc" -type f | sort ) )

#Loop and run ncatted
echo "-> Looping over ${#flist[@]} netCDF files!"
for f in ${flist[@]}
do
    echo "... editing $f"
    ncatted -O -h -a further_info_url,global,d,, -a mip_era,global,d,, $f && echo "... successful!" || echo "... failed (ERROR)"
done

print "$(date +%Y-%m-%dT%H:%M:%S): NCO cleanup ended for $dr"

}



#Initialize DataRequest/User Configuration for the if_requested function
# This will read all SettingsContainers, TimeSlices and the configuration file
# Thereafter run aggregation, diagnostic, cmorization
Read_request_config $rrc_option AllExp ${cpath}/${mip}_requested_vars_AllExp$SUFFIX.conf || die "error while reading request config"
$RUN_AGG && run_agg || echo "Could not run agg"
$RUN_DIAGS && run_diag || echo "Could not run diag"
$RUN_CMOR && run_cmor || echo "Could not run cmor"
$RUN_CMOR && post_nco || echo "Could not run NCO cleanup"
wait



print "$(date +%Y-%m-%dT%H:%M:%S): $MESSAGE finished for $cmorstart-$cmorend$SUFFIX"

# Update run dates and submit job scripts

# $Id$



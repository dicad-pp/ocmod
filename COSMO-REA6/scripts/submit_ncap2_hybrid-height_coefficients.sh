#!/bin/bash
#SBATCH -J ncap2_3D_correction         # Specify job name
#SBATCH -p compute
#SBATCH --mem=250G
#SBATCH -N 1                   # Specify number of nodes
#SBATCH -t 06:00:00            # Set a limit on the total run time
#SBATCH -A bm0021              # Charge resources on this project account
#SBATCH --qos=esgf
#SBATCH -o ncap2_3D_correction_%j.log     # File name for standard output
#SBATCH -e ncap2_3D_correction_%j.log     # File name for standard error output

HOSTNAME=$( hostname )

echo Running on $HOSTNAME
echo with PID $$

maxjobs=12

n=0 #Do not change

# Define frequency and parent directory
dir=/work/bk1261/OcMOD/COSMO-REA6/archive_3D/COSMO-REA

#Loop in parallel over parameters
for i in $( find $dir -type f -name "*.nc" )
do
  (
    echo "---------------------------------------------------"
    echo "$(date) Starting $i ..."
    bash ncap2_hybrid-height_coefficients.sh $i
    echo "... $(date) finished $i"
    echo "---------------------------------------------------"
  )&
  # limit jobs
  if (( $(($((++n)) % $maxjobs)) == 0 )) ; then
     wait # wait until all have finished (not optimal, but most times good enough)
     #echo $n wait
  fi
done
wait

exit 0

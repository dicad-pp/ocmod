#!/bin/bash
# Process COSMO-REA variables provided by MB from
#  ECMWF tape archive

# Parent directory for the downloaded files, folder structure of URLs will be craeted below this directory
DL_BASE=/work/bk1261/dwd/
#DL_BASE=/work/kd1292/k204212/OcMOD/download_ecmwf/

# OUTDATA Base (where to extract the downloaded files to)
OUTDATA_BASE=/work/bk1261/OcMOD/COSMO-REA6/outdata_ecmwf
#OUTDATA_BASE=/work/kd1292/k204212/OcMOD/outdata_ecmwf
#OUTDATA_BASE=/work/kd1292/k204212/OcMOD/outdata_3D

# Grid(s)
GRID="2D 3D"
#GRID=3D
#GRID=2D

# Frequencies to download
FREQ=hourly

# Filter for a single variable
VAR=
#VAR=PP
#VAR=QV
#VAR=TKE
#VAR=V
#VAR=U
#VAR=T
#VAR=W


# Filter for a single YYYY / YYYYMM / YYYYMMDD
#TIME=199604
TIME=

#########################################################################################

# Copy / Extract DWD opendata archives / files to destination directory (OUTDATA_BASE/<frequency>/<variable>.<grid>/)
function extract {

  { [[ -z "$1" ]] || [[ -z "$2" ]] || [[ -z "$3" ]] || [[ -z "$4" ]] ; } && { echo "ERROR: Parameter(s) not set in function extract!" ; exit 1 ; }

  l_outpath=$1
  l_var=$(echo $2 | sed "s,/,,g" )
  l_vargrid=${l_var}.$3
  l_freq=$4

  l_dldir=$( dirname $l_outpath )
  l_file=$( basename $l_outpath )

  grib_set=/sw/spack-levante/mambaforge-4.11.0-0-Linux-x86_64-sobz6z/bin/grib_set

  [[ ! -d "$l_dldir" ]] && { echo "ERROR: directory '$l_dldir' does not exist!" ; exit 1 ; }
  [[ ! -e "$l_outpath" ]] && { echo "ERROR: file '$l_outpath' does not exist!" ; exit 1 ; }

  l_f=
  [[ "$l_freq" == "hourly" ]] && {
  IntervalVars=( AUMFL_S AVMFL_S TMIN_2M VGUST_DYN VGUST_CON VABSMX_1M VABSMX_10M HTOP_CON VMAX_1M TMAX_2M VMAX_10M ATHB_T ASWDIFU_S ASOD_T ASOB_T ASHFL_S ALHFL_S ALHFL_PL ALHFL_BS ALB_RAD ATHD_S ATHB_S ASWDIR_S ASWDIFD_S ASOB_S ALWU_S TDIV_HUM AEVAP_S SNOW_GSP SNOW_CON RUNOFF_G RUNOFF_S DURSUN RAIN_GSP RAIN_CON TOT_PRECIP AUMFLS AVMFLS TMIN2M VGUSTDYN VGUSTCON VABSMX1M VABSMX10M HTOPCON VMAX1M TMAX2M VMAX10M ATHBT ASWDIFUS ASODT ASOBT ASHFLS ALHFLS ALHFLPL ALHFLBS ALBRAD ATHDS ATHBS ASWDIRS ASWDIFDS ASOBS ALWUS TDIVHUM AEVAPS SNOWGSP SNOWCON RUNOFFG RUNOFFS DURSUN RAINGSP RAINCON TOTPRECIP)
  l_f=1hrPt
  [[ " ${IntervalVars[@]} " == *" $l_var "* ]] && l_f=1hr
  }
  [[ "$l_freq" == "daily" ]] && l_f=day
  [[ -z "$l_f" ]] && { echo "ERROR: The frequency '$l_freq' could not be translated!" ; exit 1; }

  # Create target path
  tmp=tmp/tmp_$(uuid)
  mkdir -p $tmp
  l_targetdir=$OUTDATA_BASE/$l_f/$(echo $l_vargrid | sed "s/_//g" )
  mkdir -p $l_targetdir || { echo "ERROR: Could not create directory '$l_targetdir'!"; exit 1 ; }
  if [[ "$l_file" == *".grb" ]]; then
    \cp -v $l_outpath $tmp/$( echo $l_file | sed "s/_//g" ) || { echo "ERROR: Could not copy file '$l_outpath' to '$l_targetdir'!"; exit 1 ; }
  elif [[ "$l_file" == *".nc4" ]]; then
    \cp -v $l_outpath $tmp/$( echo $l_file | sed "s/_//g" ) || { echo "ERROR: Could not copy file '$l_outpath' to '$l_targetdir'!"; exit 1 ; }
  elif [[ "$l_file" == *".nc" ]]; then
    \cp -v $l_outpath $tmp/$( echo $l_file | sed "s/_//g" ) || { echo "ERROR: Could not copy file '$l_outpath' to '$l_targetdir'!"; exit 1 ; }
  elif [[ "$l_file" == *".grb.bz2" ]]; then
    bzip2 -fckd $l_outpath > $tmp/$( echo $l_file | rev | cut -d "." -f2- | rev | sed "s/_//g" ) || { echo "ERROR: Could not extract file '$l_outpath' to '$l_targetdir/$( echo $l_file | rev | cut -d "." -f2- | rev )'!"; exit 1 ; }
  elif [[ "$l_file" == *".tgz" ]]; then
    tar --transform='flags=r;s|_||g' --overwrite -xvf $l_outpath -C $tmp || { echo "ERROR: Could not extract file '$l_outpath' to '$l_targetdir'!" ; exit 1 ; }
  elif [[ "$l_file" == *".tar.bz2" ]]; then
    tar --transform='flags=r;s|_||g' --overwrite -xvf $l_outpath -C $tmp || { echo "ERROR: Could not extract file '$l_outpath' to '$l_targetdir'!" ; exit 1 ; }
  else
    echo "ERROR: No rule to extract '$l_outpath'!"
    exit 1
  fi

  for l_grbfile in $( find $tmp -type f -or -type l -name "*.grb" ); do
    l_bn=$(basename $l_grbfile)
    $grib_set -stimeRangeIndicator=0 $l_grbfile $l_targetdir/$l_bn || { echo "ERROR: grib_set failed for $l_grbfile" ; exit 1 ; }
    echo "grib_set -stimeRangeIndicator=0 $l_grbfile $l_targetdir/$l_bn"
  done
  #ls $tmp/*.nc &>/dev/null && mv -v $tmp/*.nc $l_targetdir
  #ls $tmp/*.nc4 &>/dev/null && mv -v $tmp/*.nc4 $l_targetdir
  find $tmp \( -type f -or -type l \) -and \( -name "*.nc" -or -name "*.nc4" \) -exec cp -v {} $l_targetdir \; || echo "ERROR"
  find $tmp \( -type f -or -type l \) -and \( -not -name "*.nc" -and -not -name "*.grb" -and -not -name "*.nc4" \) -exec sh -c 'echo "ERROR: no rule to treat {}"' \;
}


# Search DWD opendata server for desired files and download, eventually call extraction function
for f in $FREQ
do
  for g in $GRID
  do
    if [[ "$f" == "hourly" ]]; then
        if [[ -z "$TIME" ]]; then
            if [[ -z "$VAR" ]]; then
                [[ "$g" == "3D" ]] && FILELIST=( $(find $DL_BASE -mindepth 2 -maxdepth 3 -type f -or -type l | grep "3D" ) )
                [[ "$g" == "2D" ]] && FILELIST=( $(find $DL_BASE -mindepth 2 -maxdepth 3 -type f -or -type l | grep -v "3D" ) )
            else
                [[ "$g" == "3D" ]] && FILELIST=( $(find $DL_BASE -mindepth 2 -maxdepth 3 -type f -or -type l | grep "${VAR}" | grep "3D" ) )
                [[ "$g" == "2D" ]] && FILELIST=( $(find $DL_BASE -mindepth 2 -maxdepth 3 -type f -or -type l | grep "${VAR}" | grep -v "3D" ) )
            fi
        else
            if [[ -z "$VAR" ]]; then
                [[ "$g" == "3D" ]] && FILELIST=( $(find $DL_BASE -mindepth 2 -maxdepth 3 -type f -or -type l | grep -E "\.${TIME}|_${TIME}" | grep "3D" ) )
                [[ "$g" == "2D" ]] && FILELIST=( $(find $DL_BASE -mindepth 2 -maxdepth 3 -type f -or -type l | grep -E "\.${TIME}|_${TIME}" | grep -v "3D" ) )
            else
                [[ "$g" == "3D" ]] && FILELIST=( $(find $DL_BASE -mindepth 2 -maxdepth 3 -type f -or -type l | grep "${VAR}" | grep -E "\.${TIME}|_${TIME}" | grep "3D" ) )
                [[ "$g" == "2D" ]] && FILELIST=( $(find $DL_BASE -mindepth 2 -maxdepth 3 -type f -or -type l | grep "${VAR}" | grep -E "\.${TIME}|_${TIME}" | grep -v "3D" ) )
            fi
        fi
    elif [[ "$f" == "daily" ]]; then
        if [[ -z "$TIME" ]]; then
            if [[ -z "$VAR" ]]; then
                [[ "$g" == "3D" ]] && FILELIST=( $(find $DL_BASE/*/daily -type f -or -type l | grep "3D" ) )
                [[ "$g" == "2D" ]] && FILELIST=( $(find $DL_BASE/*/daily -type f -or -type l | grep -v "3D" ) )
            else
                [[ "$g" == "3D" ]] && FILELIST=( $(find $DL_BASE/*/daily -type f -or -type l | grep "${VAR}" | grep "3D" ) )
                [[ "$g" == "2D" ]] && FILELIST=( $(find $DL_BASE/*/daily -type f -or -type l | grep "${VAR}" | grep -v "3D" ) )
            fi
        else
            if [[ -z "$VAR" ]]; then
                [[ "$g" == "3D" ]] && FILELIST=( $(find $DL_BASE/*/daily -type f -or -type l | grep -E "\.${TIME}|_${TIME}" | grep "3D" ) )
                [[ "$g" == "2D" ]] && FILELIST=( $(find $DL_BASE/*/daily -type f -or -type l | grep -E "\.${TIME}|_${TIME}" | grep -v "3D" ) )
            else
                [[ "$g" == "3D" ]] && FILELIST=( $(find $DL_BASE/*/daily -type f -or -type l | grep "${VAR}" | grep -E "\.${TIME}|_${TIME}" | grep "3D" ) )
                [[ "$g" == "2D" ]] && FILELIST=( $(find $DL_BASE/*/daily -type f -or -type l | grep "${VAR}" | grep -E "\.${TIME}|_${TIME}" | grep -v "3D" ) )
            fi
        fi
    fi

    echo "Found ${#FILELIST[@]} files under $DL_BASE for frequency '$f' and $g grid, with time filter '$TIME' and variable filter '$VAR'. Processing..."

    # parallelization settings
    n=0
    maxjobs=6

    for i in $(seq 0 $((${#FILELIST[@]}-1)))
    do
      (
       [[ -z "${FILELIST[$i]}" ]] && { echo "ERROR: Empty filename. Something went wrong ..." ; continue ;}
       i_bn=$(basename ${FILELIST[$i]})
       echo "+++ $i_bn"

       if [[ "$g" == "3D" ]]; then
           v=$( echo $i_bn | cut -d "." -f 1 )
       elif [[ "$f" == "daily" ]]; then
           v=$( echo $i_bn | awk 'BEGIN { FS=".2D." } { print $1 }' | sed "s/_//g" | sed "s/\.//g" )
       else
           echo $i_bn | grep -q "2D" && {
               v=$( echo $i_bn | awk 'BEGIN { FS=".2D." } { print $1 }' | sed "s/_//g" | sed "s/\.//g" )
           } || {
               v=$( echo $i_bn | rev | cut -d "." -f 2- | rev | sed "s/_//g" | sed "s/\.//g" )
               v=${v::-4}
           }
       fi
       echo ${FILELIST[$i]} ${v} ${g} $f
       #continue

       extract ${FILELIST[$i]} ${v} ${g} $f
      )&
        if (( $(($((++n)) % $maxjobs)) == 0 )) ; then
          wait # wait until all have finished (not optimal, but most times good enough)
        fi
      done # FILELIST
      wait
  done # GRID
done # FREQ


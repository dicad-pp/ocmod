#!/bin/bash
#SBATCH -J checktime         # Specify job name
#SBATCH -p compute
#SBATCH --mem=250G
#SBATCH -N 1                   # Specify number of nodes
#SBATCH -t 08:00:00            # Set a limit on the total run time
#SBATCH -A bm0021              # Charge resources on this project account
#SBATCH --qos=esgf
#SBATCH -o checktime     # File name for standard output
#SBATCH -e checktime     # File name for standard error output

#Nohup:
# nohup bash Run_Job.sh > nohup_tTERMTODO.out 2>&1 &
# salloc -p compute -A bm0021 -n 1 -t 60 -- /bin/bash -c 'ssh -X $SLURM_JOB_NODELIST'

HOSTNAME=$( hostname )

echo Running on $HOSTNAME
echo with PID $$


maxjobs=12

n=0 #Do not change

#Unpredictable extension
#filename="${script##*/}"

#Usual extension 
filename=$(basename -- "$script") #/path/to/script.py -> script.py

#Extract Filename and Extension
extension="${filename##*.}"
filename="${filename%.*}"

# Define frequency and parent directory
freq=1hrPt # mon day 1hr
dir=/work/bk1261/COSMO-REA

#Loop in parallel over parameters
#for var in hfls hfss ad100m ad125m ad150m ad175m ad200m ad40m ad60m ad80m clt hurs huss pr prc prls prsn prsnc prsnls prw ps psl rlds rlus rsds rsdsdir rsus sfcWind sund tas tasmax tasmin ts uas vas wd100m wd10m wd125m wd150m wd175m wd200m wd40m wd60m wd80m ws100m ws125m ws150m ws175m ws200m ws40m ws60m ws80m wsgsmax zmla mrro mrros snw snc snd
#for var in albs hfls hfss rlut rsdt rsut sfcWindmax tauu tauv wsgscmax wsgstmax clh clivi cll clm clwvi rlut rsdt rsut tauu tauv tdps twp zf mrfsol mrsol tsl evspsbl
for var in hus tke ta ua va wa pfull
do
  (
     echo "$(date) Starting ${var}($freq) ..."
     python checktime.py -f $freq -v $var -d $dir > checktime_${freq}_$var.txt 2>&1
     echo "... $(date) finished ${var}($freq)."
  )&
  # limit jobs
  if (( $(($((++n)) % $maxjobs)) == 0 )) ; then
     wait # wait until all have finished (not optimal, but most times good enough)
     #echo $n wait
  fi
done
wait

exit 0

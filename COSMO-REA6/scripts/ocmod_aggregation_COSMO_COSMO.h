################################
# COSMO ########################
################################


# Phase 1 - Parameters on dwd opendata server
if_requested $member $esmod aggregation phase1 $chunk && {
  vars_1hr="ASWDIRS ASWDIFDS ASOBS ALWUS ATHDS DURSUN"
  vars_1hrPt="T2M PMSL PS RELHUM2M CLCT HPBL QV2M SWDIRSRAD SWDIFDSRAD SOBSRAD THBSRAD THDSRAD TQV TSOIL TSNOW WSNOW HSNOW RHOSNOW LHFLS SHFLS"
  vars_1hrAcc="TOTPRECIP RAINCON RAINGSP SNOWGSP SNOWCON RUNOFFS RUNOFFG"
  vars_1hrMax="VMAX10M"
  vars_1hrPtConv="WS010 WS040 WS060 WS080 WS100 WS125 WS150 WS175 WS200 WD010 WD040 WD060 WD080 WD100 WD125 WD150 WD175 WD200 DENS040 DENS060 DENS080 DENS100 DENS125 DENS150 DENS175 DENS200"
  vars_1hr3D=""
  vars_1hrTOday=""
  vars_1hrTOdayAcc=""
  vars_1hrPtTOday=""
  vars_6hrMax="TMAX2M"
  vars_6hrMin="TMIN2M"
  vars_day="T2M PMSL PS RELHUM2M ASWDIRS ASWDIFDS ASOBS ALWUS ATHDS CLCT HPBL QV2M TQV TSOIL TSNOW WSNOW HSNOW RHOSNOW LHFLS SHFLS"
  vars_dayAcc="DURSUN TOTPRECIP RAINCON RAINGSP SNOWGSP SNOWCON RUNOFFS RUNOFFG"
  vars_dayMax="TMAX2M VMAX10M"
  vars_dayMin="TMIN2M"
  vars_dayConv="WS010 WS040 WS060 WS080 WS100 WS125 WS150 WS175 WS200 WD010 WD040 WD060 WD080 WD100 WD125 WD150 WD175 WD200 DENS040 DENS060 DENS080 DENS100 DENS125 DENS150 DENS175 DENS200"
  vars_day3D=""
  vars_mon="TMAX2M TMIN2M VMAX10M T2M PMSL PS RELHUM2M ASWDIRS ASWDIFDS ASOBS ALWUS ATHDS CLCT HPBL QV2M TQV TSOIL TSNOW WSNOW HSNOW RHOSNOW"
  vars_monAcc="DURSUN TOTPRECIP RAINCON RAINGSP SNOWGSP SNOWCON RUNOFFS RUNOFFG"
  vars_monConv="WS010 WS040 WS060 WS080 WS100 WS125 WS150 WS175 WS200 WD010 WD040 WD060 WD080 WD100 WD125 WD150 WD175 WD200 DENS040 DENS060 DENS080 DENS100 DENS125 DENS150 DENS175 DENS200"
  vars_mon3D=""
  vars_2Dto3D=""
}

# Phase 2 - Parameters in ECMWF archive & wind speed, more complex 3D variables
if_requested $member $esmod aggregation phase2 $chunk && {
  if_requested $member $esmod aggregation phase1 $chunk && {
    vars_1hr="$vars_1hr ALBRAD ALHFLS ASHFLS ASOBT ASODT ASWDIFUS ATHBT AUMFLS AVMFLS"
    vars_1hrPt="$vars_1hrPt CLCH CLCM CLCL HZEROCL SOBTRAD SODTRAD SWDIFUSRAD TD2M TG THBTRAD TQC TQI TWATER UMFLS VMFLS"
    vars_1hrAcc="$vars_1hrAcc AEVAPS"
    vars_1hrMax="$vars_1hrMax VABSMX10M VGUSTCON VGUSTDYN"
    vars_1hr3D="$vars_1hr3D"
    vars_1hrTOday="ALBRAD ALHFLS ASHFLS ASOBT ASODT ASWDIFUS ATHBT AUMFLS AVMFLS"
    vars_1hrTOdayAcc="AEVAPS"
    vars_1hrTOdayMax="VABSMX10M VGUSTCON VGUSTDYN"
    vars_1hrPtTOday="CLCH CLCM CLCL HZEROCL TD2M TG TQC TQI TWATER"
    vars_6hrMax="$vars_6hrMax"
    vars_6hrMin="$vars_6hrMin"
    vars_day="$vars_day"
    vars_dayAcc="$vars_dayAcc"
    vars_dayMax="$vars_dayMax"
    vars_dayMin="$vars_dayMin"
    vars_day3D="$vars_day3D"
    vars_mon="$vars_mon"
    vars_monAcc="$vars_monAcc"
    vars_mon3D="$vars_mon3D"
    vars_2Dto3D="TSOIL WSOIL WSOIL.ICE"
  } || {
    vars_1hr="ALBRAD ALHFLS ASHFLS ASOBT ASODT ASWDIFUS ATHBT AUMFLS AVMFLS"
    vars_1hrPt="CLCH CLCM CLCL HZEROCL SOBTRAD SODTRAD SWDIFUSRAD TD2M TG THBTRAD TQC TQI TWATER UMFLS VMFLS"
    vars_1hrAcc="AEVAPS"
    vars_1hrMax="VABSMX10M VGUSTCON VGUSTDYN"
    vars_1hr3D=""
    vars_1hrTOday="ALBRAD ALHFLS ASHFLS ASOBT ASODT ASWDIFUS ATHBT AUMFLS AVMFLS"
    vars_1hrTOdayAcc="AEVAPS"
    vars_1hrTOdayMax="VABSMX10M VGUSTCON VGUSTDYN"
    vars_1hrPtTOday="CLCH CLCM CLCL HZEROCL TD2M TG TQC TQI TWATER"
    vars_6hrMax=""
    vars_6hrMin=""
    vars_day=""
    vars_dayAcc=""
    vars_dayMax=""
    vars_dayMin=""
    vars_day3D=""
    vars_mon=""
    vars_monAcc=""
    vars_mon3D=""
    vars_2Dto3D="TSOIL WSOIL WSOIL.ICE"
  }
}

# Phase 3 - Remaining parameters
if_requested $member $esmod aggregation phase3 $chunk && {
    vars_1hr=""
    vars_1hrPt=""
    vars_1hrAcc=""
    vars_1hrMax=""
    vars_1hr3D="TKE"
    vars_1hr3D40="QV T"
    vars_pfull="P"
    vars_1hrUV="U V"
    vars_1hrTOday=""
    vars_1hrTOdayAcc=""
    vars_1hrTOdayMax=""
    vars_1hrPtTOday=""
    vars_6hrMax=""
    vars_6hrMin=""
    vars_day=""
    vars_dayAcc=""
    vars_dayMax=""
    vars_dayMin=""
    vars_day3D=""
    vars_mon=""
    vars_monAcc=""
    vars_mon3D=""
    vars_2Dto3D=""
}





###############
# fx
###############

# under construction
# potentially to be added:
#  toz, vegFrac, treeFrac
#  landCoverFrac (incl. plant coverage, forest types?), surface geopot., lake depth
#
# info required about:
# mrsofc (soil water storage capacity), sftgif (glacier fraction), dtb (depth to bedrock)


#-- zaxis hybrid height - orog basis - setgrid operation
{ (if_requested $member $esmod aggregation HSURF $chunk && {
  if [[ $YYYY -lt 2007 ]] || [[ $YYYY -gt 2012 ]]; then
    echo "Using HSURF_1995-2006_2013-2019.grb ..."
    ifile="$rawsdir/fx/HSURF_1995-2006_2013-2019.grb"
  else
    echo "Using HSURF_2007-2012.grb ..."
    ifile="$rawsdir/fx/HSURF_2007-2012.grb"
  fi
  ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_HSURF_2D_fx_${YYYY}.nc
  ofile2=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_HSURF_2D_fx_${YYYY}.grb
  [[ ! -e $ofile ]] && {
  $cdo -f nc4 setgrid,${GRIDFILE} -setmissval,9999 -setname,orog $ifile ${ofile} || echo "ERROR: $ofile setgrid operation"
  } || echo "HSURF ZAXIS file already present: '$ofile'"
  [[ ! -e $ofile2 ]] && {
  cp -v $ifile $ofile2 || echo "ERROR copying HSURF FIELDEXTRA input file '$ifile'."
  } || echo "HSURF ZAXIS FIELDEXTRA input file already present: '$ofile2'"
}; )& ; }>$err.fx.HSURF.ZAXIS.2D 2>&1

#-- areacella - gridarea calculation - setgrid operation
{ (if_requested $member $esmod aggregation fx $chunk && {
  ifile="$rawsdir/fx/COSMO_REA6_CONST_withOUTsponge.nc"
  ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_AREACELLA_2D_fx.nc
  ofile_tmp=$sdir/tmp_aggr/${esmod}_${EXP_ID}_${member}_AREACELLA_2D_fx.nc_tmp
  [[ ! -e $ofile ]] && {
  $cdo -f nc4 -setgrid,${GRIDFILE_BNDS} -setmissval,9999 -selname,HSURF $ifile ${ofile_tmp} || echo "ERROR: $ofile_tmp setgrid operation"
  $cdo -f nc4 setname,AREACELLA -gridarea $ofile_tmp ${ofile} && rm -f $ofile_tmp || echo "ERROR: $ofile grid area calculation"
  } || echo "File already present: '$ofile'"
}; )& ; }>$err.fx.AREACELLA.2D 2>&1

#-- land fraction excl lakes - setgrid operation
{ (if_requested $member $esmod aggregation fx $chunk && {
  ifile="$rawsdir/fx/COSMO_REA6_CONST_withOUTsponge.nc"
  ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_FRLAND_2D_fx.nc
  [[ ! -e $ofile ]] && {
  $cdo -f nc4 setname,FRLAND -setgrid,${GRIDFILE} -setmissval,9999 -selname,FR_LAND $ifile ${ofile} || echo "ERROR: $ofile setgrid operation"
  } || echo "File already present: '$ofile'"
}; )& ; }>$err.fx.FRLAND.2D 2>&1

#-- lake fraction - setgrid operation
{ (if_requested $member $esmod aggregation fx $chunk && {
  ifile="$rawsdir/fx/COSMO_REA6_CONST_withOUTsponge.nc"
  ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_FRLAKE_2D_fx.nc
  [[ ! -e $ofile ]] && {
  $cdo -f nc4 setname,FRLAKE -setgrid,${GRIDFILE} -setmissval,9999 -selname,FR_LAKE $ifile ${ofile} || echo "ERROR: $ofile setgrid operation"
  } || echo "File already present: '$ofile'"
}; )& ; }>$err.fx.FRLAKE.2D 2>&1

#-- root_depth - setgrid operation
{ (if_requested $member $esmod aggregation fx $chunk && {
  ifile="$rawsdir/fx/COSMO_REA6_CONST_withOUTsponge.nc"
  ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_ROOTDP_2D_fx.nc
  [[ ! -e $ofile ]] && {
  $cdo -f nc4 -setgrid,${GRIDFILE} -setmissval,9999 -selname,ROOTDP $ifile ${ofile} || echo "ERROR: $ofile setgrid operation"
  } || echo "File already present: '$ofile'"
}; )& ; }>$err.fx.ROOTDP.2D 2>&1

#-- lai - setgrid operation
{ (if_requested $member $esmod aggregation fx $chunk && {
  ifile="$rawsdir/fx/COSMO_REA6_CONST_withOUTsponge.nc"
  ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_LAI_2D_fx.nc
  [[ ! -e $ofile ]] && {
  $cdo -f nc4 setgrid,${GRIDFILE} -setmissval,9999 -selname,LAI $ifile ${ofile} || echo "ERROR: $ofile
 setgrid operation"
  } || echo "File already present: '$ofile'"
}; )& ; }>$err.fx.LAI.2D 2>&1

#-- z0 - setgrid operation
{ (if_requested $member $esmod aggregation fx $chunk && {
  ifile="$rawsdir/fx/COSMO_REA6_CONST_withOUTsponge.nc"
  ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_Z0_2D_fx.nc
  [[ ! -e $ofile ]] && {
  $cdo -f nc4 setgrid,${GRIDFILE} -setmissval,9999 -selname,Z0 $ifile ${ofile} || echo "ERROR: $ofile
 setgrid operation"
  } || echo "File already present: '$ofile'"
}; )& ; }>$err.fx.Z0.2D 2>&1

#-- tsl - extraction of mask
#   only the lowermost layer has the correct mask applied,
#   need to extract it to hence apply it to all other layers
{ (if_requested $member $esmod aggregation tsoilmask $chunk && {
  ifile="$rawsdir/1hrPt/TSOIL1458CM.2D/TSOIL.1458CM.2D.${period}.grb"
  tsoilmask=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_tsoilmask_2D.nc
  $cdo -f nc4 -setmisstoc,0 -ifthenc,1 -setctomiss,0 -setmissval,9999 -seltimestep,1 $ifile $tsoilmask || echo "ERROR creating mask f
or TSOIL/tsl"
}; )& ; }>$err.fx.tsoilmask.2D 2>&1





wait #-- wait for all fx variables to be constructed, as they may be required below





###############
# FIELDEXTRA
###############


#-- 1hrPt - 3D - U,V 6lvl/40lvl FIELDEXTRA de-stagger, cdo rotuvb & concat to monthly chunks
{ if_requested $member $esmod aggregation 1hrPt3DUVFX $chunk && {
    freq=1hrPt ; fld=3D
    echo "aggpp | ${freq}.UVFX.${fld}        | begin $(date)">>$loadBal
    ifilebase="$rawsdir/$freq/U.${fld}/U.${fld}"
    ifiles=( $(ls ${ifilebase}.${period}010{1..9}??.grb ${ifilebase}.${period}01{10..23}??.grb ${ifilebase}.${period}0{2..9}????.grb ${ifilebase}.${period}{1..3}?????.grb ${ifilebase}.${nextperiod}010000.grb | sort -u) )
    ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_UV_${fld}_${freq}_$period.nc
    maxjobs=24
    n=0 #Do not change
    fxtmpdir=$sdir/tmp_aggr/UV_tmp_$(uuid)
    mkdir -p $fxtmpdir || echo "ERROR creating $fxtmpdir"
    for ifile in ${ifiles[@]}; do
        (
          tstmpdir=$fxtmpdir/ts_$(uuid)
          mkdir -p $tstmpdir || echo "ERROR creating $tstmpdir"
          ifile_z="$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_HSURF_2D_fx_$YYYY.grb"
          ofile_tmp=$tstmpdir/$(basename $ifile | sed -e "s,U,UV,g" | sed -e "s,.grb,.nc,g" )
          ofile_ts=$fxtmpdir/$(basename $ifile | sed -e "s,U,UV,g" | sed -e "s,.grb,.nc,g" )
          ifile_timestamp=$(basename $ifile | cut -d "." -f 3 | head -c 10)

          # Remove sponge if necessary (ToDo: if uncommented, filenames need to be adjusted for fieldextra script below)
          #$cdo verifygrid $ifile | grep "Grid consists of" | grep -q -v "(848x824)" && {
          #  echo "Removing sponge ..."
          #  $cdo selindexbox,17,864,17,840 $ifile $tstmpdir/$(basename $ifile) || echo "ERROR: selindexbox U"
          #}
          #[[ ! -e $tstmpdir/$(basename $ifile) ]] && {
          #  ln -s $ifile $tstmpdir/$(basename $ifile) || echo "ERROR: creating link for infile U"
          #}
          #$cdo verifygrid $(echo $ifile | sed -e "s,U,V,g" ) | grep "Grid consists of" | grep -q -v "(848x824)" && {
          #  echo "Removing sponge ..."
          #  $cdo selindexbox,17,864,17,840 $(echo $ifile | sed -e "s,U,V,g" ) $tstmpdir/$(basename $ifile | sed -e "s,U,V,g" ) || echo "ERROR: selindexbox V"
          #}
          #[[ ! -e $tstmpdir/$(basename $ifile | sed -e "s,U,V,g" ) ]] && {
          #  ln -s $(echo $ifile | sed -e "s,U,V,g" ) $tstmpdir/$(basename $ifile | sed -e "s,U,V,g" ) || echo "ERROR: creating link for infile V"
          #}

          if_requested $member $esmod FillerFiles FillerFiles ${ifile_timestamp}-${ifile_timestamp} && {
              # For missing-value time steps, add custom created input file
              cd $tstmpdir
              echo "WARNING (Chunk: '$chunk' time-step: '$ifile_timestamp'): Using filler file to make up for missing/lost data!"
              ifile_missing=$rawsdir/fx/UV.3D.FILLVALUE.nc
              missdate=${ifile_timestamp:0:8}
              misstime=${ifile_timestamp:8}0000
              $cdo settime,$misstime -setdate,$missdate $ifile_missing $ofile_tmp || echo "ERROR: set up filler file $ofile_tmp"
          } || {
              # FIELDEXTRA - prepare and execute namelist
              sed -e "s,HSURFINFILE,$ifile_z,g" -e "s,OUTFILE,$ofile_tmp,g" -e "s,INFILEU,$ifile,g" -e "s,INFILEV,$(echo $ifile | sed -e 's,U,V,g' ),g" $SCRIPT_DIR/fx_COSMO-REA_destagger.nl > $tstmpdir/fx.nl || echo "ERROR sed"
              cd $tstmpdir
              fieldextra fx.nl > log 2>&1 || echo "ERROR running FIELDEXTRA"
              grep -E "WARNING|Program successfully completed|ALERT" log | sort -u > log_grep
          }

          # CDO - prepare and execute vector rotation of U and V to geographical system
          { ofile_mask=$tstmpdir/$(basename $ifile | sed -e "s,U,UV_mask,g" | sed -e "s,.grb,.nc,g" )
            echo "$cdo -f nc4 -setgrid,${GRIDFILE} -expr,'maskU=(isMissval(U_destaggered)?0:1);maskV=(isMissval(V_destaggered)?0:1);' -setmissval,9999 $ofile_tmp $ofile_mask"
            $cdo -f nc4 -setgrid,${GRIDFILE} -expr,"maskU=(isMissval(U_destaggered)?0:1);maskV=(isMissval(V_destaggered)?0:1);" -setmissval,9999 $ofile_tmp $ofile_mask || echo "ERROR: creating U V missing value masks"
            echo "$cdo expr,'U=U_destaggered*maskU*maskV;V=V_destaggered*maskU*maskV' -merge -setctomiss,0 -setmissval,9999 $ofile_mask -rotuvb,U_destaggered,V_destaggered -setmisstoc,0 -setgrid,${GRIDFILE_PROJ} $ofile_tmp $ofile_ts"
            $cdo expr,"U=U_destaggered*maskU*maskV;V=V_destaggered*maskU*maskV" -merge -setctomiss,0 -setmissval,9999 $ofile_mask -rotuvb,U_destaggered,V_destaggered -setmisstoc,0 -setgrid,${GRIDFILE_PROJ} $ofile_tmp $ofile_ts || echo "ERROR cdo rotuvb" ; }> log_rot 2>&1
        )&
        if (( $(($((++n)) % $maxjobs)) == 0 )) ; then
            wait # wait until all have finished (not optimal, but most times good enough)
            #echo $n wait
        fi
     done
     wait
     grep -hv " Summary of WARNING/ALERT " $fxtmpdir/ts_*/log_grep | sort -u | sed -e "s/ALERT/ERROR/g"
     cat $fxtmpdir/ts_*/log_rot | sort -u
     #$cdo mergetime [ $(echo ${ifiles[@]} | sed -e "s,$ifilebase,$fxtmpdir/UV.3D,g" | sed -e "s,U,UV,g" | sed -e "s,UVV,UV,g" | sed -e "s,.grb,.nc,g" ) ] $ofile  && rm -rf $fxtmpdir || echo "ERROR mergetime"
     $cdo mergetime [ $(echo ${ifiles[@]} | sed -e "s,$ifilebase,$fxtmpdir/UV.3D,g" | sed -e "s,U,UV,g" | sed -e "s,UVV,UV,g" | sed -e "s,.grb,.nc,g" ) ] $ofile  || echo "ERROR mergetime"
     echo "aggpp | ${freq}.UVFX.${fld}        | end   $(date)">>$loadBal
}; }>$err.1hrPt.UVFX.3D 2>&1


#-- 1hrPt - 3D - U,V 40lvl FIELDEXTRA de-stagger + pressure level interpolation, cdo rotuvb & concat to monthly chunks
{ if_requested $member $esmod aggregation 1hrPt3DUVPLEVFX $chunk && {
    freq=1hrPt ; fld=3D
    echo "aggpp | ${freq}.UVPLEVFX.${fld}        | begin $(date)">>$loadBal
    ifilebase="$rawsdir/$freq/U.${fld}/U.${fld}"
    ifiles=( $(ls ${ifilebase}.${period}010{1..9}??.grb ${ifilebase}.${period}01{10..23}??.grb ${ifilebase}.${period}0{2..9}????.grb ${ifilebase}.${period}{1..3}?????.grb ${ifilebase}.${nextperiod}010000.grb | sort -u) )
    ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_UVFX_3Dplev_${freq}_$period.nc
    maxjobs=24
    n=0 #Do not change
    fxtmpdir=$sdir/tmp_aggr/UVplev_tmp_$(uuid)
    mkdir -p $fxtmpdir || echo "ERROR creating $fxtmpdir"
    for ifile in ${ifiles[@]}; do
        (
          tstmpdir=$fxtmpdir/ts_$(uuid)
          mkdir -p $tstmpdir || echo "ERROR creating $tstmpdir"
          ifile_z="$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_HSURF_2D_fx_${YYYY}.grb"
          ofile_tmp=$tstmpdir/$(basename $ifile | sed -e "s,U,UV,g" | sed -e "s,.grb,.nc,g" )
          ofile_ts=$fxtmpdir/$(basename $ifile | sed -e "s,U,UV,g" | sed -e "s,.grb,.nc,g" )
          ifile_timestamp=$(basename $ifile | cut -d "." -f 3 | head -c 10)

          if_requested $member $esmod FillerFiles FillerFiles ${ifile_timestamp}-${ifile_timestamp} && {
              # For missing-value time steps, add custom created input file
              cd $tstmpdir
              echo "WARNING (Chunk: '$chunk' time-step: '$ifile_timestamp'): Using filler file to make up for missing/lost data!"
              ifile_missing=$rawsdir/fx/UV.3DPlev.FILLVALUE.nc
              missdate=${ifile_timestamp:0:8}
              misstime=${ifile_timestamp:8}0000
              $cdo settime,$misstime -setdate,$missdate $ifile_missing $ofile_tmp || echo "ERROR: set up filler file $ofile_tmp"
          } || {
              # FIELDEXTRA - prepare and execute namelist
              sed -e "s,HSURFINFILE,$ifile_z,g" -e "s,PPFILE,$(echo $ifile | sed -e 's,U,PP,g' ),g" -e "s,OUTFILE,$ofile_tmp,g" -e "s,INFILEU,$ifile,g" -e "s,INFILEV,$(echo $ifile | sed -e 's,U,V,g' ),g" $SCRIPT_DIR/fx_COSMO-REA_vertical_interpolation_destagger.nl > $tstmpdir/fx.nl || echo "ERROR sed"
              cd $tstmpdir
              fieldextra fx.nl > log 2>&1 || echo "ERROR running FIELDEXTRA"
              grep -E "WARNING|Program successfully completed|ALERT" log | sort -u > log_grep
          }
          # CDO - prepare and execute vector rotation of U and V to geographical system
          { ofile_mask=$tstmpdir/$(basename $ifile | sed -e "s,U,UV_mask,g" | sed -e "s,.grb,.nc,g" )
            echo "$cdo -f nc4 -setgrid,${GRIDFILE} -expr,'maskU=(isMissval(U_destaggered)?0:1);maskV=(isMissval(V_destaggered)?0:1);' -setmissval,9999 $ofile_tmp $ofile_mask"
            $cdo -f nc4 -setgrid,${GRIDFILE} -expr,"maskU=(isMissval(U_destaggered)?0:1);maskV=(isMissval(V_destaggered)?0:1);" -setmissval,9999 $ofile_tmp $ofile_mask || echo "ERROR: creating U V missing value masks"
            echo "$cdo expr,'U=U_destaggered*maskU*maskV;V=V_destaggered*maskU*maskV' -merge -setctomiss,0 -setmissval,9999 $ofile_mask -rotuvb,U_destaggered,V_destaggered -setmisstoc,0 -setgrid,${GRIDFILE_PROJ} -selname,U_destaggered,V_destaggered $ofile_tmp $ofile_ts"
            $cdo expr,"U=U_destaggered*maskU*maskV;V=V_destaggered*maskU*maskV" -merge -setctomiss,0 -setmissval,9999 $ofile_mask -rotuvb,U_destaggered,V_destaggered -setmisstoc,0 -setgrid,${GRIDFILE_PROJ} -selname,U_destaggered,V_destaggered $ofile_tmp $ofile_ts || echo "ERROR cdo rotuvb" ; }> log_rot 2>&1
        )&
        if (( $(($((++n)) % $maxjobs)) == 0 )) ; then
            wait # wait until all have finished (not optimal, but most times good enough)
            #echo $n wait
        fi
     done
     wait
     grep -hv " Summary of WARNING/ALERT " $fxtmpdir/ts_*/log_grep | sort -u | sed -e "s/ALERT/ERROR/g"
     cat $fxtmpdir/ts_*/log_rot | sort -u
     #$cdo mergetime [ $(echo ${ifiles[@]} | sed -e "s,$ifilebase,$fxtmpdir/UV.3D,g" | sed -e "s,U,UV,g" | sed -e "s,UVV,UV,g" | sed -e "s,.grb,.nc,g" ) ] $ofile  && rm -rf $fxtmpdir || echo "ERROR mergetime"
     $cdo mergetime [ $(echo ${ifiles[@]} | sed -e "s,$ifilebase,$fxtmpdir/UV.3D,g" | sed -e "s,U,UV,g" | sed -e "s,UVV,UV,g" | sed -e "s,.grb,.nc,g" ) ] $ofile  || echo "ERROR mergetime"
     echo "aggpp | ${freq}.UVFX.${fld}        | end   $(date)">>$loadBal
}; }>$err.1hrPt.UVFX.3Dplev 2>&1


#-- 1hrPt - 3D - Q, T, ... 40lvl FIELDEXTRA pressure level interpolation & concat to monthly chunks
{ if_requested $member $esmod aggregation 1hrPt3DPLEVFX $chunk && {
    freq=1hrPt ; fld=3D
    echo "aggpp | ${freq}.PLEVFX.${fld}        | begin $(date)">>$loadBal
    ifilebase="$rawsdir/$freq/T.${fld}/T.${fld}"
    ifiles=( $(ls ${ifilebase}.${period}010{1..9}??.grb ${ifilebase}.${period}01{10..23}??.grb ${ifilebase}.${period}0{2..9}????.grb ${ifilebase}.${period}{1..3}?????.grb ${ifilebase}.${nextperiod}010000.grb | sort -u) )
    ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_FX_3Dplev_${freq}_$period.nc
    maxjobs=24
    n=0 #Do not change
    fxtmpdir=$sdir/tmp_aggr/plev_tmp_$(uuid)
    mkdir -p $fxtmpdir || echo "ERROR creating $fxtmpdir"
    for ifile in ${ifiles[@]}; do
        (
          tstmpdir=$fxtmpdir/ts_$(uuid)
          mkdir -p $tstmpdir || echo "ERROR creating $tstmpdir"
          ifile_z="$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_HSURF_2D_fx_${YYYY}.grb"
          ofile_ts=$fxtmpdir/$(basename $ifile | sed -e "s,T,PLEV,g" | sed -e "s,.grb,.nc,g" )
          ifile_timestamp=$(basename $ifile | cut -d "." -f 3 | head -c 10)

          if_requested $member $esmod FillerFiles FillerFiles ${ifile_timestamp}-${ifile_timestamp} && {
              # For missing-value time steps, add custom created input file
              cd $tstmpdir
              echo "WARNING (Chunk: '$chunk' time-step: '$ifile_timestamp'): Using filler file to make up for missing/lost data!"
              ifile_missing=$rawsdir/fx/PLEV.3D.FILLVALUE.nc
              missdate=${ifile_timestamp:0:8}
              misstime=${ifile_timestamp:8}0000
              $cdo -settime,$misstime -setdate,$missdate $ifile_missing $ofile_ts || echo "ERROR: set up filler file $ofile_ts"
          } || {
              # FIELDEXTRA - prepare and execute namelist
              sed -e "s,HSURFINFILE,$ifile_z,g" -e "s,PPFILE,$(echo $ifile | sed -e 's,T,PP,g' ),g" -e "s,OUTFILE,$ofile_ts,g" -e "s,TFILE,$ifile,g" -e "s,QFILE,$(echo $ifile | sed -e 's,T,QV,g' ),g" $SCRIPT_DIR/fx_COSMO-REA_vertical_interpolation.nl > $tstmpdir/fx.nl || echo "ERROR sed"
              cd $tstmpdir
              fieldextra fx.nl > log 2>&1 || echo "ERROR running FIELDEXTRA"
              grep -E "WARNING|Program successfully completed|ALERT" log | sort -u > log_grep
          }
        )&
        if (( $(($((++n)) % $maxjobs)) == 0 )) ; then
            wait # wait until all have finished (not optimal, but most times good enough)
            #echo $n wait
        fi
     done
     wait
     grep -hv " Summary of WARNING/ALERT " $fxtmpdir/ts_*/log_grep | sort -u | sed -e "s/ALERT/ERROR/g"
     #$cdo mergetime [ $(echo ${ifiles[@]} | sed -e "s,$ifilebase,$fxtmpdir/PLEV.3D,g" | sed -e "s,.grb,.nc,g" ) ] $ofile  && rm -rf $fxtmpdir || echo "ERROR mergetime"
     $cdo chname,QV_on_p,QV,T_on_p,T -mergetime [ $(echo ${ifiles[@]} | sed -e "s,$ifilebase,$fxtmpdir/PLEV.3D,g" | sed -e "s,.grb,.nc,g" ) ] $ofile  || echo "ERROR mergetime"
     echo "aggpp | ${freq}.PLEVFX.${fld}        | end   $(date)">>$loadBal
}; }>$err.1hrPt.PLEV.3Dplev 2>&1


#-- 1hrPt - 3D - P - 40lvl FIELDEXTRA pressure field generation & concat to monthly chunks
{ if_requested $member $esmod aggregation 1hrPt3DPLEVFX $chunk || {
    if_requested $member $esmod aggregation pfull $chunk && {
    freq=1hrPt ; fld=3D
    echo "aggpp | ${freq}.PFULL.${fld}        | begin $(date)">>$loadBal
    ifilebase="$rawsdir/$freq/PP.${fld}/PP.${fld}"
    ifiles=( $(ls ${ifilebase}.${period}010{1..9}??.grb ${ifilebase}.${period}01{10..23}??.grb ${ifilebase}.${period}0{2..9}????.grb ${ifilebase}.${period}{1..3}?????.grb ${ifilebase}.${nextperiod}010000.grb | sort -u) )
    ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_FX_PFULL_3D_${freq}_$period.nc
    maxjobs=24
    n=0 #Do not change
    fxtmpdir=$sdir/tmp_aggr/pfull_tmp_$(uuid)
    mkdir -p $fxtmpdir || echo "ERROR creating $fxtmpdir"
    for ifile in ${ifiles[@]}; do
        (
          tstmpdir=$fxtmpdir/ts_$(uuid)
          mkdir -p $tstmpdir || echo "ERROR creating $tstmpdir"
          ifile_z="$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_HSURF_2D_fx_${YYYY}.grb"
          ofile_ts=$fxtmpdir/$(basename $ifile | sed -e "s,.grb,.nc,g" )
          ifile_timestamp=$(basename $ifile | cut -d "." -f 3 | head -c 10)

          if_requested $member $esmod FillerFiles FillerFiles ${ifile_timestamp}-${ifile_timestamp} && {
              # For missing-value time steps, add custom created input file
              cd $tstmpdir
              echo "WARNING (Chunk: '$chunk' time-step: '$ifile_timestamp'): Using filler file to make up for missing/lost data!"
              ifile_missing=$rawsdir/fx/UV.3D.FILLVALUE.nc
              missdate=${ifile_timestamp:0:8}
              misstime=${ifile_timestamp:8}0000
              $cdo -selname,P -settime,$misstime -setdate,$missdate $ifile_missing $ofile_ts || echo "ERROR: set up filler file $ofile_ts"
          } || {
              # FIELDEXTRA - prepare and execute namelist
              sed -e "s,HSURFINFILE,$ifile_z,g" -e "s,PPFILE,$ifile,g" -e "s,OUTFILE,$ofile_ts,g" $SCRIPT_DIR/fx_COSMO-REA_pfull.nl > $tstmpdir/fx.nl || echo "ERROR sed"
              cd $tstmpdir
              fieldextra fx.nl > log 2>&1 || echo "ERROR running FIELDEXTRA"
              grep -E "WARNING|Program successfully completed|ALERT" log | sort -u > log_grep
          }
        )&
        if (( $(($((++n)) % $maxjobs)) == 0 )) ; then
            wait # wait until all have finished (not optimal, but most times good enough)
            #echo $n wait
        fi
     done
     wait
     grep -hv " Summary of WARNING/ALERT " $fxtmpdir/ts_*/log_grep | sort -u | sed -e "s/ALERT/ERROR/g"
     #$cdo mergetime [ $(echo ${ifiles[@]} | sed -e "s,$ifilebase,$fxtmpdir/PP.3D,g" | sed -e "s,.grb,.nc,g" ) ] $ofile  && rm -rf $fxtmpdir || echo "ERROR mergetime"
     $cdo mergetime [ $(echo ${ifiles[@]} | sed -e "s,$ifilebase,$fxtmpdir/PP.3D,g" | sed -e "s,.grb,.nc,g" ) ] $ofile  || echo "ERROR mergetime"
     echo "aggpp | ${freq}.PFULL.${fld}        | end   $(date)">>$loadBal
}; }; }>$err.1hrPt.PFULL.3D 2>&1





wait #-- wait for all FIELDEXTRA operations to be finished, as they may be required below





###############
# uas / vas
###############


#-- 1hrPt - 2D - uas / vas - set-grid operation, setmissval to 9999, merge, rotuvb
{ (if_requested $member $esmod aggregation 1hr $chunk && if_requested $member $esmod uas 1hr $chunk && {
    freq=1hrPt; fld=2D
    echo "aggpp | ${freq}.U10M.V10M.${fld}        | begin $(date)">>$loadBal
    ifileu="$rawsdir/$freq/U10M.${fld}/U10M.${fld}.${period}.grb"
    ifilev="$rawsdir/$freq/V10M.${fld}/V10M.${fld}.${period}.grb"
    ofile_tmp=$sdir/tmp_aggr/${esmod}_${EXP_ID}_${member}_UV10Munmasked_${fld}_${freq}_$period.nc; rm -f $ofile_tmp
    ofile_umask=$sdir/tmp_aggr/${esmod}_${EXP_ID}_${member}_U10Mmask_${fld}_${freq}_$period.nc; rm -f $ofile_umask
    ofile_vmask=$sdir/tmp_aggr/${esmod}_${EXP_ID}_${member}_V10Mmask_${fld}_${freq}_$period.nc; rm -f $ofile_vmask
    ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_UV10M_${fld}_${freq}_$period.nc; rm -f $ofile
    echo $cdo -f nc4 -setgrid,${GRIDFILE} -expr,"mask33=(isMissval(var33)?0:1)" -setmissval,9999 $ifileu $ofile_umask
    $cdo -f nc4 -setgrid,${GRIDFILE} -expr,"mask33=(isMissval(var33)?0:1)" -setmissval,9999 $ifileu $ofile_umask || echo "ERROR: creating u missing value mask"
    echo $cdo -f nc4 -setgrid,${GRIDFILE} -expr,"mask34=(isMissval(var34)?0:1)" -setmissval,9999 $ifilev $ofile_vmask
    $cdo -f nc4 -setgrid,${GRIDFILE} -expr,"mask34=(isMissval(var34)?0:1)" -setmissval,9999 $ifilev $ofile_vmask || echo "ERROR: creating v missing value mask"
    echo $cdo -f nc4 -setgrid,${GRIDFILE} -rotuvb,var33,var34 -merge -setmisstoc,9999 $ifileu -setmisstoc,9999 $ifilev $ofile_tmp
    $cdo -f nc4 -setgrid,${GRIDFILE} -rotuvb,var33,var34 -merge -setmisstoc,9999 $ifileu -setmisstoc,9999 $ifilev $ofile_tmp || echo "ERROR: $ofile vector rotation and merge operation"
    echo $cdo setmissval,9999 -expr,"var33=mask33*mask34*var33;var34=mask33*mask34*var34;" -merge -setctomiss,0 -setmissval,9999 $ofile_umask -setctomiss,0 -setmissval,9999 $ofile_vmask $ofile_tmp $ofile
    $cdo setmissval,9999 -expr,"var33=mask33*mask34*var33;var34=mask33*mask34*var34;" -merge -setctomiss,0 -setmissval,9999 $ofile_umask -setctomiss,0 -setmissval,9999 $ofile_vmask $ofile_tmp $ofile || echo "ERROR: masking operation"
    echo "aggpp | ${freq}.U10M.V10M.${fld}        | end   $(date)">>$loadBal
}; )& ; }>$err.1hr.U10M.V10M.2D 2>&1


#-- day - 2D - uas / vas - set-grid operation, setmissval to 9999, merge, rotuvb
{ (if_requested $member $esmod aggregation day $chunk && if_requested $member $esmod uas day $chunk && {
    freq=day ; fld=2D
    echo "aggpp | ${freq}.U10M.V10M.${fld}        | begin $(date)">>$loadBal
    ifileu="$rawsdir/$freq/U10M.${fld}/U10M.${fld}.${period}.DayMean.grb"
    ifilev="$rawsdir/$freq/V10M.${fld}/V10M.${fld}.${period}.DayMean.grb"
    ofile_tmp=$sdir/tmp_aggr/${esmod}_${EXP_ID}_${member}_UV10Munmasked_${fld}_${freq}_$period.nc; rm -f $ofile_tmp
    ofile_umask=$sdir/tmp_aggr/${esmod}_${EXP_ID}_${member}_U10Mmask_${fld}_${freq}_$period.nc; rm -f $ofile_umask
    ofile_vmask=$sdir/tmp_aggr/${esmod}_${EXP_ID}_${member}_V10Mmask_${fld}_${freq}_$period.nc; rm -f $ofile_vmask
    ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_UV10M_${fld}_${freq}_$period.nc; rm -f $ofile
    $cdo -f nc4 -setgrid,${GRIDFILE} -expr,"mask33=(isMissval(var33)?0:1)" -setmissval,9999 $ifileu $ofile_umask || echo "ERROR: creating u missing value mask"
    $cdo -f nc4 -setgrid,${GRIDFILE} -expr,"mask34=(isMissval(var34)?0:1)" -setmissval,9999 $ifilev $ofile_vmask || echo "ERROR: creating v missing value mask"
    $cdo -f nc4 -setgrid,${GRIDFILE} -rotuvb,var33,var34 -merge -setmisstoc,9999 $ifileu -setmisstoc,9999 $ifilev $ofile_tmp || echo "ERROR: $ofile vector rotation and merge operation"
    $cdo setmissval,9999 -expr,"var33=mask33*mask34*var33;var34=mask33*mask34*var34;" -merge -setctomiss,0 -setmissval,9999 $ofile_umask -setctomiss,0 -setmissval,9999 $ofile_vmask $ofile_tmp $ofile || echo "ERROR: masking operation"
    echo "aggpp | ${freq}.U10M.V10M.${fld}        | end   $(date)">>$loadBal
}; )& ; }>$err.day.U10M.V10M.2D 2>&1


#-- mon - 2D - uas / vas - set-grid operation, concat/split to monthly chunks, setmissval to 9999, merge, rotuvb
{ (if_requested $member $esmod aggregation mon $chunk && if_requested $member $esmod uas mon $chunk && {
    freq=mon ; fld=2D
    echo "aggpp | ${freq}.U10M.V10M.${fld}        | begin $(date)">>$loadBal
    ifileuYYYY="$rawsdir/$freq/U10M.${fld}/U10M.${fld}.${YYYY}.MonMean.grb"
    ifilevYYYY="$rawsdir/$freq/V10M.${fld}/V10M.${fld}.${YYYY}.MonMean.grb"
    ifileuMM="$rawsdir/$freq/U10M.${fld}/U10M.${fld}.${period}.MonMean.grb"
    ifilevMM="$rawsdir/$freq/V10M.${fld}/V10M.${fld}.${period}.MonMean.grb"
    ifileu=$sdir/tmp_aggr/U10M.${fld}.MonMean.${period}.grb
    ifilev=$sdir/tmp_aggr/V10M.${fld}.MonMean.${period}.grb
    ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_UV10M_${fld}_${freq}_$period.nc; rm -f $ofile


    if [[ -f $ifileuYYYY ]] && { [[ $period -eq ${YYYY}01 ]] || [[ $period -eq ${iniyear}${inimonth} ]]; }; then
        $cdo -O splitmon $ifileuYYYY $sdir/tmp_aggr/U10M.${fld}.MonMean.${YYYY} || echo "ERROR: $ifileuYYYY splitmon operation"
        $cdo -O splitmon $ifilevYYYY $sdir/tmp_aggr/V10M.${fld}.MonMean.${YYYY} || echo "ERROR: $ifilevYYYY splitmon operation"
    elif [[ ! -f $ifileu ]]; then
        \cp -v $ifileuMM $ifileu || echo "ERROR creating hard link at $ifileu for $ifileuMM"
        \cp -v $ifilevMM $ifilev || echo "ERROR creating hard link at $ifilev for $ifilevMM"
    fi

    $cdo -f nc4 -setgrid,${GRIDFILE} -rotuvb,var33,var34 -merge -setmissval,9999 $ifileu -setmissval,9999 $ifilev $ofile || echo "ERROR: $ofile vector rotation and merge operation"
    echo "aggpp | ${freq}.U10M.V10M.${fld}        | end   $(date)">>$loadBal
}; )& ; }>$err.mon.U10M.V10M.2D 2>&1



###############
# 2D to 3D
###############


#-- 1hrPt - 2D to 3D - tsoil, wsoil, wsoilice - set-grid operation, compose z-axis, setmissval to 9999
agg_1hrPt_2Dto3D() {
    { (if_requested $member $esmod aggregation 1hrPt $chunk && {
        freq=1hrPt ; varx=${1-} ; fld=2D
        [[ -z "$varx" ]] && { echo "Function ${0}: no variable name supplied" ; exit 1; }
        var=$(echo $varx | sed "s/_//g" | sed "s/\.//g" )
        echo "aggpp | ${freq}.${var}.2Dto3D        | begin $(date)">>$loadBal
        ifile1="$rawsdir/$freq/${var}1CM.${fld}/${varx}.1CM.${fld}.${period}.grb"
        ifile2="$rawsdir/$freq/${var}2CM.${fld}/${varx}.2CM.${fld}.${period}.grb"
        ifile3="$rawsdir/$freq/${var}6CM.${fld}/${varx}.6CM.${fld}.${period}.grb"
        ifile4="$rawsdir/$freq/${var}18CM.${fld}/${varx}.18CM.${fld}.${period}.grb"
        ifile5="$rawsdir/$freq/${var}54CM.${fld}/${varx}.54CM.${fld}.${period}.grb"
        ifile6="$rawsdir/$freq/${var}162CM.${fld}/${varx}.162CM.${fld}.${period}.grb"
        ifile7="$rawsdir/$freq/${var}486CM.${fld}/${varx}.486CM.${fld}.${period}.grb"
        ifile8="$rawsdir/$freq/${var}1458CM.${fld}/${varx}.1458CM.${fld}.${period}.grb"
        ofile="$rawsdir/$freq/${var}.3D/${var}.3D.${period}.grb"
        ofile_nc=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_3D_${freq}_$period.nc; rm -f $ofile_nc
        mkdir -p $rawsdir/${freq}/${var}.3D || echo "ERROR: Creating directory for $ofile"
        CDO_FILE_SUFFIX=.grb $cdo -z szip -merge -setlevel,1 $ifile1 -setlevel,2 $ifile2 -setlevel,3 $ifile3 -setlevel,4 $ifile4 -setlevel,5 $ifile5 -setlevel,6 $ifile6 -setlevel,7 $ifile7 -setlevel,8 $ifile8 $ofile || echo "ERROR: $ifile1 merge operation"
        if [[ "$var" == "TSOIL" ]]; then
            tsoilmask=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_tsoilmask_2D.nc
            $cdo -f nc4 -setgrid,${GRIDFILE} -setzaxis,${ZAXISSOIL} -setname,$var -setctomiss,0 -setmissval,9999 -mul $tsoilmask $ofile $ofile_nc || echo "ERROR: $ofile setgrid operation"
        else
            $cdo -f nc4 -setgrid,${GRIDFILE} -setzaxis,${ZAXISSOIL} -setname,$var -setmissval,9999 $ofile $ofile_nc || echo "ERROR: $ofile setgrid operation"
        fi
        echo "aggpp | ${freq}.${var}.2Dto3D        | end   $(date)">>$loadBal
    }; )& ; }>$err.1hrPt.${1-var}.2Dto3D 2>&1
}


#-- daily means out of hourly instantaneous data - 2D to 3D - tsoil, wsoil, wsoilice - set-grid operation, compose z-axis, setmissval to 9999
agg_1hrPtTOday_2Dto3D() {
    { (if_requested $member $esmod aggregation day $chunk && {
        freq=1hrPt ; varx=${1-} ; fld=2D
        [[ -z "$varx" ]] && { echo "Function ${0}: no variable name supplied" ; exit 1; }
        var=$(echo $varx | sed "s/_//g" | sed "s/\.//g" )
        echo "aggpp | 1hrPtTOday.${var}.2Dto3D        | begin $(date)">>$loadBal
        ifile1="$rawsdir/$freq/${var}1CM.${fld}/${varx}.1CM.${fld}.${period}.grb"
        ifile2="$rawsdir/$freq/${var}2CM.${fld}/${varx}.2CM.${fld}.${period}.grb"
        ifile3="$rawsdir/$freq/${var}6CM.${fld}/${varx}.6CM.${fld}.${period}.grb"
        ifile4="$rawsdir/$freq/${var}18CM.${fld}/${varx}.18CM.${fld}.${period}.grb"
        ifile5="$rawsdir/$freq/${var}54CM.${fld}/${varx}.54CM.${fld}.${period}.grb"
        ifile6="$rawsdir/$freq/${var}162CM.${fld}/${varx}.162CM.${fld}.${period}.grb"
        ifile7="$rawsdir/$freq/${var}486CM.${fld}/${varx}.486CM.${fld}.${period}.grb"
        ifile8="$rawsdir/$freq/${var}1458CM.${fld}/${varx}.1458CM.${fld}.${period}.grb"
        ofile="$rawsdir/day/${var}.3D/${var}.3D.${period}.DayMean.grb"
        ofile_nc=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_3D_day_$period.nc; rm -f $ofile_nc
        mkdir -p $rawsdir/day/${var}.3D || echo "ERROR: Creating directory for $ofile"
        CDO_FILE_SUFFIX=.grb $cdo -z szip daymean -shifttime,-300s -setmissval,9999 -merge -setlevel,1 $ifile1 -setlevel,2 $ifile2 -setlevel,3 $ifile3 -setlevel,4 $ifile4 -setlevel,5 $ifile5 -setlevel,6 $ifile6 -setlevel,7 $ifile7 -setlevel,8 $ifile8 $ofile || echo "ERROR: $ifile1 merge operation"
        if [[ "$var" == "TSOIL" ]]; then
            tsoilmask=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_tsoilmask_2D.nc
            $cdo -f nc4 -setgrid,${GRIDFILE} -setzaxis,${ZAXISSOIL} -setname,$var -setctomiss,0 -setmissval,9999 -mul $tsoilmask $ofile $ofile_nc || echo "ERROR: $ofile setgrid operation"
        else
            $cdo -f nc4 -setgrid,${GRIDFILE} -setzaxis,${ZAXISSOIL} -setname,$var $ofile $ofile_nc || echo "ERROR: $ofile setgrid operation"
        fi
        echo "aggpp | 1hrPtTOday.${var}.2Dto3D        | end   $(date)">>$loadBal
    }; )& ; }>$err.1hrPtTOday.${1-var}.2Dto3D 2>&1
}


#-- monthly means out of hourly instantaneous data - 2D to 3D - tsoil, wsoil, wsoilice - set-grid operation, compose z-axis, setmissval to 9999
agg_1hrPtTOmon_2Dto3D() {
    { (if_requested $member $esmod aggregation mon $chunk && {
        freq=1hrPt ; varx=${1-} ; fld=2D
        [[ -z "$varx" ]] && { echo "Function ${0}: no variable name supplied" ; exit 1; }
        var=$(echo $varx | sed "s/_//g" | sed "s/\.//g" )
        echo "aggpp | 1hrPtTOmon.${var}.2Dto3D        | begin $(date)">>$loadBal
        ifile1="$rawsdir/$freq/${var}1CM.${fld}/${varx}.1CM.${fld}.${period}.grb"
        ifile2="$rawsdir/$freq/${var}2CM.${fld}/${varx}.2CM.${fld}.${period}.grb"
        ifile3="$rawsdir/$freq/${var}6CM.${fld}/${varx}.6CM.${fld}.${period}.grb"
        ifile4="$rawsdir/$freq/${var}18CM.${fld}/${varx}.18CM.${fld}.${period}.grb"
        ifile5="$rawsdir/$freq/${var}54CM.${fld}/${varx}.54CM.${fld}.${period}.grb"
        ifile6="$rawsdir/$freq/${var}162CM.${fld}/${varx}.162CM.${fld}.${period}.grb"
        ifile7="$rawsdir/$freq/${var}486CM.${fld}/${varx}.486CM.${fld}.${period}.grb"
        ifile8="$rawsdir/$freq/${var}1458CM.${fld}/${varx}.1458CM.${fld}.${period}.grb"
        ofile="$rawsdir/mon/${var}.3D/${var}.3D.${period}.MonMean.grb"
        ofile_nc=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_3D_mon_$period.nc; rm -f $ofile_nc
        mkdir -p $rawsdir/mon/${var}.3D || echo "ERROR: Creating directory for $ofile"
        CDO_FILE_SUFFIX=.grb $cdo -z szip monmean -shifttime,-300s -setmissval,9999 -merge -setlevel,1 $ifile1 -setlevel,2 $ifile2 -setlevel,3 $ifile3 -setlevel,4 $ifile4 -setlevel,5 $ifile5 -setlevel,6 $ifile6 -setlevel,7 $ifile7 -setlevel,8 $ifile8 $ofile || echo "ERROR: $ifile1 merge operation"
        if [[ "$var" == "TSOIL" ]]; then
            tsoilmask=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_tsoilmask_2D.nc
            $cdo -f nc4 -setgrid,${GRIDFILE} -setzaxis,${ZAXISSOIL} -setname,$var -setctomiss,0 -setmissval,9999 -mul $tsoilmask $ofile $ofile_nc || echo "ERROR: $ofile setgrid operation"
        else
            $cdo -f nc4 -setgrid,${GRIDFILE} -setzaxis,${ZAXISSOIL} -setname,$var $ofile $ofile_nc || echo "ERROR: $ofile setgrid operation"
        fi
        echo "aggpp | 1hrPtTOmon.${var}.2Dto3D        | end   $(date)">>$loadBal
    }; )& ; }>$err.1hrPtTOmon.${1-var}.2Dto3D 2>&1
}



###############
# 1hr/1hrPt
###############


###### daily and monthly means

#-- daily means out of hourly instantaneous data - 2D - set-grid operation, setmissval to 9999
agg_1hrPtTOday_2D() {
    { (if_requested $member $esmod aggregation day $chunk && {
        freq=1hrPt ; var=${1-} ; fld=2D
        [[ -z "$var" ]] && { echo "Function ${0}: no variable name supplied" ; exit 1; }
        echo "aggpp | ${freq}TOday.${var}.${fld}        | begin $(date)">>$loadBal
        ifile="$rawsdir/$freq/${var}.${fld}/${var}.${fld}.${period}.grb"
        ofile="$rawsdir/day/${var}.${fld}/${var}.${fld}.${period}.DayMean.grb"
        ofile_nc=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_${fld}_day_$period.nc; rm -f $ofile_nc
        mkdir -p $rawsdir/day/${var}.${fld} || echo "ERROR: Creating directory for $ofile"
        if [[ "$var" == "HZEROCL" ]]; then
            CDO_FILE_SUFFIX=.grb $cdo -z szip daymean -setmissval,9999 -setmisstoc,9999 -setmissval,-999 -shifttime,-300s $ifile $ofile || echo "ERROR: $ifile averaging operation"
        else
            CDO_FILE_SUFFIX=.grb $cdo -z szip daymean -setmissval,9999 -shifttime,-300s $ifile $ofile || echo "ERROR: $ifile averaging operation"
        fi
        $cdo -f nc4 -setgrid,${GRIDFILE} -setname,$var $ofile $ofile_nc || echo "ERROR: $ofile setgrid operation"
        echo "aggpp | ${freq}TOday.${var}.${fld}        | end   $(date)">>$loadBal
    }; )& ; }>$err.1hrPtTOday.${1-var}.2D 2>&1
}


#-- monthly means out of hourly instantaneous data - 2D - set-grid operation, setmissval to 9999
agg_1hrPtTOmon_2D() {
    { (if_requested $member $esmod aggregation mon $chunk && {
        freq=1hrPt ; var=${1-} ; fld=2D
        [[ -z "$var" ]] && { echo "Function ${0}: no variable name supplied" ; exit 1; }
        echo "aggpp | ${freq}TOmon.${var}.${fld}        | begin $(date)">>$loadBal
        ifile="$rawsdir/$freq/${var}.${fld}/${var}.${fld}.${period}.grb"
        ofile="$rawsdir/mon/${var}.${fld}/${var}.${fld}.${period}.MonMean.grb"
        ofile_nc=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_${fld}_mon_$period.nc; rm -f $ofile_nc
        mkdir -p $rawsdir/mon/${var}.${fld} || echo "ERROR: Creating directory for $ofile"
        if [[ "$var" == "HZEROCL" ]]; then
            CDO_FILE_SUFFIX=.grb $cdo -z szip monmean -setmissval,9999 -setmisstoc,9999 -setmissval,-999 -shifttime,-300s $ifile $ofile || echo "ERROR: $ifile averaging operation"
        else
            CDO_FILE_SUFFIX=.grb $cdo -z szip monmean -setmissval,9999 -shifttime,-300s $ifile $ofile || echo "ERROR: $ifile averaging operation"
        fi
        $cdo -f nc4 -setgrid,${GRIDFILE} -setname,$var $ofile $ofile_nc || echo "ERROR: $ofile setgrid operation"
        echo "aggpp | ${freq}TOmon.${var}.${fld}        | end   $(date)">>$loadBal
    }; )& ; }>$err.1hrPtTOmon.${1-var}.2D 2>&1
}


#-- daily means out of hourly mean data - 2D - set-grid operation, setmissval to 9999
agg_1hrTOday_2D() {
    { (if_requested $member $esmod aggregation day $chunk && {
        freq=1hr ; var=${1-} ; fld=2D
        [[ -z "$var" ]] && { echo "Function ${0}: no variable name supplied" ; exit 1; }
        echo "aggpp | ${freq}TOday.${var}.${fld}        | begin $(date)">>$loadBal
        ifile="$rawsdir/$freq/${var}.${fld}/${var}.${fld}.${period}.grb"
        ofile="$rawsdir/day/${var}.${fld}/${var}.${fld}.${period}.DayMean.grb"
        ofile_nc=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_${fld}_day_$period.nc; rm -f $ofile_nc
        mkdir -p $rawsdir/day/${var}.${fld} || echo "ERROR: Creating directory for $ofile"
        CDO_FILE_SUFFIX=.grb $cdo -z szip daymean -setmissval,9999 -shifttime,-300s $ifile $ofile || echo "ERROR: $ifile average operation"
        $cdo -f nc4 -setgrid,${GRIDFILE} -setname,$var $ofile $ofile_nc || echo "ERROR: $ofile setgrid operation"
        echo "aggpp | ${freq}TOday.${var}.${fld}        | end   $(date)">>$loadBal
    }; )& ; }>$err.1hrTOday.${1-var}.2D 2>&1
}


#-- monthly means out of hourly mean data - 2D - set-grid operation, setmissval to 9999
agg_1hrTOmon_2D() {
    { (if_requested $member $esmod aggregation mon $chunk && {
        freq=1hr ; var=${1-} ; fld=2D
        [[ -z "$var" ]] && { echo "Function ${0}: no variable name supplied" ; exit 1; }
        echo "aggpp | ${freq}TOmon.${var}.${fld}        | begin $(date)">>$loadBal
        ifile="$rawsdir/$freq/${var}.${fld}/${var}.${fld}.${period}.grb"
        ofile="$rawsdir/mon/${var}.${fld}/${var}.${fld}.${period}.MonMean.grb"
        ofile_nc=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_${fld}_mon_$period.nc; rm -f $ofile_nc
        mkdir -p $rawsdir/mon/${var}.${fld} || echo "ERROR: Creating directory for $ofile"
        CDO_FILE_SUFFIX=.grb $cdo -z szip monmean -setmissval,9999 -shifttime,-300s $ifile $ofile || echo "ERROR: $ifile average operation"
        $cdo -f nc4 -setgrid,${GRIDFILE} -setname,$var $ofile $ofile_nc || echo "ERROR: $ofile setgrid operation"
        echo "aggpp | ${freq}TOmon.${var}.${fld}        | end   $(date)">>$loadBal
    }; )& ; }>$err.1hrTOmon.${1-var}.2D 2>&1
}


#-- daily sums out of hourly accumulated data - 2D - set-grid operation, setmissval to 9999
agg_1hrTOdayAcc_2D() {
    { (if_requested $member $esmod aggregation day $chunk && {
        freq=1hr ; var=${1-} ; fld=2D
        [[ -z "$var" ]] && { echo "Function ${0}: no variable name supplied" ; exit 1; }
        echo "aggpp | ${freq}TOdayAcc.${var}.${fld}        | begin $(date)">>$loadBal
        ifile="$rawsdir/$freq/${var}.${fld}/${var}.${fld}.${period}.grb"
        ofile="$rawsdir/day/${var}.${fld}/${var}.${fld}.${period}.DaySum.grb"
        ofile_nc=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_${fld}_day_$period.nc; rm -f $ofile_nc
        mkdir -p $rawsdir/day/${var}.${fld} || echo "ERROR: Creating directory for $ofile"
        CDO_FILE_SUFFIX=.grb $cdo -z szip daysum -setmissval,9999 -shifttime,-300s $ifile $ofile || echo "ERROR: $ifile summing operation"
        $cdo -f nc4 -setgrid,${GRIDFILE} -divc,86400. -setname,$var $ofile $ofile_nc || echo "ERROR: $ofile setgrid operation"
        echo "aggpp | ${freq}TOdayAcc.${var}.${fld}        | end   $(date)">>$loadBal
    }; )& ; }>$err.1hrTOdayAcc.${1-var}.2D 2>&1
}


#-- monthly sums out of hourly accumulated data - 2D - set-grid operation, setmissval to 9999
agg_1hrTOmonAcc_2D() {
    { (if_requested $member $esmod aggregation mon $chunk && {
        freq=1hr ; var=${1-} ; fld=2D
        [[ -z "$var" ]] && { echo "Function ${0}: no variable name supplied" ; exit 1; }
        echo "aggpp | ${freq}TOmonAcc.${var}.${fld}        | begin $(date)">>$loadBal
        ifile="$rawsdir/$freq/${var}.${fld}/${var}.${fld}.${period}.grb"
        ofile="$rawsdir/mon/${var}.${fld}/${var}.${fld}.${period}.MonSum.grb"
        ofile_nc=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_${fld}_mon_$period.nc; rm -f $ofile_nc
        mkdir -p $rawsdir/mon/${var}.${fld} || echo "ERROR: Creating directory for $ofile"
        CDO_FILE_SUFFIX=.grb $cdo -z szip monsum -setmissval,9999 -shifttime,-300s $ifile $ofile || echo "ERROR: $ifile summing operation"
        spm=$(( $(cal $(date +"${MM} ${YYYY}") | xargs -n1 | tail -n1) * 86400 )) #calc seconds per month
        $cdo -f nc4 -setgrid,${GRIDFILE} -divc,${spm} -setname,$var $ofile $ofile_nc || echo "ERROR: $ofile setgrid operation"
        echo "aggpp | ${freq}TOmonAcc.${var}.${fld}        | end   $(date)">>$loadBal
    }; )& ; }>$err.1hrTOmonAcc.${1-var}.2D 2>&1
}


#-- daily maxima out of hourly maxima - 2D - set-grid operation, setmissval to 9999
agg_1hrTOdayMax_2D() {
    { (if_requested $member $esmod aggregation day $chunk && {
        freq=1hr ; var=${1-} ; fld=2D
        [[ -z "$var" ]] && { echo "Function ${0}: no variable name supplied" ; exit 1; }
        echo "aggpp | ${freq}TOdayMax.${var}.${fld}        | begin $(date)">>$loadBal
        ifile="$rawsdir/$freq/${var}.${fld}/${var}.${fld}.${period}.grb"
        ofile="$rawsdir/day/${var}.${fld}/${var}.${fld}.${period}.DayMax.grb"
        ofile_nc=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_${fld}_day_$period.nc; rm -f $ofile_nc
        mkdir -p $rawsdir/day/${var}.${fld} || echo "ERROR: Creating directory for $ofile"
        CDO_FILE_SUFFIX=.grb $cdo -z szip daymax -setmissval,9999 -shifttime,-300s $ifile $ofile || echo "ERROR: $ifile maxima selection operation"
        $cdo -f nc4 -setgrid,${GRIDFILE} -setname,$var $ofile $ofile_nc || echo "ERROR: $ofile setgrid operation"
        echo "aggpp | ${freq}.TOdayMax.${var}.${fld}        | end   $(date)">>$loadBal
    }; )& ; }>$err.1hrTOdayMax.${1-var}.2D 2>&1
}

#-- daily minima out of hourly minima - 2D - set-grid operation, setmissval to 9999
agg_1hrTOdayMin_2D() {
    { (if_requested $member $esmod aggregation day $chunk && {
        freq=1hr ; var=${1-} ; fld=2D
        [[ -z "$var" ]] && { echo "Function ${0}: no variable name supplied" ; exit 1; }
        echo "aggpp | ${freq}TOdayMin.${var}.${fld}        | begin $(date)">>$loadBal
        ifile="$rawsdir/$freq/${var}.${fld}/${var}.${fld}.${period}.grb"
        ofile="$rawsdir/day/${var}.${fld}/${var}.${fld}.${period}.DayMin.grb"
        ofile_nc=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_${fld}_day_$period.nc; rm -f $ofile_nc
        mkdir -p $rawsdir/day/${var}.${fld} || echo "ERROR: Creating directory for $ofile"
        CDO_FILE_SUFFIX=.grb $cdo -z szip daymin -setmissval,9999 -shifttime,-300s $ifile $ofile || echo "ERROR: $ifile minima selection operation"
        $cdo -f nc4 -setgrid,${GRIDFILE} -setname,$var $ofile $ofile_nc || echo "ERROR: $ofile setgrid operation"
        echo "aggpp | ${freq}.TOdayMin.${var}.${fld}        | end   $(date)">>$loadBal
    }; )& ; }>$err.1hrTOdayMin.${1-var}.2D 2>&1
}

#-- monthly mean out of hourly->daily maxima - 2D - set-grid operation, setmissval to 9999
agg_1hrTOmonMax_2D() {
    { (if_requested $member $esmod aggregation mon $chunk && {
        freq=1hr ; var=${1-} ; fld=2D
        [[ -z "$var" ]] && { echo "Function ${0}: no variable name supplied" ; exit 1; }
        echo "aggpp | ${freq}MaxTOmon.${var}.${fld}        | begin $(date)">>$loadBal
        ifile="$rawsdir/$freq/${var}.${fld}/${var}.${fld}.${period}.grb"
        ofile="$rawsdir/mon/${var}.${fld}/${var}.${fld}.${period}.MonMean.grb"
        ofile_nc=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_${fld}_mon_$period.nc; rm -f $ofile_nc
        mkdir -p $rawsdir/mon/${var}.${fld} || echo "ERROR: Creating directory for $ofile"
        CDO_FILE_SUFFIX=.grb $cdo -z szip monmean -daymax -setmissval,9999 -shifttime,-300s $ifile $ofile || echo "ERROR: $ifile maxima selection and average operation"
        $cdo -f nc4 -setgrid,${GRIDFILE} -setname,$var $ofile $ofile_nc || echo "ERROR: $ofile setgrid operation"
        echo "aggpp | ${freq}MaxTOmon.${var}.${fld}        | end   $(date)">>$loadBal
    }; )& ; }>$err.1hrTOmonMax.${1-var}.2D 2>&1
}

#-- monthly mean out of hourly->daily minima - 2D - set-grid operation, setmissval to 9999
agg_1hrTOmonMin_2D() {
    { (if_requested $member $esmod aggregation mon $chunk && {
        freq=1hr ; var=${1-} ; fld=2D
        [[ -z "$var" ]] && { echo "Function ${0}: no variable name supplied" ; exit 1; }
        echo "aggpp | ${freq}MinTOmon.${var}.${fld}        | begin $(date)">>$loadBal
        ifile="$rawsdir/$freq/${var}.${fld}/${var}.${fld}.${period}.grb"
        ofile="$rawsdir/mon/${var}.${fld}/${var}.${fld}.${period}.MonMean.grb"
        ofile_nc=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_${fld}_mon_$period.nc; rm -f $ofile_nc
        mkdir -p $rawsdir/mon/${var}.${fld} || echo "ERROR: Creating directory for $ofile"
        CDO_FILE_SUFFIX=.grb $cdo -z szip monmean -daymin -setmissval,9999 -shifttime,-300s $ifile $ofile || echo "ERROR: $ifile minima selection and average operation"
        $cdo -f nc4 -setgrid,${GRIDFILE} -setname,$var $ofile $ofile_nc || echo "ERROR: $ofile setgrid operation"
        echo "aggpp | ${freq}MinTOmon.${var}.${fld}        | end   $(date)">>$loadBal
    }; )& ; }>$err.1hrTOmonMin.${1-var}.2D 2>&1
}



###### ave

#-- 1hr - 2D - set-grid operation, setmissval to 9999
agg_1hr_2D() {
    { (if_requested $member $esmod aggregation 1hr $chunk && {
        freq=1hr ; var=${1-} ; fld=2D
        [[ -z "$var" ]] && { echo "Function ${0}: no variable name supplied" ; exit 1; }
        echo "aggpp | ${freq}.${var}.${fld}        | begin $(date)">>$loadBal
        ifile="$rawsdir/$freq/${var}.${fld}/${var}.${fld}.${period}.grb"
        ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_${fld}_${freq}_$period.nc; rm -f $ofile
        $cdo -f nc4 -setgrid,${GRIDFILE} -setname,$var -setmissval,9999 -shifttime,-300s $ifile $ofile || echo "ERROR: $ofile setgrid operation"
        echo "aggpp | ${freq}.${var}.${fld}        | end   $(date)">>$loadBal
    }; )& ; }>$err.1hr.${1-var}.2D 2>&1
}


###### inst

#-- 1hrPt - 2D - set-grid operation, setmissval to 9999
agg_1hrPt_2D() {
    { (if_requested $member $esmod aggregation 1hrPt $chunk && {
        freq=1hrPt ; var=${1-} ; fld=2D
	[[ -z "$var" ]] && { echo "Function ${0}: no variable name supplied" ; exit 1; }
        echo "aggpp | ${freq}.${var}.${fld}        | begin $(date)">>$loadBal
        ifile="$rawsdir/$freq/${var}.${fld}/${var}.${fld}.${period}.grb"
        ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_${fld}_${freq}_$period.nc; rm -f $ofile
        if [[ "$var" == "HZEROCL" ]]; then
            $cdo -f nc4 -setgrid,${GRIDFILE} -setname,$var -setmissval,9999 -setmisstoc,9999 -setmissval,-999 $ifile $ofile || echo "ERROR: $ofile setgrid operation"
        else
            $cdo -f nc4 -setgrid,${GRIDFILE} -setname,$var -setmissval,9999 $ifile $ofile || echo "ERROR: $ofile setgrid operation"
        fi
        echo "aggpp | ${freq}.${var}.${fld}        | end   $(date)">>$loadBal
    }; )& ; }>$err.1hrPt.${1-var}.2D 2>&1
}


###### conv

#-- 1hrPt converted - 2D - set-grid operation
agg_1hrPtConv_2D() {
    { (if_requested $member $esmod aggregation 1hrPt $chunk && {
        freq=1hrPt ; var=${1-} ; fld=2D
        [[ -z "$var" ]] && { echo "Function ${0}: no variable name supplied" ; exit 1; }
        echo "aggpp | ${freq}.${var}.${fld}        | begin $(date)">>$loadBal
        ifile="$rawsdir/$freq/${var}.${fld}/${var}m.${fld}.${period}.nc4"
        ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}m_${fld}_${freq}_$period.nc; rm -f $ofile
        $cdo -f nc4 -setgrid,${GRIDFILE} $ifile $ofile || echo "ERROR: $ofile setgrid operation"
        echo "aggpp | ${freq}.${var}.${fld}        | end   $(date)">>$loadBal
    }; )& ; }>$err.1hrPt.${1-var}.2D 2>&1
}


##### Max / Min

#-- 1hrMax - 2D - set-grid operation, setmissval to 9999
agg_1hrMax_2D() {
    { (if_requested $member $esmod aggregation 1hr $chunk && {
        freq=1hr ; var=${1-} ; fld=2D
        [[ -z "$var" ]] && { echo "Function ${0}: no variable name supplied" ; exit 1; }
        echo "aggpp | ${freq}.${var}.${fld}        | begin $(date)">>$loadBal
        ifile="$rawsdir/$freq/${var}.${fld}/${var}.${fld}.${period}.grb"
        ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_${fld}_${freq}_$period.nc; rm -f $ofile
        $cdo -f nc4 -setgrid,${GRIDFILE} -setname,$var -setmissval,9999 -shifttime,-300s $ifile $ofile || echo "ERROR: $ofile setgrid operation"
        echo "aggpp | ${freq}.${var}.${fld}        | end   $(date)">>$loadBal
    }; )& ; }>$err.1hr.${1-var}.2D 2>&1
}


#-- 1hrMin - 2D - set-grid operation, setmissval to 9999
agg_1hrMin_2D() {
    { (if_requested $member $esmod aggregation 1hr $chunk && {
        freq=1hr ; var=${1-} ; fld=2D
        [[ -z "$var" ]] && { echo "Function ${0}: no variable name supplied" ; exit 1; }
        echo "aggpp | ${freq}.${var}.${fld}        | begin $(date)">>$loadBal
        ifile="$rawsdir/$freq/${var}.${fld}/${var}.${fld}.${period}.grb"
        ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_${fld}_${freq}_$period.nc; rm -f $ofile
        $cdo -f nc4 -setgrid,${GRIDFILE} -setname,$var -setmissval,9999 -shifttime,-300s $ifile $ofile || echo "ERROR: $ofile setgrid operation"
        echo "aggpp | ${freq}.${var}.${fld}        | end   $(date)">>$loadBal
    }; )& ; }>$err.1hr.${1-var}.2D 2>&1
}


#-- 6hrMax - 2D - set-grid operation, setmissval to 9999
agg_6hrMax_2D() {
    { (if_requested $member $esmod aggregation 6hr $chunk && {
        freq=6hr ; var=${1-} ; fld=2D
        [[ -z "$var" ]] && { echo "Function ${0}: no variable name supplied" ; exit 1; }
        echo "aggpp | ${freq}.${var}.${fld}        | begin $(date)">>$loadBal
        ifile="$rawsdir/1hr/${var}.${fld}/${var}.${fld}.${period}.grb"
        ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_${fld}_6hr_$period.nc; rm -f $ofile
        $cdo -f nc4 -setgrid,${GRIDFILE} -setname,$var -setmissval,9999 -shifttime,-300s $ifile $ofile || echo "ERROR: $ofile setgrid operation"
        echo "aggpp | ${freq}.${var}.${fld}        | end   $(date)">>$loadBal
    }; )& ; }>$err.6hr.${1-var}.2D 2>&1
}


#-- 6hrMin - 2D - set-grid operation, setmissval to 9999
agg_6hrMin_2D() {
    { (if_requested $member $esmod aggregation 6hr $chunk && {
        freq=6hr ; var=${1-} ; fld=2D
        [[ -z "$var" ]] && { echo "Function ${0}: no variable name supplied" ; exit 1; }
        echo "aggpp | ${freq}.${var}.${fld}        | begin $(date)">>$loadBal
        ifile="$rawsdir/1hr/${var}.${fld}/${var}.${fld}.${period}.grb"
        ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_${fld}_${freq}_$period.nc; rm -f $ofile
        $cdo -f nc4 -setgrid,${GRIDFILE} -setname,$var -setmissval,9999 -shifttime,-300s $ifile $ofile || echo "ERROR: $ofile setgrid operation"
        echo "aggpp | ${freq}.${var}.${fld}        | end   $(date)">>$loadBal
    }; )& ; }>$err.6hr.${1-var}.2D 2>&1
}


###### Acc

#-- 1hrAcc - 2D - set-grid operation, setmissval to 9999, acc->mean calculation
agg_1hrAcc_2D() {
    { (if_requested $member $esmod aggregation 1hr $chunk && {
        freq=1hr ; var=${1-} ; fld=2D
        [[ -z "$var" ]] && { echo "Function ${0}: no variable name supplied" ; exit 1; }
        echo "aggpp | ${freq}.${var}.${fld}        | begin $(date)">>$loadBal
        ifile="$rawsdir/$freq/${var}.${fld}/${var}.${fld}.${period}.grb"
        ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_${fld}_${freq}_$period.nc; rm -f $ofile
        $cdo -f nc4 -setgrid,${GRIDFILE} -divc,3600. -setname,$var -setmissval,9999 -shifttime,-300s $ifile $ofile || echo "ERROR: $ofile setgrid and division operation"
        echo "aggpp | ${freq}.${var}.${fld}        | end   $(date)">>$loadBal
    }; )& ; }>$err.1hr.${1-var}.2D 2>&1
}


###### 3D

#-- 1hrPt - 3D (6 lvl opendata) - set-grid operation, concat/split to monthly chunks, setmissval to 9999
agg_1hrPt_3D() {
    { (if_requested $member $esmod aggregation 1hrPt3D $chunk && {
        freq=1hrPt ; var=${1-} ; fld=3D
        [[ -z "$var" ]] && { echo "Function ${0}: no variable name supplied" ; exit 1; }
        varx=$var ; [[ "$var" == "Q" ]] && varx=QV
        echo "aggpp | ${freq}.${var}.${fld}        | begin $(date)">>$loadBal
        if [[ "$var" == "QV" ]]; then
            varcode=51
            vartab=2
        elif [[ "$var" == "T" ]]; then
            varcode=11
            vartab=2
        elif [[ "$var" == "TKE" ]]; then
            varcode=52
            vartab=201
        elif [[ "$var" == "W" ]]; then
            varcode=40
            vartab=2
        else
            echo "ERROR: only variables supported in this function are 'QV', 'T', 'TKE' and 'W'. '$var' specified!"
            varcode=0
            vartab=0
        fi
        ifilebase="$rawsdir/$freq/${var}.${fld}/${var}.${fld}"
        ifiles=( $(ls ${ifilebase}.${period}010{1..9}??.grb ${ifilebase}.${period}01{10..23}??.grb ${ifilebase}.${period}0{2..9}????.grb ${ifilebase}.${period}{1..3}?????.grb ${ifilebase}.${nextperiod}010000.grb | sort -u) )
        ofile_tmp=$sdir/tmp_aggr/${varx}_${fld}_${freq}_$period.nc; rm -f $ofile_tmp
        ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${varx}_${fld}_${freq}_$period.nc; rm -f $ofile
        ifile_z="$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_HSURF_2D_fx_$YYYY.nc"
        echo "$cdo -f nc4 setgrid,${GRIDFILE} -setzaxis,${ZAXISFILE6} -setmissval,9999 -setparam,${varcode}.${vartab} -setname,$varx -mergetime [ ${ifiles[@]} ] $ofile_tmp"
        $cdo -f nc4 setgrid,${GRIDFILE} -setzaxis,${ZAXISFILE6} -setmissval,9999 -setparam,${varcode}.${vartab} -setname,$varx -mergetime [ ${ifiles[@]} ] $ofile_tmp || echo "ERROR: $ofile mergetime and setgrid operation"
        echo "$cdo -f nc4 merge $ofile_tmp $ifile_z $ofile"
        $cdo -f nc4 merge $ofile_tmp $ifile_z $ofile && rm -f $ofile_tmp || echo "ERROR: $ofile merge operation"

        # temporal averaging if does not exist yet
        ofile_day=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_${fld}_day_$period.nc
        ofile_mon=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_${fld}_mon_$period.nc
        $cdo daymean -shifttime,-300s $ofile $ofile_day || echo "ERROR daily averaging"
        $cdo monmean -shifttime,-300s $ofile $ofile_mon || echo "ERROR monthly averaging"
        echo "aggpp | ${freq}.${var}.${fld}        | end   $(date)">>$loadBal
    }; )& ; }>$err.1hrPt.${1-var}.3D 2>&1
}

#-- 1hrPt - 3D (40 lvl) - daily+monthly averaging, set-grid operation, concat/split to monthly chunks, setmissval to 9999
agg_1hrPt_3D_40() {
    { (if_requested $member $esmod aggregation 1hrPt3D40 $chunk && {
        freq=1hrPt ; var=${1-} ; fld=3D
        [[ -z "$var" ]] && { echo "Function ${0}: no variable name supplied" ; exit 1; }
        echo "aggpp | ${freq}.${var}.${fld}        | begin $(date)">>$loadBal
        if [[ "$var" == "QV" ]]; then
            varcode=51
            vartab=2
        elif [[ "$var" == "T" ]]; then
            varcode=11
            vartab=2
        elif [[ "$var" == "TKE" ]]; then
            varcode=52
            vartab=201
        elif [[ "$var" == "W" ]]; then
            varcode=40
            vartab=2
        else
            echo "ERROR: only variables supported in this function are 'QV', 'T', 'TKE' and 'W'. '$var' specified!"
            varcode=0
            vartab=0
        fi
        ifilebase="$rawsdir/$freq/${var}.${fld}/${var}.${fld}"
        ifiles=( $(ls ${ifilebase}.${period}010{1..9}??.grb ${ifilebase}.${period}01{10..23}??.grb ${ifilebase}.${period}0{2..9}????.grb ${ifilebase}.${period}{1..3}?????.grb ${ifilebase}.${nextperiod}010000.grb | sort -u) )
        ofile_tmp=$sdir/tmp_aggr/${var}_${fld}_${freq}_$period.nc; rm -f $ofile_tmp
        ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_${fld}_${freq}_$period.nc; rm -f $ofile
        ifile_z="$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_HSURF_2D_fx_$YYYY.nc"
        echo "$cdo -f nc4 setgrid,${GRIDFILE} -setzaxis,${ZAXISFILE40} -setmissval,9999 -setparam,${varcode}.${vartab} -setname,$var -mergetime [ ${ifiles[@]} ] $ofile_tmp"
        $cdo -f nc4 setgrid,${GRIDFILE} -setzaxis,${ZAXISFILE40} -setmissval,9999 -setparam,${varcode}.${vartab} -setname,$var -mergetime [ ${ifiles[@]} ] $ofile_tmp || echo "ERROR: $ofile mergetime and setgrid operation"
        echo "$cdo -f nc4 merge $ofile_tmp $ifile_z $ofile"
        $cdo -f nc4 merge $ofile_tmp $ifile_z $ofile && rm -f $ofile_tmp || echo "ERROR: $ofile merge operation"

        # temporal averaging if does not exist yet
        ofile_day=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_${fld}_day_$period.nc
        ofile_mon=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_${fld}_mon_$period.nc
        $cdo daymean -shifttime,-300s $ofile $ofile_day || echo "ERROR daily averaging"
        $cdo monmean -shifttime,-300s $ofile $ofile_mon || echo "ERROR monthly averaging"
        echo "aggpp | ${freq}.${var}.${fld}        | end   $(date)">>$loadBal
    }; )& ; }>$err.1hrPt.${1-var}.3D40 2>&1
}

#-- 1hrPt - 3D (U&V 40lvl) - daily+monthly averaging, set-grid operation, concat/split to monthly chunks, setmissval to 9999
agg_1hrPt_3D_UV() {
    { (if_requested $member $esmod aggregation 1hrPt3DUVFX $chunk && {
        freq=1hrPt ; var=${1-} ; fld=3D
        [[ -z "$var" ]] && { echo "Function ${0}: no variable name supplied" ; exit 1; }
        echo "aggpp | ${freq}.${var}.${fld}        | begin $(date)">>$loadBal
        if [[ "$var" == "U" ]]; then
            varcode=33
            vartab=2
        elif [[ "$var" == "V" ]]; then
            varcode=34
            vartab=2
        else
            echo "ERROR: only variables supported in this function are 'U' and 'V'. '$var' specified!"
            varcode=0
            vartab=0
        fi
        ifile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_UV_${fld}_${freq}_$period.nc
        ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_${fld}_${freq}_$period.nc; rm -f $ofile
        ifile_z="$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_HSURF_2D_fx_$YYYY.nc"
        echo "$cdo -f nc4 setgrid,${GRIDFILE} -setzaxis,${ZAXISFILE40} -setmissval,9999 -merge -setparam,${varcode}.${vartab} -selname,$var $ifile $ifile_z $ofile"
        $cdo -f nc4 setgrid,${GRIDFILE} -setzaxis,${ZAXISFILE40} -setmissval,9999 -merge -setparam,${varcode}.${vartab} -selname,$var $ifile $ifile_z $ofile || echo "ERROR: $ofile merge and setgrid operation"
        ofile_day=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_${fld}_day_$period.nc
        ofile_mon=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_${fld}_mon_$period.nc
        $cdo -O daymean -shifttime,-300s $ofile $ofile_day || echo "ERROR daily averaging"
        $cdo -O monmean -shifttime,-300s $ofile $ofile_mon || echo "ERROR monthly averaging"
        echo "aggpp | ${freq}.${var}.${fld}        | end   $(date)">>$loadBal
    }; )& ; }>$err.1hrPt.${1-var}.3D 2>&1
}

#-- 1hrPt - 3D (U,V plev) - daily+monthly averaging, set-grid operation, concat/split to monthly chunks, setmissval to 9999
agg_1hrPt_3Dplev_UV() {
    { (if_requested $member $esmod aggregation 1hrPt3DUVPLEVFX $chunk && {
        freq=1hrPt ; var=${1-} ; fld=3D
        [[ -z "$var" ]] && { echo "Function ${0}: no variable name supplied" ; exit 1; }
        echo "aggpp | ${freq}.${var}.3Dplev        | begin $(date)">>$loadBal
        if [[ "$var" == "U" ]]; then
            varcode=33
            vartab=2
        elif [[ "$var" == "V" ]]; then
            varcode=34
            vartab=2
        else
            echo "ERROR: only variables supported in this function are 'U' and 'V'. '$var' specified!"
            varcode=0
            vartab=0
        fi
        ifile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_UVFX_3Dplev_${freq}_$period.nc
        ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_3Dplev_${freq}_$period.nc; rm -f $ofile
        echo "$cdo -f nc4 setgrid,${GRIDFILE} -setzaxis,${ZAXISFILEPLEV} -setmissval,9999 -setparam,${varcode}.${vartab} -selname,$var $ifile $ofile"
        $cdo -f nc4 setgrid,${GRIDFILE} -setzaxis,${ZAXISFILEPLEV} -setmissval,9999 -setparam,${varcode}.${vartab} -selname,$var $ifile $ofile || echo "ERROR: $ofile setgrid operation"
        ofile_day=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_3Dplev_day_$period.nc
        ofile_mon=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_3Dplev_mon_$period.nc
        $cdo -O daymean -shifttime,-300s $ofile $ofile_day || echo "ERROR daily averaging"
        $cdo -O monmean -shifttime,-300s $ofile $ofile_mon || echo "ERROR monthly averaging"
        echo "aggpp | ${freq}.${var}.3Dplev        | end   $(date)">>$loadBal
    }; )& ; }>$err.1hrPt.${1-var}.3Dplev 2>&1
}

#-- 1hrPt - 3D (plev) - daily+monthly averaging, set-grid operation, concat/split to monthly chunks, setmissval to 9999
agg_1hrPt_3Dplev() {
    { (if_requested $member $esmod aggregation 1hrPt3DPLEVFX $chunk && {
        freq=1hrPt ; var=${1-} ; fld=3D
        [[ -z "$var" ]] && { echo "Function ${0}: no variable name supplied" ; exit 1; }
        echo "aggpp | ${freq}.${var}.3Dplev        | begin $(date)">>$loadBal
        if [[ "$var" == "QV" ]]; then
            varcode=51
            vartab=2
        elif [[ "$var" == "T" ]]; then
            varcode=11
            vartab=2
        elif [[ "$var" == "TKE" ]]; then
            varcode=52
            vartab=201
        elif [[ "$var" == "W" ]]; then
            varcode=40
            vartab=2
        else
            echo "ERROR: only variables supported in this function are 'QV', 'T', 'TKE' and 'W'. '$var' specified!"
            varcode=0
            vartab=0
        fi
        ifile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_FX_3Dplev_${freq}_$period.nc
        ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_3Dplev_${freq}_$period.nc; rm -f $ofile
        echo "$cdo -f nc4 setgrid,${GRIDFILE} -setzaxis,${ZAXISFILEPLEV} -setmissval,9999 -setparam,${varcode}.${vartab} -selname,$var $ifile $ofile"
        $cdo -f nc4 setgrid,${GRIDFILE} -setzaxis,${ZAXISFILEPLEV} -setmissval,9999 -setparam,${varcode}.${vartab} -selname,$var $ifile $ofile || echo "ERROR: $ofile setgrid operation"
        ofile_day=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_3Dplev_day_$period.nc
        ofile_mon=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_3Dplev_mon_$period.nc
        $cdo -O daymean -shifttime,-300s $ofile $ofile_day || echo "ERROR daily averaging"
        $cdo -O monmean -shifttime,-300s $ofile $ofile_mon || echo "ERROR monthly averaging"
        echo "aggpp | ${freq}.${var}.3Dplev        | end   $(date)">>$loadBal
    }; )& ; }>$err.1hrPt.${1-var}.3Dplev 2>&1
}

#-- 1hrPt - 3D (plev) - daily+monthly averaging, set-grid operation, concat/split to monthly chunks, setmissval to 9999
agg_1hrPt_pfull() {
    { (if_requested $member $esmod aggregation pfull $chunk && {
        freq=1hrPt ; var=${1-} ; fld=3D
        [[ -z "$var" ]] && { echo "Function ${0}: no variable name supplied" ; exit 1; }
        echo "aggpp | ${freq}.${var}.3D        | begin $(date)">>$loadBal
        if [[ "$var" == "P" ]]; then
            varcode=1
            vartab=2
        else
            echo "ERROR: only variables supported in this function is 'P'. '$var' specified!"
            varcode=0
            vartab=0
        fi
        ifile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_FX_PFULL_3D_${freq}_$period.nc
        if_requested $member $esmod aggregation 1hrPt3DPLEVFX $chunk && ifile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_FX_3Dplev_${freq}_$period.nc
        ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_3D_${freq}_$period.nc; rm -f $ofile
        ifile_z="$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_HSURF_2D_fx_$YYYY.nc"
        echo "$cdo -f nc4 setgrid,${GRIDFILE} -setzaxis,${ZAXISFILE40} -setmissval,9999 -merge -setparam,${varcode}.${vartab} -selname,$var $ifile $ifile_z $ofile"
        $cdo -f nc4 setgrid,${GRIDFILE} -setzaxis,${ZAXISFILE40} -setmissval,9999 -merge -setparam,${varcode}.${vartab} -selname,$var $ifile $ifile_z $ofile || echo "ERROR: $ofile setgrid operation"
        ofile_day=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_3D_day_$period.nc
        ofile_mon=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_3D_mon_$period.nc
        $cdo -O daymean -shifttime,-300s $ofile $ofile_day || echo "ERROR daily averaging"
        $cdo -O monmean -shifttime,-300s $ofile $ofile_mon || echo "ERROR monthly averaging"
        echo "aggpp | ${freq}.${var}.3D        | end   $(date)">>$loadBal
    }; )& ; }>$err.1hrPt.${1-var}.3D 2>&1
}





###############
# day
###############


###### Ave

#-- day - 2D - set-grid operation, setmissval to 9999
agg_day_2D() {
    { (if_requested $member $esmod aggregation day $chunk && {
        freq=day ; var=${1-} ; fld=2D
        [[ -z "$var" ]] && { echo "Function ${0}: no variable name supplied" ; exit 1; }
        echo "aggpp | ${freq}.${var}.${fld}        | begin $(date)">>$loadBal
        ifile="$rawsdir/$freq/${var}.${fld}/${var}.${fld}.${period}.DayMean.grb"
        ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_${fld}_${freq}_$period.nc; rm -f $ofile
        $cdo -f nc4 -setgrid,${GRIDFILE} -setname,$var -setmissval,9999 $ifile $ofile || echo "ERROR: $ofile setgrid operation"
        echo "aggpp | ${freq}.${var}.${fld}        | end   $(date)">>$loadBal
    }; )& ; }>$err.day.${1-var}.2D 2>&1
}


#-- day converted - 2D - set-grid operation
agg_dayConv_2D() {
    { (if_requested $member $esmod aggregation day $chunk && {
        freq=day ; var=${1-} ; fld=2D
        [[ -z "$var" ]] && { echo "Function ${0}: no variable name supplied" ; exit 1; }
        echo "aggpp | ${freq}.${var}.${fld}        | begin $(date)">>$loadBal
        ifile="$rawsdir/$freq/${var}.${fld}/${var}m.${fld}.${period}.DayMean.nc4"
        ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}m_${fld}_${freq}_$period.nc; rm -f $ofile
        $cdo -f nc4 -setgrid,${GRIDFILE} $ifile $ofile || echo "ERROR: $ofile setgrid operation"
        echo "aggpp | ${freq}.${var}.${fld}        | end   $(date)">>$loadBal
    }; )& ; }>$err.day.${1-var}.2D 2>&1
}


###### Max / Min

#-- dayMax - 2D - set-grid operation, setmissval to 9999
agg_dayMax_2D() {
    { (if_requested $member $esmod aggregation day $chunk && {
        freq=day ; var=${1-} ; fld=2D
        [[ -z "$var" ]] && { echo "Function ${0}: no variable name supplied" ; exit 1; }
        echo "aggpp | ${freq}.${var}.${fld}        | begin $(date)">>$loadBal
        ifile="$rawsdir/$freq/${var}.${fld}/${var}.${fld}.${period}.DayMax.grb"
        ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_${fld}_${freq}_$period.nc; rm -f $ofile
        $cdo -f nc4 -setgrid,${GRIDFILE} -setname,$var -setmissval,9999 $ifile $ofile || echo "ERROR: $ofile setgrid operation"
        echo "aggpp | ${freq}.${var}.${fld}        | end   $(date)">>$loadBal
    }; )& ; }>$err.day.${1-var}.2D 2>&1
}


#-- dayMin - 2D - set-grid operation, setmissval to 9999
agg_dayMin_2D() {
    { (if_requested $member $esmod aggregation day $chunk && {
        freq=day ; var=${1-} ; fld=2D
        [[ -z "$var" ]] && { echo "Function ${0}: no variable name supplied" ; exit 1; }
        echo "aggpp | ${freq}.${var}.${fld}        | begin $(date)">>$loadBal
        ifile="$rawsdir/$freq/${var}.${fld}/${var}.${fld}.${period}.DayMin.grb"
        ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_${fld}_${freq}_$period.nc; rm -f $ofile
        $cdo -f nc4 -setgrid,${GRIDFILE} -setname,$var -setmissval,9999 $ifile $ofile || echo "ERROR: $ofile setgrid operation"
        echo "aggpp | ${freq}.${var}.${fld}        | end   $(date)">>$loadBal
    }; )& ; }>$err.day.${1-var}.2D 2>&1
}


###### Acc

#-- dayAcc - 2D - set-grid operation, setmissval to 9999 , acc->mean calculation
agg_dayAcc_2D() {
    { (if_requested $member $esmod aggregation day $chunk && {
        freq=day ; var=${1-} ; fld=2D
        [[ -z "$var" ]] && { echo "Function ${0}: no variable name supplied" ; exit 1; }
        echo "aggpp | ${freq}.${var}.${fld}        | begin $(date)">>$loadBal
        ifile="$rawsdir/$freq/${var}.${fld}/${var}.${fld}.${period}.DaySum.grb"
        ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_${fld}_${freq}_$period.nc; rm -f $ofile
        if  [[ "$var" == "DURSUN" ]]; then
            $cdo -f nc4 -setgrid,${GRIDFILE} -setname,$var -setmissval,9999 $ifile $ofile || echo "ERROR: $ofile setgrid operation"
        else
            $cdo -f nc4 -setgrid,${GRIDFILE} -divc,86400. -setname,$var -setmissval,9999 $ifile $ofile || echo "ERROR: $ofile setgrid and division operation"
        fi
        echo "aggpp | ${freq}.${var}.${fld}        | end   $(date)">>$loadBal
    }; )& ; }>$err.day.${1-var}.2D 2>&1
}


###### 3D

#-- day - 3D - set-grid operation, concat/split to monthly chunks, setmissval to 9999
agg_day_3D() {
    { (if_requested $member $esmod aggregation day3D $chunk && {
        freq=day ; var=${1-} ; fld=3D
        [[ -z "$var" ]] && { echo "Function ${0}: no variable name supplied" ; exit 1; }
        echo "aggpp | ${freq}.${var}.${fld}        | begin $(date)">>$loadBal
        if [[ "$var" == "QV" ]]; then
            varcode=51
            vartab=2
        elif [[ "$var" == "T" ]]; then
            varcode=11
            vartab=2
        elif [[ "$var" == "TKE" ]]; then
            varcode=52
            vartab=201
        elif [[ "$var" == "W" ]]; then
            varcode=40
            vartab=2
        else
            echo "ERROR: only variables supported in this function are 'QV', 'T', 'TKE' and 'W'. '$var' specified!"
            varcode=0
            vartab=0
        fi
        ifiles="$rawsdir/$freq/${var}.${fld}/${var}.${fld}.${period}*.grb"
        ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_${fld}_${freq}_$period.nc; rm -f $ofile
        ofile_tmp=$sdir/tmp_aggr/${var}_${fld}_${freq}_$period.nc; rm -f $ofile_tmp
        ifile_z="$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_HSURF_2D_fx_$YYYY.nc"
        echo "$cdo -f nc4 setgrid,${GRIDFILE} -setzaxis,${ZAXISFILE6} -setmissval,9999 -merge -setparam,${varcode}.${vartab} -setname,$var -mergetime [ $( ls $ifiles ) ] $ofile_tmp"
        $cdo -f nc4 setgrid,${GRIDFILE} -setzaxis,${ZAXISFILE6} -setmissval,9999 -merge -setparam,${varcode}.${vartab} -setname,$var -mergetime [ $( ls $ifiles ) ] $ofile_tmp || echo "ERROR: $ofile mergetime and setgrid operation"
        echo "$cdo -f nc4 merge $ofile_tmp $ifile_z $ofile"
        $cdo -f nc4 merge $ofile_tmp $ifile_z $ofile && rm -f $ofile_tmp || echo "ERROR: $ofile merge operation"
        echo "aggpp | ${freq}.${var}.${fld}        | end   $(date)">>$loadBal
    }; )& ; }>$err.day.${1-var}.3D 2>&1
}





###############
# mon
###############


###### orog_mon

#-- orog_mon as fix for two HSURF / surface altitude fields in COSMO-REA6
{ (if_requested $member $esmod mon orog $chunk && {
  ifile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_HSURF_2D_fx_${YYYY}.grb
  ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_HSURF_2D_mon_${period}.nc
  echo "$cdo -f nc4 settaxis,${YYYY}-${MM}-16,12:00:00 -setgrid,${GRIDFILE} -setmissval,9999 -setname,HSURF $ifile ${ofile}"
  $cdo -f nc4 settaxis,${YYYY}-${MM}-16,12:00:00 -setgrid,${GRIDFILE} -setmissval,9999 -setname,HSURF $ifile ${ofile} || echo "ERROR: $ofile setgrid and settaxis operation"
}; )& ; }>$err.mon.orog.2D 2>&1


###### Ave

#-- mon - 2D - set-grid operation, concat/split to monthly chunks, setmissval to 9999
agg_mon_2D() {
    { (if_requested $member $esmod aggregation mon $chunk && {
        freq=mon ; var=${1-} ; fld=2D
        [[ -z "$var" ]] && { echo "Function ${0}: no variable name supplied" ; exit 1; }
        echo "aggpp | ${freq}.${var}.${fld}        | begin $(date)">>$loadBal
        ifileYYYY="$rawsdir/$freq/${var}.${fld}/${var}.${fld}.${YYYY}.MonMean.grb"
        ifileMM="$rawsdir/$freq/${var}.${fld}/${var}.${fld}.${period}.MonMean.grb"
        ifile=$sdir/tmp_aggr/${var}.${fld}.MonMean.${period}.grb
        ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_${fld}_${freq}_$period.nc; rm -f $ofile

        if [[ -f $ifileYYYY ]] && { [[ $period -eq ${YYYY}01 ]] || [[ $period -eq ${iniyear}${inimonth} ]]; }; then
            $cdo -O splitmon $ifileYYYY $sdir/tmp_aggr/${var}.${fld}.MonMean.${YYYY} || echo "ERROR: $ifileYYYY splitmon operation"
        elif [[ ! -f $ifile ]]; then
            \cp -v $ifileMM $ifile || echo "ERROR: Creating hard link at $ifile for $ifileMM"
        fi

        ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_${fld}_${freq}_$period.nc; rm -f $ofile
        $cdo -f nc4 -setgrid,${GRIDFILE} -setname,$var -setmissval,9999 $ifile $ofile || echo "ERROR: $ofile setgrid operation"
        echo "aggpp | ${freq}.${var}.${fld}        | end   $(date)">>$loadBal
    }; )& ; }>$err.mon.${1-var}.2D 2>&1
}


#-- mon converted - 2D - set-grid operation, concat/split to monthly chunks
agg_monConv_2D() {
    { (if_requested $member $esmod aggregation mon $chunk && {
        freq=mon ; var=${1-} ; fld=2D
        [[ -z "$var" ]] && { echo "Function ${0}: no variable name supplied" ; exit 1; }
        echo "aggpp | ${freq}.${var}.${fld}        | begin $(date)">>$loadBal
        ifileYYYY="$rawsdir/$freq/${var}.${fld}/${var}m.${fld}.${YYYY}.MonMean.nc4"
        ifileMM="$rawsdir/$freq/${var}.${fld}/${var}m.${fld}.${period}.MonMean.nc"
        ifile=$sdir/tmp_aggr/${var}m.${fld}.MonMean.${period}.nc4
        ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}m_${fld}_${freq}_$period.nc; rm -f $ofile
        if [[ -f $ifileYYYY ]] && { [[ $period -eq ${YYYY}01 ]] || [[ $period -eq ${iniyear}${inimonth} ]]; }; then
            echo "$cdo -O splitmon $ifileYYYY $sdir/tmp_aggr/${var}m.${fld}.MonMean.${YYYY}"
            $cdo -O splitmon $ifileYYYY $sdir/tmp_aggr/${var}m.${fld}.MonMean.${YYYY} || echo "ERROR: $ifileYYYY splitmon operation"
        elif [[ ! -f $ifile ]]; then
            echo "cp -v $ifileMM $ifile"
            \cp -v $ifileMM $ifile || echo "ERROR: Creating hard link at $ifile for $ifileMM"
        fi
        echo "$cdo -f nc4 -setgrid,${GRIDFILE} $ifile $ofile"
        $cdo -f nc4 -setgrid,${GRIDFILE} $ifile $ofile || echo "ERROR: $ofile setgrid operation"
        echo "aggpp | ${freq}.${var}.${fld}        | end   $(date)">>$loadBal
    }; )& ; }>$err.mon.${1-var}.2D 2>&1
}


###### Acc

#-- monAcc - 2D - set-grid operation, concat/split to monthly chunks, setmissval to 9999, acc->mean calculation
agg_monAcc_2D() {
    { (if_requested $member $esmod aggregation mon $chunk && {
        freq=mon ; var=${1-} ; fld=2D
        [[ -z "$var" ]] && { echo "Function ${0}: no variable name supplied" ; exit 1; }
        echo "aggpp | ${freq}.${var}.${fld}        | begin $(date)">>$loadBal
        ifileYYYY="$rawsdir/$freq/${var}.${fld}/${var}.${fld}.${YYYY}.MonSum.grb"
        ifileMM="$rawsdir/$freq/${var}.${fld}/${var}.${fld}.${period}.MonSum.grb"
        ifile=$sdir/tmp_aggr/${var}.${fld}.MonSum.${period}.grb
        ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_${fld}_${freq}_$period.nc; rm -f $ofile

        if [[ -f $ifileYYYY ]] && { [[ $period -eq ${YYYY}01 ]] || [[ $period -eq ${iniyear}${inimonth} ]]; }; then
            $cdo -O splitmon $ifileYYYY $sdir/tmp_aggr/${var}.${fld}.MonSum.${YYYY} || echo "ERROR: $ifileYYYY splitmon operation"
        elif [[ ! -f $ifileYYYY ]]; then
            \cp -v $ifileMM $ifile || echo "ERROR: Creating hard link at $ifile for $ifileMM"
        fi

        spm=$(( $(cal $(date +"${MM} ${YYYY}") | xargs -n1 | tail -n1) * 86400 )) #calc seconds per month
        if [[ "$var" == "DURSUN" ]]; then
            $cdo -f nc4 -setgrid,${GRIDFILE} -setname,$var -setmissval,9999 $ifile $ofile || echo "ERROR: $ofile setgrid operation"
        else
            $cdo -f nc4 -setgrid,${GRIDFILE} -divc,${spm}. -setname,$var -setmissval,9999 $ifile $ofile || echo "ERROR: $ofile setgrid and division operation"
        fi
        echo "aggpp | ${freq}.${var}.${fld}        | end   $(date)">>$loadBal
    }; )& ; }>$err.mon.${1-var}.2D 2>&1
}


###### 3D

#-- mon - 3D - set-grid operation, setmissval to 9999, merge
agg_mon_3D() {
    { (if_requested $member $esmod aggregation mon3D $chunk && {
        freq=mon ; var=${1-} ; fld=3D
        [[ -z "$var" ]] && { echo "Function ${0}: no variable name supplied" ; exit 1; }
        echo "aggpp | ${freq}.${var}.${fld}        | begin $(date)">>$loadBal
        if [[ "$var" == "QV" ]]; then
            varcode=51
            vartab=2
        elif [[ "$var" == "T" ]]; then
            varcode=11
            vartab=2
        elif [[ "$var" == "TKE" ]]; then
            varcode=52
            vartab=201
        elif [[ "$var" == "W" ]]; then
            varcode=40
            vartab=2
        else
            echo "ERROR: only variables supported in this function are 'QV', 'T', 'TKE' and 'W'. '$var' specified!"
            varcode=0
            vartab=0
        fi
        ifile="$rawsdir/$freq/${var}.${fld}/${var}.${fld}.${period}.MonMean.grb"
        ofile=$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_${var}_${fld}_${freq}_$period.nc; rm -f $ofile
        ifile_z="$sdir/out_aggr/${esmod}_${EXP_ID}_${member}_HSURF_2D_fx_$YYYY.nc"
        echo "$cdo -f nc4 setgrid,${GRIDFILE} -setzaxis,${ZAXISFILE6} -setmissval,9999 -merge -setparam,${varcode}.${vartab} -setname,$var $ifile -setname,orog $ifile_z $ofile"
        $cdo -f nc4 setgrid,${GRIDFILE} -setzaxis,${ZAXISFILE6} -setmissval,9999 -merge -setparam,${varcode}.${vartab} -setname,$var $ifile $ifile_z $ofile || echo "ERROR: $ofile merge and setgrid operation"
        echo "aggpp | ${freq}.${var}.${fld}        | end   $(date)">>$loadBal
    }; )& ; }>$err.mon.${1-var}.3D 2>&1
}





###
### -- Execute aggregation functions for all variables
###

for v in ${vars_1hrPtTOday-}; do agg_1hrPtTOday_2D $v ; done
for v in ${vars_1hrPtTOday-}; do agg_1hrPtTOmon_2D $v ; done
for v in ${vars_1hrTOday-}; do agg_1hrTOday_2D $v ; done
for v in ${vars_1hrTOday-}; do agg_1hrTOmon_2D $v ; done
for v in ${vars_1hrTOdayAcc-}; do agg_1hrTOdayAcc_2D $v ; done
for v in ${vars_1hrTOdayAcc-}; do agg_1hrTOmonAcc_2D $v ; done
for v in ${vars_1hrTOdayMax-}; do agg_1hrTOdayMax_2D $v ; done
for v in ${vars_1hrTOdayMax-}; do agg_1hrTOmonMax_2D $v ; done
for v in ${vars_2Dto3D-}; do agg_1hrPt_2Dto3D $v ; done
for v in ${vars_2Dto3D-}; do agg_1hrPtTOday_2Dto3D $v ; done
for v in ${vars_2Dto3D-}; do agg_1hrPtTOmon_2Dto3D $v ; done
for v in ${vars_1hr-}; do agg_1hr_2D $v ; done
for v in ${vars_1hrPt-}; do agg_1hrPt_2D $v ; done
for v in ${vars_1hrPtConv-}; do agg_1hrPtConv_2D $v ; done
for v in ${vars_1hrAcc-}; do agg_1hrAcc_2D $v ; done
for v in ${vars_1hrMax-}; do agg_1hrMax_2D $v ; done
for v in ${vars_1hr3D-}; do agg_1hrPt_3D $v ; done
for v in ${vars_1hr3D40-}; do agg_1hrPt_3D_40 $v ; done
for v in ${vars_1hr3D40-}; do agg_1hrPt_3Dplev $v ; done
for v in ${vars_1hrUV-}; do agg_1hrPt_3D_UV $v ; done
for v in ${vars_1hrUV-}; do agg_1hrPt_3Dplev_UV $v ; done
for v in ${vars_pfull-}; do agg_1hrPt_pfull $v ; done
for v in ${vars_6hrMax-}; do agg_6hrMax_2D $v ; done
for v in ${vars_6hrMin-}; do agg_6hrMin_2D $v ; done
for v in ${vars_day-}; do agg_day_2D $v ; done
for v in ${vars_dayConv-}; do agg_dayConv_2D $v ; done
for v in ${vars_dayAcc-}; do agg_dayAcc_2D $v ; done
for v in ${vars_dayMax-}; do agg_dayMax_2D $v ; done
for v in ${vars_dayMin-}; do agg_dayMin_2D $v ; done
for v in ${vars_day3D-}; do agg_day_3D $v ; done
for v in ${vars_mon-}; do agg_mon_2D $v ; done
for v in ${vars_monConv-}; do agg_monConv_2D $v ; done
for v in ${vars_monAcc-}; do agg_monAcc_2D $v ; done
for v in ${vars_mon3D-}; do agg_mon_3D $v ; done

wait #-- for all operations to finish

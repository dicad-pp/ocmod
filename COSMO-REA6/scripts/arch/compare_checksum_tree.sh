#!/bin/bash
#
#SBATCH --job-name=checksum
#SBATCH --partition=interactive
#SBATCH --time=08:00:00
#SBATCH --mem=64G
#SBATCH --output=%x_%j.log
#SBATCH --mail-type=FAIL
#SBATCH --account=bk1192
#
##################################
# Martin Schupfner, DKRZ, 2023
#
# Short script to compare de-archived 
# data with data in archive.
#
# Works recursively.
#
# Does not work if slk is
# overloaded or hanging.
##################################



function print_usage {
  echo "Run as 'bash compare_checksum_tree.sh <archive_dir> <local_dir> [<uuid>]'"
  echo "  or 'sbatch -A <account> compare_checksum_tree.sh <archive_dir> <local_dir>'"
  echo "  archive_dir: archive parent directory."
  echo "  local_dir: disk parent directory."
  echo "  uuid: if continuing a previous run, provide its uuid."
  echo "Compares checksums for files under <archive_dir> and files ander <local_dir>."
  echo " <archive_dir> is considered the truth. Works recursively."
  exit 1
}

if [[ -z "$1" ]] || [[ -z "$2" ]] ; then
  print_usage
fi

[[ "$1" != "/arch/"* ]] && {
  echo "The first argument has to be a tape directory (/arch/*)"
  exit 1
}
[[ "$2" != "/work/"* ]] && {
  echo "The second argument has to be the full path on disk (/work/*)"
  exit 1
}

if ! command -v slk &> /dev/null
then
    module load slk || { echo "ERROR: Could not load slk module!" ; exit 1 ; }
fi

function checksum_dir {
  #echo 1: $1
  #echo 2: $2
  #echo 3: $3
  #echo 4: $4
  #echo 5: $5
  # Create temporary output dir for checksums and result txt files
  tmpdir=tmp_slk_checksum_tree_${4}
  difffile=diff_${4}.txt
  nofile=nonexistant_${4}.txt
  mkdir -p $tmpdir || { echo "ERROR: could not create \"$tmpdir\""; exit 1 ; }
  touch $difffile || { echo "ERROR: could not create \"$difffile\""; exit 1 ; }
  touch $nofile || { echo "ERROR: could not create \"$nofile\""; exit 1 ; }
  # Construct array of slk list output
  mapfile -t sl < <(slk list -b $2 | grep -v "^[[:space:]]*$" | grep -v "^Files:")
  # Construct array of files in current dir
  mapfile -t files < <(printf "%s\n" "${sl[@]}" | awk 'substr($1,1,1) != "d" { print $4, $9 }' | grep -v "^[[:space:]]*$" )
  mapfile -t filenames < <(printf "%s\n" "${sl[@]}" | awk 'substr($1,1,1) != "d" { print $9 }' | grep -v "^[[:space:]]*$" )
  #echo ${sl[@]}
  #echo ${files[@]}
  #echo ${filenames[@]}
  # Construct list of folders in current dir
  folders=$(printf "%s\n" "${sl[@]}" | awk 'substr($1,1,1) == "d" { print $9 }')
  # Sum up filesizes in current dir (not recursively)
  sum=$(printf "%s\n" "${files[@]}" | awk 'BEGIN {FS = "[[:space:]]+"} ; {sum+=$1} END {printf "%.2f", sum/1024/1024/1024}')
  # Print number of files and filesize if files are found
  [[ ${#files[@]} -ge 1 ]] && echo "$(printf '%*s' $5)Files: ${#files[@]} ($sum G)"
  # Compute and compare checksums for files in arch and on disk
  maxjobs=24
  n=0
  for file in ${filenames[@]}; do
    (
      l_file=$(echo $2 | sed "s,$1,$3,g" )/$file
      if [[ -e $l_file ]]; then
          if [[ ! -e $tmpdir/$(echo "$2/$file" | sed "s,/,_-_,g" | sed "s,_-__-_,_-_,g" ).sha512_remote ]]; then
          sha512sum $l_file | cut -d " " -f 1 > $tmpdir/$(echo "$2/$file" | sed "s,/,_-_,g" | sed "s,_-__-_,_-_,g" ).sha512 || { echo ERROR could not create checksum for $l_file && continue ; }
          fi
          slk_helpers checksum -t sha512 $2/$file > $tmpdir/$(echo "$2/$file" | sed "s,/,_-_,g" | sed "s,_-__-_,_-_,g" ).sha512_remote || { echo ERROR could not retrieve remote checksum for $2/$file && continue ; }
          if ! diff -q $tmpdir/$(echo "$2/$file" | sed "s,/,_-_,g" | sed "s,_-__-_,_-_,g" ).sha512 $tmpdir/$(echo "$2/$file" | sed "s,/,_-_,g" | sed "s,_-__-_,_-_,g" ).sha512_remote &>/dev/null; then
            echo $( echo $2/$file | sed "s,//,/,g" ) >> $difffile
          fi
      else
          echo $( echo $2/$file | sed "s,//,/,g" ) >> $nofile
      fi
    )&
  if (( $(($((++n)) % $maxjobs)) == 0 )) ; then
      wait
  fi
  done
  wait
  # for each subfolder, start tree
  for i in $(echo ${folders[@]} | sort)
  do
      echo "$(printf '%*s' $5)$i  -  $2/$i"
      checksum_dir $1 $2/$i $3 $4 $(($5 +4 ))
  done
}

# Check slk token
slk_helpers session &> /dev/null || {
  tty -s || { echo "ERROR: slk session token not found or expired, please run 'slk login'!"; exit 1 ; }
  echo "INFO: slk session token not found or expired, running 'slk login'..."
  slk login
}

# If namespace/file exists on tape, start tree
slk_helpers exists $1 &> /dev/null || {
  echo "\"$1\" cannot be read or does not exist!"
  exit 1
}
[[ -d $2 ]] || {
  echo "\"$2\" cannot be read or does not exist!"
} 
if [[ -z "$3" ]]; then
  checksum_dir $1 $1 $2 $(uuid) 0
else
  checksum_dir $1 $1 $2 $3 0
fi

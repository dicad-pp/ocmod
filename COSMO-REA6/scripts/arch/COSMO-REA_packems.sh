#!/bin/bash

#
# PACKEMS + slk
#
#SBATCH --job-name=packems
#SBATCH --partition=compute
#SBATCH --time=24:00:00
#SBATCH --mem=200G
#SBATCH --output=%x_%j.log
#SBATCH --mail-type=FAIL
#SBATCH --account=bm0021
#SBATCH --qos=esgf

module load python3
module load slk
module load packems 

cwd=$(pwd)
njobs=12

WORKDIR="/work/bk1261/OcMOD/COSMO-REA6/HSM/arch"
#WORKDIR="/work/bk1261/OcMOD/COSMO-REA6/HSM/archecmwf"
EXPDIR="/work/bk1261/OcMOD/COSMO-REA6/outdata"
#EXPDIR="/work/bk1261/OcMOD/COSMO-REA6/outdata_ecmwf"
TAPEDIR_ROOT="/arch/bk1261/COSMO-REA/COSMO-REA6"
TARNAMEBASE="COSMO-REA6"

# DWD OpenData
hrPt=("CLCT.2D" "DENS040.2D" "DENS060.2D" "DENS080.2D" "DENS100.2D" "DENS125.2D" "DENS150.2D" "DENS175.2D" "DENS200.2D" "HPBL.2D" "HSNOW.2D" "LHFLS.2D" "LWUS.2D" "PMSL.2D" "PS.2D" "QV2M.2D" "QVS.2D" "RELHUM2M.2D" "RHOSNOW.2D" "SHFLS.2D" "SNOWLMT.2D" "SOBSRAD.2D" "SWDIFDSRAD.2D" "SWDIRSRAD.2D" "T2M.2D" "THBSRAD.2D" "THDSRAD.2D" "TQV.2D" "TSNOW.2D" "TSOIL.2D" "U100.2D" "U10M.2D" "V100.2D" "V10M.2D" "WD010.2D" "WD040.2D" "WD060.2D" "WD080.2D" "WD100.2D" "WD125.2D" "WD150.2D" "WD175.2D" "WD200.2D" "WS010.2D" "WS040.2D" "WS060.2D" "WS080.2D" "WS100.2D" "WS125.2D" "WS150.2D" "WS175.2D" "WS200.2D" "WSNOW.2D")
hr3D=("Q.3D" "U.3D" "V.3D" "T.3D" "TKE.3D")
hr=("ALWUS.2D" "ASOBS.2D" "ASWDIFDS.2D" "ASWDIRS.2D" "ATHBS.2D" "ATHDS.2D" "DURSUN.2D" "RAINCON.2D" "RAINGSP.2D" "RUNOFFG.2D" "RUNOFFS.2D" "SNOWCON.2D" "SNOWGSP.2D" "TMAX2M.2D" "TMIN2M.2D" "TOTPRECIP.2D" "VMAX10M.2D")
day=( "${hrPt[@]}" "${hr[@]}" ) 

# ECMWF
#hrPt=("HZEROCL.2D" "CLCH.2D" "CLCL.2D" "CLCM.2D" "SOBTRAD.2D" "SODTRAD.2D" "SWDIFUSRAD.2D" "TD2M.2D" "TG.2D" "TQC.2D" "TQI.2D" "THBTRAD.2D" "TSOIL0CM.2D" "TSOIL1458CM.2D" "TSOIL162CM.2D" "TSOIL18CM.2D" "TSOIL1CM.2D" "TSOIL2CM.2D" "TSOIL486CM.2D" "TSOIL54CM.2D" "TSOIL6CM.2D" "TWATER.2D" "UMFLS.2D" "VMFLS.2D" "WSOIL1458CM.2D" "WSOIL162CM.2D" "WSOIL18CM.2D" "WSOIL1CM.2D" "WSOIL2CM.2D" "WSOIL486CM.2D" "WSOIL54CM.2D" "WSOIL6CM.2D" "WSOILICE1458CM.2D" "WSOILICE162CM.2D" "WSOILICE18CM.2D" "WSOILICE1CM.2D" "WSOILICE2CM.2D" "WSOILICE486CM.2D" "WSOILICE54CM.2D" "WSOILICE6CM.2D")
#hr=("AEVAPS.2D" "ALBRAD.2D" "ALHFLS.2D" "ASHFLS.2D" "ASOBT.2D" "ASODT.2D" "ASWDIFUS.2D" "ATHBT.2D" "AUMFLS.2D" "AVMFLS.2D" "VABSMX10M.2D" "VGUSTCON.2D" "VGUSTDYN.2D")
#hr3D=("QV.3D" "U.3D" "V.3D" "W.3D" "T.3D" "PP.3D")
#day=( "${hrPt[@]}" "${hr[@]}" )


#
# PACKING and ARCHIVING done by packems
#
echo "----------------------------------------"
echo "- Start"
echo "----------------------------------------"
    
cd $EXPDIR
echo $(pwd)

for FREQUENCY in mon day 1hr 1hrPt
do
    for GRID in 2D 3D
    do
        echo "##########################"
        echo "$FREQUENCY $GRID"
        echo "##########################"
        TAPEDIR=$TAPEDIR_ROOT/$FREQUENCY
        if [[ "$FREQUENCY" == "mon" ]]
        then
            if [[ "$GRID" == "2D" ]]
            then
                TARNAME=${TARNAMEBASE}_mon.2D
                INFOLDER=
                for var in ${day[@]}
                do
                    INFOLDER="$INFOLDER $FREQUENCY/$var"
                done # var
                sh -c 'scontrol update JobId=$SLURM_JOB_ID Comment="$*"' scomment "$TARNAME"
                packems_wrapper -j ${njobs} --lock-break -F -L -a -x -S ${TAPEDIR}/ -d ${WORKDIR} -o "$TARNAME" -O by_name -D by_order --archive-index INDEX.txt $INFOLDER || { echo "ERROR: packems/slk error." ; exit 1 ; }
            elif [[ "$GRID" == "3D" ]]
            then
                for var in ${hr3D[@]}
                do
                    TARNAME=${TARNAMEBASE}_$var
                    INFOLDER=$FREQUENCY/${FREQUENCY}_$var
                    sh -c 'scontrol update JobId=$SLURM_JOB_ID Comment="$*"' scomment "$INFOLDER $TARNAME"
                    packems_wrapper -j ${njobs} --lock-break -F -L -a -x -S ${TAPEDIR}/ -d ${WORKDIR} -o "$TARNAME" -O by_name -D by_order --archive-index INDEX.txt $INFOLDER || { echo "ERROR: packems/slk error." ; exit 1 ; }
                done # var
            fi # GRID 
        elif [[ "$FREQUENCY" == "day" ]]
        then
            if [[ "$GRID" == "2D" ]]
            then
                for var in ${day[@]}
                do
                    TARNAME=${TARNAMEBASE}_$var
                    INFOLDER=$FREQUENCY/${FREQUENCY}_$var
                    sh -c 'scontrol update JobId=$SLURM_JOB_ID Comment="$*"' scomment "$INFOLDER $TARNAME"
                    packems_wrapper -j ${njobs} --lock-break -F -L -a -x -S ${TAPEDIR}/ -d ${WORKDIR} -o "$TARNAME" -O by_name -D by_order --archive-index INDEX.txt $INFOLDER || { echo "ERROR: packems/slk error." ; exit 1 ; }
                done # var
            elif [[ "$GRID" == "3D" ]]
            then
                for var in ${hr3D[@]}
                do
                    TARNAME=${TARNAMEBASE}_$var
                    INFOLDER=$FREQUENCY/${FREQUENCY}_$var
                    sh -c 'scontrol update JobId=$SLURM_JOB_ID Comment="$*"' scomment "$INFOLDER $TARNAME"
                    packems_wrapper -j ${njobs} --lock-break -F -L -a -x -S ${TAPEDIR}/ -d ${WORKDIR} -o "$TARNAME" -O by_name -D by_order --archive-index INDEX.txt $INFOLDER || { echo "ERROR: packems/slk error." ; exit 1 ; }
                done # var
            fi # GRID
        elif [[ "$FREQUENCY" == "1hr" ]]
        then
            if [[ "$GRID" == "2D" ]]
            then
                for var in ${hr[@]}
                do
                    TARNAME=${TARNAMEBASE}_$var
                    INFOLDER=$FREQUENCY/${FREQUENCY}_$var
                    sh -c 'scontrol update JobId=$SLURM_JOB_ID Comment="$*"' scomment "$INFOLDER $TARNAME"
                    packems_wrapper -j ${njobs} --lock-break -F -L -a -x -S ${TAPEDIR}/ -d ${WORKDIR} -o "$TARNAME" -O by_name -D by_order --archive-index INDEX.txt $INFOLDER || { echo "ERROR: packems/slk error." ; exit 1 ; }
                done # var
            fi # GRID
        elif [[ "$FREQUENCY" == "1hrPt" ]]
        then
            if [[ "$GRID" == "2D" ]]
            then
                for var in ${hrPt[@]}
                do
                    TARNAME=${TARNAMEBASE}_$var
                    INFOLDER=$FREQUENCY/${FREQUENCY}_$var
                    sh -c 'scontrol update JobId=$SLURM_JOB_ID Comment="$*"' scomment "$INFOLDER $TARNAME"
                    packems_wrapper -j ${njobs} --lock-break -F -L -a -x -S ${TAPEDIR}/ -d ${WORKDIR} -o "$TARNAME" -O by_name -D by_order --archive-index INDEX.txt $INFOLDER || { echo "ERROR: packems/slk error." ; exit 1 ; }
                done # var
            elif [[ "$GRID" == "3D" ]]
            then
                for var in ${hr3D[@]}
                do
                    TARNAME=${TARNAMEBASE}_$var
                    INFOLDER=$FREQUENCY/${FREQUENCY}_$var
                    sh -c 'scontrol update JobId=$SLURM_JOB_ID Comment="$*"' scomment "$INFOLDER $TARNAME"
                    packems_wrapper -j ${njobs} --lock-break -F -L -a -x -S ${TAPEDIR}/ -d ${WORKDIR} -o "$TARNAME" -O by_name -D by_order --archive-index INDEX.txt $INFOLDER || { echo "ERROR: packems/slk error." ; exit 1 ; }
                done # var
            fi
        fi # FREQUENCY
    done # GRID
done # FREQUENCY



# compute sha512 checksums
sh -c 'scontrol update JobId=$SLURM_JOB_ID Comment="$*"' scomment "sha512_checksum"
echo
echo "Creating checksums ..."
maxjobs=12
n=0
for tarfile in $(ls ${WORKDIR}/*.tar ) ; do
 (
    bn=$( basename $tarfile )
    sha512sum $tarfile | cut -d " " -f1 > ${WORKDIR}/${bn}.sha512 ||  { echo "ERROR creating sha512 checksum for ${WORKDIR}/${bn}.sha512" ; }
)&
if (( $(($((++n)) % $maxjobs)) == 0 )) ; then
    wait
fi
done
wait

echo "----------------------------------------"
echo "- Complete"
echo "----------------------------------------"

cd $cwd


# packems:
# -j parallelisation 
# -F fail fast
# -d output for tar files
# -o prefix for tarfiles
# -O/-D how to search/add files to tar files
# 
# -S destination in hpss
# -a generate archiving commands for hpss transfer
# -n dry run
# -I index file name
# -x add group to index file
#
# --purge-archived : delete tar-files after they are in hpss
# -i --input : read files to be archived from textfile
# -L --dereference : follow links
# -s START_WITH, --start-with START_WITH
# --lock-break remove locks to allow resume after interrupt
#
# folder to archive as $1

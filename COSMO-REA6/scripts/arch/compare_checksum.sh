#
# PACKEMS + slk
#
#SBATCH --job-name=slk_checksum
#SBATCH --partition=interactive
#SBATCH --time=08:00:00
#SBATCH --mem=64G
#SBATCH --output=%x_%j.log
#SBATCH --mail-type=FAIL
#SBATCH --account=bk1192

module load slk

cwd=$(pwd)
TMP_DIR=$cwd/arch
ARCH_DIR=/arch/bk1261/COSMO-REA/COSMO-REA6

#
# control checksums simulation - restart and outdata
#

source_numfiles=$( cat ${TMP_DIR}/*.tar.idx | wc -l )
target_numtars=$( find ${TMP_DIR}/ -type f -name "*.tar.idx" | wc -l )

echo "--------------------------------------------------------------------"
echo "- $source_numfiles files in $target_numtars tar-archives"
echo "--------------------------------------------------------------------"
    
# compute/retrieve sha512 checksums and compare
sh -c 'scontrol update JobId=$SLURM_JOB_ID Comment="$*"' scomment "sha512_checksum"
maxjobs=6
n=0
for tarfile in $(ls ${TMP_DIR}/*.tar ); do
 (
    bn=$( basename $tarfile )
    freq=$( echo $bn | cut -d "_" -f 1)
    #echo $bn
    # local
    if [[ ! -e ${TMP_DIR}/${bn}.sha512 ]]; then
        sha512sum $tarfile | cut -d " " -f1 > ${TMP_DIR}/${bn}.sha512 || { echo ERROR could not create checksum for $bn && continue ; }
    fi
    # remote
    if [[ ! -e ${TMP_DIR}/${bn}.sha512_remote ]]; then
        slk_helpers checksum -t sha512 ${ARCH_DIR}/${freq}/${bn} > ${TMP_DIR}/${bn}.sha512_remote || { echo ERROR could not retrieve remote checksum for $bn && continue ; }
    fi
    if diff -q ${TMP_DIR}/${bn}.sha512 ${TMP_DIR}/${bn}.sha512_remote &>/dev/null; then
        echo "SUCCESS ${bn} - checksums are equal"
    else
        echo "FAILURE $bn - checksums differ"
    fi

)&
if (( $(($((++n)) % $maxjobs)) == 0 )) ; then
    wait
fi
done
wait


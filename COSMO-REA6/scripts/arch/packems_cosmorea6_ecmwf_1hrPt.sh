#!/bin/bash

#
# PACKEMS + slk
#
#SBATCH --job-name=packems
#SBATCH --partition=compute
#SBATCH --time=4:00:00
#SBATCH --mem=250G
#SBATCH --output=%x_%j.log
#SBATCH --mail-type=FAIL
#SBATCH --account=bm0021
#SBATCH --qos=esgf

#module load python3
#module load slk
module load packems 

EXP=1hrPt
VAR=QV #TKE #V #U #QV #T #PP

EXP_ROOT=/work/kd1292/k204212/OcMOD/outdata_3D/

cwd=$(pwd)
TMP_DIR=$cwd/3D_${EXP}_${VAR}
mkdir -p $TMP_DIR

njobs=24

#
# BACKUP simulation - restart and outdata
#
echo "----------------------------------------"
echo "- Start"
echo "----------------------------------------"
    
cd $EXP_ROOT
echo $(pwd)

# outdata
sh -c 'scontrol update JobId=$SLURM_JOB_ID Comment="$*"' scomment "packems COSMO-REA6-ECMWF $VAR $EXP"
packems_wrapper -j ${njobs} --lock-break -F -L -x -d ${TMP_DIR} +a -o "COSMO-REA6_3D_${EXP}_${VAR}" -O by_name -D by_order --archive-index INDEX_${EXP}_${VAR}.txt $EXP/${VAR}.3D || { echo "ERROR: packems/slk error." ; exit 1 ; }


echo "----------------------------------------"
echo "- Complete"
echo "----------------------------------------"

cd $cwd


# packems:
# -j parallelisation 
# -F fail fast
# -d output for tar files
# -o prefix for tarfiles
# -O/-D how to search/add files to tar files
# 
# -S destination in hpss
# -a generate archiving commands for hpss transfer
# -n dry run
# -I index file name
# -x add group to index file
#
# --purge-archived : delete tar-files after they are in hpss
# -i --input : read files to be archived from textfile
# -L --dereference : follow links
# -s START_WITH, --start-with START_WITH
# --lock-break remove locks to allow resume after interrupt
#
# folder to archive as $1

#!/bin/bash

#
# slk
#
#SBATCH --job-name=slk_archive_exps
#SBATCH --partition=interactive
#SBATCH --mem=60G
#SBATCH --time=108:00:00
#SBATCH --output=%x_%j.log
#SBATCH --mail-type=FAIL
#SBATCH --account=bm0021
#SBATCH --qos=esgf


module load slk

cwd=$(pwd)
SOURCEDIR=$cwd
ARCH_DIR=/arch/bk1261/COSMO-REA6_rawdata


cd $cwd

i=0
for var in 2D_1hr_AEVAPS 2D_1hr_ALBRAD 2D_1hr_ALHFLS 2D_1hr_ALWUS 2D_1hr_ASHFLS 2D_1hr_ASOBS 2D_1hr_ASOBT 2D_1hr_ASODT 2D_1hr_ASWDIFDS 2D_1hr_ASWDIFUS 2D_1hr_ASWDIRS 2D_1hr_ATHBS 2D_1hr_ATHBT 2D_1hr_ATHDS 2D_1hr_AUMFLS 2D_1hr_AVMFLS 2D_1hr_DURSUN 2D_1hrPt_CLCH 2D_1hrPt_CLCL 2D_1hrPt_CLCM 2D_1hrPt_CLCT 2D_1hrPt_DENS040 2D_1hrPt_DENS060 2D_1hrPt_DENS080 2D_1hrPt_DENS100 2D_1hrPt_DENS125 2D_1hrPt_DENS150 2D_1hrPt_DENS175 2D_1hrPt_DENS200 2D_1hrPt_HPBL 2D_1hrPt_HSNOW 2D_1hrPt_HZEROCL 2D_1hrPt_LHFLS 2D_1hrPt_LWUS 2D_1hrPt_PMSL 2D_1hrPt_PS 2D_1hrPt_QV2M 2D_1hrPt_QVS 2D_1hrPt_RELHUM2M 2D_1hrPt_RHOSNOW 2D_1hrPt_SHFLS 2D_1hrPt_SNOWLMT 2D_1hrPt_SOBSRAD 2D_1hrPt_SOBTRAD 2D_1hrPt_SODTRAD 2D_1hrPt_SWDIFDSRAD 2D_1hrPt_SWDIFUSRAD 2D_1hrPt_SWDIRSRAD 2D_1hrPt_T2M 2D_1hrPt_TD2M 2D_1hrPt_TG 2D_1hrPt_THBSRAD 2D_1hrPt_THBTRAD 2D_1hrPt_THDSRAD 2D_1hrPt_TQC 2D_1hrPt_TQI 2D_1hrPt_TQV 2D_1hrPt_TSNOW 2D_1hrPt_TSOIL 2D_1hrPt_TSOIL0CM 2D_1hrPt_TSOIL1458CM 2D_1hrPt_TSOIL162CM 2D_1hrPt_TSOIL18CM 2D_1hrPt_TSOIL1CM 2D_1hrPt_TSOIL2CM 2D_1hrPt_TSOIL486CM 2D_1hrPt_TSOIL54CM 2D_1hrPt_TSOIL6CM 2D_1hrPt_TWATER 2D_1hrPt_U100 2D_1hrPt_U10M 2D_1hrPt_UMFLS 2D_1hrPt_V100 2D_1hrPt_V10M 2D_1hrPt_VMFLS 2D_1hrPt_WD010 2D_1hrPt_WD040 2D_1hrPt_WD060 2D_1hrPt_WD080 2D_1hrPt_WD100 2D_1hrPt_WD125 2D_1hrPt_WD150 2D_1hrPt_WD175 2D_1hrPt_WD200 2D_1hrPt_WS010 2D_1hrPt_WS040 2D_1hrPt_WS060 2D_1hrPt_WS080 2D_1hrPt_WS100 2D_1hrPt_WS125 2D_1hrPt_WS150 2D_1hrPt_WS175 2D_1hrPt_WS200 2D_1hrPt_WSNOW 2D_1hrPt_WSOIL1458CM 2D_1hrPt_WSOIL162CM 2D_1hrPt_WSOIL18CM 2D_1hrPt_WSOIL1CM 2D_1hrPt_WSOIL2CM 2D_1hrPt_WSOIL486CM 2D_1hrPt_WSOIL54CM 2D_1hrPt_WSOIL6CM 2D_1hrPt_WSOILICE1458CM 2D_1hrPt_WSOILICE162CM 2D_1hrPt_WSOILICE18CM 2D_1hrPt_WSOILICE1CM 2D_1hrPt_WSOILICE2CM 2D_1hrPt_WSOILICE486CM 2D_1hrPt_WSOILICE54CM 2D_1hrPt_WSOILICE6CM 2D_1hr_RAINCON 2D_1hr_RAINGSP 2D_1hr_RUNOFFG 2D_1hr_RUNOFFS 2D_1hr_SNOWCON 2D_1hr_SNOWGSP 2D_1hr_TMAX2M 2D_1hr_TMIN2M 2D_1hr_TOTPRECIP 2D_1hr_VABSMX10M 2D_1hr_VGUSTCON 2D_1hr_VGUSTDYN 2D_1hr_VMAX10M 2D_day_AEVAPS 2D_day_ALBRAD 2D_day_ALHFLS 2D_day_ALWUS 2D_day_ASHFLS 2D_day_ASOBS 2D_day_ASOBT 2D_day_ASODT 2D_day_ASWDIFDS 2D_day_ASWDIFUS 2D_day_ASWDIRS 2D_day_ATHBS 2D_day_ATHBT 2D_day_ATHDS 2D_day_AUMFLS 2D_day_AVMFLS 2D_day_CLCH 2D_day_CLCL 2D_day_CLCM 2D_day_CLCT 2D_day_DENS040 2D_day_DENS060 2D_day_DENS080 2D_day_DENS100 2D_day_DENS125 2D_day_DENS150 2D_day_DENS175 2D_day_DENS200 2D_day_DURSUN 2D_day_HPBL 2D_day_HSNOW 2D_day_HZEROCL 2D_day_LHFLS 2D_day_LWUS 2D_day_PMSL 2D_day_PS 2D_day_QV2M 2D_day_QVS 2D_day_RAINCON 2D_day_RAINGSP 2D_day_RELHUM2M 2D_day_RHOSNOW 2D_day_RUNOFFG 2D_day_RUNOFFS 2D_day_SHFLS 2D_day_SNOWCON 2D_day_SNOWGSP 2D_day_SNOWLMT 2D_day_SOBSRAD 2D_day_SWDIFDSRAD 2D_day_SWDIRSRAD 2D_day_T2M 2D_day_TD2M 2D_day_TG 2D_day_THBSRAD 2D_day_THDSRAD 2D_day_TMAX2M 2D_day_TMIN2M 2D_day_TOTPRECIP 2D_day_TQC 2D_day_TQI 2D_day_TQV 2D_day_TSNOW 2D_day_TSOIL 2D_day_TWATER 2D_day_U100 2D_day_U10M 2D_day_V100 2D_day_V10M 2D_day_VABSMX10M 2D_day_VGUSTCON 2D_day_VGUSTDYN 2D_day_VMAX10M 2D_day_WD010 2D_day_WD040 2D_day_WD060 2D_day_WD080 2D_day_WD100 2D_day_WD125 2D_day_WD150 2D_day_WD175 2D_day_WD200 2D_day_WS010 2D_day_WS040 2D_day_WS060 2D_day_WS080 2D_day_WS100 2D_day_WS125 2D_day_WS150 2D_day_WS175 2D_day_WS200 2D_day_WSNOW 
do
    #
    # BACKUP variable
    #
    (( i++ ))
    echo "----------------------------------------"
    echo "- $i Start $var"
    echo "----------------------------------------"
    
    cd $SOURCEDIR/$var
    echo $(pwd)
   
    sh -c 'scontrol update JobId=$SLURM_JOB_ID Comment="$*"' scomment "Archiving $var"
 
    # Create remote directories
    echo "Creating /arch directory: ${ARCH_DIR}/${var}"
    slk_helpers mkdir -R ${ARCH_DIR}/${var} || echo ERROR

    # Transfer
    echo "Transferring..."
    ls -lah ./*.tar | sort
    slk archive ./*.tar ${ARCH_DIR}/${var} || echo ERROR
    
    echo "----------------------------------------"
    echo "- $i Complete $var"
    echo "----------------------------------------"
done
cd $cwd











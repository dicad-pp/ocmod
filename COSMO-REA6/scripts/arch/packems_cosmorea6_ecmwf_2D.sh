#!/bin/bash

#
# PACKEMS + slk
#
#SBATCH --job-name=packems
#SBATCH --partition=compute
#SBATCH --time=24:00:00
#SBATCH --mem=250G
#SBATCH --output=%x_%j.log
#SBATCH --mail-type=FAIL
#SBATCH --account=bm0021
#SBATCH --qos=esgf

#module load python3
#module load slk
module load packems 

EXP=1hrPt

EXP_ROOT=/work/kd1292/k204212/OcMOD/outdata_ecmwf/
EXP_ROOT=/work/kd1292/k204212/OcMOD/outdata/

cwd=$(pwd)

njobs=24

#1hrPt ecmwf
#for VAR in CLCH CLCL CLCM HZEROCL SOBTRAD SODTRAD SWDIFUSRAD TD2M TG THBTRAD TQC TQI TSOIL0CM TSOIL1458CM TSOIL162CM TSOIL18CM TSOIL1CM TSOIL2CM TSOIL486CM TSOIL54CM TSOIL6CM TWATER UMFLS VMFLS WSOIL1458CM WSOIL162CM WSOIL18CM WSOIL1CM WSOIL2CM WSOIL486CM WSOIL54CM WSOIL6CM WSOILICE1458CM WSOILICE162CM WSOILICE18CM WSOILICE1CM WSOILICE2CM WSOILICE486CM WSOILICE54CM WSOILICE6CM 
#1hr ecmwf
#for VAR in AEVAPS ALBRAD ALHFLS ASHFLS ASOBT ASODT ASWDIFUS ATHBT AUMFLS AVMFLS VABSMX10M VGUSTCON VGUSTDYN
#day ecmwf
#for VAR in AEVAPS ALBRAD ALHFLS ASHFLS ASOBT ASODT ASWDIFUS ATHBT AUMFLS AVMFLS CLCH CLCL CLCM HZEROCL TD2M TG TQC TQI TWATER VABSMX10M VGUSTCON VGUSTDYN
#1hrPt opendata
for VAR in CLCT DENS125 HSNOW QV2M SNOWLMT THBSRAD U100 WD040 WD150 WS060 WS175 DENS040 DENS150 LHFLS QVS SOBSRAD THDSRAD U10M WD060 WD175 WS080 WS200 DENS060 DENS175 LWUS RELHUM2M SWDIFDSRAD TQV V100 WD080 WD200 WS100 WSNOW DENS080 DENS200 PMSL RHOSNOW SWDIRSRAD TSNOW V10M WD100 WS010 WS125 DENS100 HPBL PS SHFLS T2M TSOIL WD010 WD125 WS040 WS150
#1hr opendata
#for VAR in ALWUS ASWDIFDS ATHBS DURSUN RAINGSP RUNOFFS SNOWGSP TMIN2M VMAX10M ASOBS ASWDIRS ATHDS RAINCON RUNOFFG SNOWCON TMAX2M TOTPRECIP
#day opendata
#for VAR in ALWUS ASOBS ASWDIFDS ASWDIRS ATHBS ATHDS CLCT DENS040 DENS060 DENS080 DENS100 DENS125 DENS150 DENS175 DENS200 DURSUN HPBL HSNOW LHFLS LWUS PMSL PS QV2M QVS RAINCON RAINGSP RELHUM2M RHOSNOW RUNOFFG RUNOFFS SHFLS SNOWCON SNOWGSP SNOWLMT SOBSRAD SWDIFDSRAD SWDIRSRAD T2M THBSRAD THDSRAD TMAX2M TMIN2M TOTPRECIP TQV TSNOW TSOIL U100 U10M V100 V10M VMAX10M WD010 WD040 WD060 WD080 WD100 WD125 WD150 WD175 WD200 WS010 WS040 WS060 WS080 WS100 WS125 WS150 WS175 WS200 WSNOW
do
  #
  # BACKUP simulation - restart and outdata
  #
  echo "----------------------------------------"
  echo "- Start $EXP $VAR"
  echo "----------------------------------------"
    
  TMP_DIR=$cwd/2D_${EXP}_${VAR}
  mkdir -p $TMP_DIR
  cd $EXP_ROOT
  echo $(pwd)

  # outdata
  sh -c 'scontrol update JobId=$SLURM_JOB_ID Comment="$*"' scomment "packems $VAR $EXP COSMO-REA rawdata"
  packems_wrapper -j ${njobs} --lock-break -F -L -x -d ${TMP_DIR} +a -o "COSMO-REA6_2D_${EXP}_${VAR}" -O by_name -D by_order --archive-index INDEX_${EXP}_${VAR}.txt $EXP/${VAR}.2D || { echo "ERROR: packems/slk error." ; exit 1 ; }

  echo "----------------------------------------"
  echo "- Complete $EXP $VAR"
  echo "----------------------------------------"
  cd $cwd
done


# packems:
# -j parallelisation 
# -F fail fast
# -d output for tar files
# -o prefix for tarfiles
# -O/-D how to search/add files to tar files
# 
# -S destination in hpss
# -a generate archiving commands for hpss transfer
# -n dry run
# -I index file name
# -x add group to index file
#
# --purge-archived : delete tar-files after they are in hpss
# -i --input : read files to be archived from textfile
# -L --dereference : follow links
# -s START_WITH, --start-with START_WITH
# --lock-break remove locks to allow resume after interrupt
#
# folder to archive as $1

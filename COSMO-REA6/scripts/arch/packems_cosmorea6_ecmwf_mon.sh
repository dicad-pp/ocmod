#!/bin/bash

#
# PACKEMS + slk
#
#SBATCH --job-name=packems
#SBATCH --partition=compute
#SBATCH --time=4:00:00
#SBATCH --mem=250G
#SBATCH --output=%x_%j.log
#SBATCH --mail-type=FAIL
#SBATCH --account=bm0021
#SBATCH --qos=esgf

#module load python3
#module load slk
module load packems 

EXP=mon

EXP_ROOT=/work/kd1292/k204212/OcMOD/outdata/
#EXP_ROOT=/work/kd1292/k204212/OcMOD/outdata_ecmwf/

cwd=$(pwd)
TMP_DIR=$cwd/2D_${EXP}
#TMP_DIR=$cwd/3D_${EXP}
mkdir -p $TMP_DIR

#ARCH_DIR=/arch/bk1261/COSMO-REA6_DWD_ecmwf/$EXP
njobs=24

#
# BACKUP simulation - restart and outdata
#
echo "----------------------------------------"
echo "- Start"
echo "----------------------------------------"
    
cd $EXP_ROOT
echo $(pwd)

# outdata ecmwf
sh -c 'scontrol update JobId=$SLURM_JOB_ID Comment="$*"' scomment "packems $EXP COSMO-REA6 rawdata"
#packems_wrapper -j ${njobs} --lock-break -F -L -x -d ${TMP_DIR} +a -o "COSMO-REA6_2D_${EXP}" -O by_name -D by_order --archive-index INDEX_${EXP}.txt $EXP || { echo "ERROR: packems/slk error." ; exit 1 ; }

# outdata
packems_wrapper -j ${njobs} --lock-break -F -L -x -d ${TMP_DIR}_part2 +a -o "COSMO-REA6_2D_${EXP}" -O by_name -D by_order --archive-index INDEX_${EXP}_part2.txt -s 2 $EXP || { echo "ERROR: packems/slk error." ; exit 1 ; }

# outdata ecmwf 3D
#packems_wrapper -j ${njobs} --lock-break -F -L -x -d ${TMP_DIR} +a -o "COSMO-REA6_3D_${EXP}" -O by_name -D by_order --archive-index INDEX_${EXP}_3D.txt ${EXP}_3D || { echo "ERROR: packems/slk error." ; exit 1 ; } 

echo "----------------------------------------"
echo "- Complete"
echo "----------------------------------------"

cd $cwd


# packems:
# -j parallelisation 
# -F fail fast
# -d output for tar files
# -o prefix for tarfiles
# -O/-D how to search/add files to tar files
# 
# -S destination in hpss
# -a generate archiving commands for hpss transfer
# -n dry run
# -I index file name
# -x add group to index file
#
# --purge-archived : delete tar-files after they are in hpss
# -i --input : read files to be archived from textfile
# -L --dereference : follow links
# -s START_WITH, --start-with START_WITH
# --lock-break remove locks to allow resume after interrupt
#
# folder to archive as $1

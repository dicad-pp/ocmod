#!/bin/python

import json,csv,sys,os,io

def jsonread(infile):
    """
    Read json file
    """
    if os.path.isfile(infile):
        with io.open(infile, 'r', encoding='utf-8') as f:
            content=json.load(f)
        return content
    else:
        return {}

def correct_none(invar):
    """
    In: Invar
    Out: Returns empty string if invar is of Type NoneType.
         Else returns invar.
    """

    if invar is None: return ""
    else: return str(invar).strip()

def getcode(code):
        """
        Separate parameter/code/variable name from input file.
        123.321|ECHAM.nc,234.321|JSBACH.nc --> 123.321,234.321
        Input: modvarcode / modvarname entry
        Output: code / name
        """
        codestring=list()
        code=code.replace(";",",").split(",")
        for i in code:
                if "|" in i:
                        codestring.append(i.split("|")[1].lstrip("0"))
                else:
                        codestring.append(i.lstrip("0"))
        #if leading zeros are needed for codes
        """
        for i in range(0, len(codestring)):
                try:
                        codestring[i]="{:03}".format(int(codestring[i]))
                except ValueError:
                        pass
        """
        return ",".join(codestring)

def getorigname(name, d):
    name=name.strip("'")
    if name in d:
      return d[name]
    else:
      #print(f"Not found:' {name} '")
      return name

origdict={}
origdict["FRSNOW"]="snc"
origdict["AEVAPS"]="AEVAP_S"
origdict["ALBRAD"]="ALB_RAD"
origdict["ALHFLS"]="ALHFL_S"
origdict["ASHFLS"]="ASHFL_S"
origdict["ASOBT"]="ASOB_T"
origdict["ASODT"]="ASOD_T"
origdict["ASWDIFUS"]="ASWDIFU_S"
origdict["ATHBT"]="ATHB_T"
origdict["AUMFLS"]="AUMFL_S"
origdict["AVMFLS"]="AVMFL_S"
origdict["SOBTRAD"]="SOBT_RAD"
origdict["SODTRAD"]="SODT_RAD"
origdict["SWDIFUSRAD"]="SWDIFUS_RAD"
origdict["TD2M"]="TD_2M"
origdict["TG"]="T_G"
origdict["THBTRAD"]="THBT_RAD"
origdict["TSOIL0CM"]="T_SOIL.0CM"
origdict["TSOIL1458CM"]="T_SOIL.1458CM"
origdict["TSOIL162CM"]="T_SOIL.162CM"
origdict["TSOIL18CM"]="T_SOIL.18CM"
origdict["TSOIL1CM"]="T_SOIL.1CM"
origdict["TSOIL2CM"]="T_SOIL.2CM"
origdict["TSOIL"]="T_SOIL"
origdict["TSOIL486CM"]="T_SOIL.486CM"
origdict["TSOIL54CM"]="T_SOIL.54CM"
origdict["TSOIL6CM"]="T_SOIL.6CM"
origdict["UMFLS"]="UMFL_S"
origdict["VABSMX10M"]="VABSMX_10M"
origdict["VGUSTCON"]="VGUST_CON"
origdict["VGUSTDYN"]="VGUST_DYN"
origdict["VMFLS"]="VMFL_S"
origdict["WSOIL1458CM"]="W_SOIL.1458CM"
origdict["WSOIL162CM"]="W_SOIL.162CM"
origdict["WSOIL18CM"]="W_SOIL.18CM"
origdict["WSOIL1CM"]="W_SOIL.1CM"
origdict["WSOIL2CM"]="W_SOIL.2CM"
origdict["WSOIL"]="W_SOIL"
origdict["WSOIL486CM"]="W_SOIL.486CM"
origdict["WSOIL54CM"]="W_SOIL.54CM"
origdict["WSOIL6CM"]="W_SOIL.6CM"
origdict["WSOILICE1458CM"]="W_SOIL.ICE.1458CM"
origdict["WSOILICE162CM"]="W_SOIL.ICE.162CM"
origdict["WSOILICE18CM"]="W_SOIL.ICE.18CM"
origdict["WSOILICE1CM"]="W_SOIL.ICE.1CM"
origdict["WSOILICE2CM"]="W_SOIL.ICE.2CM"
origdict["WSOILICE"]="W_SOIL.ICE"
origdict["WSOILICE486CM"]="W_SOIL.ICE.486CM"
origdict["WSOILICE54CM"]="W_SOIL.ICE.54CM"
origdict["WSOILICE6CM"]="W_SOIL.ICE.6CM"
origdict["ALWUS"]="ALWU_S"
origdict["ASOBS"]="ASOB_S"
origdict["ASWDIFDS"]="ASWDIFD_S"
origdict["ASWDIRS"]="ASWDIR_S"
origdict["ATHBS"]="ATHB_S"
origdict["ATHDS"]="ATHD_S"
origdict["DENS040"]="DENS_040"
origdict["DENS060"]="DENS_060"
origdict["DENS080"]="DENS_080"
origdict["DENS100"]="DENS_100"
origdict["DENS125"]="DENS_125"
origdict["DENS150"]="DENS_150"
origdict["DENS175"]="DENS_175"
origdict["DENS200"]="DENS_200"
origdict["HPBL"]="H_PBL"
origdict["HSNOW"]="H_SNOW"
origdict["LHFLS"]="LHFL_S"
origdict["LWUS"]="LWU_S"
origdict["QV2M"]="QV_2M"
origdict["QVS"]="Q_VS"
origdict["RAINCON"]="RAIN_CON"
origdict["RAINGSP"]="RAIN_GSP"
origdict["RELHUM2M"]="RELHUM_2M"
origdict["RHOSNOW"]="RHO_SNOW"
origdict["WSNOW"]="W_SNOW"
origdict["WS200"]="WS_200"
origdict["WS175"]="WS_175"
origdict["WS150"]="WS_150"
origdict["WS125"]="WS_125"
origdict["WS100"]="WS_100"
origdict["WS080"]="WS_080"
origdict["WS060"]="WS_060"
origdict["WS040"]="WS_040"
origdict["WS010"]="WS_010"
origdict["WD200"]="WD_200"
origdict["WD175"]="WD_175"
origdict["WD150"]="WD_150"
origdict["WD125"]="WD_125"
origdict["WD100"]="WD_100"
origdict["WD080"]="WD_080"
origdict["WD060"]="WD_060"
origdict["WD040"]="WD_040"
origdict["WD010"]="WD_010"
origdict["VMAX10M"]="VMAX_10M"
origdict["V10M"]="V_10M"
origdict["V100"]="V_100"
origdict["U10M"]="U_100"
origdict["TSOIL"]="T_SOIL"
origdict["TSNOW"]="T_SNOW"
origdict["TOTPRECIP"]="TOT_PRECIP"
origdict["TMIN2M"]="TMIN_2M"
origdict["TMAX2M"]="TMAX_2M"
origdict["THDSRAD"]="THDS_RAD"
origdict["THBSRAD"]="THBS_RAD"
origdict["T2M"]="T2M"
origdict["SWDIRSRAD"]="SWDIRS_RAD"
origdict["SWDIFDSRAD"]="SWDIFDS_RAD"
origdict["SOBSRAD"]="SOBS_RAD"
origdict["SNOWGSP"]="SNOW_GSP"
origdict["SNOWCON"]="SNOW_CON"
origdict["SHFLS"]="SHFL_S"
origdict["RUNOFFS"]="RUNOFF_S"
origdict["RUNOFFG"]="RUNOFF_G"
origdict["FRLAND"]="FR_LAND"
origdict["FRLAKE"]="FR_LAKE"
origdict["AREACELLA"]=""


mi = jsonread("COSMO_OcMOD_recipes.json")
ci = list()
with open("cmorvars") as csvfile:
    ci_read = csv.reader(csvfile, delimiter="|", quotechar='"')

    for line in ci_read:
        ci.append(line)
        if any([line[11]!="", line[10]!="", line[9]!="", line[8]!=""]): print(line)
        if len(line) != 13:
            print("Invalid field count")
            print(" | ".join(line))
            sys.exit(1)


## ci
#select uid,frequency,outname,units,title,standardname,modeling_realm,description,var_description,processing,var_processing,comment,positive from CMORvar order by frequency,outname;
###
#day_clc|day|clc|%|Convective Cloud Area Percentage|convective_cloud_area_fraction_in_atmosphere_layer|atmos|Include only convective cloud.|||||
#uid  freq  varname  units  title  sn   realm  desc desc2 desc3 desc4 comment positive

## mi
# "variable_comment": "''",
# "code": "''",
# "dec_check": "''",
# "uid": "'1hr_hfls'",
# "character_axis": "''",
# "positive": "'d'",
# "recipe": "''",
# "standard_name": "'surface_upward_latent_heat_flux'",
# "note": "",
# "cmor_name": "'hfls'",
# "codes": "''",
# "frequency": "'1hr'",
# "names": "'*_*_*_ALHFLS_2D_1hr_DATE.nc|ALHFLS'",
# "cell_methods": "'m'",
# "units": "'W m-2'",
# "z_axis": "''",
# "project_mip_table": "'1hr'",
# "name": "'ALHFLS'"

csvfile=open("Ocmod_lta_varlist.csv", "w")
pathfile=open("Ocmod_lta_paths.csv", "w")
pathwriter=csv.writer(pathfile, delimiter=",", quotechar="'", quoting=csv.QUOTE_MINIMAL)
csvwriter=csv.writer(csvfile, delimiter=";", quotechar="'", quoting=csv.QUOTE_MINIMAL)
csvwriter.writerow(["PARENT_ACRO", "ENTRY_TYPE", "ENTRY_NAME", "ENTRY_ACRONYM", "START_DATE", "STOP_DATE", "CODE_TOPIC_LIST", "UNIT_ACRONYM", "AGGREGATION_DESCR", "SUMMARY"])
pathwriter.writerow(["ENTRY_ACRONYM", "PATH"])
for item in ci:
  uid=item[0]
  freq=item[1]
  outname=item[2]
  units=item[3]
  title=item[4]
  sn=item[5]
  realm=item[6]
  desc=item[7]
  pos_cv=item[12]
  entry=[i for i in mi if i["uid"]=="'"+uid+"'"]
  if not entry==list():
    orig=getcode(entry[0]["names"])
    diag=entry[0]["recipe"]
    for var_key in origdict:
      diagold=diag
      diag=diag.replace(var_key, origdict[var_key])
      if diagold!=diag:
        print(f"Recipe for {uid}: altered {diagold} to {diag}.")
    zaxis=entry[0]["z_axis"].strip("'")
    pos_map=entry[0]["positive"].strip("'").replace('u','up').replace('d','down')
    units_map=entry[0]["units"].strip("'")
    if not any([i.isupper() for i in orig.split(",")]): orig=""
  else:
    print(f"Skipping unavailable/unmapped variable: {uid}.")
    continue

  if zaxis=="alevel" and uid.endswith("6"):
    print(f"Skipping double entry on lowest 6 model levels: {uid}")
    continue

  # Only "3D" variables (2nd batch)
  #if zaxis!="alevel" and not zaxis.startswith("plev"):
  #  print(f"Skipping already archived variable: {uid}")
  #  continue

  # Only "2D" variables (1st batch)
  #if zaxis.startswith("plev"):
  #  print(f"Skipping 3D variable on pressure levels: {uid}")
  #  continue
  #elif zaxis=="alevel":
  #  print(f"Skipping 3D variable on model levels: {uid}")
  #  continue

  # nProv variables
  if outname.startswith("wa"):
    print(f"Skipping nProv variable: {uid}")
    continue
  elif outname in ["albs", "snden", "snl", "mrfso", "mrso", "mrfsos", "mrsos", "ua30", "ua20", "ua10", "va30", "va20", "va10", "hus30", "hus20", "hus10", "ta30", "ta20", "ta10"]:
    print(f"Skipping nProv variable: {uid}")
    continue

  optdesc=""
  if desc: optdesc+=f":\\n{desc}"
  if orig:
      orignamestr = ', '.join([getorigname(i, origdict) for i in orig.split(',')])
      if orignamestr:
          optdesc+=f"\\nCOSMO model original name(s): {orignamestr}"
  if orig and diag!="''": optdesc+=f"\\nDiagnostic recipe: {diag}"
  if pos_cv!=pos_map: optdesc+=f"\\nAdditional diagnostic: Altered positive flux direction from \"{pos_map}\" to \"{pos_cv}\"."
  if units!=units_map: optdesc+=f"\\nAdditional diagnostic: Converted units from \"{units_map}\" to \"{units}\"."
  if outname=="orog":
    optdesc+="\\nPlease note: Owed to the experimental nature of this regional reanalysis, two slightly different time-invariant fields were used as surface altitude. One for the period 2007-2012, another for the rest of the experiment (i.e. 1995-2006 and 2013-2019). Therefore, the variable is provided as a monthly mean field (that is constant within each of the named periods) and not as time invariant field."
  if zaxis=="alevel":
    optdesc+="\\nPlease note: Variable provided on the 6 lowest model levels. Owed to the experimental nature of this regional reanalysis, two slightly different time-invariant fields were used as surface altitude. One for the period 2007-2012, another for the rest of the experiment (i.e. 1995-2006 and 2013-2019). The surface altitude serves as auxiliary coordinate variable in describing the height based hybrid Gal-Chen coordinate."
  optdesc=optdesc.replace("'",'"').replace(";", ",")
  optdesc=optdesc.strip("'")
  row=[
        f"CR6_EU6_{freq}",
        "DS",
        f"COSMO-REA6 COSMO reanalysis EUR-6km - frequency {freq} - {outname}",
        f"CR6_EU6_{freq}_{outname}",
        "1995-01-01",
        "2019-08-31",
        sn,
        units,
        freq,
        f"Dataset of the regional reanalysis COSMO-REA6.\\n\\n{outname} ({title} in modeling realm(s) {', '.join(realm.split(' '))}){optdesc}"
  ]
  csvwriter.writerow(row)
  pathwriter.writerow([f"CR6_EU6_{freq}_{outname}", f"/work/pd1416/COSMO-REA/reanalysis/EUR-6km/DWD/ECMWF-ERAINT/REA6/r1i1p1f1/COSMO/v1/{freq}/{realm.split(' ')[0]}/{outname}/v20230918/"])
csvfile.close()
pathfile.close()


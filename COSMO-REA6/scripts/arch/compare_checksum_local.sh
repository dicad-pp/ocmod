#!/bin/bash
#
#SBATCH --job-name=checksum
#SBATCH --partition=interactive
#SBATCH --time=24:00:00
#SBATCH --mem=128G
#SBATCH --output=%x_%j.log
#SBATCH --mail-type=FAIL
#SBATCH --account=bm0021
#SBATCH --qos=esgf

cwd=/work/kd1292/HSM/check_dwd
TMP_DIR=/work/bk1261/dwd
ARCH_DIR=/work/kd1292/k204212/OcMOD/dwd

mkdir -p $cwd

#
# control checksums simulation - restart and outdata
#

source_files=( $( find $TMP_DIR -type f ) )

echo "--------------------------------------------------------------------"
echo "- ${#source_files[@]} files found"
echo "--------------------------------------------------------------------"
    
# compute/retrieve sha512 checksums and compare
sh -c 'scontrol update JobId=$SLURM_JOB_ID Comment="$*"' scomment "sha512_checksum"
maxjobs=24
n=0
for ncfile in ${source_files[@]} ; do
 (
    bn=$( basename $ncfile )
    path_ARCH=$( echo $ncfile | sed "s,$TMP_DIR,$ARCH_DIR,g" )
    #echo $ncfile
    #echo $path_ARCH
    #echo $bn
    # local
    if [[ ! -e $cwd/${bn}.sha512 ]]; then
        sha512sum $ncfile | cut -d " " -f1 > ${cwd}/${bn}.sha512 || { echo ERROR could not create checksum for $bn && continue ; }
    fi
    # remote
    if [[ ! -e ${cwd}/${bn}.sha512_remote ]]; then
        sha512sum $path_ARCH | cut -d " " -f1 > ${cwd}/${bn}.sha512_remote || { echo ERROR could not retrieve remote checksum for $bn && continue ; }
    fi
    if diff -q ${cwd}/${bn}.sha512 ${cwd}/${bn}.sha512_remote &>/dev/null; then
        echo "SUCCESS ${bn} - checksums are equal"
    else
        echo "FAILURE $bn - checksums differ"
    fi
)&
if (( $(($((++n)) % $maxjobs)) == 0 )) ; then
    wait
fi
done
wait


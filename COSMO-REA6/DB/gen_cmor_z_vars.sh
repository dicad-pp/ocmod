#!/bin/bash

# Add deflate settings using vim:
#:%s/"ok_max_mean_abs": ""\n/"ok_max_mean_abs": "",\r            "deflate": "1",\r            "deflate_level": "1",\r            "shuffle": "1"\r/gc

#####

var="ws"
sn="wind_speed"
ln="Wind Speed"
c="Wind Speed"
units="m s-1"

#####

#var="wd"
#sn="wind_from_direction"
#ln="Wind From Direction"
#c="Wind from direction"
#units="degree"

#####

var="ad"
sn="air_density"
ln="Air Density"
c="Air density"
units="kg m-3"



##############################

cm="area: time: mean"
#cm="area: mean time: point"

f="mon"
#f="day"
#f="1hrPt"

t=time
#t=time1

s1="        "
s2="${s1}    "

for zlev in 10 40 60 80 100 125 150 175 200 
do

echo "${s1}\"${var}${zlev}m\": {"

echo "${s2}\"frequency\": \"$f\","
echo "${s2}\"modeling_realm\": \"atmos\","
echo "${s2}\"standard_name\": \"$sn\","
echo "${s2}\"units\": \"$units\","
echo "${s2}\"cell_methods\": \"$cm\","
echo "${s2}\"cell_measures\": \"area: areacella\","
echo "${s2}\"long_name\": \"$ln at ${zlev}m\","
echo "${s2}\"comment\": \"$c at ${zlev}m height.\","
echo "${s2}\"dimensions\": \"longitude latitude $t height${zlev}m\","
echo "${s2}\"out_name\": \"${var}${zlev}m\","
echo "${s2}\"type\": \"real\","
echo "${s2}\"positive\": \"\","
echo "${s2}\"valid_min\": \"\","
echo "${s2}\"valid_max\": \"\","
echo "${s2}\"ok_min_mean_abs\": \"\","
echo "${s2}\"ok_max_mean_abs\": \"\","
echo "${s2}\"deflate\": \"1\","
echo "${s2}\"deflate_level\": \"1\","
echo "${s2}\"shuffle\": \"1\""

echo "${s1}},"

done


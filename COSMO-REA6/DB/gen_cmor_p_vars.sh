#!/bin/bash

# Add deflate settings using vim:
#:%s/"ok_max_mean_abs": ""\n/"ok_max_mean_abs": "",\r            "deflate": "1",\r            "deflate_level": "1",\r            "shuffle": "1"\r/gc

var=ua
sn="eastward_wind"
ln="Eastward Wind"
c="Eastward wind"

var=va
sn="northward_wind"
ln="Northward Wind"
c="Northward wind"

var=wa
sn="upward_air_velocity"
ln="Upward Air Velocity"
c="Upward air velocity"

units="m s-1"

#####

var=ta
sn="air_temperature"
ln="Air Temperature"
c="Air temperature"
units="K"

#####

var=tke
sn="turbulent_kinetic_energy_content_of_atmosphere_layer"
ln="Turbulent Kinetic Energy"
c="Turbulent kinetic energy"
units="m2 s-2"

#####

#var=hus
#sn="specific_humidity"
#ln="Specific Humidity"
#c="Specific humidity is the mass fraction of water vapor in (moist) air. Provided"
#units="1"

#####

#var=cl
#sn="cloud_area_fraction_in_atmosphere_layer"
#ln="Percentage Cloud Cover"
#c="Percentage cloud cover, including both large-scale and convective cloud,"
#units="%"


#####

#var=cl
#sn="convective_cloud_area_fraction_in_atmosphere_layer"
#ln="Convective Cloud Area Percentage"
#c="Percentage cloud cover, including only convective cloud,"
#units="%"





##############################

cm="area: time: mean"
#cm="area: mean time: point"

f="mon"
f="day"
#f="1hrPt"

t=time
#t=time1

s1="        "
s2="${s1}    "

for plev in 1000 925 850 750 700 600 500 400 300 250 200 150 100 70 50 30 20 10
do

echo "${s1}\"${var}${plev}\": {"

echo "${s2}\"frequency\": \"$f\","
echo "${s2}\"modeling_realm\": \"atmos\","
echo "${s2}\"standard_name\": \"$sn\","
echo "${s2}\"units\": \"$units\","
echo "${s2}\"cell_methods\": \"$cm\","
echo "${s2}\"cell_measures\": \"area: areacella\","
echo "${s2}\"long_name\": \"$ln\","
echo "${s2}\"comment\": \"$c on the $plev hPa surface\","
echo "${s2}\"dimensions\": \"longitude latitude $t plev$plev\","
echo "${s2}\"out_name\": \"${var}$plev\","
echo "${s2}\"type\": \"real\","
echo "${s2}\"positive\": \"\","
echo "${s2}\"valid_min\": \"\","
echo "${s2}\"valid_max\": \"\","
echo "${s2}\"ok_min_mean_abs\": \"\","
echo "${s2}\"ok_max_mean_abs\": \"\","
echo "${s2}\"deflate\": \"1\","
echo "${s2}\"deflate_level\": \"1\","
echo "${s2}\"shuffle\": \"1\""

echo "${s1}},"

done


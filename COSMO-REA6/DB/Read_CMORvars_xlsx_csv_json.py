#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

##############################################
#
# Create c6dreq WebGUI compatible projectdb.sqlite
# by reading a projects mipTables and additional
# information (Dreq-light, title in mipTable header)
#
# Martin Schupfner, DKRZ-DM, 2020-2021
#
##############################################
# Prerequisites:
# Python3
##############################################

from __future__ import print_function
import csv, io, json
from collections.abc import Mapping
import sqlite3 as lite

import sys, os
if sys.version_info.major == 3:
    unicode = str

############################################
############################################
#       Definitions
############################################
############################################

#
# MIP-Table Source (json or csv)
#   CV, coordinates have to be read from json
#   Dreq-light has to be read from csv
source="json" #"csv"

#
# MIP information
#
mipinfo={"url":"https://nfdi4earth.de/images/nfdi4earth/documents/pilots/proposals/135-Observations_closer_to_Model_Data_OcMOD.pdf",
         "label":"OcMOD",
         "title":"OcMOD - Observations closer to MOdel Data (NFDI4Earth Pilot)",
         }

#
# Path of sqlite3 DB
#
db="ocmoddb.sqlite"

#
# Var/Code separator
#
separator=","

#
# Comment
#
comment="#"

#
# MIP-Tables-CSV:
#     File: [name, [comment-lines]]
csv_miptables={"OcMOD_mon.json":["mon", [0]],
               "OcMOD_day.json":["day", [0]],
               "OcMOD_6hr.json":["6hr", [0]],
               "OcMOD_1hr.json":["1hr", [0]],
               "OcMOD_1hrPt.json":["1hrPt", [0]],
               "OcMOD_fx.json":["fx", [0]],
    }

#
# MIP-Tables-json:
#
json_miptables={"OcMOD_mon.json",
                "OcMOD_6hr.json",
                "OcMOD_1hr.json",
                "OcMOD_1hrPt.json",
                "OcMOD_day.json",
                "OcMOD_fx.json",
    }

#
# Dreq-Light:
#
dreq_light="OcMOD_Dreq-light.csv"

#
# CV:
#
json_cv="OcMOD_CV.json"

#
# Coordinates:
#
json_coordinates="OcMOD_coordinate.json"

############################################
############################################
#       Functions
############################################
############################################
attlist=list()
attlist.append("param")
attlist.append("code")
attlist.append("name")
attlist.append("cmor_name")
attlist.append("p")
attlist.append("cell_methods")
attlist.append("comment")
attlist.append("standard_name")
attlist.append("char_axis")
attlist.append("factor")
attlist.append("units")
attlist.append("mip_table")

def __unicode__(self):
   return unicode(self.some_field) or u''

def get_csv_comment(ifile, linenrs=[0], separator=",", comment="#"):
    with open(ifile) as csvfile:
        i=0
        mt_comment=""
        for row in csvfile.readlines():
            if i in linenrs:
                mt_comment+=(mt_comment+" "+separator.join([j for j in row.split(separator) if j.strip()])).replace("'","").replace('"','').replace(comment,'').strip()
            i+=1
        return mt_comment

def decomment(lines, comment="#"):
    for row in lines:
        raw = row.split(comment)[0].strip()
        if raw: yield raw

def open_csv(ifile, separator=",", comment="#"):
    with open(ifile) as csvfile:
        reader = csv.reader(decomment(csvfile, comment), delimiter=separator)
        for row in reader: print(row)
    #return(json.dumps([row for row in reader]))

def correct_none(invar):
    """
    In: Invar
    Out: Returns empty string if invar is of Type NoneType.
         Else returns invar.
    """

    if invar is None: return ""
    else: return invar

def jsonwrite(jarg, outfile):
        """
        Write output to file read by webserver
        """
        with io.open(outfile, 'w', encoding='utf-8') as f:
                f.write(unicode(json.dumps(jarg, indent=4, sort_keys=True, ensure_ascii=False)))

def jsonappend(jarg, outfile):
        """
        Append output to file read by webserver
        """
        if os.path.isfile(outfile):
            with io.open(outfile, 'r', encoding='utf-8') as f:
                data=json.load(f)
                data.update(jarg)
                jarg=data
        with io.open(outfile, 'w', encoding='utf-8') as f:
            f.write(unicode(json.dumps(jarg, ensure_ascii=False)))

def jsonread(infile):
    """
    Read json file
    """
    if os.path.isfile(infile):
        with io.open(infile, 'r', encoding='utf-8') as f:
            content=json.load(f)
        return content
    else:
        return {}

def readCoordinates(infile):
    """
    Read coordinate json file.

    Parameters
    ----------
    infile : string
        Path to coordinate json file.

    Returns
    -------
    File[axis_entry].

    """
    if not os.path.exists(infile):
        print("ERROR (coordinate): '%s' cannot be found! Coordinates cannot be read." % infile)
        sys.exit(1)
    coordinates=jsonread(infile)
    return coordinates["axis_entry"]

def readMipTable(infile):
    """
    Read mipTable variable_entry.

    Parameters
    ----------
    infile : string
        Path to mip table json file.

    Returns
    -------
    Tuple of File[variable_entry] and File[Header].

    """
    if not os.path.exists(infile):
        print("ERROR (MIPTable): '%s' cannot be found! Variables cannot be read." % infile)
        sys.exit(1)
    mt=jsonread(infile)
    return mt["variable_entry"],mt["Header"]

def readCV(infile):
    """
    Read experiment_id and activity_id from json CV.

    Parameters
    ----------
    infile : string
        Path to json CV file.

    Returns
    -------
    Tuple of File[CV][experiment_id] and File[CV][activity_id].

    """
    if not os.path.exists(infile):
        print("ERROR (CV): '%s' cannot be found! Experiments cannot be read." % infile)
        sys.exit(1)
    cv=jsonread(infile)["CV"]
    return cv["experiment_id"],cv["activity_id"]

def CoordinatesToZaxisAndCdim(coordinates):
    """
    Create list of coordinates which are zaxes or character dimensions.

    Parameters
    ----------
    coordinates : dict
        axis_entry content of a coordinate.json

    Returns
    -------
    Tuple of lists: zaxes, cdims.
    """
    zaxis=list()
    cdim=list()
    for entry in coordinates:
        if coordinates[entry]["axis"]=="Z":
            if coordinates[entry]["generic_level_name"]!="":
                zaxis.append(coordinates[entry]["generic_level_name"])
            else:
                zaxis.append(entry)
        elif coordinates[entry]["type"]=="character":
            cdim.append(entry)
    return zaxis,cdim

def readDreqLight(infile):
    """
    Read a Dreq-light formatted csv and generates dreq list.

    Parameters
    ----------
    infile : string
        Path to a Dreq-light formatted file.

    Returns
    -------
    List of Settings.
    """
    dreq=list()
    if not os.path.exists(infile):
        print("Warning (Dreq-light): '%s' cannot be found! Empty Dreq." % infile)
        return dreq
    dreqfile=open(infile,"r")
    dreqraw=dreqfile.readlines()
    dreq=list()
    for line in dreqraw:
        entry=["TOTAL","TOTAL","TOTAL","True"]
        ls=line.strip().split(";")
        for lsi in ls:
            if lsi.startswith("experiment-id:"):
                entry[0]=lsi.split(":")[1].strip()
            elif  lsi.startswith("table:"):
                entry[1]=lsi.split(":")[1].strip()
            elif lsi.startswith("var:"):
                entry[2]=lsi.split(":")[1].strip()
            elif lsi.startswith("setting:"):
                entry[3]=lsi.split(":")[1].strip()
        dreq.append(entry)
    return dreq

def if_requested_DreqLight(dreq, exp, tab, var):
    """
    Check if the combination of exp,tab,var is requested by dreq.

    Parameters
    ----------
    dreq : List
        List as generated by readDreqLight or empty list.
    exp : string
        experiment_id of the projects CV.
    tab : string
        mipTable name of the project.
    var : TYPE
        mipTable variable_entry.

    Returns
    -------
    True or False boolean.
    """
    specific_setting=""
    specific_setting_found=False
    general_setting=""
    general_setting_found=False
    for entry in dreq:
        if entry[0]=="TOTAL" and tab in entry[1].split(",") and var in entry[2].split(","):
            if entry[3]=="True": general_setting=True
            else: general_setting=False
            general_setting_found=True
        elif general_setting_found==False and entry[0]=="TOTAL" and tab in entry[1].split(",") and entry[2]=="TOTAL":
            if entry[3].lower()=="true": general_setting=True
            else: general_setting=False
        elif exp in entry[0].split(",") and tab in entry[1].split(",") and var in entry[2].split(","):
            if entry[3].lower()=="true": specific_setting=True
            else: specific_setting=False
            specific_setting_found=True
        elif specific_setting_found==False and exp in entry[0].split(",") and tab in entry[1].split(",") and entry[2]=="TOTAL":
            if entry[3].lower()=="true": specific_setting=True
            else: specific_setting=False
    if specific_setting!="": return specific_setting
    elif general_setting!="": return general_setting
    else: return True

def insertTable(con, cur, table, values):
    """
    Insert values into table of active sqlite3 database connection cursor.

    Parameters
    ----------
    con : object
        connection object of sqlite3 database
    cur : object
        cursor of sqlite3 database connection.
    table : string
        existing table of the sqlite3 database.
    values :
        dict of keys and values to be inserted or list of values.

    Returns
    -------
    None.
    """
    keystr=""
    valuestr=""
    if isinstance(values, Mapping):
        for key,value in values.items():
            keystr+=","+key
            valuestr+=","+enc(value) #.replace(",", "\,")
    elif isinstance(values, list):
        #valuestr=",".join([val.replace(",", "\,") for val in values])
        valuestr=",".join([enc(val) for val in values])
        #valuestr=",".join(values)
    if keystr!="": keystr="("+keystr.strip(",")+")"
    valuestr=valuestr.strip(",")
    exestr="INSERT OR REPLACE INTO "+table+" "+keystr+" VALUES ("+valuestr+")"
    #print(exestr)
    try:
        cur.execute(exestr)
    except lite.Error as e:
        print("ERROR (sqlite) %s:" % e.args[0])
        if con: con.close()
        sys.exit(1)

def flatsort(liststr):
    return sorted(list(set([item if type(sublist)==list else sublist for sublist in liststr for item in sublist])))

def enc(string):
    if type(string)==list:
        string=",".join(string)
    return "'"+string.replace("'","''")+"'"

def enc2(string):
    if string is None:
        return "''"
    elif not (isinstance(string, int) or isinstance(string, float)):
        return "'"+string+"'"
    else:
        return str(string)

def prepare_insert(uid, model, v):
    try:
        cur.execute("SELECT cv.uid, cv.label, v.sn standardname, cv.frequency, cv.cmt, cv.mipTable from var v, CMORvar cv WHERE v.uid = cv.vid and cv.uid='"+uid+"'")
        result=cur.fetchall()
        if len(result)>1:
            #print "ERROR: Found more than one variable for", uid
            #for i in result: print i
            sys.exit(1)
        elif len(result)==0:
            #print "ERROR: Found no variable for", uid
            sys.exit(1)
    except lite.Error as e:
        #print "ERROR %s:" % e.args[0]
        return ""
    #cstring="INSERT INTO CMORvar_Mapping_"+model+" (uid, shortname, comment, flag, flagt, flagby, flaguid, flagh, modvarunits, modvarname, modvarcode, miptable, frequency, cell_methods, standardname, username, changed, availability, note, positive, recipe, chardim) VALUES ("
    cstring="INSERT OR REPLACE INTO CMORvar_Mapping_"+model+" (uid, shortname, comment, flag, flagt, flagby, flaguid, flagh, modvarunits, modvarname, modvarcode, miptable, frequency, cell_methods, standardname, username, changed, availability, note, positive, recipe, chardim) VALUES ("
    #cstring="INSERT INTO CMORvar_Mapping_"+model+" (uid, shortname, comment, flag, flagt, flagby, modvarunits, modvarname, modvarcode, miptable, frequency, cell_methods, standardname, username, changed, availability, note, positive, recipe, chardim) VALUES ("
    #print result[0]
    cstring+=",".join([u"'"+uid+u"'",
                       u"'"+result[0][1]+u"'",
                       u"'"+v['comment'].replace(',', '\,')+u"'",
                       u"0",
                       u"0",
                       u"'Legacy(CMIP5)'",
                       u"''",
                       u"0",
                       u"'"+v['units']+u"'",
                       u"'"+v['name']+u"'",
                       u"'"+v['code']+u"'",
                       u"'"+result[0][5]+u"'",
                       u"'"+result[0][3]+u"'",
                       u"'"+correct_none(result[0][4])+u"'",
                       u"'"+result[0][2]+u"'",
                       u"'Legacy'",
                       u"0",
                       u"0",
                       u"'Read in from an old Mapping Table'",
                       u"'"+v['p']+u"'",
                       u"'"+v['factor']+u"'",
                       u"'"+v['char_axis']+u"'"])+")"
    #print cstring
    return cstring


################################
################################
# PROGRAM ######################
################################
################################

if source=="csv":
    for mt in csv_miptables.keys():
        mt_comment=get_csv_comment(mt,csv_miptables[mt][1], separator,  comment)
        mt_json=open_csv(mt, separator, comment)
        csv_miptables[mt].append(mt_json)
        csv_miptables[mt].append(mt_comment)
    sys.exit(0)
    # if source csv end
elif source=="json":
    #Connect to DB
    print("Using SQLITE in version", lite.sqlite_version)
    print("\nLoading Database ...")
    try:
        con = lite.connect(db)
        cur = con.cursor()
        cur.execute('SELECT SQLITE_VERSION()')
        data=cur.fetchone()
        print("SQLite version of Database: %s" % data)
        cur.execute("select name from sqlite_master where type = 'table'")
        data=cur.fetchall()
        print("\nTables read:")
        for i in data: print(" - "+ i[0])
    except lite.Error as e:
        print("Error %s:" % e.args[0])
        sys.exit(1)

    # Read Dreq-light
    dreq=readDreqLight(dreq_light)

    dreq_version="00.00.00"
    dreq_version_set=False

    # Read coordinates
    coordinates=readCoordinates(json_coordinates)

    # Get zaxis and cdim entries
    zaxis,cdim=CoordinatesToZaxisAndCdim(coordinates)

    # Read CV
    exps,mips=readCV(json_cv)

    # Write experiment
    print("Reading %3s experiments ..." % len(exps.keys()))
    explist=list()
    for exp in exps:
        expdict={}
        expdict["uid"]        = exps[exp]["experiment_id"]   # Record identifier (aa:st__uid)
        expdict["mip"]        = exps[exp]["activity_id"]     # MIP defining experiment (xs:string)
        expdict["description"]= ""                           # Description (xs:string)
        expdict["title"]      = exps[exp]["experiment"]      # Record Title (xs:string)
        expdict["label"]      = exps[exp]["experiment_id"]   # Record Label (xs:string)
        expdict["nstart"]     = "NULL"      # Number of start dates (xs:integer)
        expdict["yps"]        = "NULL"      # Years per simulation (xs:integer)
        expdict["starty"]     = ""          # Start year (xs:string)
        expdict["endy"]       = ""          # End year (xs:string)
        expdict["ensz"]       = "NULL"      # Ensemble size (aa:st__integerListMonInc)
        expdict["ntot"]       = "NULL"      # Total number of years (xs:integer)
        expdict["egid"]       = ""          # Identifier for experiment group (xs:string)
        expdict["tier"]       = "1"         # Tier of experiment (aa:st__integerListMonInc)
        expdict["mcfg"]       = ""          # Model category (xs:string)
        expdict["comment"]    = ""          # Comment (xs:string)
        explist.append([expdict["label"], expdict["mip"]])
        insertTable(con, cur, "experiment", expdict)

    # Write mip
    print("Reading %3s MIPs ..." % len(mips.keys()))
    for mip in mips:
        mipdict={}
        mipdict["uid"]        = mip             # Record identifier (aa:st__uid)
        mipdict["label"]      = mip             # MIP short name (xs:string)
        mipdict["title"]      = mips[mip]       # MIP title (xs:string)
        mipdict["url"]        = mipinfo["url"]  # URL with more infos
        insertTable(con, cur, "mip", mipdict)

    # Write grids
    print("Reading %3s coordinate entries ..." % len(coordinates.keys()))
    dimdict={}
    for grid in coordinates:
        if coordinates[grid]["generic_level_name"]=="":
            dimdict[grid]={"type":coordinates[grid]["type"],
                           "units":coordinates[grid]["units"],
                           "out_name":coordinates[grid]["out_name"],
                           "long_name":coordinates[grid]["long_name"],
                           "label":grid
                           }
        else:
            dimdict[coordinates[grid]["generic_level_name"]]={
                           "type":"",
                           "units":"",
                           "out_name":"",
                           "long_name":"generic vertical coordinate",
                           "label":coordinates[grid]["generic_level_name"]
                           }
        griddict={}
        griddict["uid"]             = grid                                     # Identifier (aa:st__uid)
        griddict["isGrid"]          = ""                                       # grid? (xs:string)         # that this can be found in the grids.json?
        griddict["boundsValues"]    = coordinates[grid]["bounds_values"]       # bounds _values (xs:string)
        griddict["valid_min"]       = coordinates[grid]["valid_min"]           # valid_min (xs:float)
        griddict["valid_max"]       = coordinates[grid]["valid_max"]           # valid_max (xs:float)
        griddict["tolRequested"]    = coordinates[grid]["tolerance"]           # tol_on_requests: variance from requested values that is tolerated (xs:string)
        griddict["axis"]            = coordinates[grid]["axis"]                # axis (xs:string)
        griddict["tables"]          = ""                                       # CMOR table(s) (xs:string)  # could be generated out of variable infos if necessary
        griddict["title"]           = coordinates[grid]["long_name"]           # long name (xs:string)
        griddict["positive"]        = coordinates[grid]["positive"]            # positive (xs:string)
        griddict["label"]           = grid                                     # CMOR dimension (xs:string)
        griddict["units"]           = coordinates[grid]["units"]               # units (xs:string)
        griddict["altLabel"]        = coordinates[grid]["out_name"]            # output dimension name (xs:string)
        griddict["type"]            = coordinates[grid]["type"]                # type (aa:st__fortranType)
        griddict["requested"]       = ""                                       # requested (xs:string)
        griddict["direction"]       = coordinates[grid]["stored_direction"]    # stored direction (xs:string)
        griddict["description"]     = ""                                       # description (xs:string)
        griddict["isIndex"]         = ""                                       # index axis? (xs:string)    # no idea how to find out from CMOR table
        griddict["standardName"]    = coordinates[grid]["standard_name"]       # standard name (xs:string)
        griddict["boundsRequested"] = ""                                       # bounds_ requested (aa:st__floatList)  # difference from boundsValues??
        griddict["bounds"]          = coordinates[grid]["must_have_bounds"]    # bounds? (xs:string)
        griddict["value"]           = coordinates[grid]["value"]               # value of a scalar coordinate (xs:string)
        griddict["coords"]          = ""                                       # coords_attrib (xs:string)
        insertTable(con, cur, "grids", griddict)

    # Write miptable
    print("Reading %3s miptables ..." % len(json_miptables))
    for mt in json_miptables:
        print(mt)
        vars,header=readMipTable(mt)

        miptabledict={}
        miptabledict["uid"]         = mt.split("_")[1].split(".")[0]          # primary key -- Record identifier (aa:st__uid)
        miptabledict["label"]       = miptabledict["uid"]                     # Label (xs:string)
        try:
            miptabledict["title"]       = header["title"].replace("'","")     # Title (xs:string)
        except KeyError:
            miptabledict["title"]       = header["table_id"]                  # Title (xs:string)
        miptabledict["frequency"]   = ""                              # Frequency (xs:string)
        miptabledict["altLabel"]    = ""                              # Alternative Label (xs:string)
        miptabledict["description"] = ""                              # Description (xs:string)
        miptabledict["comment"]     = ""                              # Comment (xs:string)
        insertTable(con, cur, "miptable", miptabledict)

        # Write CMORvar
        print("Reading %3s variables from MIPtable '%s' ..." % (len(vars.keys()), mt))
        for var in vars:
            vardims=vars[var]["dimensions"].strip().split()
            gridinfo=list()
            for dim in vardims:
                cgridinfo=dimdict[dim]["label"]
                if dimdict[dim]["out_name"]!="": cgridinfo+="/"+dimdict[dim]["out_name"]
                cgridinfo+=" ("
                if dimdict[dim]["type"]!="": cgridinfo+=dimdict[dim]["type"]+", "
                cgridinfo+=dimdict[dim]["long_name"]+")"
                if dimdict[dim]["units"]!="": cgridinfo+=" ["+dimdict[dim]["units"]+"]"
                gridinfo.append(cgridinfo)
            expsreq=list()
            mipsreq=list()
            for exp in explist:
                if if_requested_DreqLight(dreq, exp[0], miptabledict["uid"], var):
                    expsreq.append(exp[0])
                    mipsreq.append(exp[1])
            vardict={}
            vardict["uid"]              = miptabledict["uid"]+"_"+var          # primary key -- Record Identifier (aa:st__uid)
            vardict["vid"]              = vars[var]["out_name"]                # outname (xs:string)
            vardict["label"]            = var                                  # CMOR Variable Name (xs:string)
            vardict["title"]            = vars[var]["long_name"]               # Long name (xs:string)
            vardict["standardname"]     = vars[var]["standard_name"]           # standard name (xs:string)
            vardict["mipTable"]         = miptabledict["uid"]                  # The MIP table: each table identifies a collection of variables (xs:string)
            vardict["outname"]          = vars[var]["out_name"]                # Name of the variable in the File (xs:string)
            vardict["frequency"]        = vars[var]["frequency"]               # Frequency of time steps to be archived. (xs:string)
            vardict["units"]            = vars[var]["units"]                   # units (xs:string)
            vardict["positive"]         = vars[var]["positive"]                # CMOR Directive Positive (xs:string)
            vardict["type"]             = vars[var]["type"]                    # Data value type, e.g. float or double (aa:st__fortranType)
            vardict["valid_min"]        = vars[var]["valid_min"]               # Minimum expected value for this variable. (xs:float)
            vardict["valid_max"]        = vars[var]["valid_max"]               # Maximum expected value for this variable. (xs:float)
            vardict["ok_min_mean_abs"]  = ""                                   # Minimum expected value of the global mean absolute value at each point in time (xs:float)
            vardict["ok_max_mean_abs"]  = ""                                   # Maximum expected value of the global mean absolute value at each point in time (xs:float)
            vardict["description"]      = vars[var]["comment"].replace("'","") # Description (xs:string)
            vardict["processing"]       = ""                                   # Processing notes (xs:string)
            vardict["var_description"]  = ""                                   # varchar(2047)
            vardict["var_processing"]   = ""                                   # varchar(2047)
            vardict["comment"]          = ""                                   # (xs:string)
            vardict["modeling_realm"]   = vars[var]["modeling_realm"]          # Modeling Realm (xs:string)
            vardict["prov"]             = ""                                   # Provenance (xs:string)
            vardict["provmip"]          = ""                                   # varchar(2047)
            vardict["cmt"]              = vars[var]["cell_methods"]            # Cell methods (xs:string)
            vardict["cms"]              = vars[var]["cell_measures"]           # Cell measures (xs:string)
            vardict["dims"]             = vars[var]["dimensions"]              # Dimensions separated by space (xs:string)
            vardict["ltier"]            = "1"                                  # (xs:integer)
            vardict["lpriority"]        = "1"                                  # (xs:integer)
            vardict["reqgrid"]          = "native"                             # comma separated grids variable is requested on (xs:string)
            vardict["grid"]             = "<br>".join(gridinfo)                # Dimensions incl additional info, separated by <br> (xs:string)
            vardict["cdim"]             = ",".join([dim for dim in vardims if dim in cdim])         # character dimension name(s), comma separated (xs:string)
            vardict["zaxis"]            = ",".join([dim for dim in vardims if dim in zaxis])        # zaxis name (xs:string)
            vardict["expsreq"]          = ",".join(flatsort(expsreq))    # comma separated list of exps variable is requested for (xs:string)
            vardict["mipsreq"]          = ",".join(flatsort(mipsreq))    # comma separated list of mips variable is requested by (xs:string)
            insertTable(con, cur, "CMORvar", vardict)

        # Test/Set dreq_version:
            if dreq_version_set==True:
                if dreq_version!=header["data_specs_version"]:
                    print("ERROR (MIPtable): Inconsistent data_spec_versions found in the different miptables!")
                    sys.exit(1)
            else:
                dreq_version=header["data_specs_version"]

    # Write Dreq_Version
    print("Identified data_specs_version to be '%s'." % dreq_version)
    dreqversiondict={}
    dreqversiondict["uid"]             = "0"           # INTEGER PRIMARY KEY CHECK (uid = 0)
    dreqversiondict["dreq_version"]    = dreq_version  # dreq_version VARCHAR(10)
    insertTable(con, cur, "Dreq_Version", dreqversiondict)

    # Commiting changes and closing database connection
    con.commit()
    if con: con.close()
    # elif source json end



print("Successful!")
sys.exit(0)





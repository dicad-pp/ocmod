#!/usr/bin/python
# -*- coding: utf-8 -*-

# Martin Schupfner, DKRZ 2017

#########################################################
# The DB tables CMORvar_Mapping_MODEL contain Dreq
# dependend information that might change with
# a dreq update. This routine will update all of this
# attributes to the Dreq Information stored in the
# tables CMORvar and var.
# This routine is also part of the Version_Mapping.py.
# The UIDs/VIDs have to be the same in
# CMORvar_Mapping_MODEL, CMORvar and var in order
# for the routine to work!
##########################################################

import sqlite3 as lite
import sys
import datetime
import shutil

######################################
# Create DB Backup
######################################
models=["cosmo"]
db="ocmoddb.sqlite"
nowdate="_".join(str(datetime.datetime.now()).split(" "))
phptime=int((datetime.datetime.now() - datetime.datetime(1970,1,1)).total_seconds())

#Read DB
print("Using SQLITE in version", lite.sqlite_version)
print("\nLoading Database ...")
try:
    con = lite.connect(db)
    cur = con.cursor()
    cur.execute('SELECT SQLITE_VERSION()')
    data=cur.fetchone()
except lite.Error as e:           
    print(f"Error: {e.args[0]}")
    sys.exit(1)   


    
####################################
# Copy up to date dreq information
# over CMORvar_Mapping_model tables
####################################
print("\nUpdating CMORvar_Mapping_MODEL tables with up-to-date Dreq attributes...")
for model in models:
    #Select all UIDs that have been mapped for this MODEL
    cur.execute("SELECT uid FROM CMORvar_Mapping_"+model+" where username IS NOT NULL AND username!=''")
    data_uid=cur.fetchall()
    if len(data_uid)==0: continue
    print(f" ... {len(data_uid)} variables for model {model}")
    
    #For every Variable/UID, select up-to-date info from CMORvar and var tables
    for i in range(0, len(data_uid)):
        uid=data_uid[i][0]            
        cur.execute("SELECT uid,vid,label,mipTable,cmt,zaxis,cdim,frequency,standardname FROM CMORvar WHERE uid=?", [uid])
        data_cm_uid=cur.fetchall()        
           
        if len(data_cm_uid)!=1:
            print(f"Error: Not exaclty one variable found for UID {uid}")
            sys.exit(1)       
        
        #...and update the mapped information with this up-to-date info  
        try:
            cur.execute("Update CMORvar_Mapping_"+model+" SET shortname=?, miptable=?, cell_methods=?, zaxis=?, chardim=?, frequency=?, standardname=? where uid=?", list(data_cm_uid[0][2::])+[uid])     
        except lite.Error as e:
            print(f"Error: {e.args[0]}")
            sys.exit(1)       
        con.commit()    
                        
print("\nDone.")

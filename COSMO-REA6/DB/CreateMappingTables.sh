#! /usr/bin/bash

for MODEL in cosmo 
do
        echo $MODEL
        sed "
                   s;MODEL;${MODEL};g
        " <createmappingdb.sql> CreateMappingModel.sql
        sqlite3 ocmoddb.sqlite < CreateMappingModel.sql
        rm CreateMappingModel.sql
done


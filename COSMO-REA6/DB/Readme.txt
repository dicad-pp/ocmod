Update the ocmoddb.sqlite database

- Delete old data from tables
sqlite3 ocmoddb.sqlite
> delete from CMORvar;
> delete from experiment; 
> delete from grids;       
> delete from mip;   
> delete from miptable;   
> .quit

- Load new data into the database
 (make sure all json files are listed in the python script)
(python3)
python Read_CMORvars_xlsx_csv_json.py

- Update attributes in mapping tables
  (python3)
python DB_Update_Attributes.py

- Create Update infos
(python2, dill needs to be installed - "pip install dill -t .")
python Version_Mapping_DB.py 


#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

##############################################
#
# Create c6dreq WebGUI compatible projectdb.sqlite
# by reading a projects mipTables and additional
# information (Dreq-light, title in mipTable header)
#
# Martin Schupfner, DKRZ-DM, 2020-2021
#
##############################################
# Prerequisites:
# Python3
##############################################

from __future__ import print_function
import csv, io, json
import sqlite3 as lite

import sys, os
if sys.version_info.major == 3:
    unicode = str

############################################
############################################
#       Definitions
############################################
############################################

#
# MIP-Tables-json:
#
json_miptables={"OcMOD_mon.json",
                "OcMOD_6hr.json",
                "OcMOD_1hr.json",
                "OcMOD_1hrPt.json",
                "OcMOD_day.json",
                "OcMOD_fx.json",
    }



############################################
############################################
#       Functions
############################################
############################################


def jsonread(infile):
    """
    Read json file
    """
    if os.path.isfile(infile):
        with io.open(infile, 'r', encoding='utf-8') as f:
            content=json.load(f)
        return content
    else:
        return {}

def readMipTable(infile):
    """
    Read mipTable variable_entry.

    Parameters
    ----------
    infile : string
        Path to mip table json file.

    Returns
    -------
    Tuple of File[variable_entry] and File[Header].

    """
    if not os.path.exists(infile):
        print("ERROR (MIPTable): '%s' cannot be found! Variables cannot be read." % infile)
        sys.exit(1)
    mt=jsonread(infile)
    return mt["variable_entry"],mt["Header"]



################################
################################
# PROGRAM ######################
################################
################################


common_header=["Default Priority", "Long name", "units", "description", "comment", "Variable Name", "CF Standard Name", "cell_methods", "positive", "type", "dimensions", "CMOR Name", "modeling_realm", "frequency", "cell_measures", "prov", "provNote", "rowIndex", "UID", "vid", "stid", "Structure Title", "valid_min", "valid_max", "ok_min_mean_abs", "ok_max_mean_abs", "MIPs (requesting)", "MIPs (by experiment)"]

# Read miptable
print("Reading %3s MIPtables in total:" % len(json_miptables))
for mt in json_miptables:
    print(mt)
    vars,header=readMipTable(mt)

    title = header["table_id"].split()[-1]

    varlist = list()
    varlist.append(common_header)

    #1|long_name|units|None|comment|out_name|standard_name|cell_methods|positive|type|dimensions|<entry>|modeling_realm|frequency|cell_masures|None|None|None|None|None|None|None|valid_min|valid_max|ok_min_mean_abs|ok_max_mean_abs|None|None

    # Read variable_entries from json MIPtable 
    print("Reading %3s variables from MIPtable '%s' ..." % (len(vars.keys()), mt))
    for var in vars:
        vardict={}
        vardict["Default Priority"] = 1
        vardict["Long name"]        = vars[var]["long_name"]
        vardict["units"]            = vars[var]["units"]
        vardict["description"]      = "None" 
        vardict["comment"]          = vars[var]["comment"]
        vardict["Variable Name"]    = vars[var]["out_name"]
        vardict["CF Standard Name"] = vars[var]["standard_name"]
        vardict["cell_methods"]     = vars[var]["cell_methods"]
        vardict["positive"]         = vars[var]["positive"]
        vardict["type"]             = vars[var]["type"]
        vardict["dimensions"]       = vars[var]["dimensions"]
        vardict["CMOR Name"]        = var
        vardict["modeling_realm"]   = vars[var]["modeling_realm"]
        vardict["frequency"]        = vars[var]["frequency"]
        vardict["cell_measures"]    = vars[var]["cell_measures"]
        vardict["prov"]             = "None"
        vardict["provNote"]         = "None"
        vardict["rowIndex"]         = "None"
        vardict["UID"]              = "None"
        vardict["vid"]              = "None"
        vardict["stid"]             = "None"
        vardict["Structure Title"]  = "None"
        vardict["valid_min"]        = vars[var]["valid_min"]
        vardict["valid_max"]        = vars[var]["valid_max"]
        vardict["ok_min_mean_abs"]  = vars[var]["ok_min_mean_abs"]
        vardict["ok_max_mean_abs"]  = vars[var]["ok_max_mean_abs"]
        vardict["MIPs (requesting)"]     = "None"
        vardict["MIPs (by experiment)"]  = "None"

        varlist.append([vardict[key] for key in common_header])

    # Write csv - header and variable_entrys
    print("Reading %3s variables to csv-File for MIPtable '%s' ..." % (len(vars.keys()), mt))
    with open(title+".csv", mode="w") as csv_file:
        csv_file_writer = csv.writer(csv_file, delimiter="|", quotechar='"')
        for entry in varlist: csv_file_writer.writerow(entry)

print("Successful!")
sys.exit(0)
